package com.legacy.farlanders.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

import com.legacy.farlanders.entity.hostile.ClassicEndermanEntity;
import com.legacy.farlanders.entity.hostile.FanmadeEndermanEntity;
import com.legacy.farlanders.entity.hostile.MysticEndermanEntity;

import net.minecraft.world.entity.monster.EnderMan;

@Mixin(EnderMan.class)
public class EnderManMixin
{
	@ModifyConstant(method = "aiStep()V", constant = @Constant(intValue = 2))
	private int farlanders$enderParticleCountModifier(int value)
	{
		EnderMan man = (EnderMan) (Object) this;
		if (man instanceof ClassicEndermanEntity || man instanceof FanmadeEndermanEntity || man instanceof MysticEndermanEntity)
			return 0;

		return value;
	}
}
