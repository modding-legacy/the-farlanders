package com.legacy.farlanders.data;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.registry.FLBlocks;
import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLItems;

import net.minecraft.Util;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.data.loot.packs.VanillaEntityLoot;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.storage.loot.BuiltInLootTables;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.entries.LootTableReference;
import net.minecraft.world.level.storage.loot.entries.TagEntry;
import net.minecraft.world.level.storage.loot.functions.EnchantRandomlyFunction;
import net.minecraft.world.level.storage.loot.functions.EnchantWithLevelsFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.LootingEnchantFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemDamageFunction;
import net.minecraft.world.level.storage.loot.functions.SetNbtFunction;
import net.minecraft.world.level.storage.loot.functions.SmeltItemFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemKilledByPlayerCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemRandomChanceCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

public class FLLootProv extends LootTableProvider
{
	public static final ResourceLocation ELDER_VILLAGE_HOUSE = TheFarlandersMod.locate("chests/elder_house_chest");
	/*public static final ResourceLocation SMALL_LOOT_CHEST = TheFarlandersMod.locate("chests/small_loot_chest");*/
	public static final ResourceLocation SPIRE_CHEST = TheFarlandersMod.locate("chests/spire_chest");

	public static final ResourceLocation ENDERMAN_WANDS = TheFarlandersMod.locate("entities/mystic_enderman_held_wands");

	public FLLootProv(DataGenerator gen, ExistingFileHelper fileHelper, CompletableFuture<Provider> lookup)
	{
		super(gen.getPackOutput(), Set.of(), List.of(new LootTableProvider.SubProviderEntry(ChestGen::new, LootContextParamSets.CHEST), new LootTableProvider.SubProviderEntry(BlockGen::new, LootContextParamSets.BLOCK), new LootTableProvider.SubProviderEntry(EntityGen::new, LootContextParamSets.ENTITY)));
	}

	@Override
	protected void validate(Map<ResourceLocation, LootTable> map, ValidationContext validationtracker)
	{
	}

	private static class BlockGen extends BlockLootSubProvider
	{
		protected BlockGen()
		{
			super(Set.of(), FeatureFlags.REGISTRY.allFlags());
		}

		@Override
		protected void generate()
		{

			blocks().forEach(block ->
			{
				if (block == FLBlocks.endumium_ore || block == FLBlocks.deepslate_endumium_ore)
					add(block, this.createOreDrop(block, FLItems.endumium_crystal));
				else
					dropSelf(block);
			});
		}

		@Override
		protected Iterable<Block> getKnownBlocks()
		{
			return blocks()::iterator;
		}

		private Stream<Block> blocks()
		{
			return BuiltInRegistries.BLOCK.stream().filter(b -> BuiltInRegistries.BLOCK.getKey(b).getNamespace().equals(TheFarlandersMod.MODID) && !b.getLootTable().equals(BuiltInLootTables.EMPTY));
		}
	}

	private static class EntityGen extends VanillaEntityLoot implements LootPoolUtil
	{
		@Override
		public void generate()
		{
			this.add(FLEntityTypes.FARLANDER, LootTable.lootTable());
			this.add(FLEntityTypes.ELDER_FARLANDER, LootTable.lootTable());
			this.add(FLEntityTypes.WANDERER, LootTable.lootTable());

			LootPool.Builder looterHood = basicPool(FLItems.looter_hood).when(LootItemRandomChanceCondition.randomChance(0.10F)).when(LootItemKilledByPlayerCondition.killedByPlayer());
			this.add(FLEntityTypes.LOOTER, tableOf(looterHood));

			LootPool.Builder rebelHelmet = basicPool(FLItems.rebel_farlander_helmet).when(LootItemRandomChanceCondition.randomChance(0.10F)).when(LootItemKilledByPlayerCondition.killedByPlayer());
			this.add(FLEntityTypes.REBEL, tableOf(rebelHelmet));

			LootPool.Builder arrow = lootingPool(Items.ARROW, 1, 4, 0, 2);
			this.add(FLEntityTypes.ENDER_GUARDIAN, tableOf(arrow));

			LootPool.Builder golemHorn = lootingPool(FLItems.ender_horn, 1, 3, 0, 1);
			this.add(FLEntityTypes.ENDER_GOLEM, tableOf(golemHorn));

			LootPool.Builder titanHide = lootingPool(FLItems.titan_hide, 3, 5, 0, 2);
			this.add(FLEntityTypes.TITAN, tableOf(titanHide));

			this.add(FLEntityTypes.ENDERMINION, LootTable.lootTable());
			this.add(FLEntityTypes.MYSTIC_ENDERMINION, LootTable.lootTable());

			var endermanTable = poolOf(List.of(LootTableReference.lootTableReference(EntityType.ENDERMAN.getDefaultLootTable())));
			/*LootPool.Builder portal = basicPool(Blocks.END_PORTAL_FRAME, 0, 1).when(LootItemRandomChanceCondition.randomChance(0.05F)).when(LootItemKilledByPlayerCondition.killedByPlayer());*/

			this.add(FLEntityTypes.CLASSIC_ENDERMAN, tableOf(List.of(endermanTable)));
			this.add(FLEntityTypes.FANMADE_ENDERMAN, tableOf(endermanTable));
			this.add(FLEntityTypes.MYSTIC_ENDERMAN, LootTable.lootTable());
			this.add(FLEntityTypes.MYSTIC_ENDERMAN, ENDERMAN_WANDS, tableOf(poolOf(List.of(basicEntry(FLItems.mystic_wand_ore), basicEntry(FLItems.mystic_wand_teleport), basicEntry(FLItems.mystic_wand_regen), basicEntry(FLItems.mystic_wand_invisible))).setRolls(ConstantValue.exactly(1.0F))));
		}

		private LootPool.Builder lootingPool(ItemLike item, int min, int max, int minLooting, int maxLooting)
		{
			return basicPool(item, min, max).apply(LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(minLooting, maxLooting)));
		}

		/*private String entityName(EntityType<?> entity)
		{
			return ForgeRegistries.ENTITY_TYPES.getKey(entity).getPath();
		}*/

		@Override
		protected Stream<EntityType<?>> getKnownEntityTypes()
		{
			return BuiltInRegistries.ENTITY_TYPE.stream().filter(e -> BuiltInRegistries.ENTITY_TYPE.getKey(e).getNamespace().contains(TheFarlandersMod.MODID));
		}
	}

	public static class ChestGen implements LootPoolUtil, LootTableSubProvider
	{
		@Override
		public void generate(BiConsumer<ResourceLocation, LootTable.Builder> consumer)
		{
			// @formatter:off
			
			//ConstantValue.exactly(1)
			//UniformGenerator.between(5, 7)

			 consumer.accept(ELDER_VILLAGE_HOUSE, tableOf(List.of(
						poolOf(List.of(
								basicEntry(FLItems.endumium_crystal, 1, 2).setWeight(8),
								basicEntry(Items.EMERALD, 1, 2).setWeight(5),
								basicEntry(Items.GOLD_INGOT).setWeight(6),
								basicEntry(Items.IRON_INGOT, 3, 4).setWeight(7),
								basicEntry(Items.COAL, 4, 5).setWeight(5),
								basicEntry(Items.EXPERIENCE_BOTTLE).setWeight(2),
								basicEntry(Items.SPIDER_EYE, 2, 4).setWeight(3),
								basicEntry(Items.SLIME_BALL, 1, 2).setWeight(3),
								basicEntry(Items.BLAZE_POWDER, 1, 2).setWeight(1),
								basicEntry(Items.ENDER_PEARL, 1, 2).setWeight(2)
							)).setRolls(UniformGenerator.between(3, 5)))));

				LootPool.Builder wand = poolOf(List.of(TagEntry.expandTag(FLTags.Items.MYSTIC_WANDS)));
				LootPool.Builder disc = poolOf(List.of(TagEntry.expandTag(ItemTags.CREEPER_DROP_MUSIC_DISCS))).when(LootItemRandomChanceCondition.randomChance(0.3F));

				var spireChest = poolOf(List.of(
								basicEntry(FLItems.endumium_crystal, 2, 3).setWeight(6),
								basicEntry(Items.IRON_INGOT, 2, 4).setWeight(7),
								basicEntry(Items.EXPERIENCE_BOTTLE).setWeight(1),
								basicEntry(FLItems.titan_hide).setWeight(3),
								basicEntry(FLItems.ender_horn, 2, 3).setWeight(5),
								basicEntry(Items.NETHER_WART, 2, 3).setWeight(2),
								basicEntry(Items.ENDER_PEARL, 3, 4).setWeight(2)
							)).setRolls(UniformGenerator.between(6, 7));

			 consumer.accept(SPIRE_CHEST, tableOf(List.of(wand, spireChest, disc)));
			// @formatter:on
		}
	}

	/**
	 * Interface with basic loot table generators
	 * 
	 * @author David
	 *
	 */
	public interface LootPoolUtil
	{
		/**
		 * Creates a table from the given loot pools.
		 * 
		 * @param pools
		 * @return
		 */
		default LootTable.Builder tableOf(List<LootPool.Builder> pools)
		{
			LootTable.Builder table = LootTable.lootTable();
			pools.forEach(pool -> table.withPool(pool));
			return table;
		}

		/**
		 * Creates a table from the given loot pool.
		 * 
		 * @param pool
		 * @return
		 */
		default LootTable.Builder tableOf(LootPool.Builder pool)
		{
			return LootTable.lootTable().withPool(pool);
		}

		/**
		 * Creates a loot pool with the given item. Gives an amount between the min and
		 * max.
		 * 
		 * @param item
		 * @param min
		 * @param max
		 * @return
		 */
		default LootPool.Builder basicPool(ItemLike item, int min, int max)
		{
			return LootPool.lootPool().add(basicEntry(item, min, max));
		}

		/**
		 * Creates a loot pool with the given item. Will only give one item.
		 * 
		 * @param item
		 * @return
		 */
		default LootPool.Builder basicPool(ItemLike item)
		{
			return LootPool.lootPool().add(basicEntry(item));
		}

		/**
		 * Creates a loot pool that will give a random item from the list.
		 * 
		 * @param items
		 * @return
		 */
		default LootPool.Builder randItemPool(List<ItemLike> items)
		{
			return poolOf(items.stream().map((i) -> basicEntry(i)).collect(Collectors.toList()));
		}

		/**
		 * Creates a loot pool with multiple entries. One of these entries will be
		 * picked at random each time the pool rolls.
		 * 
		 * @param lootEntries
		 * @return
		 */
		default LootPool.Builder poolOf(List<LootPoolEntryContainer.Builder<?>> lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			lootEntries.forEach(entry -> pool.add(entry));
			return pool;
		}

		/**
		 * Creates a loot entry for the given item. Gives an amount between the min and
		 * max.
		 * 
		 * @param item
		 * @param min
		 * @param max
		 * @return
		 */
		default LootItem.Builder<?> basicEntry(ItemLike item, int min, int max)
		{
			return basicEntry(item).apply(SetItemCountFunction.setCount(UniformGenerator.between(min, max)));
		}

		/**
		 * Creates a loot entry for the given item. Will only give one item.
		 * 
		 * @param item
		 * @return
		 */
		default LootItem.Builder<?> basicEntry(ItemLike item)
		{
			return LootItem.lootTableItem(item);
		}

		/**
		 * Sets the damage of the item (percentage)
		 * 
		 * @param min 0 - 100
		 * @param max 0 - 100
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> setDamage(int min, int max)
		{
			return SetItemDamageFunction.setDamage(UniformGenerator.between(min / 100F, max / 100F));
		}

		/**
		 * Cooks the item if the predicate passes
		 * 
		 * @param predicate
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> smeltItem(EntityPredicate.Builder predicate)
		{
			return SmeltItemFunction.smelted().when(LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, predicate));
		}

		/**
		 * Enchants the item randomly between the levels provided
		 * 
		 * @param minLevel
		 * @param maxLevel
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> enchant(int minLevel, int maxLevel)
		{
			return EnchantWithLevelsFunction.enchantWithLevels(UniformGenerator.between(minLevel, maxLevel));
		}

		/**
		 * Enchants the item randomly with the enchantments passed
		 * 
		 * @param enchantments
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> enchant(Enchantment... enchantments)
		{
			EnchantRandomlyFunction.Builder func = new EnchantRandomlyFunction.Builder();
			for (Enchantment enchantment : enchantments)
				func.withEnchantment(enchantment);
			return func;
		}

		/**
		 * Sets the nbt of the item
		 * 
		 * @param nbt
		 * @return
		 */
		@SuppressWarnings("deprecation")
		default LootItemConditionalFunction.Builder<?> setNbt(Consumer<CompoundTag> nbt)
		{
			return SetNbtFunction.setTag(Util.make(new CompoundTag(), nbt));
		}
	}
}
