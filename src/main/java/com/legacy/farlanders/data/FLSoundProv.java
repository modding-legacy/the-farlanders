package com.legacy.farlanders.data;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.common.data.SoundDefinition;
import net.neoforged.neoforge.common.data.SoundDefinition.Sound;
import net.neoforged.neoforge.common.data.SoundDefinition.SoundType;
import net.neoforged.neoforge.common.data.SoundDefinitionsProvider;

public class FLSoundProv extends SoundDefinitionsProvider
{
	public FLSoundProv(PackOutput output, ExistingFileHelper helper)
	{
		super(output, TheFarlandersMod.MODID, helper);
	}

	@Override
	public void registerSounds()
	{
		
		/*this.add(FLSounds.ITEM_MYSTIC_WAND_USE, definition().with(event(SoundEvents.EVOKER_CAST_SPELL)).subtitle(itemSub("mystic_wand.use")));*/

		String wandSub = itemSub("mystic_wand.use");
		String item = "mystic_wand";
		this.add(FLSounds.ITEM_MYSTIC_WAND_CAST_FIREBALL, itemDef(item, "cast_fireball", 1).subtitle(wandSub));
		this.add(FLSounds.ITEM_MYSTIC_WAND_CAST_REGEN, itemDef(item, "cast_regen", 1).subtitle(wandSub));
		this.add(FLSounds.ITEM_MYSTIC_WAND_CAST_ORE, itemDef(item, "cast_ore", 1).subtitle(wandSub));
		this.add(FLSounds.ITEM_MYSTIC_WAND_CAST_INVIS, itemDef(item, "cast_invis", 2).subtitle(wandSub));

		String entity = "farlander";
		this.add(FLSounds.ENTITY_FARLANDER_IDLE, entityDef(entity, "idle", 5));
		this.add(FLSounds.ENTITY_FARLANDER_HURT, entityDef(entity, "hurt", 3));
		this.add(FLSounds.ENTITY_FARLANDER_DEATH, entityDef(entity, "death", 1));

		entity = "looter";
		this.add(FLSounds.ENTITY_LOOTER_IDLE, definition().with(event(FLSounds.ENTITY_FARLANDER_IDLE)).subtitle(entitySub(entity + ".idle")));
		this.add(FLSounds.ENTITY_LOOTER_HURT, definition().with(event(FLSounds.ENTITY_FARLANDER_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(FLSounds.ENTITY_LOOTER_DEATH, definition().with(event(FLSounds.ENTITY_FARLANDER_DEATH)).subtitle(entitySub(entity + ".death")));

		entity = "rebel";
		this.add(FLSounds.ENTITY_REBEL_IDLE, definition().with(event(FLSounds.ENTITY_FARLANDER_IDLE)).subtitle(entitySub(entity + ".idle")));
		this.add(FLSounds.ENTITY_REBEL_HURT, definition().with(event(FLSounds.ENTITY_FARLANDER_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(FLSounds.ENTITY_REBEL_DEATH, definition().with(event(FLSounds.ENTITY_FARLANDER_DEATH)).subtitle(entitySub(entity + ".death")));

		entity = "wanderer";
		this.add(FLSounds.ENTITY_WANDERER_IDLE, definition().with(event(FLSounds.ENTITY_FARLANDER_IDLE)).subtitle(entitySub(entity + ".idle")));
		this.add(FLSounds.ENTITY_WANDERER_HURT, definition().with(event(FLSounds.ENTITY_FARLANDER_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(FLSounds.ENTITY_WANDERER_DEATH, definition().with(event(FLSounds.ENTITY_FARLANDER_DEATH)).subtitle(entitySub(entity + ".death")));

		String enderminion = (entity = "enderminion");
		this.add(FLSounds.ENTITY_ENDERMINION_IDLE, entityDef(entity, "idle", 4));
		this.add(FLSounds.ENTITY_ENDERMINION_HURT, entityDef(entity, "hurt", 3));
		this.add(FLSounds.ENTITY_ENDERMINION_DEATH, entityDef(entity, "death", 1));

		entity = "mystic_enderminion";
		this.add(FLSounds.ENTITY_MYSTIC_ENDERMINION_IDLE, entityDef(entity, "idle", 4).subtitle(entitySub(enderminion + ".idle")));
		this.add(FLSounds.ENTITY_MYSTIC_ENDERMINION_HURT, entityDef(entity, "hurt", 3).subtitle(entitySub(enderminion + ".hurt")));
		this.add(FLSounds.ENTITY_MYSTIC_ENDERMINION_DEATH, entityDef(entity, "death", 1).subtitle(entitySub(enderminion + ".death")));

		entity = "mystic_enderman";
		this.add(FLSounds.ENTITY_MYSTIC_ENDERMAN_IDLE, entityDef(entity, "idle", 5).subtitle("subtitles.entity.enderman.ambient"));
		this.add(FLSounds.ENTITY_MYSTIC_ENDERMAN_HURT, entityDef(entity, "hurt", 4).subtitle("subtitles.entity.enderman.hurt"));
		this.add(FLSounds.ENTITY_MYSTIC_ENDERMAN_DEATH, entityDef(entity, "death", 1).subtitle("subtitles.entity.enderman.death"));
		this.add(FLSounds.ENTITY_MYSTIC_ENDERMAN_BLINDNESS, entityDef(entity, "blindness", 1));
		this.add(FLSounds.ENTITY_MYSTIC_ENDERMAN_CONFUSION, entityDef(entity, "confusion", 1));

		entity = "fanmade_enderman";
		this.add(FLSounds.ENTITY_FANMADE_ENDERMAN_IDLE, entityDef(entity, "idle", 3));
		this.add(FLSounds.ENTITY_FANMADE_ENDERMAN_DEATH, entityDef(entity, "death", 1).subtitle("subtitles.entity.enderman.death"));

		entity = "ender_golem";
		this.add(FLSounds.ENTITY_ENDER_GOLEM_IDLE, entityDef(entity, "idle", 5));
		this.add(FLSounds.ENTITY_ENDER_GOLEM_HURT, definition().with(event(SoundEvents.ENDERMAN_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(FLSounds.ENTITY_ENDER_GOLEM_DEATH, entityDef(entity, "death", 1));

		entity = "ender_guardian";
		this.add(FLSounds.ENTITY_ENDER_GUARDIAN_IDLE, definition().with(event(FLSounds.ENTITY_ENDER_GOLEM_IDLE)).subtitle(entitySub(entity + ".idle")));
		this.add(FLSounds.ENTITY_ENDER_GUARDIAN_HURT, definition().with(event(FLSounds.ENTITY_ENDER_GOLEM_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(FLSounds.ENTITY_ENDER_GUARDIAN_DEATH, definition().with(event(FLSounds.ENTITY_ENDER_GOLEM_DEATH)).subtitle(entitySub(entity + ".death")));
		this.add(FLSounds.ENTITY_ENDER_GUARDIAN_SHOOT, definition().with(event(SoundEvents.ARROW_SHOOT)).subtitle(entitySub(entity + ".shoot")));

		entity = "titan";
		String holdingSub = entitySub(entity + ".holding");
		this.add(FLSounds.ENTITY_TITAN_IDLE, definition().with(event(FLSounds.ENTITY_ENDER_GOLEM_IDLE)).subtitle(entitySub(entity + ".idle")));
		this.add(FLSounds.ENTITY_TITAN_HURT, entityDef(entity, "hurt", 4));
		this.add(FLSounds.ENTITY_TITAN_DEATH, entityDef(entity, "death", 1));
		this.add(FLSounds.ENTITY_TITAN_HOLDING_SHORT, entityDef(entity, "holding_short", 1).subtitle(holdingSub));
		this.add(FLSounds.ENTITY_TITAN_HOLDING_MED, entityDef(entity, "holding_med", 1).subtitle(holdingSub));
		this.add(FLSounds.ENTITY_TITAN_HOLDING_LONG, entityDef(entity, "holding_long", 1).subtitle(holdingSub));
		this.add(FLSounds.ENTITY_TITAN_HURT_HEART, entityDef(entity, "hurt_heart", 1).subtitle(null));
		this.add(FLSounds.ENTITY_TITAN_ENRAGE, entityDef(entity, "enrage", 1));

		this.add(FLSounds.MUSIC_NIGHTFALL_AMBIENT, musicDef("nightfall"));
	}

	protected static SoundDefinition def(Dir dir, String name, String subtitle)
	{
		return def(dir, name, 1, subtitle);
	}

	protected static SoundDefinition def(Dir dir, String name, int count, String subtitle)
	{
		return def(dir, name, count, subtitle, s ->
		{
		});
	}

	protected static SoundDefinition def(Dir dir, String name, int count, String subtitle, Consumer<Sound> extra)
	{
		var def = count > 1 ? definition().with(sky(dir.folderName.concat(name), count, extra)) : definition().with(fl(dir.folderName.concat(name), extra));
		return def.subtitle(dir == Dir.ENTITY ? entitySub(subtitle) : dir == Dir.BLOCKS ? blockSub(subtitle) : dir == Dir.ITEMS ? itemSub(subtitle) : subtitle);
	}

	protected static SoundDefinition.Sound fl(final String name)
	{
		return fl(name, s ->
		{
		});
	}

	protected static SoundDefinition.Sound fl(final String name, Consumer<Sound> extra)
	{
		var sound = sound(TheFarlandersMod.locate(name));
		extra.accept(sound);
		return sound;
	}

	protected static Sound[] sky(String name, int count)
	{
		return sky(name, count, s ->
		{
		});
	}

	protected static Sound[] sky(String name, int count, Consumer<Sound> extra)
	{
		List<Sound> sounds = new ArrayList<>();

		for (int i = 1; i <= count; ++i)
		{
			Sound sound = fl(name.concat("_" + i));
			extra.accept(sound);
			sounds.add(sound);
		}
		return sounds.toArray(new Sound[count]);
	}

	protected static SoundDefinition.Sound event(final SoundEvent event)
	{
		return sound(BuiltInRegistries.SOUND_EVENT.getKey(event), SoundType.EVENT);
	}

	protected static String sub(final String name)
	{
		return "subtitles.".concat(name);
	}

	protected static String blockSub(final String name)
	{
		return sub("block.".concat(name));
	}

	protected static String itemSub(final String name)
	{
		return sub("item.".concat(name));
	}

	protected static String entitySub(final String name)
	{
		return sub("entity.".concat(name));
	}

	protected static SoundDefinition musicDef(final String name)
	{
		return musicDef(name, false);
	}

	protected static SoundDefinition musicDef(final String name, boolean preload)
	{
		String music = "music/";
		return definition().with(fl(music.concat(name)).stream().preload(preload));
	}

	protected static SoundDefinition entityDef(String entityName, String soundName, int count, String subtitle)
	{
		return def(Dir.ENTITY, entityName + "/" + soundName, count, entityName + "." + subtitle);
	}

	protected static SoundDefinition entityDef(String entityName, String soundName, int count)
	{
		return entityDef(entityName, soundName, count, soundName);
	}

	protected static SoundDefinition blockDef(String blockName, String soundName, int count)
	{
		return blockDef(blockName, soundName, count, 0);
	}

	protected static SoundDefinition blockDef(String blockName, String soundName, int count, final int attenuationDistance)
	{
		String defName = (blockName + "/" + soundName);
		String defSubtitle = blockName + "." + soundName;

		return def(Dir.BLOCKS, defName, count, defSubtitle, s ->
		{
			if (attenuationDistance > 0)
				s.attenuationDistance(attenuationDistance);
		});
	}
	
	protected static SoundDefinition itemDef(String itemName, String soundName, int count, String subtitle)
	{
		return def(Dir.ITEMS, itemName + "/" + soundName, count, itemName + "." + subtitle);
	}
	
	protected static SoundDefinition itemDef(String itemName, String soundName, int count)
	{
		return def(Dir.ITEMS, itemName + "/" + soundName, count, itemName);
	}

	private enum Dir
	{
		BLOCKS("block/"), ITEMS("item/"), ENTITY("entity/"), MUSIC("music/");

		public final String folderName;

		Dir(String folderName)
		{
			this.folderName = folderName;
		}
	}
}
