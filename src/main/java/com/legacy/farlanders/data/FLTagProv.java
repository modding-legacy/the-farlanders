package com.legacy.farlanders.data;

import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Stream;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.item.wand.MysticWandBaseItem;
import com.legacy.farlanders.registry.FLBlocks;
import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLItems;
import com.legacy.farlanders.registry.FLStructures;

import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BiomeTagsProvider;
import net.minecraft.data.tags.EntityTypeTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.data.tags.StructureTagsProvider;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.StructureTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.structure.BuiltinStructures;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

public class FLTagProv
{
	public static class BlockTagProv extends BlockTagsProvider
	{
		public BlockTagProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, TheFarlandersMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			farlanders();
			vanilla();
			forge();
		}

		@SuppressWarnings("unchecked")
		void farlanders()
		{
			this.tag(FLTags.Blocks.CLASSIC_ENDERMAN_HOLDABLE).add(Blocks.NETHERRACK, Blocks.CAKE, Blocks.JUKEBOX, Blocks.ENCHANTING_TABLE, Blocks.FURNACE, Blocks.SPONGE, Blocks.WET_SPONGE, Blocks.COBWEB, Blocks.GLOWSTONE, Blocks.OBSIDIAN).addTags(BlockTags.WOOL, BlockTags.ICE, BlockTags.PLANKS, Tags.Blocks.COBBLESTONE, BlockTags.LOGS, BlockTags.FENCES, BlockTags.FENCE_GATES, BlockTags.ENDERMAN_HOLDABLE, Tags.Blocks.BOOKSHELVES, Tags.Blocks.STORAGE_BLOCKS, BlockTags.STONE_BRICKS, Tags.Blocks.ORES);

			this.tag(FLTags.Blocks.STORAGE_BLOCKS_ENDUMIUM).add(FLBlocks.endumium_block);
			this.tag(FLTags.Blocks.ORES_ENDUMIUM).add(FLBlocks.endumium_ore, FLBlocks.deepslate_endumium_ore);
		}

		void vanilla()
		{
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).addTag(FLTags.Blocks.STORAGE_BLOCKS_ENDUMIUM).addTag(FLTags.Blocks.ORES_ENDUMIUM);
			this.tag(BlockTags.BEACON_BASE_BLOCKS).addTag(FLTags.Blocks.STORAGE_BLOCKS_ENDUMIUM);
		}

		void forge()
		{
			this.tag(Tags.Blocks.ORE_RATES_SINGULAR).addTag(FLTags.Blocks.ORES_ENDUMIUM);
			this.tag(Tags.Blocks.ORES).addTag(FLTags.Blocks.ORES_ENDUMIUM);
		}

		@Override
		public String getName()
		{
			return "Farlanders Block Tags";
		}
	}

	public static class ItemProv extends ItemTagsProvider
	{
		public ItemProv(DataGenerator gen, CompletableFuture<TagLookup<Block>> blockTagProv, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(gen.getPackOutput(), lookup, blockTagProv, TheFarlandersMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			farlanders();
			vanilla();
			forge();
		}

		@SuppressWarnings("unchecked")
		void farlanders()
		{
			this.copy(FLTags.Blocks.STORAGE_BLOCKS_ENDUMIUM, FLTags.Items.STORAGE_BLOCKS_ENDUMIUM);
			this.copy(FLTags.Blocks.ORES_ENDUMIUM, FLTags.Items.ORES_ENDUMIUM);
			this.tag(FLTags.Items.GEMS_ENDUMIUM).add(FLItems.endumium_crystal);

			this.addMatching(FLTags.Items.MYSTIC_WANDS, i -> i instanceof MysticWandBaseItem);

			this.tag(FLTags.Items.ENDERMINION_HOLDABLE).addTags(ItemTags.SWORDS, ItemTags.AXES);
			this.tag(FLTags.Items.ENDERMINION_TAME_FOOD).add(Items.APPLE);

			this.tag(FLTags.Items.LOOTER_STEALABLE).addTags(Tags.Items.TOOLS);
		}

		void vanilla()
		{
			this.tag(ItemTags.BEACON_PAYMENT_ITEMS).add(FLItems.endumium_crystal);
			this.tag(ItemTags.SWORDS).add(FLItems.nightfall_sword);
		}

		void forge()
		{
			this.tag(Tags.Items.ARMORS_HELMETS).add(FLItems.nightfall_helmet, FLItems.rebel_farlander_helmet);
			this.tag(Tags.Items.ARMORS_CHESTPLATES).add(FLItems.nightfall_chestplate);
			this.tag(Tags.Items.ARMORS_LEGGINGS).add(FLItems.nightfall_leggings);
			this.tag(Tags.Items.ARMORS_BOOTS).add(FLItems.nightfall_boots);
		}

		@Override
		public String getName()
		{
			return "Farlanders Item Tags";
		}

		private Stream<Item> getMatching(Function<Item, Boolean> condition)
		{
			return BuiltInRegistries.ITEM.stream().filter(block -> BuiltInRegistries.ITEM.getKey(block).getNamespace().equals(this.modId) && condition.apply(block));
		}

		private void addMatching(TagKey<Item> itemTag, Function<Item, Boolean> condition)
		{
			getMatching(condition).forEach(this.tag(itemTag)::add);
		}
	}

	public static class EntityProv extends EntityTypeTagsProvider
	{
		public EntityProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, TheFarlandersMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			farlanders();
			vanilla();
			forge();
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			FLTags.init();
			return super.run(cache);
		}

		@SuppressWarnings("unchecked")
		private void farlanders()
		{
			this.tag(FLTags.Entities.ENDERMEN).add(EntityType.ENDERMAN, FLEntityTypes.CLASSIC_ENDERMAN, FLEntityTypes.FANMADE_ENDERMAN, FLEntityTypes.MYSTIC_ENDERMAN);
			this.tag(FLTags.Entities.FARLANDER_VILLAGE_GUARDS).add(FLEntityTypes.ENDER_GOLEM, FLEntityTypes.ENDER_GUARDIAN);
			this.tag(FLTags.Entities.FARLANDER_ALLIES).add(FLEntityTypes.FARLANDER, FLEntityTypes.ELDER_FARLANDER, FLEntityTypes.TITAN, FLEntityTypes.ENDERMINION, FLEntityTypes.MYSTIC_ENDERMINION).addTags(FLTags.Entities.FARLANDER_VILLAGE_GUARDS, FLTags.Entities.ENDERMEN);
			this.tag(FLTags.Entities.HUNTED_BY_REBELS).add(FLEntityTypes.FARLANDER, FLEntityTypes.ELDER_FARLANDER).addTags(FLTags.Entities.FARLANDER_VILLAGE_GUARDS);
		}

		private void vanilla()
		{
		}

		private void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Farlanders EntityType Tags";
		}
	}

	public static class BiomeProv extends BiomeTagsProvider
	{
		public BiomeProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, TheFarlandersMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			farlanders();
			vanilla();
			forge();
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			FLTags.init();
			return super.run(cache);
		}

		@SuppressWarnings("unchecked")
		private void farlanders()
		{
			/*this.tag(FLTags.Biomes.HAS_LOOT_WELLS).addTags(Tags.Biomes.IS_PLAINS, Tags.Biomes.IS_DESERT);*/
			this.tag(FLTags.Biomes.HAS_TITAN_SPIRES).addTags(Tags.Biomes.IS_PLAINS, Tags.Biomes.IS_DESERT);
			this.tag(FLTags.Biomes.HAS_ENDUMIUM_ORE).addTags(Tags.Biomes.IS_SWAMP, Tags.Biomes.IS_MOUNTAIN).add(Biomes.JUNGLE, Biomes.BAMBOO_JUNGLE, Biomes.SPARSE_JUNGLE);

			this.tag(FLTags.Biomes.HAS_HOUSES).addTags(Tags.Biomes.IS_PLAINS, Tags.Biomes.IS_DESERT);
			this.tag(FLTags.Biomes.HAS_VILLAGES).add(Biomes.BIRCH_FOREST, Biomes.OLD_GROWTH_BIRCH_FOREST, Biomes.PLAINS, Biomes.SUNFLOWER_PLAINS);

			this.tag(FLTags.Biomes.HAS_NATURAL_SPAWNS).addTags(BiomeTags.IS_OVERWORLD);
			this.tag(FLTags.Biomes.NATURAL_SPAWN_BLACKLIST).addTags(Tags.Biomes.IS_MUSHROOM).add(Biomes.DEEP_DARK, Biomes.LUSH_CAVES);
			this.tag(FLTags.Biomes.DECREASED_NATURAL_SPAWNS).addTags(Tags.Biomes.IS_DESERT, BiomeTags.IS_BADLANDS, BiomeTags.IS_OCEAN, BiomeTags.IS_BEACH);
		}

		private void vanilla()
		{
		}

		private void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Farlanders Biome Tags";
		}
	}

	public static class StructureProv extends StructureTagsProvider
	{
		public StructureProv(DataGenerator generatorIn, ExistingFileHelper existingFileHelper, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, TheFarlandersMod.MODID, existingFileHelper);
		}

		@Override
		protected void addTags(Provider prov)
		{
			farlanders();
			vanilla();
			forge();
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cache)
		{
			FLTags.init();
			return super.run(cache);
		}

		@SuppressWarnings("unchecked")
		private void farlanders()
		{
			this.tag(FLTags.Structures.FARLANDER_VILLAGE_BAD_NEIGHBORS).addTags(StructureTags.VILLAGE).add(FLStructures.TITAN_SPIRE.getStructure().getKey());
			this.tag(FLTags.Structures.FARLANDER_HOUSE_BAD_NEIGHBORS).addTags(FLTags.Structures.FARLANDER_VILLAGE_BAD_NEIGHBORS).add(FLStructures.FARLANDER_VILLAGE.getStructure().getKey());
			this.tag(FLTags.Structures.TITAN_SPIRE_BAD_NEIGHBORS).addTags(StructureTags.VILLAGE).add(BuiltinStructures.PILLAGER_OUTPOST);
		}

		private void vanilla()
		{
		}

		private void forge()
		{
		}

		@Override
		public String getName()
		{
			return "Farlanders Structure Tags";
		}
	}
}
