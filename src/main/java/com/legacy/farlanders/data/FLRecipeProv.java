package com.legacy.farlanders.data;

import java.util.concurrent.CompletableFuture;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.registry.FLBlocks;
import com.legacy.farlanders.registry.FLItems;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.packs.VanillaRecipeProvider;
import net.minecraft.world.item.Items;

public class FLRecipeProv extends VanillaRecipeProvider
{
	public FLRecipeProv(PackOutput output, CompletableFuture<HolderLookup.Provider> lookupProvider)
	{
		super(output, lookupProvider);
	}

	@Override
	protected void buildRecipes(RecipeOutput cons)
	{
		nineBlockStorageRecipesRecipesWithCustomUnpacking(cons, RecipeCategory.MISC, FLItems.endumium_crystal, RecipeCategory.BUILDING_BLOCKS, FLBlocks.endumium_block, find("endumium_crystal_from_endumium_block"), find("endumium_crystal"));

		ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, FLItems.nightfall_helmet).define('X', FLItems.titan_hide).pattern("XXX").pattern("X X").unlockedBy("has_hide", has(FLItems.titan_hide)).save(cons);
		ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, FLItems.nightfall_chestplate).define('X', FLItems.titan_hide).pattern("X X").pattern("XXX").pattern("XXX").unlockedBy("has_hide", has(FLItems.titan_hide)).save(cons);
		ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, FLItems.nightfall_leggings).define('X', FLItems.titan_hide).pattern("XXX").pattern("X X").pattern("X X").unlockedBy("has_hide", has(FLItems.titan_hide)).save(cons);
		ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, FLItems.nightfall_boots).define('X', FLItems.titan_hide).pattern("X X").pattern("X X").unlockedBy("has_hide", has(FLItems.titan_hide)).save(cons);
		ShapedRecipeBuilder.shaped(RecipeCategory.COMBAT, FLItems.nightfall_sword).define('#', Items.STICK).define('X', Items.DIAMOND).define('L', FLItems.ender_horn).pattern("LXL").pattern("LXL").pattern(" # ").unlockedBy("has_diamond", has(Items.DIAMOND)).unlockedBy("has_horn", has(FLItems.ender_horn)).save(cons);
	}

	private String find(String key)
	{
		return TheFarlandersMod.find(key);
	}
}
