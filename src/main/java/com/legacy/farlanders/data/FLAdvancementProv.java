package com.legacy.farlanders.data;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLItems;
import com.legacy.farlanders.triggers.ClassicEndermanTrigger;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementType;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.advancements.critereon.KilledTrigger;
import net.minecraft.advancements.critereon.LocationPredicate;
import net.minecraft.advancements.critereon.PlayerHurtEntityTrigger;
import net.minecraft.advancements.critereon.PlayerTrigger;
import net.minecraft.advancements.critereon.TameAnimalTrigger;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.data.DataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.neoforged.neoforge.common.data.AdvancementProvider;
import net.neoforged.neoforge.common.data.AdvancementProvider.AdvancementGenerator;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

@SuppressWarnings("unused")
public class FLAdvancementProv extends AdvancementProvider
{
	private static final String ADVANCEMENT_LOC = "the_farlanders";
	private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().create();

	public FLAdvancementProv(DataGenerator gen, CompletableFuture<HolderLookup.Provider> lookup, ExistingFileHelper existingFileHelper)
	{
		super(gen.getPackOutput(), lookup, existingFileHelper, List.of(new FLAdvancements()));
	}

	private static class FLAdvancements implements AdvancementGenerator
	{

		@Override
		public void generate(Provider lookup, Consumer<AdvancementHolder> saver, ExistingFileHelper existingFileHelper)
		{
			AdvancementHolder root = builder(FLItems.farlander_spawn_egg, "the_farlanders.root", new ResourceLocation("textures/block/end_stone_bricks.png"), AdvancementType.TASK, false, false, false).requirements(AdvancementRequirements.Strategy.OR).addCriterion("attack_anything", PlayerHurtEntityTrigger.TriggerInstance.playerHurtEntity()).save(saver, TheFarlandersMod.find("root"));

			AdvancementHolder tameEnderminion = builder(Items.APPLE, "leader_minions", AdvancementType.TASK, true, true, false).parent(root).requirements(AdvancementRequirements.Strategy.OR).addCriterion("tame_minion", TameAnimalTrigger.TriggerInstance.tamedAnimal(EntityPredicate.Builder.entity().of(FLEntityTypes.ENDERMINION))).addCriterion("tame_mystic_minion", TameAnimalTrigger.TriggerInstance.tamedAnimal(EntityPredicate.Builder.entity().of(FLEntityTypes.MYSTIC_ENDERMINION))).save(saver, TheFarlandersMod.find("tame_enderminion"));
			AdvancementHolder obtainFireballWand = builder(FLItems.mystic_wand_fire_small, "wand_destruction", AdvancementType.TASK, true, true, false).parent(tameEnderminion).requirements(AdvancementRequirements.Strategy.OR).addCriterion("obtain_small_fireball", InventoryChangeTrigger.TriggerInstance.hasItems(FLItems.mystic_wand_fire_small)).addCriterion("obtain_large_fireball", InventoryChangeTrigger.TriggerInstance.hasItems(FLItems.mystic_wand_fire_large)).save(saver, TheFarlandersMod.find("buy_fire_wand"));

			AdvancementHolder killClassicEnderman = builder(Items.ENDER_PEARL, "this_is_the_end", AdvancementType.TASK, true, true, false).parent(root).addCriterion("kill_enderman", KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity().of(FLEntityTypes.CLASSIC_ENDERMAN))).save(saver, TheFarlandersMod.find("kill_classic_enderman"));
			AdvancementHolder classicHoldingIron = builder(Items.IRON_BLOCK, "iron_maiden", AdvancementType.TASK, true, true, false).parent(killClassicEnderman).addCriterion("kill_enderman", ClassicEndermanTrigger.Instance.withHeldBlock(Blocks.IRON_BLOCK)).save(saver, TheFarlandersMod.find("kill_classic_enderman_holding_iron"));
			AdvancementHolder classicHoldingGold = builder(Items.GOLD_BLOCK, "blazing_gold_digger", AdvancementType.TASK, true, true, false).parent(classicHoldingIron).addCriterion("kill_enderman", ClassicEndermanTrigger.Instance.withHeldBlock(Blocks.GOLD_BLOCK)).save(saver, TheFarlandersMod.find("kill_classic_enderman_holding_gold"));
			AdvancementHolder classicHoldingDiamond = builder(Items.DIAMOND_BLOCK, "diamonds_oh_wait", AdvancementType.TASK, true, true, false).parent(classicHoldingGold).addCriterion("kill_enderman", ClassicEndermanTrigger.Instance.withHeldBlock(Blocks.DIAMOND_BLOCK)).save(saver, TheFarlandersMod.find("kill_classic_enderman_holding_diamond"));
			AdvancementHolder classicHoldingObsFire = builder(Items.OBSIDIAN, "rock_bottom", AdvancementType.CHALLENGE, true, true, false).parent(classicHoldingDiamond).addCriterion("kill_enderman", ClassicEndermanTrigger.Instance.withHeldBlockOnFire(Blocks.OBSIDIAN)).save(saver, TheFarlandersMod.find("kill_classic_enderman_holding_obsidian"));
			AdvancementHolder classicHoldingNetherOnFire = builder(Items.NETHERRACK, "what_the_hell", AdvancementType.CHALLENGE, true, true, false).parent(classicHoldingObsFire).requirements(AdvancementRequirements.Strategy.OR).addCriterion("kill_enderman_netherrack", ClassicEndermanTrigger.Instance.withHeldBlockOnFire(Blocks.NETHERRACK)).addCriterion("kill_enderman_soul_sand", ClassicEndermanTrigger.Instance.withHeldBlockOnFire(Blocks.SOUL_SAND)).save(saver, TheFarlandersMod.find("kill_classic_enderman_holding_nether_material"));

			/*AdvancementHolder obtainPortal = builder(Items.END_PORTAL_FRAME, "alternative_ending", AdvancementType.GOAL, true, true, false).parent(killClassicEnderman).addCriterion("obtain_portal", InventoryChangeTrigger.TriggerInstance.hasItems(Items.END_PORTAL_FRAME)).save(saver, TheFarlandersMod.find("obtain_end_portal_frame"));*/

			AdvancementHolder killMysticEnderman = builder(FLItems.mystic_wand_teleport, "abra_kadabra", AdvancementType.TASK, true, true, false).parent(killClassicEnderman).addCriterion("kill_enderman", KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity().of(FLEntityTypes.MYSTIC_ENDERMAN))).save(saver, TheFarlandersMod.find("kill_mystic_enderman"));
			AdvancementHolder obtainNightfallSword = builder(FLItems.nightfall_sword, "cutting_edge", AdvancementType.TASK, true, true, false).parent(killMysticEnderman).addCriterion("craft_sword", InventoryChangeTrigger.TriggerInstance.hasItems(FLItems.nightfall_sword)).save(saver, TheFarlandersMod.find("craft_nightfall_sword"));
			AdvancementHolder obtainNightfallSet = builder(FLItems.nightfall_helmet, "among_endermen", AdvancementType.CHALLENGE, true, true, false).parent(obtainNightfallSword).requirements(AdvancementRequirements.Strategy.AND).addCriterion("get_helmet", InventoryChangeTrigger.TriggerInstance.hasItems(FLItems.nightfall_helmet)).addCriterion("get_chestplate", InventoryChangeTrigger.TriggerInstance.hasItems(FLItems.nightfall_chestplate)).addCriterion("get_leggings", InventoryChangeTrigger.TriggerInstance.hasItems(FLItems.nightfall_leggings)).addCriterion("get_boots", InventoryChangeTrigger.TriggerInstance.hasItems(FLItems.nightfall_boots)).save(saver, TheFarlandersMod.find("obtain_full_nightfall"));
		}

		private Advancement.Builder enterAnyStructure(Advancement.Builder builder, List<ResourceKey<Structure>> structures)
		{
			structures.forEach(structure -> builder.addCriterion("entered_" + structure.location().getPath(), enterStructure(structure)));
			return builder;
		}

		private Criterion<PlayerTrigger.TriggerInstance> enterStructure(ResourceKey<Structure> type)
		{
			return PlayerTrigger.TriggerInstance.located(LocationPredicate.Builder.inStructure(type));
		}

		private Component translate(String key)
		{
			// this is stupid to preserve translations
			return Component.translatable("advancement." + key);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, ResourceLocation background, AdvancementType frameType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return Advancement.Builder.advancement().display(displayItem, translate(name), translate(name + ".desc"), background, frameType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, ResourceLocation background, AdvancementType frameType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return Advancement.Builder.advancement().display(displayItem, translate(name), translate(name + ".desc"), background, frameType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, AdvancementType frameType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return builder(displayItem, name, (ResourceLocation) null, frameType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, AdvancementType frameType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return builder(displayItem, name, (ResourceLocation) null, frameType, showToast, announceToChat, hidden);
		}
	}
}
