package com.legacy.farlanders.data;

import com.legacy.farlanders.TheFarlandersMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.structure.Structure;

public class FLTags
{
	public static void init()
	{
		Blocks.init();
		Items.init();
		Entities.init();
		Biomes.init();
	}

	public static interface Blocks
	{
		TagKey<Block> ORES_ENDUMIUM = tag("ores/endumium");
		TagKey<Block> STORAGE_BLOCKS_ENDUMIUM = tag("storage_blocks/endumium");

		TagKey<Block> CLASSIC_ENDERMAN_HOLDABLE = tag("classic_enderman_holdable");

		private static TagKey<Block> tag(String name)
		{
			return BlockTags.create(TheFarlandersMod.locate(name));
		}

		public static void init()
		{
		}
	}

	public static interface Items
	{
		TagKey<Item> ORES_ENDUMIUM = tag("ores/endumium");
		TagKey<Item> STORAGE_BLOCKS_ENDUMIUM = tag("storage_blocks/endumium");
		TagKey<Item> GEMS_ENDUMIUM = tag("gems/endumium");

		TagKey<Item> MYSTIC_WANDS = tag("mystic_wands");

		TagKey<Item> ENDERMINION_HOLDABLE = tag("enderminion_holdable");
		TagKey<Item> ENDERMINION_TAME_FOOD = tag("enderminion_tame_food");
		TagKey<Item> LOOTER_STEALABLE = tag("looter_stealable");

		private static TagKey<Item> tag(String name)
		{
			return ItemTags.create(TheFarlandersMod.locate(name));
		}

		public static void init()
		{
		}
	}

	public static interface Entities
	{
		TagKey<EntityType<?>> ENDERMEN = tag("endermen");
		TagKey<EntityType<?>> FARLANDER_ALLIES = tag("farlander_allies");
		TagKey<EntityType<?>> FARLANDER_VILLAGE_GUARDS = tag("farlander_village_guards");
		TagKey<EntityType<?>> HUNTED_BY_REBELS = tag("hunted_by_rebels");

		private static TagKey<EntityType<?>> tag(String key)
		{
			return TagKey.create(Registries.ENTITY_TYPE, TheFarlandersMod.locate(key));
		}

		public static void init()
		{
		}
	}

	public static interface Biomes
	{
		/*TagKey<Biome> HAS_LOOT_WELLS = tag("has_loot_wells");*/
		TagKey<Biome> HAS_TITAN_SPIRES = tag("has_titan_spires");
		TagKey<Biome> HAS_ENDUMIUM_ORE = tag("has_endumium_ore");
		TagKey<Biome> HAS_HOUSES = tag("has_farlander_houses");
		TagKey<Biome> HAS_VILLAGES = tag("has_farlander_villages");

		TagKey<Biome> HAS_NATURAL_SPAWNS = tag("has_natural_spawns");
		TagKey<Biome> NATURAL_SPAWN_BLACKLIST = tag("natural_spawn_blacklist");
		TagKey<Biome> DECREASED_NATURAL_SPAWNS = tag("decreased_natural_spawns");

		private static TagKey<Biome> tag(String key)
		{
			return TagKey.create(Registries.BIOME, TheFarlandersMod.locate(key));
		}

		public static void init()
		{
		}
	}

	public static interface Structures
	{
		TagKey<Structure> FARLANDER_VILLAGE_BAD_NEIGHBORS = tag("farlander_village_bad_neighbors");
		TagKey<Structure> FARLANDER_HOUSE_BAD_NEIGHBORS = tag("farlander_house_bad_neighbors");
		TagKey<Structure> TITAN_SPIRE_BAD_NEIGHBORS = tag("titan_spire_bad_neighbors");

		private static TagKey<Structure> tag(String key)
		{
			return TagKey.create(Registries.STRUCTURE, TheFarlandersMod.locate(key));
		}

		public static void init()
		{
		}
	}
}
