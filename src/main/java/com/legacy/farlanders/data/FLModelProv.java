package com.legacy.farlanders.data;

import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.item.wand.MysticWandBaseItem;
import com.legacy.farlanders.registry.FLItems;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.client.model.generators.BlockModelProvider;
import net.neoforged.neoforge.client.model.generators.BlockStateProvider;
import net.neoforged.neoforge.client.model.generators.ItemModelBuilder;
import net.neoforged.neoforge.client.model.generators.ItemModelProvider;
import net.neoforged.neoforge.client.model.generators.ModelFile;
import net.neoforged.neoforge.common.data.ExistingFileHelper;

public class FLModelProv
{
	protected static List<Block> getAllBlocks()
	{
		return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(TheFarlandersMod.MODID)).toList();
	}

	protected static List<Item> getAllItems(Function<Item, Boolean> condition)
	{
		return BuiltInRegistries.ITEM.stream().filter(item -> BuiltInRegistries.ITEM.getKey(item).getNamespace().equals(TheFarlandersMod.MODID) && condition.apply(item)).toList();
	}

	protected static List<Item> getAllItems()
	{
		return getAllItems(i -> true);
	}

	public static class Items extends ItemModelProvider
	{
		public Items(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, TheFarlandersMod.MODID, existingFileHelper);
		}

		@Override
		protected void registerModels()
		{
			for (Block block : getAllBlocks())
			{
				String name = BuiltInRegistries.BLOCK.getKey(block).getPath();
				withExistingParent(name, modLoc("block/" + name));
			}

			getAllItems(i -> !(i instanceof BlockItem)).forEach(item ->
			{
				if (item instanceof MysticWandBaseItem)
					handheldItem(item);
				else
					basicItem(item);
			});

			handheldItem(FLItems.nightfall_sword);
		}

		public ItemModelBuilder handheldItem(Item item)
		{
			return handheldItem(Objects.requireNonNull(BuiltInRegistries.ITEM.getKey(item)));
		}

		public ItemModelBuilder handheldItem(ResourceLocation item)
		{
			return getBuilder(item.toString()).parent(new ModelFile.UncheckedModelFile("item/handheld")).texture("layer0", new ResourceLocation(item.getNamespace(), "item/" + item.getPath()));
		}
	}

	public static class Blocks extends BlockModelProvider
	{
		public Blocks(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, TheFarlandersMod.MODID, existingFileHelper);
		}

		@Override
		protected void registerModels()
		{
			/*for (Block block : getAllBlocks())
			{
				String name = ForgeRegistries.BLOCKS.getKey(block).getPath();
				cubeAll(name, modLoc("block/" + name));
			}*/
		}
	}

	public static class States extends BlockStateProvider
	{
		public States(PackOutput output, ExistingFileHelper existingFileHelper)
		{
			super(output, TheFarlandersMod.MODID, existingFileHelper);
		}

		@Override
		protected void registerStatesAndModels()
		{
			getAllBlocks().forEach(block -> this.simpleBlock(block));
		}
	}
}
