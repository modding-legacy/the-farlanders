package com.legacy.farlanders.data;

import java.util.Set;
import java.util.concurrent.CompletableFuture;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.HolderLookup;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.PackOutput;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.common.data.ExistingFileHelper;
import net.neoforged.neoforge.data.event.GatherDataEvent;

public class FarlandersDataGen
{
	@SubscribeEvent
	public static void gatherData(GatherDataEvent event)
	{
		DataGenerator gen = event.getGenerator();
		ExistingFileHelper helper = event.getExistingFileHelper();
		PackOutput output = gen.getPackOutput();
		boolean server = event.includeServer();

		DatapackBuiltinEntriesProvider provider = new DatapackBuiltinEntriesProvider(gen.getPackOutput(), event.getLookupProvider(), RegistrarHandler.injectRegistries(new RegistrySetBuilder()), Set.of(TheFarlandersMod.MODID));;
		CompletableFuture<HolderLookup.Provider> lookup = provider.getRegistryProvider();
		gen.addProvider(server, provider);

		BlockTagsProvider blockTagProv = new FLTagProv.BlockTagProv(gen, helper, lookup);
		gen.addProvider(server, blockTagProv);
		gen.addProvider(server, new FLTagProv.ItemProv(gen, blockTagProv.contentsGetter(), helper, lookup));
		gen.addProvider(server, new FLTagProv.EntityProv(gen, helper, lookup));
		gen.addProvider(server, new FLTagProv.BiomeProv(gen, helper, lookup));
		gen.addProvider(server, new FLTagProv.StructureProv(gen, helper, lookup));

		gen.addProvider(server, new FLLootProv(gen, helper, lookup));

		gen.addProvider(server, new FLRecipeProv(output, lookup));
		gen.addProvider(server, new FLAdvancementProv(gen, lookup, helper));

		gen.addProvider(server, new FLModelProv.States(output, helper));
		gen.addProvider(server, new FLModelProv.Blocks(output, helper));
		gen.addProvider(server, new FLModelProv.Items(output, helper));

		gen.addProvider(server, new FLSoundProv(output, helper));
	}
}
