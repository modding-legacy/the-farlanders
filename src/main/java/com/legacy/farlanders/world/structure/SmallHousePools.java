package com.legacy.farlanders.world.structure;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.registry.FLProcessors;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawPoolBuilder;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawRegistryHelper;
import com.legacy.structure_gel.api.util.GelCollectors;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class SmallHousePools
{
	public static final ResourceKey<StructureTemplatePool> ROOT = ResourceKey.create(Registries.TEMPLATE_POOL, TheFarlandersMod.locate("small_house/root"));

	public static void bootstrap(BootstapContext<StructureTemplatePool> bootstrap)
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(TheFarlandersMod.MODID, "small_house/", bootstrap);
		JigsawPoolBuilder builder = registry.poolBuilder();

		registry.registerBuilder().pools(builder.clone().names("root")).register("root");
		registry.register("house", registry.poolBuilder().names(GelCollectors.mapOf("house_basic", 15, "house_long", 15, "house_elder_large", 3)).processors(FLProcessors.STAIRS_TO_GRASS.getKey()).maintainWater(false).build());
		registry.register("golem", registry.poolBuilder().names("golem").maintainWater(false).build());
	}
}
