package com.legacy.farlanders.world.structure;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.registry.FLProcessors;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawRegistryHelper;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool.Projection;

public class FarlanderVillagePools
{
	public static final ResourceKey<StructureTemplatePool> ROOT = ResourceKey.create(Registries.TEMPLATE_POOL, TheFarlandersMod.locate("village/normal/centers"));

	public static void bootstrap(BootstapContext<StructureTemplatePool> bootstrap)
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(TheFarlandersMod.MODID, "village/normal/", bootstrap);
		registry.register("centers", registry.poolBuilder().names("centers/watch_tower", "centers/wooden_tower", "centers/gazebo").maintainWater(false).processors(FLProcessors.PATH_TO_DIRT_TO_PLANKS.getKey()).build());
		registry.register("streets", registry.registerBuilder().pools(registry.poolBuilder().names("streets/path_1", "streets/path_2", "streets/path_3", "streets/path_4").maintainWater(false).processors(FLProcessors.PATH_TO_DIRT_TO_PLANKS.getKey())).projection(Projection.TERRAIN_MATCHING));
		registry.register("house", registry.poolBuilder().names("house_1", "house_2", "house_3", "house_4").maintainWater(false).build());
		registry.register("house_special", registry.poolBuilder().names("house_special_1", "house_special_2").maintainWater(false).processors(FLProcessors.LEAVES_TO_GROUND.getKey()).build());
	}
}
