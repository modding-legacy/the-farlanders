package com.legacy.farlanders.world.structure;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.entity.ElderFarlanderEntity;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLJigsawTypes;
import com.legacy.farlanders.registry.FLStructures;
import com.legacy.structure_gel.api.structure.jigsaw.ExtendedJigsawStructurePiece;
import com.legacy.structure_gel.api.structure.jigsaw.IPieceFactory;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.RandomizableContainer;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.StructureStart;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;

public class FarlanderVillageStructure
{
	public static class Capability implements JigsawCapability
	{
		public static final Capability INSTANCE = new Capability();
		public static final Codec<Capability> CODEC = Codec.unit(INSTANCE);

		@Override
		public JigsawCapabilityType<?> getType()
		{
			return FLJigsawTypes.FARLANDER_VILLAGE;
		}

		@Override
		public IPieceFactory getPieceFactory()
		{
			return Piece::new;
		}
	}

	public static class Piece extends ExtendedJigsawStructurePiece
	{
		public Piece(IPieceFactory.Context context)
		{
			super(context);
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(context, nbt);
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor level, RandomSource rand, BoundingBox bounds)
		{
			StructureStart start = level.getLevel().structureManager().getStructureAt(pos, FLStructures.FARLANDER_VILLAGE.getStructure().get(level));
			BlockPos startPos = start.isValid() ? start.getChunkPos().getWorldPosition() : pos;

			if (key.equals("farlander"))
			{
				setAir(level, pos);

				FarlanderEntity entity = createEntity(FLEntityTypes.FARLANDER, level, pos, this.rotation);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();

				entity.setOrigins(pos, startPos.atY(pos.getY()));
				entity.restrictDistance = 45;

				level.addFreshEntity(entity);
			}

			if (key.equals("elder"))
			{
				setAir(level, pos);
				ElderFarlanderEntity entity = createEntity(FLEntityTypes.ELDER_FARLANDER, level, pos, this.rotation);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();

				entity.setOrigins(pos, startPos.atY(pos.getY()));
				entity.restrictDistance = 45;

				level.addFreshEntity(entity);
			}

			if (key.equals("golem"))
			{
				setAir(level, pos);

				EnderGolemEntity entity = createEntity(FLEntityTypes.ENDER_GOLEM, level, pos, this.rotation);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				level.addFreshEntity(entity);
			}

			if (key.contains("guardian"))
			{
				boolean up = key.contains("up");

				if (up)
					level.setBlock(pos, Blocks.DIRT_PATH.defaultBlockState(), 3);
				else
					setAir(level, pos);

				EnderGuardianEntity entity = createEntity(FLEntityTypes.ENDER_GUARDIAN, level, up ? pos.above() : pos, this.rotation);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				level.addFreshEntity(entity);
			}

			if (key.equals("elder-loot"))
			{
				setAir(level, pos);
				RandomizableContainer.setBlockEntityLootTable(level, level.getRandom(), pos.below(), TheFarlandersMod.locate("chests/elder_house_chest"));
			}
		}

		@Override
		public BlockState modifyState(ServerLevelAccessor level, RandomSource rand, BlockPos pos, BlockState originalState)
		{
			if (originalState.is(Blocks.BROWN_WOOL))
				return rand.nextFloat() < 0.4F ? Blocks.GRAVEL.defaultBlockState() : Blocks.COARSE_DIRT.defaultBlockState();

			if (originalState.is(Blocks.GREEN_WOOL))
				return rand.nextFloat() < 0.3F ? Blocks.END_STONE_BRICKS.defaultBlockState() : Blocks.END_STONE.defaultBlockState();

			if (originalState.is(Blocks.YELLOW_WOOL))
				return rand.nextFloat() < 0.8F ? Blocks.END_STONE_BRICKS.defaultBlockState() : Blocks.END_STONE.defaultBlockState();

			return super.modifyState(level, rand, pos, originalState);
		}

		@Override
		public StructurePieceType getType()
		{
			return FLStructures.FARLANDER_VILLAGE.getPieceType().get();
		}
	}
}
