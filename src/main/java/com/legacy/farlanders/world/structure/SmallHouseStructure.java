package com.legacy.farlanders.world.structure;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.entity.ElderFarlanderEntity;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLJigsawTypes;
import com.legacy.farlanders.registry.FLStructures;
import com.legacy.structure_gel.api.structure.jigsaw.ExtendedJigsawStructurePiece;
import com.legacy.structure_gel.api.structure.jigsaw.IPieceFactory;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.RandomizableContainer;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;

public class SmallHouseStructure
{
	public static class Capability implements JigsawCapability
	{
		public static final Capability INSTANCE = new Capability();
		public static final Codec<Capability> CODEC = Codec.unit(INSTANCE);

		@Override
		public JigsawCapabilityType<?> getType()
		{
			return FLJigsawTypes.SMALL_HOUSE;
		}

		@Override
		public IPieceFactory getPieceFactory()
		{
			return Piece::new;
		}
	}

	public static class Piece extends ExtendedJigsawStructurePiece
	{
		public Piece(IPieceFactory.Context context)
		{
			super(context);
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(context, nbt);
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor worldIn, RandomSource rand, BoundingBox bounds)
		{
			if (key.equals("farlander"))
			{
				setAir(worldIn, pos);
				FarlanderEntity entity = createEntity(FLEntityTypes.FARLANDER, worldIn, pos, this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				
				entity.setOrigins(pos, null);
				
				worldIn.addFreshEntity(entity);
			}

			if (key.equals("elder"))
			{
				setAir(worldIn, pos);
				ElderFarlanderEntity entity = createEntity(FLEntityTypes.ELDER_FARLANDER, worldIn, pos, this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				
				entity.setOrigins(pos, null);

				worldIn.addFreshEntity(entity);
			}

			if (key.equals("golem"))
			{
				setAir(worldIn, pos);

				int safeY = worldIn.getHeight(Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, pos.getX(), pos.getZ());
				BlockPos groundPos = pos.atY(safeY);

				EnderGolemEntity entity = createEntity(FLEntityTypes.ENDER_GOLEM, worldIn, groundPos, this.rotation);
				entity.finalizeSpawn(worldIn, worldIn.getCurrentDifficultyAt(groundPos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				worldIn.addFreshEntity(entity);
			}

			if (key.equals("chest"))
			{
				worldIn.setBlock(pos, Blocks.COBWEB.defaultBlockState(), 3);
				RandomizableContainer.setBlockEntityLootTable(worldIn, worldIn.getRandom(), pos, TheFarlandersMod.locate("chests/elder_house_chest"));
			}
		}
		
		@Override
		public BlockState modifyState(ServerLevelAccessor level, RandomSource rand, BlockPos pos, BlockState originalState)
		{
			if (originalState.is(Blocks.BROWN_WOOL))
				return rand.nextFloat() < 0.4F ? Blocks.GRAVEL.defaultBlockState() : Blocks.COARSE_DIRT.defaultBlockState();

			if (originalState.is(Blocks.GREEN_WOOL))
				return rand.nextFloat() < 0.3F ? Blocks.END_STONE_BRICKS.defaultBlockState() : Blocks.END_STONE.defaultBlockState();

			if (originalState.is(Blocks.YELLOW_WOOL))
				return rand.nextFloat() < 0.8F ? Blocks.END_STONE_BRICKS.defaultBlockState() : Blocks.END_STONE.defaultBlockState();

			return super.modifyState(level, rand, pos, originalState);
		}

		@Override
		public StructurePieceType getType()
		{
			return FLStructures.SMALL_HOUSE.getPieceType().get();
		}
	}
}
