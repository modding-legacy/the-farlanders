package com.legacy.farlanders.world.structure;

import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLJigsawTypes;
import com.legacy.farlanders.registry.FLStructures;
import com.legacy.structure_gel.api.structure.jigsaw.ExtendedJigsawStructurePiece;
import com.legacy.structure_gel.api.structure.jigsaw.IPieceFactory;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapability;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceType;

public class TitanSpireStructure
{
	public static class Capability implements JigsawCapability
	{
		public static final Capability INSTANCE = new Capability();
		public static final Codec<Capability> CODEC = Codec.unit(INSTANCE);

		@Override
		public JigsawCapabilityType<?> getType()
		{
			return FLJigsawTypes.TITAN_SPIRE;
		}

		@Override
		public IPieceFactory getPieceFactory()
		{
			return Piece::new;
		}
	}

	public static class Piece extends ExtendedJigsawStructurePiece
	{
		public Piece(IPieceFactory.Context context)
		{
			super(context);
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(context, nbt);
		}

		@Override
		public void handleDataMarker(String key, BlockPos pos, ServerLevelAccessor level, RandomSource rand, BoundingBox bounds)
		{
			if (key.equals("titan"))
			{
				setAir(level, pos);

				TitanEntity entity = createEntity(FLEntityTypes.TITAN, level, pos, this.rotation);
				entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
				entity.setPersistenceRequired();
				level.addFreshEntity(entity);
			}

			if (key.equals("golem"))
			{
				level.setBlock(pos, Blocks.OBSIDIAN.defaultBlockState(), 3);

				for (int i = 0; i < 2; ++i)
				{
					float offset = rand.nextFloat() * 360.0F;
					int dist = 10;
					double x = dist * Math.cos(offset);
					double z = dist * Math.sin(offset);

					BlockPos offsetPos = pos.offset((int) x, 0, (int) z);
					offsetPos = offsetPos.atY(level.getHeight(Heightmap.Types.MOTION_BLOCKING, offsetPos.getX(), offsetPos.getZ()));

					EnderGolemEntity entity = createEntity(FLEntityTypes.ENDER_GOLEM, level, offsetPos, this.rotation);
					entity.finalizeSpawn(level, level.getCurrentDifficultyAt(pos), MobSpawnType.STRUCTURE, null, null);
					entity.setPersistenceRequired();
					
					entity.spirePos = pos.atY(i);
					level.addFreshEntity(entity);					
				}
			}
		}

		@Override
		public BlockState modifyState(ServerLevelAccessor level, RandomSource rand, BlockPos pos, BlockState originalState)
		{
			if (originalState.is(Blocks.PURPLE_WOOL))
				return rand.nextFloat() < 0.7F ? Blocks.CRYING_OBSIDIAN.defaultBlockState() : Blocks.OBSIDIAN.defaultBlockState();

			if (originalState.is(Blocks.MAGENTA_WOOL))
				return rand.nextFloat() < 0.3F ? Blocks.CRYING_OBSIDIAN.defaultBlockState() : Blocks.OBSIDIAN.defaultBlockState();

			return super.modifyState(level, rand, pos, originalState);
		}

		@Override
		public StructurePieceType getType()
		{
			return FLStructures.TITAN_SPIRE.getPieceType().get();
		}
	}
}
