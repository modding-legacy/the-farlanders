package com.legacy.farlanders.world.structure;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawPoolBuilder;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawRegistryHelper;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class TitanSpirePools
{
	public static final ResourceKey<StructureTemplatePool> ROOT = ResourceKey.create(Registries.TEMPLATE_POOL, TheFarlandersMod.locate("titan_spire/root"));

	public static void bootstrap(BootstapContext<StructureTemplatePool> bootstrap)
	{
		JigsawRegistryHelper registry = new JigsawRegistryHelper(TheFarlandersMod.MODID, "titan_spire/", bootstrap);
		JigsawPoolBuilder builder = registry.poolBuilder();

		registry.registerBuilder().pools(builder.clone().names("root")).register("root");
		registry.register("spire", registry.poolBuilder().names("spire_1").maintainWater(false).build());
	}
}
