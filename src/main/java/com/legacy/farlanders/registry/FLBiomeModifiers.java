package com.legacy.farlanders.registry;

import java.util.List;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.data.FLTags;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.mojang.serialization.Codec;

import net.minecraft.core.Holder;
import net.minecraft.core.Holder.Reference;
import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.biome.MobSpawnSettings.SpawnerData;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.neoforged.neoforge.common.world.BiomeModifier;
import net.neoforged.neoforge.common.world.BiomeModifiers;
import net.neoforged.neoforge.common.world.MobSpawnSettingsBuilder;
import net.neoforged.neoforge.common.world.ModifiableBiomeInfo.BiomeInfo.Builder;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

public class FLBiomeModifiers
{
	public static final RegistrarHandler<BiomeModifier> HANDLER = RegistrarHandler.getOrCreate(NeoForgeRegistries.Keys.BIOME_MODIFIERS, TheFarlandersMod.MODID);

	/*public static final Pointer<BiomeModifier> ADD_LOOT_WELL = HANDLER.createPointer("add_loot_well", c -> addFeature(c, Decoration.SURFACE_STRUCTURES, FLTags.Biomes.HAS_LOOT_WELLS, FLFeatures.Placed.LOOT_WELL.getKey()));*/
	/*public static final Pointer<BiomeModifier> ADD_TITAN_SPIRE = HANDLER.createPointer("add_titan_spire", c -> addFeature(c, Decoration.SURFACE_STRUCTURES, FLTags.Biomes.HAS_TITAN_SPIRES, FLFeatures.Placed.TITAN_SPIRE.getKey()));*/
	public static final Pointer<BiomeModifier> ADD_ENDUMIUM_ORE = HANDLER.createPointer("add_endumium_ore", c -> addFeature(c, Decoration.UNDERGROUND_ORES, FLTags.Biomes.HAS_ENDUMIUM_ORE, FLFeatures.Placed.ENDUMIUM_ORE.getKey()));

	public static final Pointer<BiomeModifier> ADD_SPAWNS = HANDLER.createPointer("add_farlanders_natural_spawns", c ->
	{
		var enderminion = new MobSpawnSettings.SpawnerData(FLEntityTypes.ENDERMINION, 1, 1, 1);
		var mysticEnderminion = new MobSpawnSettings.SpawnerData(FLEntityTypes.MYSTIC_ENDERMINION, 1, 1, 1);
		var wanderer = new MobSpawnSettings.SpawnerData(FLEntityTypes.WANDERER, 1, 1, 1);

		/*enderminion.addMobCharge(EntityType.ENDERMAN, 1.0D, 0.12D);*/
		var looter = new MobSpawnSettings.SpawnerData(FLEntityTypes.LOOTER, 15, 1, 2);
		var rebel = new MobSpawnSettings.SpawnerData(FLEntityTypes.REBEL, 10, 2, 3);
		var classic = new MobSpawnSettings.SpawnerData(FLEntityTypes.CLASSIC_ENDERMAN, 7, 1, 2);
		var fanmade = new MobSpawnSettings.SpawnerData(FLEntityTypes.FANMADE_ENDERMAN, 5, 1, 1);
		var mystic = new MobSpawnSettings.SpawnerData(FLEntityTypes.MYSTIC_ENDERMAN, 3, 1, 1);

		return addSpawn(c, FLTags.Biomes.HAS_NATURAL_SPAWNS, enderminion, mysticEnderminion, wanderer, looter, rebel, classic, fanmade, mystic);
	});

	public static final Pointer<BiomeModifier> REMOVE_SPAWNS_FROM_BLACKLISTED = HANDLER.createPointer("remove_farlanders_from_disallowed_biomes", c -> removeSpawns(c, FLTags.Biomes.NATURAL_SPAWN_BLACKLIST, HolderSet.direct(getAllEntities())));

	private static BiomeModifier addFeature(BootstapContext<?> bootstrap, GenerationStep.Decoration step, TagKey<Biome> spawnTag, ResourceKey<PlacedFeature> feature)
	{
		return new BiomeModifiers.AddFeaturesBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), HolderSet.direct(bootstrap.lookup(Registries.PLACED_FEATURE).getOrThrow(feature)), step);
	}

	private static BiomeModifier addSpawn(BootstapContext<?> bootstrap, TagKey<Biome> spawnTag, SpawnerData... data)
	{
		return new BiomeModifiers.AddSpawnsBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), List.of(data));
	}

	private static BiomeModifier removeSpawns(BootstapContext<?> bootstrap, TagKey<Biome> spawnTag, HolderSet<EntityType<?>> entityTypes)
	{
		return new BiomeModifiers.RemoveSpawnsBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), entityTypes);
	}

	private static List<Reference<EntityType<?>>> getAllEntities()
	{
		return BuiltInRegistries.ENTITY_TYPE.holders().toList().stream().filter(item -> item.key().location().getNamespace().equals(TheFarlandersMod.MODID)).toList();
	}

	public static class Serializers
	{
		public static final RegistrarHandler<Codec<? extends BiomeModifier>> SERIALIZER_HANDLER = RegistrarHandler.getOrCreate(NeoForgeRegistries.Keys.BIOME_MODIFIER_SERIALIZERS, TheFarlandersMod.MODID);
		public static final Registrar.Static<Codec<FLSpawnCostModifier>> SPAWN_COST_SERIALIZER = SERIALIZER_HANDLER.createStatic("farlanders_charges", () -> FLSpawnCostModifier.CODEC);

		static
		{
			RegistrarHandler<BiomeModifier> handler = RegistrarHandler.getOrCreate(NeoForgeRegistries.Keys.BIOME_MODIFIERS, TheFarlandersMod.MODID);
			handler.createPointer("farlanders_charges", () -> FLSpawnCostModifier.INSTANCE);
		}

		public static class FLSpawnCostModifier implements BiomeModifier
		{
			public static final FLSpawnCostModifier INSTANCE = new FLSpawnCostModifier();
			public static final Codec<FLSpawnCostModifier> CODEC = Codec.unit(() -> INSTANCE);

			private FLSpawnCostModifier()
			{
			}

			@Override
			public void modify(Holder<Biome> biome, Phase phase, Builder builder)
			{
				if (phase == Phase.ADD && biome.is(FLTags.Biomes.HAS_NATURAL_SPAWNS) && !biome.is(FLTags.Biomes.NATURAL_SPAWN_BLACKLIST))
				{
					MobSpawnSettingsBuilder spawns = builder.getMobSpawnSettings();

					spawns.addMobCharge(FLEntityTypes.ENDERMINION, 0.5F, 1F);
					spawns.addMobCharge(FLEntityTypes.MYSTIC_ENDERMINION, 0.7F, 1F);
					spawns.addMobCharge(FLEntityTypes.WANDERER, 0.8F, 1F);
				}
			}

			@Override
			public Codec<? extends BiomeModifier> codec()
			{
				return CODEC;
			}
		}
	}
}
