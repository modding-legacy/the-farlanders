package com.legacy.farlanders.registry;

import java.util.List;
import java.util.function.Supplier;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

public class FLFeatures
{
	@SubscribeEvent
	public static void init(RegisterEvent event)
	{
	}

	public static class Configured
	{
		public static final RegistrarHandler<ConfiguredFeature<?, ?>> HANDLER = RegistrarHandler.getOrCreate(Registries.CONFIGURED_FEATURE, TheFarlandersMod.MODID);

		public static final Registrar.Pointer<ConfiguredFeature<?, ?>> ENDUMIUM_ORE = register("endumium_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES), FLBlocks.endumium_ore.defaultBlockState()), OreConfiguration.target(new TagMatchTest(BlockTags.DEEPSLATE_ORE_REPLACEABLES), FLBlocks.deepslate_endumium_ore.defaultBlockState())), 3));

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Registrar.Pointer<ConfiguredFeature<?, ?>> register(String key, F feature, Supplier<FC> config)
		{
			return HANDLER.createPointer(key, () -> new ConfiguredFeature<>(feature, config.get()));
		}
	}

	public static class Placed
	{
		public static final RegistrarHandler<PlacedFeature> HANDLER = RegistrarHandler.getOrCreate(Registries.PLACED_FEATURE, TheFarlandersMod.MODID);

		public static final Registrar.Pointer<PlacedFeature> ENDUMIUM_ORE = register("endumium_ore", Configured.ENDUMIUM_ORE, commonOrePlacement(100, HeightRangePlacement.triangle(VerticalAnchor.absolute(-160), VerticalAnchor.absolute(16))));

		private static Pointer<PlacedFeature> register(String key, Registrar.Pointer<ConfiguredFeature<?, ?>> feature, List<PlacementModifier> mods)
		{
			return HANDLER.createPointer(key, (b) -> new PlacedFeature(feature.getHolder(b).get(), List.copyOf(mods)));
		}

		private static List<PlacementModifier> commonOrePlacement(int pCount, PlacementModifier pHeightRange)
		{
			return orePlacement(CountPlacement.of(pCount), pHeightRange);
		}

		private static List<PlacementModifier> orePlacement(PlacementModifier p_195347_, PlacementModifier p_195348_)
		{
			return List.of(p_195347_, InSquarePlacement.spread(), p_195348_, BiomeFilter.biome());
		}
	}
}
