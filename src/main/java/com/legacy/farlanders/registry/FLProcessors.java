package com.legacy.farlanders.registry;

import java.util.List;
import java.util.function.Supplier;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.structure.templatesystem.AlwaysTrueTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.ProcessorRule;
import net.minecraft.world.level.levelgen.structure.templatesystem.RandomBlockMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleProcessor;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureProcessorList;

public class FLProcessors
{
	public static final RegistrarHandler<StructureProcessorList> HANDLER = RegistrarHandler.getOrCreate(Registries.PROCESSOR_LIST, TheFarlandersMod.MODID);

	public static final Registrar.Pointer<StructureProcessorList> STAIRS_TO_GRASS = register("stairs_to_grass", () -> new StructureProcessorList(List.of(new RuleProcessor(List.of(new ProcessorRule(new BlockMatchTest(Blocks.STONE_BRICK_STAIRS), new BlockMatchTest(Blocks.GRASS_BLOCK), Blocks.GRASS_BLOCK.defaultBlockState()), new ProcessorRule(new BlockMatchTest(Blocks.STONE_BRICK_STAIRS), new BlockMatchTest(Blocks.DIRT), Blocks.DIRT.defaultBlockState()), new ProcessorRule(new BlockMatchTest(Blocks.STONE_BRICK_STAIRS), new BlockMatchTest(Blocks.STONE), Blocks.STONE.defaultBlockState()))))));
	public static final Registrar.Pointer<StructureProcessorList> LEAVES_TO_GROUND = register("leaves_to_ground", () -> new StructureProcessorList(List.of(new RuleProcessor(List.of(new ProcessorRule(new BlockMatchTest(Blocks.BIRCH_LEAVES), new BlockMatchTest(Blocks.GRASS_BLOCK), Blocks.GRASS_BLOCK.defaultBlockState()), new ProcessorRule(new BlockMatchTest(Blocks.BIRCH_LEAVES), new BlockMatchTest(Blocks.DIRT), Blocks.DIRT.defaultBlockState()), new ProcessorRule(new BlockMatchTest(Blocks.BIRCH_LEAVES), new BlockMatchTest(Blocks.STONE), Blocks.STONE.defaultBlockState()))))));
	public static final Registrar.Pointer<StructureProcessorList> PATH_TO_DIRT_TO_PLANKS = register("path_to_dirt_to_planks", () -> new StructureProcessorList(List.of(new RuleProcessor(List.of(new ProcessorRule(new BlockMatchTest(Blocks.DIRT_PATH), new BlockMatchTest(Blocks.WATER), Blocks.BIRCH_PLANKS.defaultBlockState()), new ProcessorRule(new RandomBlockMatchTest(Blocks.DIRT_PATH, 0.1F), AlwaysTrueTest.INSTANCE, Blocks.GRASS_BLOCK.defaultBlockState()), new ProcessorRule(new BlockMatchTest(Blocks.GRASS_BLOCK), new BlockMatchTest(Blocks.WATER), Blocks.WATER.defaultBlockState()), new ProcessorRule(new BlockMatchTest(Blocks.DIRT), new BlockMatchTest(Blocks.WATER), Blocks.WATER.defaultBlockState()))))));

	private static Registrar.Pointer<StructureProcessorList> register(String key, Supplier<StructureProcessorList> processorList)
	{
		return HANDLER.createPointer(key, processorList);
	}
}
