package com.legacy.farlanders.registry;

import com.legacy.farlanders.TheFarlandersMod;

import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;

public class FLSounds
{
	public static final SoundEvent ITEM_MYSTIC_WAND_CAST_FIREBALL = create("item.mystic_wand.cast_fireball");
	public static final SoundEvent ITEM_MYSTIC_WAND_CAST_REGEN = create("item.mystic_wand.cast_regen");
	public static final SoundEvent ITEM_MYSTIC_WAND_CAST_ORE = create("item.mystic_wand.cast_ore");
	public static final SoundEvent ITEM_MYSTIC_WAND_CAST_INVIS = create("item.mystic_wand.cast_invis");

	public static final SoundEvent ENTITY_FARLANDER_IDLE = create("entity.farlander.idle");
	public static final SoundEvent ENTITY_FARLANDER_HURT = create("entity.farlander.hurt");
	public static final SoundEvent ENTITY_FARLANDER_DEATH = create("entity.farlander.death");

	public static final SoundEvent ENTITY_LOOTER_IDLE = create("entity.looter.idle");
	public static final SoundEvent ENTITY_LOOTER_HURT = create("entity.looter.hurt");
	public static final SoundEvent ENTITY_LOOTER_DEATH = create("entity.looter.death");

	public static final SoundEvent ENTITY_REBEL_IDLE = create("entity.rebel.idle");
	public static final SoundEvent ENTITY_REBEL_HURT = create("entity.rebel.hurt");
	public static final SoundEvent ENTITY_REBEL_DEATH = create("entity.rebel.death");

	public static final SoundEvent ENTITY_WANDERER_IDLE = create("entity.wanderer.idle");
	public static final SoundEvent ENTITY_WANDERER_HURT = create("entity.wanderer.hurt");
	public static final SoundEvent ENTITY_WANDERER_DEATH = create("entity.wanderer.death");

	public static final SoundEvent ENTITY_ENDERMINION_IDLE = create("entity.enderminion.idle");
	public static final SoundEvent ENTITY_ENDERMINION_HURT = create("entity.enderminion.hurt");
	public static final SoundEvent ENTITY_ENDERMINION_DEATH = create("entity.enderminion.death");

	public static final SoundEvent ENTITY_MYSTIC_ENDERMINION_IDLE = create("entity.mystic_enderminion.idle");
	public static final SoundEvent ENTITY_MYSTIC_ENDERMINION_HURT = create("entity.mystic_enderminion.hurt");
	public static final SoundEvent ENTITY_MYSTIC_ENDERMINION_DEATH = create("entity.mystic_enderminion.death");

	public static final SoundEvent ENTITY_MYSTIC_ENDERMAN_IDLE = create("entity.mystic_enderman.idle");
	public static final SoundEvent ENTITY_MYSTIC_ENDERMAN_HURT = create("entity.mystic_enderman.hurt");
	public static final SoundEvent ENTITY_MYSTIC_ENDERMAN_DEATH = create("entity.mystic_enderman.death");
	public static final SoundEvent ENTITY_MYSTIC_ENDERMAN_CONFUSION = create("entity.mystic_enderman.confusion");
	public static final SoundEvent ENTITY_MYSTIC_ENDERMAN_BLINDNESS = create("entity.mystic_enderman.blindness");

	public static final SoundEvent ENTITY_FANMADE_ENDERMAN_IDLE = create("entity.fanmade_enderman.idle");
	public static final SoundEvent ENTITY_FANMADE_ENDERMAN_DEATH = create("entity.fanmade_enderman.death");

	public static final SoundEvent ENTITY_ENDER_GOLEM_IDLE = create("entity.ender_golem.idle");
	public static final SoundEvent ENTITY_ENDER_GOLEM_HURT = create("entity.ender_golem.hurt");
	public static final SoundEvent ENTITY_ENDER_GOLEM_DEATH = create("entity.ender_golem.death");

	public static final SoundEvent ENTITY_ENDER_GUARDIAN_IDLE = create("entity.ender_guardian.idle");
	public static final SoundEvent ENTITY_ENDER_GUARDIAN_HURT = create("entity.ender_guardian.hurt");
	public static final SoundEvent ENTITY_ENDER_GUARDIAN_DEATH = create("entity.ender_guardian.death");
	public static final SoundEvent ENTITY_ENDER_GUARDIAN_SHOOT = create("entity.ender_guardian.shoot");

	public static final SoundEvent ENTITY_TITAN_IDLE = create("entity.titan.idle");
	public static final SoundEvent ENTITY_TITAN_HURT = create("entity.titan.hurt");
	public static final SoundEvent ENTITY_TITAN_HURT_HEART = create("entity.titan.hurt_heart");
	public static final SoundEvent ENTITY_TITAN_DEATH = create("entity.titan.death");
	public static final SoundEvent ENTITY_TITAN_ENRAGE = create("entity.titan.enrage");

	public static final SoundEvent ENTITY_TITAN_HOLDING_LONG = create("entity.titan.holding_long");
	public static final SoundEvent ENTITY_TITAN_HOLDING_MED = create("entity.titan.holding_med");
	public static final SoundEvent ENTITY_TITAN_HOLDING_SHORT = create("entity.titan.holding_short");

	public static final SoundEvent MUSIC_NIGHTFALL_AMBIENT = create("music.nightfall.ambient");

	public static void init()
	{
	}

	private static SoundEvent create(String name)
	{
		ResourceLocation location = TheFarlandersMod.locate(name);
		SoundEvent sound = SoundEvent.createVariableRangeEvent(location);
		Registry.register(BuiltInRegistries.SOUND_EVENT, location, sound);
		return sound;
	}
}