package com.legacy.farlanders.registry;

import java.util.ArrayList;
import java.util.List;

import com.legacy.farlanders.TheFarlandersMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DropExperienceBlock;
import net.neoforged.neoforge.registries.RegisterEvent;

public class FLBlocks
{
	public static Block endumium_ore, deepslate_endumium_ore, endumium_block;

	public static List<Block> blockItems = new ArrayList<>();
	private static RegisterEvent registry;

	public static void init(RegisterEvent event)
	{
		registry = event;

		endumium_ore = register("endumium_ore", new DropExperienceBlock(UniformInt.of(3, 7), Block.Properties.ofFullCopy(Blocks.EMERALD_ORE)));
		deepslate_endumium_ore = register("deepslate_endumium_ore", new DropExperienceBlock(UniformInt.of(3, 7), Block.Properties.ofFullCopy(Blocks.DEEPSLATE_EMERALD_ORE)));
		endumium_block = register("endumium_block", new Block(Block.Properties.ofFullCopy(Blocks.EMERALD_BLOCK)));
	}


	private static <B extends Block> B register(String key, B block)
	{
		registry.register(Registries.BLOCK, TheFarlandersMod.locate(key), () -> block);
		blockItems.add(block);
		return block;
	}
}