package com.legacy.farlanders.registry;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.triggers.ClassicEndermanTrigger;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.advancements.CriterionTrigger;

public class FLTriggers
{
	public static void init()
	{
	}

	public static final ClassicEndermanTrigger CLASSIC_ENDERMAN_KILL = register("classic_enderman_block", new ClassicEndermanTrigger());

	public static <T extends CriterionTrigger<?>> T register(String name, T criterion)
	{
		return CriteriaTriggers.register(TheFarlandersMod.find(name), criterion);
	}
}
