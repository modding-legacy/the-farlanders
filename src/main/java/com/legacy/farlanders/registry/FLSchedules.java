package com.legacy.farlanders.registry;

import com.legacy.farlanders.TheFarlandersMod;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.schedule.Activity;
import net.minecraft.world.entity.schedule.Schedule;
import net.minecraft.world.entity.schedule.ScheduleBuilder;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class FLSchedules
{
	public static final Lazy<Schedule> FARLANDER_DEFAULT = Lazy.of(() -> buildSchedule("farlander_default").changeActivityAt(0, Activity.REST).changeActivityAt(13000, Activity.IDLE).changeActivityAt(19000, Activity.MEET).changeActivityAt(20000, Activity.IDLE).build());
	public static final Lazy<Schedule> FARLANDER_BABY = Lazy.of(() -> buildSchedule("farlander_baby").changeActivityAt(0, Activity.REST).changeActivityAt(13000, Activity.IDLE).changeActivityAt(16000, Activity.PLAY).changeActivityAt(18000, Activity.IDLE).changeActivityAt(20000, Activity.PLAY).build());

	private static ScheduleBuilder buildSchedule(String string)
	{
		return new ScheduleBuilder(new Schedule());
	}

	public static void init(RegisterEvent event)
	{
		event.register(Registries.SCHEDULE, TheFarlandersMod.locate("farlander_default"), FARLANDER_DEFAULT);
		event.register(Registries.SCHEDULE, TheFarlandersMod.locate("farlander_baby"), FARLANDER_BABY);
	}
}
