package com.legacy.farlanders.registry;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.world.structure.FarlanderVillageStructure;
import com.legacy.farlanders.world.structure.SmallHouseStructure;
import com.legacy.farlanders.world.structure.TitanSpireStructure;
import com.legacy.structure_gel.api.registry.StructureGelRegistries;
import com.legacy.structure_gel.api.structure.jigsaw.JigsawCapabilityType;

import net.neoforged.neoforge.registries.RegisterEvent;

public class FLJigsawTypes
{
	public static final JigsawCapabilityType<SmallHouseStructure.Capability> SMALL_HOUSE = () -> SmallHouseStructure.Capability.CODEC;
	public static final JigsawCapabilityType<FarlanderVillageStructure.Capability> FARLANDER_VILLAGE = () -> FarlanderVillageStructure.Capability.CODEC;
	public static final JigsawCapabilityType<TitanSpireStructure.Capability> TITAN_SPIRE = () -> TitanSpireStructure.Capability.CODEC;

	public static void init(final RegisterEvent event)
	{
		register(event, "small_house", SMALL_HOUSE);
		register(event, "farlander_village", FARLANDER_VILLAGE);
		register(event, "titan_spire", TITAN_SPIRE);
	}

	private static void register(RegisterEvent event, String key, JigsawCapabilityType<?> type)
	{
		event.register(StructureGelRegistries.Keys.JIGSAW_TYPE, TheFarlandersMod.locate(key), () -> type);
	}
}
