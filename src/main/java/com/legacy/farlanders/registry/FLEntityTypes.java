package com.legacy.farlanders.registry;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.entity.ElderFarlanderEntity;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.hostile.ClassicEndermanEntity;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.legacy.farlanders.entity.hostile.FanmadeEndermanEntity;
import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.legacy.farlanders.entity.hostile.MysticEndermanEntity;
import com.legacy.farlanders.entity.hostile.RebelEntity;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.entity.tameable.EnderminionEntity;
import com.legacy.farlanders.entity.tameable.MysticEnderminionEntity;

import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.entity.monster.EnderMan;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.levelgen.Heightmap;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;
import net.neoforged.neoforge.event.entity.SpawnPlacementRegisterEvent;
import net.neoforged.neoforge.event.entity.SpawnPlacementRegisterEvent.Operation;
import net.neoforged.neoforge.registries.RegisterEvent;

public class FLEntityTypes
{
	public static final EntityType<FarlanderEntity> FARLANDER = buildEntity("farlander", EntityType.Builder.of(FarlanderEntity::new, MobCategory.CREATURE).sized(0.6F, 1.8F));
	public static final EntityType<ElderFarlanderEntity> ELDER_FARLANDER = buildEntity("elder_farlander", EntityType.Builder.of(ElderFarlanderEntity::new, MobCategory.CREATURE).sized(0.6F, 1.8F));
	public static final EntityType<WandererEntity> WANDERER = buildEntity("wanderer", EntityType.Builder.of(WandererEntity::new, MobCategory.CREATURE).sized(0.6F, 1.8F));
	public static final EntityType<EnderGuardianEntity> ENDER_GUARDIAN = buildEntity("ender_guardian", EntityType.Builder.of(EnderGuardianEntity::new, MobCategory.CREATURE).sized(0.6F, 1.8F));
	public static final EntityType<LooterEntity> LOOTER = buildEntity("looter", EntityType.Builder.of(LooterEntity::new, MobCategory.MONSTER).sized(0.6F, 1.8F));
	public static final EntityType<RebelEntity> REBEL = buildEntity("rebel", EntityType.Builder.of(RebelEntity::new, MobCategory.MONSTER).sized(0.6F, 1.8F));
	public static final EntityType<EnderminionEntity> ENDERMINION = buildEntity("enderminion", EntityType.Builder.of(EnderminionEntity::new, MobCategory.CREATURE).sized(0.8F, 2.35F));
	public static final EntityType<MysticEnderminionEntity> MYSTIC_ENDERMINION = buildEntity("mystic_enderminion", EntityType.Builder.of(MysticEnderminionEntity::new, MobCategory.CREATURE).sized(0.8F, 2.35F));
	public static final EntityType<MysticEndermanEntity> MYSTIC_ENDERMAN = buildEntity("mystic_enderman", EntityType.Builder.of(MysticEndermanEntity::new, MobCategory.MONSTER).sized(0.6F, 2.9F));
	public static final EntityType<ClassicEndermanEntity> CLASSIC_ENDERMAN = buildEntity("classic_enderman", EntityType.Builder.of(ClassicEndermanEntity::new, MobCategory.MONSTER).sized(0.6F, 2.9F));
	public static final EntityType<FanmadeEndermanEntity> FANMADE_ENDERMAN = buildEntity("fanmade_enderman", EntityType.Builder.of(FanmadeEndermanEntity::new, MobCategory.MONSTER).sized(0.6F, 2.9F));
	public static final EntityType<EnderGolemEntity> ENDER_GOLEM = buildEntity("ender_golem", EntityType.Builder.of(EnderGolemEntity::new, MobCategory.MONSTER).sized(1.2F, 3.9F));
	public static final EntityType<TitanEntity> TITAN = buildEntity("titan", EntityType.Builder.of(TitanEntity::new, MobCategory.MONSTER).sized(1.8F, 5.3F));

	/*public static final EntityType<EndSpiritEntity> END_SPIRIT = buildEntity("end_spirit", EntityType.Builder.of(EndSpiritEntity::new, MobCategory.CREATURE).sized(0.6F, 1.9F));
	public static final EntityType<NightfallSpiritEntity> NIGHTFALL_SPIRIT = buildEntity("nightfall_spirit", EntityType.Builder.of(NightfallSpiritEntity::new, MobCategory.CREATURE).sized(0.6F, 1.9F));
	
	public static final EntityType<EnderColossusEntity> ENDER_COLOSSUS = buildEntity("ender_colossus", EntityType.Builder.of(EnderColossusEntity::new, MobCategory.MONSTER).fireImmune().sized(3.6F, 21.0F));
	public static final EntityType<MiniDragonEntity> MINI_ENDER_DRAGON = buildEntity("mini_ender_dragon", EntityType.Builder.of(MiniDragonEntity::new, MobCategory.MONSTER).fireImmune().sized(4.0F, 2.5F));
	public static final EntityType<EnderColossusShadowEntity> ENDER_COLOSSUS_SHADOW = buildEntity("ender_colossus_shadow", EntityType.Builder.of(EnderColossusShadowEntity::new, MobCategory.MONSTER).fireImmune().sized(3.5F, 15.5F));
	public static final EntityType<ThrownBlockEntity> THROWN_BLOCK = buildEntity("thrown_block", EntityType.Builder.<ThrownBlockEntity>of(ThrownBlockEntity::new, MobCategory.MISC).setCustomClientFactory(ThrownBlockEntity::new).setShouldReceiveVelocityUpdates(true).fireImmune().sized(3.0F, 3.0F));*/

	private static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		register("farlander", FARLANDER);
		register("elder_farlander", ELDER_FARLANDER);
		register("wanderer", WANDERER);
		register("ender_guardian", ENDER_GUARDIAN);
		register("looter", LOOTER);
		register("rebel", REBEL);
		register("enderminion", ENDERMINION);
		register("mystic_enderminion", MYSTIC_ENDERMINION);
		register("mystic_enderman", MYSTIC_ENDERMAN);
		register("classic_enderman", CLASSIC_ENDERMAN);
		register("fanmade_enderman", FANMADE_ENDERMAN);
		register("ender_golem", ENDER_GOLEM);
		register("titan", TITAN);

		/*register("end_spirit", END_SPIRIT);
		register("nightfall_spirit", NIGHTFALL_SPIRIT);
		
		register("ender_colossus", ENDER_COLOSSUS);
		register("mini_ender_dragon", MINI_ENDER_DRAGON);
		register("ender_colossus_shadow", ENDER_COLOSSUS_SHADOW);
		register("thrown_block", THROWN_BLOCK);*/
	}

	private static void register(String name, EntityType<?> type)
	{
		if (registerEvent == null)
			return;

		registerEvent.register(Registries.ENTITY_TYPE, TheFarlandersMod.locate(name), () -> type);
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(TheFarlandersMod.find(key));
	}

	public static void onAttributesRegistered(EntityAttributeCreationEvent event)
	{
		event.put(FLEntityTypes.FARLANDER, FarlanderEntity.registerAttributeMap().build());
		event.put(FLEntityTypes.ELDER_FARLANDER, ElderFarlanderEntity.registerAttributeMap().build());
		event.put(FLEntityTypes.WANDERER, WandererEntity.registerAttributeMap().build());
		event.put(FLEntityTypes.ENDER_GUARDIAN, EnderGuardianEntity.registerAttributeMap().build());
		event.put(FLEntityTypes.LOOTER, LooterEntity.registerAttributeMap().build());
		event.put(FLEntityTypes.REBEL, RebelEntity.registerAttributeMap().build());
		event.put(FLEntityTypes.ENDERMINION, EnderminionEntity.registerAttributeMap().build());
		event.put(FLEntityTypes.MYSTIC_ENDERMINION, MysticEnderminionEntity.registerAttributeMap().build());
		event.put(FLEntityTypes.MYSTIC_ENDERMAN, MysticEndermanEntity.createMysticAttributes().build());
		event.put(FLEntityTypes.CLASSIC_ENDERMAN, EnderMan.createAttributes().build());
		event.put(FLEntityTypes.FANMADE_ENDERMAN, EnderMan.createAttributes().build());
		event.put(FLEntityTypes.ENDER_GOLEM, EnderGolemEntity.registerAttributeMap().build());
		event.put(FLEntityTypes.TITAN, TitanEntity.registerAttributeMap().build());
	}

	public static void registerPlacements(SpawnPlacementRegisterEvent event)
	{
		mobPlacement(event, FLEntityTypes.FARLANDER, false);
		mobPlacement(event, FLEntityTypes.ELDER_FARLANDER, false);
		mobPlacement(event, FLEntityTypes.WANDERER, true);
		mobPlacement(event, FLEntityTypes.ENDER_GUARDIAN, false);
		monsterPlacement(event, FLEntityTypes.LOOTER);
		monsterPlacement(event, FLEntityTypes.REBEL);
		mobPlacement(event, FLEntityTypes.ENDERMINION, true);
		mobPlacement(event, FLEntityTypes.MYSTIC_ENDERMINION, true);
		monsterPlacement(event, FLEntityTypes.MYSTIC_ENDERMAN);
		monsterPlacement(event, FLEntityTypes.CLASSIC_ENDERMAN);
		monsterPlacement(event, FLEntityTypes.FANMADE_ENDERMAN);
		mobPlacement(event, FLEntityTypes.ENDER_GOLEM, false);
		mobPlacement(event, FLEntityTypes.TITAN, false);

		/*SpawnPlacements.register(FarlandersEntityTypes.END_SPIRIT, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Mob::checkMobSpawnRules);
		SpawnPlacements.register(FarlandersEntityTypes.NIGHTFALL_SPIRIT, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Mob::checkMobSpawnRules);
		SpawnPlacements.register(FarlandersEntityTypes.ENDER_COLOSSUS, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Monster::checkMonsterSpawnRules);*/
	}

	private static void mobPlacement(SpawnPlacementRegisterEvent event, EntityType<? extends Mob> pType, boolean infrequent)
	{
		event.register(pType, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, infrequent ? FLEntityTypes::infrequentMobSpawnRules : Mob::checkMobSpawnRules, Operation.REPLACE);
	}

	private static void monsterPlacement(SpawnPlacementRegisterEvent event, EntityType<? extends Monster> pType)
	{
		event.register(pType, SpawnPlacements.Type.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Monster::checkMonsterSpawnRules, Operation.REPLACE);
	}

	public static boolean infrequentMobSpawnRules(EntityType<? extends Mob> type, LevelAccessor level, MobSpawnType reason, BlockPos pos, RandomSource rand)
	{
		if (reason.equals(MobSpawnType.CHUNK_GENERATION))
		{
			if (level.getBiome(pos).is(FLTags.Biomes.DECREASED_NATURAL_SPAWNS))
			{
				if (rand.nextFloat() < 0.95F)
				{
					// System.out.println("cancelled 95 in decreased " + type);
					return false;
				}
			}
			else
			{
				if (rand.nextFloat() < 0.8F)
				{
					// System.out.println("cancelled 50/50 " + type);
					return false;
				}
			}
		}

		// System.out.println("allowed " + type);

		return Mob.checkMobSpawnRules(type, level, reason, pos, rand);
	}
}
