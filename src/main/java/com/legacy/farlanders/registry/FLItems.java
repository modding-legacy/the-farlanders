package com.legacy.farlanders.registry;

import java.util.List;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.item.NightfallSwordItem;
import com.legacy.farlanders.item.armor.FarlandersArmorMaterial;
import com.legacy.farlanders.item.armor.LooterHoodItem;
import com.legacy.farlanders.item.armor.NightfallArmorItem;
import com.legacy.farlanders.item.armor.NightfallHelmetItem;
import com.legacy.farlanders.item.armor.RebelHelmetItem;
import com.legacy.farlanders.item.wand.InvisibilityWandItem;
import com.legacy.farlanders.item.wand.LargeFireballWandItem;
import com.legacy.farlanders.item.wand.OreWandItem;
import com.legacy.farlanders.item.wand.RegenWandItem;
import com.legacy.farlanders.item.wand.SmallFireballWandItem;
import com.legacy.farlanders.item.wand.TeleportationWandItem;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Tiers;
import net.neoforged.neoforge.common.DeferredSpawnEggItem;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class FLItems
{
	private static RegisterEvent registry;

	public static Item endumium_crystal, ender_horn, titan_hide, nightfall_shard;

	public static Item nightfall_sword, nightfall_helmet, nightfall_chestplate, nightfall_leggings, nightfall_boots,
			rebel_farlander_helmet, looter_hood;

	public static Item nightfall_staff, mystic_wand_fire_small, mystic_wand_fire_large, mystic_wand_ore,
			mystic_wand_teleport, mystic_wand_regen, mystic_wand_invisible;

	public static Item farlander_spawn_egg, elder_spawn_egg, wanderer_spawn_egg, enderminion_spawn_egg,
			mystic_enderminion_spawn_egg, ender_guardian_spawn_egg;

	public static Item looter_spawn_egg, rebel_spawn_egg, mystic_enderman_spawn_egg, classic_enderman_spawn_egg,
			fanmade_enderman_spawn_egg, ender_golem_spawn_egg, titan_spawn_egg;

	/*public static Item end_spirit_spawn_egg, nightfall_spirit_spawn_egg, mini_ender_dragon_spawn_egg,
			ender_colossus_spawn_egg;*/

	public static void init(RegisterEvent event)
	{
		registry = event;
		registerBlockItems();

		farlander_spawn_egg = register("farlander_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.FARLANDER, 0xffffff, 0xffffff, new Item.Properties()));
		elder_spawn_egg = register("elder_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.ELDER_FARLANDER, 0xffffff, 0xffffff, new Item.Properties()));
		wanderer_spawn_egg = register("wanderer_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.WANDERER, 0xffffff, 0xffffff, new Item.Properties()));
		ender_guardian_spawn_egg = register("ender_guardian_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.ENDER_GUARDIAN, 0xffffff, 0xffffff, new Item.Properties()));
		looter_spawn_egg = register("looter_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.LOOTER, 0xffffff, 0xffffff, new Item.Properties()));
		rebel_spawn_egg = register("rebel_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.REBEL, 0xffffff, 0xffffff, new Item.Properties()));
		enderminion_spawn_egg = register("enderminion_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.ENDERMINION, 0xffffff, 0xffffff, new Item.Properties()));
		mystic_enderminion_spawn_egg = register("mystic_enderminion_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.MYSTIC_ENDERMINION, 0xffffff, 0xffffff, new Item.Properties()));
		mystic_enderman_spawn_egg = register("mystic_enderman_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.MYSTIC_ENDERMAN, 0xffffff, 0xffffff, new Item.Properties()));
		classic_enderman_spawn_egg = register("classic_enderman_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.CLASSIC_ENDERMAN, 0xffffff, 0xffffff, new Item.Properties()));
		fanmade_enderman_spawn_egg = register("fanmade_enderman_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.FANMADE_ENDERMAN, 0xffffff, 0xffffff, new Item.Properties()));
		ender_golem_spawn_egg = register("ender_golem_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.ENDER_GOLEM, 0xffffff, 0xffffff, new Item.Properties()));
		titan_spawn_egg = register("titan_spawn_egg", new DeferredSpawnEggItem(() -> FLEntityTypes.TITAN, 0xffffff, 0xffffff, new Item.Properties()));

		/*end_spirit_spawn_egg = register("end_spirit_spawn_egg", new ForgeSpawnEggItem(() -> FarlandersEntityTypes.END_SPIRIT, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		nightfall_spirit_spawn_egg = register("nightfall_spirit_spawn_egg", new ForgeSpawnEggItem(() -> FarlandersEntityTypes.NIGHTFALL_SPIRIT, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		mini_ender_dragon_spawn_egg = register("mini_ender_dragon_spawn_egg", new ForgeSpawnEggItem(() -> FarlandersEntityTypes.MINI_ENDER_DRAGON, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));
		ender_colossus_spawn_egg = register("ender_colossus_spawn_egg", new ForgeSpawnEggItem(() -> FarlandersEntityTypes.ENDER_COLOSSUS, 0xffffff, 0xffffff, new Item.Properties().group(FarlandersItemGroup.FARLANDER_TAB)));*/

		endumium_crystal = register("endumium_crystal", new Item(new Item.Properties()));
		ender_horn = register("ender_horn", new Item(new Item.Properties()));
		titan_hide = register("titan_hide", new Item(new Item.Properties()));
		/*nightfall_shard = register("nightfall_shard", new Item(new Item.Properties().tab(null)));*/
		nightfall_sword = register("nightfall_sword", new NightfallSwordItem(Tiers.IRON, 3, -2.4F, new Item.Properties().durability((int) (1561F * 1.2F))));

		nightfall_helmet = register("nightfall_helmet", new NightfallHelmetItem(FarlandersArmorMaterial.NIGHTFALL, ArmorItem.Type.HELMET, Lazy.of(() -> MobEffects.NIGHT_VISION), new Item.Properties()));
		nightfall_chestplate = register("nightfall_chestplate", new NightfallArmorItem(FarlandersArmorMaterial.NIGHTFALL, ArmorItem.Type.CHESTPLATE, Lazy.of(() -> MobEffects.DIG_SPEED), new Item.Properties()));
		nightfall_leggings = register("nightfall_leggings", new NightfallArmorItem(FarlandersArmorMaterial.NIGHTFALL, ArmorItem.Type.LEGGINGS, Lazy.of(() -> MobEffects.JUMP), new Item.Properties()));
		nightfall_boots = register("nightfall_boots", new NightfallArmorItem(FarlandersArmorMaterial.NIGHTFALL, ArmorItem.Type.BOOTS, Lazy.of(() -> MobEffects.MOVEMENT_SPEED), new Item.Properties()));
		rebel_farlander_helmet = register("rebel_farlander_helmet", new RebelHelmetItem(FarlandersArmorMaterial.REBEL, ArmorItem.Type.HELMET, new Item.Properties()));
		looter_hood = register("looter_hood", new LooterHoodItem(FarlandersArmorMaterial.LOOTER, ArmorItem.Type.HELMET, new Item.Properties()));

		mystic_wand_fire_small = register("mystic_wand_fire_small", new SmallFireballWandItem(new Item.Properties().durability(30)));
		mystic_wand_fire_large = register("mystic_wand_fire_large", new LargeFireballWandItem(new Item.Properties().durability(20)));
		mystic_wand_ore = register("mystic_wand_ore", new OreWandItem(new Item.Properties().durability(3)));
		mystic_wand_teleport = register("mystic_wand_teleport", new TeleportationWandItem(new Item.Properties().durability(5)));
		mystic_wand_regen = register("mystic_wand_regen", new RegenWandItem(new Item.Properties().durability(10)));
		mystic_wand_invisible = register("mystic_wand_invisible", new InvisibilityWandItem(new Item.Properties().durability(10)));
		// nightfall_staff = register("nightfall_staff", new ItemNightfallStaff(new
		// Item.Properties().maxStackSize(1).maxDamage(5).group(FarlandersItemGroup.FARLANDER_TAB)));
	}

	private static void registerBlockItems()
	{
		FLBlocks.blockItems.stream().forEach((block) -> register(BuiltInRegistries.BLOCK.getKey(block).getPath(), new BlockItem(block, new Item.Properties())));
		FLBlocks.blockItems = List.of();
	}

	private static <T extends Item> T register(String name, T item)
	{
		registry.register(Registries.ITEM, TheFarlandersMod.locate(name), () -> item);
		return item;
	}
}