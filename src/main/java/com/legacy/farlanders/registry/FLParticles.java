package com.legacy.farlanders.registry;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.particle.FarlandDustData;
import com.legacy.farlanders.client.particle.FarlandDustParticle;
import com.mojang.serialization.Codec;

import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.Registries;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

public class FLParticles
{
	public static final ParticleType<FarlandDustData> FARLAND_DUST = new ParticleType<FarlandDustData>(true, FarlandDustData.DESERIALIZER)
	{
		@Override
		public Codec<FarlandDustData> codec()
		{
			return FarlandDustData.CODEC;
		}
	};

	public static void init(RegisterEvent event)
	{
		register(event, "farland_dust", FARLAND_DUST);
	}

	private static void register(RegisterEvent event, String key, ParticleType<?> particle)
	{
		event.register(Registries.PARTICLE_TYPE, TheFarlandersMod.locate(key), () -> particle);
	}

	public static class Register
	{
		@SubscribeEvent
		public static void registerParticleFactories(net.neoforged.neoforge.client.event.RegisterParticleProvidersEvent event)
		{
			event.registerSpriteSet(FARLAND_DUST, FarlandDustParticle.Provider::new);
		}
	}
}
