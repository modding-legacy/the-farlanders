package com.legacy.farlanders.registry;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.world.structure.FarlanderVillagePools;
import com.legacy.farlanders.world.structure.FarlanderVillageStructure;
import com.legacy.farlanders.world.structure.SmallHousePools;
import com.legacy.farlanders.world.structure.SmallHouseStructure;
import com.legacy.farlanders.world.structure.TitanSpirePools;
import com.legacy.farlanders.world.structure.TitanSpireStructure;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;
import com.legacy.structure_gel.api.structure.ExtendedJigsawStructure;
import com.legacy.structure_gel.api.structure.GridStructurePlacement;
import com.legacy.structure_gel.api.structure.GridStructurePlacement.TaggedExclusionZone;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstapContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.structure.StructureSpawnOverride;
import net.minecraft.world.level.levelgen.structure.TerrainAdjustment;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

public class FLStructures
{
	public static final RegistrarHandler<StructureTemplatePool> HANDLER = RegistrarHandler.getOrCreate(Registries.TEMPLATE_POOL, TheFarlandersMod.MODID).bootstrap(FLStructures::bootstrap);

	// @formatter:off
	public static final StructureRegistrar<ExtendedJigsawStructure> SMALL_HOUSE = StructureRegistrar.jigsawBuilder(TheFarlandersMod.locate("small_house"))
			.addPiece(() -> SmallHouseStructure.Piece::new)
			.pushStructure((c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(SmallHousePools.ROOT)).heightmap(Heightmap.Types.WORLD_SURFACE_WG).capability(SmallHouseStructure.Capability.INSTANCE).build())
				.terrainAdjustment(TerrainAdjustment.BEARD_THIN).noSpawns(StructureSpawnOverride.BoundingBoxType.PIECE).biomes(FLTags.Biomes.HAS_HOUSES).dimensions(Level.OVERWORLD)
			.popStructure()
			.placement((c) -> GridStructurePlacement.builder(24, 10, 0.5F).exclusionZone(new TaggedExclusionZone(c.lookup(Registries.STRUCTURE).getOrThrow(FLTags.Structures.FARLANDER_HOUSE_BAD_NEIGHBORS), 10)).build(FLStructures.SMALL_HOUSE.getRegistryName()))
			.build();
	
	public static final StructureRegistrar<ExtendedJigsawStructure> FARLANDER_VILLAGE = StructureRegistrar.jigsawBuilder(TheFarlandersMod.locate("farlander_village"))
			.addPiece(() -> FarlanderVillageStructure.Piece::new)
			.pushStructure((c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(FarlanderVillagePools.ROOT)).heightmap(Heightmap.Types.WORLD_SURFACE_WG).capability(FarlanderVillageStructure.Capability.INSTANCE).build())
				.terrainAdjustment(TerrainAdjustment.BEARD_THIN).noSpawns(StructureSpawnOverride.BoundingBoxType.PIECE).biomes(FLTags.Biomes.HAS_VILLAGES).dimensions(Level.OVERWORLD)
			.popStructure()
			.placement((c) -> GridStructurePlacement.builder(32, 14,  0.5F).exclusionZone(new TaggedExclusionZone(c.lookup(Registries.STRUCTURE).getOrThrow(FLTags.Structures.FARLANDER_VILLAGE_BAD_NEIGHBORS), 10)).build(FLStructures.FARLANDER_VILLAGE.getRegistryName()))
			.build();
	
	public static final StructureRegistrar<ExtendedJigsawStructure> TITAN_SPIRE = StructureRegistrar.jigsawBuilder(TheFarlandersMod.locate("titan_spire"))
			.addPiece(() -> TitanSpireStructure.Piece::new)
			.pushStructure((c, s) -> ExtendedJigsawStructure.builder(s, c.lookup(Registries.TEMPLATE_POOL).getOrThrow(TitanSpirePools.ROOT)).heightmap(Heightmap.Types.WORLD_SURFACE_WG).capability(TitanSpireStructure.Capability.INSTANCE).build())
				.terrainAdjustment(TerrainAdjustment.BEARD_THIN).noSpawns(StructureSpawnOverride.BoundingBoxType.PIECE).biomes(FLTags.Biomes.HAS_TITAN_SPIRES).dimensions(Level.OVERWORLD)
			.popStructure()
			.placement((c) -> GridStructurePlacement.builder(30, 11, 0.2F).exclusionZone(new TaggedExclusionZone(c.lookup(Registries.STRUCTURE).getOrThrow(FLTags.Structures.TITAN_SPIRE_BAD_NEIGHBORS), 10)).build(FLStructures.TITAN_SPIRE.getRegistryName()))
			.build();
	// @formatter:on

	public static void bootstrap(BootstapContext<StructureTemplatePool> bootstrap)
	{
		SmallHousePools.bootstrap(bootstrap);
		FarlanderVillagePools.bootstrap(bootstrap);
		TitanSpirePools.bootstrap(bootstrap);
	}
}
