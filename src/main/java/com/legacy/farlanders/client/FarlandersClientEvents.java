package com.legacy.farlanders.client;

import com.legacy.farlanders.FarlandersConfig;
import com.legacy.farlanders.event.FarlandersEvents;

import net.minecraft.client.CameraType;
import net.minecraft.client.Minecraft;
import net.minecraft.core.particles.ParticleTypes;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.entity.living.LivingEvent.LivingTickEvent;

public class FarlandersClientEvents
{

	@SubscribeEvent
	public static void onLivingTick(LivingTickEvent event)
	{
		var entity = event.getEntity();

		if (entity.level().isClientSide() && FarlandersEvents.wearingFullNightfall(entity))
		{
			var mc = Minecraft.getInstance();
			if (!FarlandersConfig.CLIENT.firstPersonNightfallParticles.get() && entity == mc.player && mc.options.getCameraType() == CameraType.FIRST_PERSON)
				return;

			for (int i = 0; i < 2; ++i)
				entity.level().addParticle(ParticleTypes.PORTAL, entity.getRandomX(0.5D), entity.getRandomY() - 0.25D, entity.getRandomZ(0.5D), (entity.getRandom().nextDouble() - 0.5D) * 2.0D, -entity.getRandom().nextDouble(), (entity.getRandom().nextDouble() - 0.5D) * 2.0D);
		}
	}

	/*public static <T extends LivingEntity, M extends HumanoidModel<T>, A extends HumanoidModel<T>> void testing(HumanoidArmorLayer humanoidArmorLayer, PoseStack pPoseStack, MultiBufferSource pBuffer, T pLivingEntity, EquipmentSlot pSlot, int p_117123_, A pModel, ItemStack itemstack, ArmorItem armoritem, Model model)
	{
		if (armoritem == FLItems.nightfall_helmet)
		{
			VertexConsumer vertexconsumer = pBuffer.getBuffer(RenderType.eyes(humanoidArmorLayer.getArmorResource(pLivingEntity, itemstack, pSlot, "overlay")));
			pPoseStack.pushPose();
			//pPoseStack.translate(0, 0, -0.005F);
			pModel.renderToBuffer(pPoseStack, vertexconsumer, 15728640, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
			if (model instanceof NightfallHelmetModel m)
				m.head.xScale = m.head.yScale = m.head.zScale = 1.0F;
			pPoseStack.popPose();
			//VertexConsumer vertexconsumer = ItemRenderer.getArmorFoilBuffer(pBuffer, RenderType.eyes(humanoidArmorLayer.getArmorResource(pLivingEntity, itemstack, pSlot, "overlay")), false, itemstack.hasFoil());
	
			//model.renderToBuffer(pPoseStack, vertexconsumer, p_117123_, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
		}
	
	}*/
}