package com.legacy.farlanders.client;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.ClassicEndermanRenderer;
import com.legacy.farlanders.client.render.entity.ElderFarlanderRenderer;
import com.legacy.farlanders.client.render.entity.EnderGolemRenderer;
import com.legacy.farlanders.client.render.entity.EnderGuardianRenderer;
import com.legacy.farlanders.client.render.entity.EnderminionRenderer;
import com.legacy.farlanders.client.render.entity.FanmadeEndermanRenderer;
import com.legacy.farlanders.client.render.entity.FarlanderRenderer;
import com.legacy.farlanders.client.render.entity.LooterRenderer;
import com.legacy.farlanders.client.render.entity.MysticEndermanRenderer;
import com.legacy.farlanders.client.render.entity.RebelRenderer;
import com.legacy.farlanders.client.render.entity.TitanRenderer;
import com.legacy.farlanders.client.render.entity.WandererRenderer;
import com.legacy.farlanders.client.render.model.ElderModel;
import com.legacy.farlanders.client.render.model.EnderGolemModel;
import com.legacy.farlanders.client.render.model.EnderGuardianModel;
import com.legacy.farlanders.client.render.model.EnderminionModel;
import com.legacy.farlanders.client.render.model.FarlanderModel;
import com.legacy.farlanders.client.render.model.MysticEndermanModel;
import com.legacy.farlanders.client.render.model.TitanModel;
import com.legacy.farlanders.client.render.model.WandererModel;
import com.legacy.farlanders.client.render.model.armor.LooterHoodModel;
import com.legacy.farlanders.client.render.model.armor.NightfallHelmetModel;
import com.legacy.farlanders.client.render.model.armor.RebelHelmetModel;
import com.legacy.farlanders.registry.FLEntityTypes;

import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;

@Mod.EventBusSubscriber(modid = TheFarlandersMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class FLEntityRendering
{
	@SubscribeEvent
	public static void initLayers(final EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(FLRenderRefs.FARLANDER, () -> FarlanderModel.createBodyLayer());
		event.registerLayerDefinition(FLRenderRefs.ELDER_FARLANDER, () -> ElderModel.createBodyLayer());
		event.registerLayerDefinition(FLRenderRefs.ENDER_GUARDIAN, () -> EnderGuardianModel.createBodyLayer());
		event.registerLayerDefinition(FLRenderRefs.WANDERER, () -> WandererModel.createBodyLayer());
		event.registerLayerDefinition(FLRenderRefs.ENDERMINION, () -> EnderminionModel.createBodyLayer());
		event.registerLayerDefinition(FLRenderRefs.MYSTIC_ENDERMAN, () -> MysticEndermanModel.createBodyLayer());
		event.registerLayerDefinition(FLRenderRefs.ENDER_GOLEM, () -> EnderGolemModel.createBodyLayer());
		event.registerLayerDefinition(FLRenderRefs.TITAN, () -> TitanModel.createBodyLayer());

		event.registerLayerDefinition(FLRenderRefs.NIGHTFALL_HELMET, () -> NightfallHelmetModel.createHelmetLayer());
		event.registerLayerDefinition(FLRenderRefs.REBEL_HELMET, () -> RebelHelmetModel.createHelmetLayer());
		event.registerLayerDefinition(FLRenderRefs.LOOTER_HOOD, () -> LooterHoodModel.createHelmetLayer());
	}

	@SubscribeEvent
	public static void initRenders(final EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(FLEntityTypes.FARLANDER, FarlanderRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.ELDER_FARLANDER, ElderFarlanderRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.WANDERER, WandererRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.ENDER_GUARDIAN, EnderGuardianRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.LOOTER, LooterRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.REBEL, RebelRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.ENDERMINION, EnderminionRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.MYSTIC_ENDERMINION, EnderminionRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.MYSTIC_ENDERMAN, MysticEndermanRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.CLASSIC_ENDERMAN, ClassicEndermanRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.FANMADE_ENDERMAN, FanmadeEndermanRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.ENDER_GOLEM, EnderGolemRenderer::new);
		event.registerEntityRenderer(FLEntityTypes.TITAN, TitanRenderer::new);

	}

	/*@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	@SubscribeEvent
	public static void initRenderLayers(EntityRenderersEvent.AddLayers event)
	{
		for (EntityRenderer<?> renderer : Minecraft.getInstance().getEntityRenderDispatcher().renderers.values())
		{
			if (renderer instanceof LivingEntityRenderer<?, ?> living)
			{
				boolean hasArmor = false;
				for (var layer : living.layers)
				{
					if (layer instanceof HumanoidArmorLayer)
					{
						hasArmor = true;
					}
				}
	
				if (hasArmor)
				{
					System.out.println("adding to " + living.getClass());
					living.addLayer(new ArmorGlowLayer(living, event.getEntityModels()));
				}
				else
					System.out.println("not " + living.getClass());
	
			}
		}
	}*/
}