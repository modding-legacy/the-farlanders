package com.legacy.farlanders.client.render.model;

import com.legacy.farlanders.entity.hostile.MysticEndermanEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.item.ItemStack;

public class MysticEndermanModel<T extends MysticEndermanEntity> extends HierarchicalModel<T> implements ArmedModel
{
	private final ModelPart root;

	protected final ModelPart head;
	protected final ModelPart body;
	protected final ModelPart rightArm;
	protected final ModelPart leftArm;
	protected final ModelPart leftLeg;
	protected final ModelPart leftLegBottom;
	protected final ModelPart rightLeg;
	protected final ModelPart rightLegBottom;
	protected final ModelPart cape;

	public MysticEndermanModel(ModelPart root)
	{
		this.root = root;

		this.head = root.getChild("head");
		this.body = root.getChild("body");
		this.rightArm = root.getChild("right_arm");
		this.leftArm = root.getChild("left_arm");
		this.leftLeg = root.getChild("left_leg");
		this.leftLegBottom = root.getChild("left_leg_bottom");
		this.rightLeg = root.getChild("right_leg");
		this.rightLegBottom = root.getChild("right_leg_bottom");
		this.cape = root.getChild("cape");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition mesh = new MeshDefinition();
		PartDefinition root = mesh.getRoot();

		root.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 16).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -13.8667F, -0.4667F));

		root.addOrReplaceChild("body", CubeListBuilder.create().texOffs(88, 16).addBox(0.0F, 0.0F, 0.0F, 8.0F, 12.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-4.0F, -14.0F, -2.0F));

		root.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(56, 0).addBox(0.0F, 0.0F, 0.0F, 2.0F, 17.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-6.0F, -14.0F, -1.0F));
		root.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(56, 0).addBox(0.0F, 0.0F, 0.0F, 2.0F, 17.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(4.0F, -14.0F, -1.0F));

		root.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(33, 0).addBox(0.0F, 0.0F, 0.0F, 2.0F, 13.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.0F, -3.0F, -1.0F, -0.2231F, 0.0F, 0.0F));
		root.addOrReplaceChild("left_leg_bottom", CubeListBuilder.create().texOffs(44, 0).addBox(0.0F, 11.6667F, -5.0F, 2.0F, 12.0F, 2.0F, new CubeDeformation(0.001F)), PartPose.offsetAndRotation(-3.0F, -3.0F, -1.0F, 0.1859F, 0.0F, 0.0F));

		root.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(33, 0).addBox(0.0F, 0.0F, 0.0F, 2.0F, 13.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, -3.0F, -1.0F, -0.8923F, 0.0F, 0.0F));
		root.addOrReplaceChild("right_leg_bottom", CubeListBuilder.create().texOffs(44, 0).addBox(0.0F, -4.2667F, -12.4F, 2.0F, 12.0F, 2.0F, new CubeDeformation(0.001F)), PartPose.offsetAndRotation(1.0F, -3.0F, -1.0F, 1.1525F, 0.0F, 0.0F));

		root.addOrReplaceChild("cape", CubeListBuilder.create().texOffs(67, 0).addBox(0.0F, 0.0F, 0.0F, 10.0F, 29.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(-5.0F, -14.0F, 2.2F));

		return LayerDefinition.create(mesh, 128, 32);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.head.resetPose();
		this.rightArm.resetPose();
		this.leftArm.resetPose();

		head.yRot = netHeadYaw / 57.29578F;
		head.xRot = headPitch / 57.29578F;
		cape.xRot = limbSwingAmount;
		cape.zRot = 0.0F;
		float var7 = Mth.sin(1.03F * Mth.PI);
		float var8 = Mth.sin(0.75F * Mth.PI);

		this.leftLeg.zRot = 0.01F;
		this.leftLeg.yRot = -(0.1F - var7 * 0.6F);
		this.leftLeg.xRot = -((Mth.PI - 3.55F) / 2F);
		this.leftLeg.xRot -= var7 * 1.2F - var8 * (-0.43F);
		this.leftLeg.zRot += Mth.cos(ageInTicks * 0.09F) * 0.015F;
		this.leftLeg.xRot += Mth.sin(ageInTicks * 0.067F) * 0.05F;
		this.leftLegBottom.zRot = 0.01F;
		this.leftLegBottom.yRot = -(0.1F - var7 * 0.6F);
		this.leftLegBottom.xRot = -((Mth.PI - 3.55F) / 2F);
		this.leftLegBottom.xRot -= var7 * 1.2F - var8 * (0.14F);
		this.leftLegBottom.zRot += Mth.cos(ageInTicks * 0.09F) * 0.015F;
		this.leftLegBottom.xRot += Mth.sin(ageInTicks * 0.067F) * 0.05F;

		this.rightLeg.zRot = 0.01F;
		this.rightLeg.yRot = -(0.1F - var7 * 0.6F);
		this.rightLeg.xRot = -((Mth.PI - 3.55F) / 2F);
		this.rightLeg.xRot -= var7 * 1.2F - var8 * (-1.33F);
		this.rightLeg.zRot += Mth.cos(ageInTicks * 0.09F) * 0.005F;
		this.rightLeg.xRot += Mth.sin(ageInTicks * 0.067F) * 0.04F;
		this.rightLegBottom.zRot = 0.01F;
		this.rightLegBottom.yRot = -(0.1F - var7 * 0.6F);
		this.rightLegBottom.xRot = -((Mth.PI - 3.55F) / 2F);
		this.rightLegBottom.xRot -= var7 * 1.2F - var8 * (1.55F);
		this.rightLegBottom.zRot += Mth.cos(ageInTicks * 0.09F) * 0.005F;
		this.rightLegBottom.xRot += Mth.sin(ageInTicks * 0.067F) * 0.04F;

		float legMov = limbSwingAmount * 1.3F;
		leftLeg.xRot += legMov;
		leftLegBottom.xRot += legMov;
		rightLeg.xRot += legMov;
		rightLegBottom.xRot += legMov;

		AnimationUtils.bobArms(rightArm, leftArm, ageInTicks);

		boolean rightHanded = entity.getMainArm() == HumanoidArm.RIGHT;
		ItemStack leftItem = rightHanded ? entity.getOffhandItem() : entity.getMainHandItem();
		ItemStack rightItem = rightHanded ? entity.getMainHandItem() : entity.getOffhandItem();

		float zItemBob = (Mth.cos(limbSwing * 0.3F) * 0.1F) * limbSwingAmount;
		if (!rightItem.isEmpty())
		{
			this.rightArm.zRot += (0.3F * limbSwingAmount) + zItemBob;
			this.rightArm.xRot += -0.25F/* + (Mth.cos(limbSwing * 0.2F + Mth.PI) * limbSwingAmount * 0.3F)*/;
		}
		else
			this.rightArm.xRot += (0.5F + Mth.sin(ageInTicks * 0.2F) * 0.1F) * limbSwingAmount;

		if (!leftItem.isEmpty())
		{
			this.leftArm.zRot += -(0.3F * limbSwingAmount) - zItemBob;
			this.leftArm.xRot += -0.25F/* + (Mth.cos(limbSwing * 0.2F) * limbSwingAmount * 0.3F)*/;
		}
		else
			this.leftArm.xRot += (0.5F + Mth.sin(ageInTicks * 0.2F) * 0.1F) * limbSwingAmount;

		if (entity.swingingArm == InteractionHand.MAIN_HAND && rightHanded || entity.swingingArm == InteractionHand.OFF_HAND && !rightHanded)
			setupAttackAnimation(entity, ageInTicks, this.rightArm);
		else
			setupAttackAnimation(entity, ageInTicks, this.leftArm);

		/*if (entity instanceof MysticEndermanEntity e && !e.getMainHandItem().isEmpty())
			this.rightArm.xRot += 0.05f + Mth.cos(2.567F) * 0.35f;*/
	}

	@Override
	public void translateToHand(HumanoidArm pSide, PoseStack pPoseStack)
	{
		boolean left = pSide == HumanoidArm.LEFT;
		ModelPart arm = left ? this.leftArm : this.rightArm;

		arm.translateAndRotate(pPoseStack);
		pPoseStack.translate(left ? 0F : 0.13F, 0.35F, 0.15F);
	}

	protected void setupAttackAnimation(T pLivingEntity, float pAgeInTicks, ModelPart modelpart)
	{
		if (!(this.attackTime <= 0.0F))
		{
			float f = this.attackTime;

			f = 1.0F - this.attackTime;
			f *= f;
			f *= f;
			f = 1.0F - f;
			float f1 = Mth.sin(f * (float) Math.PI);
			float f2 = Mth.sin(this.attackTime * (float) Math.PI) * -(this.head.xRot - 0.7F) * 0.75F;
			modelpart.xRot -= f1 * 1.2F + f2;
			modelpart.yRot += this.body.yRot * 2.0F;

			float z = Mth.sin(this.attackTime * (float) Math.PI) * -0.4F;

			if (modelpart == this.leftArm)
				z = -z;

			modelpart.zRot += z;
		}
	}
}