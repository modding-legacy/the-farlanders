package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.TexturedGlowingLayer;
import com.legacy.farlanders.client.render.model.WandererModel;
import com.legacy.farlanders.entity.WandererEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.CustomHeadLayer;
import net.minecraft.resources.ResourceLocation;

public class WandererRenderer<T extends WandererEntity> extends MobRenderer<T, WandererModel<T>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/wanderer.png");
	private static final ResourceLocation TEXTURE_EYES = TheFarlandersMod.locate("textures/entity/wanderer_eyes.png");

	public WandererRenderer(EntityRendererProvider.Context context)
	{
		super(context, new WandererModel<>(context.bakeLayer(FLRenderRefs.WANDERER)), 0.5F);
		this.addLayer(new TexturedGlowingLayer<>(this, TEXTURE_EYES));

		this.addLayer(new CustomHeadLayer<>(this, context.getModelSet(), context.getItemInHandRenderer()));
	}

	@Override
	protected void scale(T entity, PoseStack pose, float partialTicks)
	{
		float f1 = 0.9375F;
		pose.scale(f1, f1, f1);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}