package com.legacy.farlanders.client.render.model.armor;

import java.util.List;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.world.entity.LivingEntity;

public class NightfallHelmetModel<T extends LivingEntity> extends HumanoidModel<T>
{
	private final ModelPart helmet;

	public NightfallHelmetModel(ModelPart root)
	{
		super(root);
		this.helmet = root.getChild("helmet");
	}

	public static LayerDefinition createHelmetLayer()
	{
		MeshDefinition meshdefinition = HumanoidModel.createMesh(CubeDeformation.NONE, 0.0F);
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition helmet = partdefinition.addOrReplaceChild("helmet", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, new CubeDeformation(0.501F)), PartPose.offset(0.0F, -1.0F, 0.5F));

		PartDefinition left_lower_horn = helmet.addOrReplaceChild("left_lower_horn", CubeListBuilder.create().texOffs(24, 0).addBox(-4.0F, -1.0F, -1.0F, 4.0F, 2.0F, 2.0F), PartPose.offset(-4.5F, -5.25F, 0.0F));
		left_lower_horn.addOrReplaceChild("left_upper_horn", CubeListBuilder.create().texOffs(37, 0).mirror().addBox(-1.0F, -3.0F, -1.0F, 2.0F, 3.0F, 2.0F).mirror(false), PartPose.offset(-3.0F, -1.0F, 0.0F));

		PartDefinition right_lower_horn = helmet.addOrReplaceChild("right_lower_horn", CubeListBuilder.create().texOffs(24, 0).mirror().addBox(-0.5F, -1.0F, -1.0F, 4.0F, 2.0F, 2.0F).mirror(false), PartPose.offset(4.5F, -5.25F, 0.0F));
		right_lower_horn.addOrReplaceChild("right_upper_horn", CubeListBuilder.create().texOffs(37, 0).addBox(-1.5F, -3.0F, -1.0F, 2.0F, 3.0F, 2.0F), PartPose.offset(3.0F, -1.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 32);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		this.helmet.copyFrom(this.head);
		return List.of(this.helmet);
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return List.of();
	}
}