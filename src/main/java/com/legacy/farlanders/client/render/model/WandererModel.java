package com.legacy.farlanders.client.render.model;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.HeadedModel;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;

public class WandererModel<T extends Entity> extends HierarchicalModel<T> implements HeadedModel
{
	private final ModelPart root;

	protected final ModelPart head;
	protected final ModelPart mouth;
	protected final ModelPart body;
	protected final ModelPart rightArm;
	protected final ModelPart leftArmBottom;
	protected final ModelPart leftArm;
	protected final ModelPart rightLeg;
	protected final ModelPart leftLeg;
	protected final ModelPart neck;
	protected final ModelPart bag;
	protected final ModelPart stick;

	public WandererModel(ModelPart root)
	{
		this.root = root;

		this.head = root.getChild("head");
		this.mouth = root.getChild("mouth");
		this.body = root.getChild("body");
		this.rightArm = root.getChild("right_arm");
		this.leftArmBottom = root.getChild("left_arm_bottom");
		this.leftArm = root.getChild("left_arm");
		this.rightLeg = root.getChild("right_leg");
		this.leftLeg = root.getChild("left_leg");
		this.neck = root.getChild("neck");
		this.bag = root.getChild("bag");
		this.stick = root.getChild("stick");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();
		var fightFix = new CubeDeformation(0.001F);

		partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F), PartPose.offset(0.0F, 0.0F, 0.0F));
		partdefinition.addOrReplaceChild("mouth", CubeListBuilder.create().texOffs(0, 16).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F), PartPose.offset(0.0F, 0.0F, 0.0F));

		partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(32, 16).addBox(-4.0F, 0.0F, -2.0F, 8.0F, 6.0F, 4.0F), PartPose.offset(0.0F, 1.0F, 0.0F));

		partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(56, 0).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 19.0F, 2.0F), PartPose.offset(-5.0F, 3.0F, 0.0F));

		partdefinition.addOrReplaceChild("left_arm_bottom", CubeListBuilder.create().texOffs(16, 43).addBox(-1.0F, -1.6667F, 5.1333F, 2.0F, 8.0F, 2.0F), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -2.0076F, 0.0F, 0.0F));
		partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(16, 32).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 9.0F, 2.0F), PartPose.offsetAndRotation(5.0F, 3.0F, 0.0F, -0.2974F, 0.0F, 0.0F));

		partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(56, 21).mirror().addBox(-1.0F, 0.0F, -1.0F, 2.0F, 17.0F, 2.0F).mirror(false), PartPose.offset(-2.0F, 7.0F, 0.0F));
		partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(56, 21).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 17.0F, 2.0F), PartPose.offset(2.0F, 7.0F, 0.0F));

		partdefinition.addOrReplaceChild("neck", CubeListBuilder.create().texOffs(24, 5).addBox(0.0F, 0.0F, 0.0F, 2.0F, 1.0F, 2.0F), PartPose.offset(-1.0F, 0.0F, -1.0F));

		partdefinition.addOrReplaceChild("bag", CubeListBuilder.create().texOffs(39, 50).addBox(-2.0F, -3.0F, 9.0F, 5.0F, 9.0F, 5.0F), PartPose.offsetAndRotation(5.0F, 3.0F, 0.0F, 0.3718F, 0.0F, 0.0F));

		partdefinition.addOrReplaceChild("stick", CubeListBuilder.create().texOffs(60, 42).addBox(0.0F, -9.0F, 1.6F, 1.0F, 21.0F, 1.0F, fightFix), PartPose.offsetAndRotation(5.0F, 3.0F, 0.0F, 2.0634F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		/*limbSwing = ageInTicks * 0.7F;
		limbSwingAmount = 1.0F * Mth.clamp(Mth.sin(ageInTicks * 0.04F) * 2, 0, 1);*/

		this.leftArmBottom.setPos(5F, 3F, 9.992007E-15F);

		this.head.yRot = netHeadYaw / 57.29578F;
		this.head.xRot = headPitch / 57.29578F;
		this.mouth.yRot = netHeadYaw / 57.29578F;
		this.mouth.xRot = headPitch / 57.29578F;
		this.rightArm.xRot = limbSwingAmount;
		this.rightArm.zRot = 0.0F;
		this.rightLeg.xRot = Mth.cos(limbSwing * 0.6662F) * limbSwingAmount;
		this.leftLeg.xRot = Mth.cos(limbSwing * 0.6662F + 3.141593F) * limbSwingAmount;
		this.rightLeg.yRot = 0.0F;
		this.leftLeg.yRot = 0.0f;

		float var7 = Mth.sin(1.03F * Mth.PI);
		float var8 = Mth.sin(0.25F * Mth.PI);
		this.bag.zRot = -0.07F;
		this.bag.yRot = -(0.1F - var7 * 0.6F);
		this.bag.xRot = -((Mth.PI - 3.55F) / 2F);
		this.bag.xRot -= var7 * 1.2F - var8 * 0.43F;
		this.bag.zRot += Mth.cos(ageInTicks * 0.09F) * 0.035F;
		this.bag.xRot += Mth.sin(ageInTicks * 0.067F) * 0.15F;
		this.stick.zRot = -0.07F;
		this.stick.yRot = -(0.1F - var7 * 0.6F);
		this.stick.xRot = -((Mth.PI - 3.55F) / 2F);
		this.stick.xRot -= var7 * 1.2F - var8 * 2.78F;
		this.stick.zRot += Mth.cos(ageInTicks * 0.09F) * 0.035F;
		this.stick.xRot += Mth.sin(ageInTicks * 0.067F) * 0.15F;
		this.leftArm.zRot = -0.07F;
		this.leftArm.yRot = -(0.1F - var7 * 0.6F);
		this.leftArm.xRot = -((Mth.PI - 3.55F) / 2F);
		this.leftArm.xRot -= var7 * 1.2F - var8 * (-0.5F);
		this.leftArm.zRot += Mth.cos(ageInTicks * 0.09F) * 0.035F;
		this.leftArm.xRot += Mth.sin(ageInTicks * 0.067F) * 0.15F;
		this.leftArmBottom.zRot = -0.07F;
		this.leftArmBottom.yRot = -(0.1F - var7 * 0.6F);
		this.leftArmBottom.xRot = -((Mth.PI - 3.55F) / 2F);
		this.leftArmBottom.xRot -= var7 * 1.2F - var8 * 6.0F;
		this.leftArmBottom.zRot += Mth.cos(ageInTicks * 0.09F) * 0.035F;
		this.leftArmBottom.xRot += Mth.sin(ageInTicks * 0.067F) * 0.15F;

		AnimationUtils.bobModelPart(this.rightArm, ageInTicks, 1);
	}

	@Override
	public ModelPart getHead()
	{
		return this.mouth;
	}

}