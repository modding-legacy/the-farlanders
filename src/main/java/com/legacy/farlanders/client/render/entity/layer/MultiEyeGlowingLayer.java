package com.legacy.farlanders.client.render.entity.layer;

import java.util.Locale;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class MultiEyeGlowingLayer<T extends LivingEntity & IColoredEyes, M extends EntityModel<T>> extends RenderLayer<T, M>
{
	private final String texturePrefix;

	public MultiEyeGlowingLayer(RenderLayerParent<T, M> rendererIn, String texturePrefix)
	{
		super(rendererIn);
		this.texturePrefix = texturePrefix;
	}

	@Override
	public void render(PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn, T entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		VertexConsumer ivertexbuilder = bufferIn.getBuffer(this.getRenderType(entitylivingbaseIn));
		this.getParentModel().renderToBuffer(matrixStackIn, ivertexbuilder, 15728640, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
	}

	public RenderType getRenderType(T entity)
	{
		ResourceLocation glowTexture = TheFarlandersMod.locate("textures/entity/" + this.texturePrefix + this.getEyeTexture(entity) + "_eyes" + ".png");
		return RenderType.eyes(glowTexture);
	}

	public String getEyeTexture(IColoredEyes entity)
	{
		return entity.getEyeData().name().toLowerCase(Locale.ENGLISH);
	}
}