package com.legacy.farlanders.client.render.model;

import com.legacy.farlanders.entity.tameable.EnderminionEntity;
import com.legacy.farlanders.entity.tameable.MysticEnderminionEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.item.ItemStack;

public class EnderminionModel<T extends EnderminionEntity> extends HierarchicalModel<T> implements ArmedModel
{
	private final ModelPart root;

	protected final ModelPart body;
	protected final ModelPart head, mouth;
	protected final ModelPart rightArm;
	protected final ModelPart leftArm;
	protected final ModelPart leftLegTop;
	protected final ModelPart leftLegBottom;
	protected final ModelPart rightLegTop;
	protected final ModelPart rightLegBottom;
	protected final ModelPart capeUpper;
	protected final ModelPart capeLower;

	public EnderminionModel(ModelPart root)
	{
		this.root = root;

		this.body = root.getChild("body");
		this.head = root.getChild("head");
		this.mouth = this.head.getChild("mouth");
		this.rightArm = this.body.getChild("right_arm");
		this.leftArm = this.body.getChild("left_arm");
		this.leftLegTop = this.body.getChild("left_leg_top");
		this.leftLegBottom = this.leftLegTop.getChild("left_leg_bottom");
		this.rightLegTop = this.body.getChild("right_leg_top");
		this.rightLegBottom = this.rightLegTop.getChild("right_leg_bottom");
		this.capeUpper = this.body.getChild("cape_upper");
		this.capeLower = this.capeUpper.getChild("cape_lower");

	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition mesh = new MeshDefinition();
		PartDefinition root = mesh.getRoot();
		var fightFix = new CubeDeformation(0.001F);

		PartDefinition body = root.addOrReplaceChild("body", CubeListBuilder.create().texOffs(44, 20).addBox(0.0F, 0.0F, 0.0F, 6.0F, 8.0F, 4.0F, fightFix), PartPose.offset(-3.0F, -7.0F, -1.0F));

		PartDefinition right_leg_top = body.addOrReplaceChild("right_leg_top", CubeListBuilder.create().texOffs(38, 0).addBox(-2.0F, 0.0F, -1.0F, 2.0F, 10.0F, 2.0F, fightFix), PartPose.offset(6.0F, 7.0F, 1.0F));
		right_leg_top.addOrReplaceChild("right_leg_bottom", CubeListBuilder.create().texOffs(47, 0).addBox(-1.0F, 0.0F, 0.0F, 2.0F, 9.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 10.0F, 1.0F));

		PartDefinition left_leg_top = body.addOrReplaceChild("left_leg_top", CubeListBuilder.create().texOffs(38, 0).mirror().addBox(0.0F, 0.0F, -1.0F, 2.0F, 10.0F, 2.0F, fightFix).mirror(false), PartPose.offset(0.0F, 7.0F, 1.0F));
		left_leg_top.addOrReplaceChild("left_leg_bottom", CubeListBuilder.create().texOffs(47, 0).mirror().addBox(-1.0F, 0.0F, 0.0F, 2.0F, 9.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(1.0F, 10.0F, 1.0F));

		body.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(56, 0).mirror().addBox(-2.0F, 0.0F, -1.0F, 2.0F, 17.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(0.0F, 1.0F, 2.0F));
		body.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(56, 0).addBox(0.0F, 0.0F, -1.0F, 2.0F, 17.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(6.0F, 1.0F, 2.0F));

		PartDefinition cape_upper = body.addOrReplaceChild("cape_upper", CubeListBuilder.create().texOffs(0, 9).addBox(-4.0F, 0.0F, 0.025F, 8.0F, 8.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(3.0F, 0.0F, 4.1F));
		cape_upper.addOrReplaceChild("cape_lower", CubeListBuilder.create().texOffs(0, 17).addBox(-4.0F, 0.0F, 0.0F, 8.0F, 15.0F, 0.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 8.0F, 0.025F));

		PartDefinition head = root.addOrReplaceChild("head", CubeListBuilder.create().texOffs(17, 19).addBox(-3.0F, -7.0F, -6.0F, 6.0F, 6.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -7.75F, 0.0F));
		head.addOrReplaceChild("mouth", CubeListBuilder.create().texOffs(0, 0).addBox(-2.5F, -1.5F, -5.0F, 5.0F, 2.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -0.5F, 0.0F));

		return LayerDefinition.create(mesh, 64, 32);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		/*limbSwing = ageInTicks * 0.7F;
		limbSwingAmount = 1.0F * Mth.clamp(Mth.sin(ageInTicks * 0.04F) * 2, 0, 1);*/

		float angle = 0.6109F/* + (Mth.cos(ageInTicks * 0.1F) * 0.05F)*/;
		this.head.setRotation(headPitch * Mth.DEG_TO_RAD, netHeadYaw * Mth.DEG_TO_RAD, 0);
		this.mouth.resetPose();
		this.body.setRotation(angle, 0, 0);
		this.rightArm.setRotation(-angle, 0, 0.08F);
		this.leftArm.setRotation(-angle, 0, -0.08F);
		this.rightLegTop.setRotation(-angle, 0, 0);
		this.leftLegTop.setRotation(-angle, 0, 0);
		this.rightLegBottom.setRotation(0, 0, 0);
		this.leftLegBottom.setRotation(0, 0, 0);
		this.capeUpper.setRotation(limbSwingAmount * 0.8F, 0, 0);
		this.capeLower.setRotation(-angle * Mth.clamp(1.0F - (limbSwingAmount * 2.4F), 0.0F, 1.0F), 0, 0);

		this.rightLegBottom.z = this.leftLegBottom.z = 1;

		// this.attackTime = entity.tickCount;
		this.capeUpper.visible = entity instanceof MysticEnderminionEntity;

		this.mouth.xRot += 0.025F + Mth.sin(Mth.PI + Mth.HALF_PI - ageInTicks * 0.1F) * 0.025F;

		if (entity.isTame())
			this.mouth.xRot += (((entity.getMaxHealth() - entity.getHealth()) * 0.01F) * Mth.PI);
		else
			this.mouth.xRot += 0.2F;

		int legPose = entity.getUUID().hashCode() & 1;
		float upwardMov = Mth.sin(ageInTicks * 0.1F);
		float upwardMovOffset = Mth.cos(ageInTicks * 0.1F);

		AnimationUtils.bobModelPart(this.rightLegTop, ageInTicks * 0.5F, -1);
		AnimationUtils.bobModelPart(this.leftLegTop, ageInTicks * 0.5F, 1);

		boolean rightHanded = entity.getMainArm() == HumanoidArm.RIGHT;

		ModelPart rTop = rightHanded ? this.leftLegTop : this.rightLegTop;
		ModelPart rBottom = rightHanded ? this.leftLegBottom : this.rightLegBottom;

		ModelPart lTop = rightHanded ? this.rightLegTop : this.leftLegTop;
		ModelPart lBottom = rightHanded ? this.rightLegBottom : this.leftLegBottom;

		if (legPose == 1)
		{
			lBottom.z -= 2;

			lTop.xRot -= -0.0F + -upwardMovOffset * 0.15F;
			lBottom.xRot += (0.3F - (1.0F * limbSwingAmount)) + upwardMov * 0.15F;

			rBottom.z -= 2;

			rTop.xRot -= 0.3F + upwardMovOffset * 0.05F;
			rBottom.xRot += (0.3F - (0.7F * limbSwingAmount)) + upwardMov * 0.15F;
		}
		else
		{
			lTop.xRot -= 1.0F + upwardMovOffset * 0.15F;
			lBottom.xRot += (2F - (2F * limbSwingAmount)) + upwardMov * 0.15F;

			rBottom.z -= 2;

			rTop.xRot -= 0.3F + upwardMovOffset * 0.05F;
			rBottom.xRot += (0.5F - (1.5F * limbSwingAmount)) + upwardMov * 0.1F;
		}

		ItemStack leftItem = rightHanded ? entity.getOffhandItem() : entity.getMainHandItem();
		ItemStack rightItem = rightHanded ? entity.getMainHandItem() : entity.getOffhandItem();

		float armX = limbSwingAmount * (rightItem.isEmpty() ? 0.4F : 0.1F);
		this.rightArm.xRot += armX;
		this.leftArm.xRot += armX;

		/*rightArm.setRotation(0, 0, 0);
		leftArm.setRotation(0, 0, 0);*/
		AnimationUtils.bobArms(this.rightArm, this.leftArm, Mth.PI + Mth.HALF_PI - ageInTicks);

		float zItemBob = (Mth.cos(limbSwing * 0.3F) * 0.1F) * limbSwingAmount;
		if (!rightItem.isEmpty())
		{
			this.rightArm.zRot += (0.3F * limbSwingAmount) + zItemBob;
			this.rightArm.xRot += -0.25F/* + (Mth.cos(limbSwing * 0.2F + Mth.PI) * limbSwingAmount * 0.3F)*/;
		}
		else
			this.rightArm.xRot += (0.5F + Mth.sin(ageInTicks * 0.2F) * 0.1F) * limbSwingAmount;

		if (!leftItem.isEmpty())
		{
			this.leftArm.zRot += -(0.3F * limbSwingAmount) - zItemBob;
			this.leftArm.xRot += -0.25F/* + (Mth.cos(limbSwing * 0.2F) * limbSwingAmount * 0.3F)*/;
		}
		else
			this.leftArm.xRot += (0.5F + Mth.sin(ageInTicks * 0.2F) * 0.1F) * limbSwingAmount;

		if (entity.swingingArm == InteractionHand.MAIN_HAND && rightHanded || entity.swingingArm == InteractionHand.OFF_HAND && !rightHanded)
			setupAttackAnimation(entity, ageInTicks, this.rightArm);
		else
			setupAttackAnimation(entity, ageInTicks, this.leftArm);

		/*float mul = 1.0F - limbSwingAmount;
		System.out.println(mul);
		leftLegTop.xRot *= mul;
		leftLegBottom.xRot *= mul;
		rightLegTop.xRot *= mul;
		rightLegBottom.xRot *= mul;*/

		float legMov = limbSwingAmount * 1.3F;
		leftLegTop.xRot += legMov;
		leftLegBottom.xRot += legMov;
		rightLegTop.xRot += legMov;
		rightLegBottom.xRot += legMov;

		float legOffset = -3 * Mth.clamp(limbSwingAmount * 1F, 0, 1);
		this.leftLegBottom.z += legOffset;
		this.rightLegBottom.z += legOffset;

		this.leftLegBottom.z = Mth.clamp(this.leftLegBottom.z, -1.0F, 1.0F);
		this.rightLegBottom.z = Mth.clamp(this.rightLegBottom.z, -1.0F, 1.0F);
	}

	@Override
	public void translateToHand(HumanoidArm pSide, PoseStack pPoseStack)
	{
		boolean left = pSide == HumanoidArm.LEFT;
		ModelPart arm = left ? this.leftArm : this.rightArm;

		this.body.translateAndRotate(pPoseStack);
		arm.translateAndRotate(pPoseStack);

		pPoseStack.translate(0, 0.35F, 0F);
	}

	protected void setupAttackAnimation(T pLivingEntity, float pAgeInTicks, ModelPart modelpart)
	{
		if (!(this.attackTime <= 0.0F))
		{
			float f = this.attackTime;

			f = 1.0F - this.attackTime;
			f *= f;
			f *= f;
			f = 1.0F - f;
			float f1 = Mth.sin(f * (float) Math.PI);
			float f2 = Mth.sin(this.attackTime * (float) Math.PI) * -(this.mouth.xRot - 0.7F) * 0.75F;
			modelpart.xRot -= f1 * 1.2F + f2;
			modelpart.yRot += this.body.yRot * 2.0F;

			float z = Mth.sin(this.attackTime * (float) Math.PI) * -0.4F;

			if (modelpart == this.leftArm)
				z = -z;

			modelpart.zRot += z;
		}
	}
}