package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.TexturedGlowingLayer;
import com.legacy.farlanders.client.render.model.ElderModel;
import com.legacy.farlanders.entity.ElderFarlanderEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

public class ElderFarlanderRenderer<T extends ElderFarlanderEntity> extends MobRenderer<T, ElderModel<T>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/farlander/farlander_elder.png");
	private static final ResourceLocation TEXTURE_EYES = TheFarlandersMod.locate("textures/entity/farlander/elder_eyes.png");

	public ElderFarlanderRenderer(EntityRendererProvider.Context context)
	{
		super(context, new ElderModel<>(context.bakeLayer(FLRenderRefs.ELDER_FARLANDER)), 0.5F);
		this.addLayer(new TexturedGlowingLayer<>(this, TEXTURE_EYES));
	}

	@Override
	protected void scale(T entity, PoseStack pose, float partialTicks)
	{
		float f1 = 0.9375F;
		pose.scale(f1, f1, f1);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}