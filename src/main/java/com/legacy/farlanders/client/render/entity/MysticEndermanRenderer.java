package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.MultiEyeGlowingLayer;
import com.legacy.farlanders.client.render.model.MysticEndermanModel;
import com.legacy.farlanders.entity.hostile.MysticEndermanEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;

public class MysticEndermanRenderer<T extends MysticEndermanEntity> extends MobRenderer<T, MysticEndermanModel<T>>
{
	private static final ResourceLocation TEXTURE_PURPLE = TheFarlandersMod.locate("textures/entity/mystic_enderman/mystic_purple.png");
	private static final ResourceLocation TEXTURE_GREEN = TheFarlandersMod.locate("textures/entity/mystic_enderman/mystic_green.png");

	public MysticEndermanRenderer(EntityRendererProvider.Context context)
	{
		super(context, new MysticEndermanModel<>(context.bakeLayer(FLRenderRefs.MYSTIC_ENDERMAN)), 0.5F);
		this.addLayer(new MultiEyeGlowingLayer<>(this, "mystic_enderman/mystic_"));

		this.addLayer(new ItemInHandLayer<T, MysticEndermanModel<T>>(this, context.getItemInHandRenderer()));
	}

	@Override
	protected void scale(T entity, PoseStack pose, float pPartialTickTime)
	{
		pose.translate(0, (float) Math.sin((entity.tickCount + pPartialTickTime) * 0.1F) * 0.05F, 0);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return entity.getEyeColor() == 1 ? TEXTURE_GREEN : TEXTURE_PURPLE;
	}
}