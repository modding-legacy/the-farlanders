package com.legacy.farlanders.client.render;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.model.armor.LooterHoodModel;
import com.legacy.farlanders.client.render.model.armor.NightfallHelmetModel;
import com.legacy.farlanders.registry.FLItems;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.ItemRenderer;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ArmorGlowLayer<T extends LivingEntity, M extends EntityModel<T>> extends RenderLayer<T, M>
{
	private static final ResourceLocation NIGHTFALL_EYES = TheFarlandersMod.locate("textures/models/armor/nightfall_layer_1_overlay.png");
	private static final ResourceLocation LOOTER_HOOD_EYES = TheFarlandersMod.locate("textures/models/armor/looter_layer_1_overlay.png");

	private final NightfallHelmetModel<T> nightfallModel;
	private final LooterHoodModel<T> hoodModel;

	public ArmorGlowLayer(RenderLayerParent<T, M> pRenderer, EntityModelSet p_174494_)
	{
		super(pRenderer);
		this.nightfallModel = new NightfallHelmetModel<>(p_174494_.bakeLayer(FLRenderRefs.NIGHTFALL_HELMET));
		this.hoodModel = new LooterHoodModel<>(p_174494_.bakeLayer(FLRenderRefs.LOOTER_HOOD));
	}

	@Override
	public void render(PoseStack pose, MultiBufferSource pBuffer, int pPackedLight, T pLivingEntity, float pLimbSwing, float pLimbSwingAmount, float pPartialTicks, float pAgeInTicks, float pNetHeadYaw, float pHeadPitch)
	{
		ItemStack itemstack = pLivingEntity.getItemBySlot(EquipmentSlot.HEAD);

		boolean looter = itemstack.is(FLItems.looter_hood);
		if (looter || itemstack.is(FLItems.nightfall_helmet))
		{
			ResourceLocation resourcelocation = looter ? LOOTER_HOOD_EYES : NIGHTFALL_EYES;

			pose.pushPose();
			var model = looter ? this.hoodModel : this.nightfallModel;

			//pose.translate(0.0F, 0.0F, -0.001F);
			this.getParentModel().copyPropertiesTo(model);
			model.setupAnim(pLivingEntity, pLimbSwing, pLimbSwingAmount, pAgeInTicks, pNetHeadYaw, pHeadPitch);

			//VertexConsumer vertexconsumer = pBuffer.getBuffer(RenderType.eyes(resourcelocation));
			//this.getParentModel().renderToBuffer(pose, vertexconsumer, 15728640, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);

			VertexConsumer vertexconsumer = ItemRenderer.getArmorFoilBuffer(pBuffer, RenderType.eyes(resourcelocation), false, itemstack.hasFoil());
			this.getParentModel().renderToBuffer(pose, vertexconsumer, 15728640, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
			pose.popPose();
		}
	}
}
