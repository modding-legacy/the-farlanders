package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.EnderminionBraceletLayer;
import com.legacy.farlanders.client.render.entity.layer.MultiEyeGlowingLayer;
import com.legacy.farlanders.client.render.model.EnderminionModel;
import com.legacy.farlanders.entity.tameable.EnderminionEntity;
import com.legacy.farlanders.entity.tameable.MysticEnderminionEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;

public class EnderminionRenderer<T extends EnderminionEntity> extends MobRenderer<T, EnderminionModel<T>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/enderminion/enderminion.png");
	private static final ResourceLocation TEXTURE_MYSTIC = TheFarlandersMod.locate("textures/entity/enderminion/mystic_enderminion.png");

	public EnderminionRenderer(EntityRendererProvider.Context context)
	{
		super(context, new EnderminionModel<>(context.bakeLayer(FLRenderRefs.ENDERMINION)), 0.5F);
		this.addLayer(new EnderminionBraceletLayer<>(this));
		this.addLayer(new ItemInHandLayer<T, EnderminionModel<T>>(this, context.getItemInHandRenderer()));

		this.addLayer(new MultiEyeGlowingLayer<>(this, "enderminion/"));
	}

	@Override
	public void render(T entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
	{
		/*this.model.attackTime = this.getAttackAnim(entityIn, partialTicks);
		System.out.println(this.model.attackTime);*/

		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	@Override
	protected void scale(T entity, PoseStack pose, float pPartialTickTime)
	{
		pose.translate(0, (float) Math.sin((entity.tickCount + pPartialTickTime) * 0.1F) * 0.05F, -0.1F);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return entity instanceof MysticEnderminionEntity ? TEXTURE_MYSTIC : TEXTURE;
	}
}