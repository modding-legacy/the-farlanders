package com.legacy.farlanders.client.render.model;

import com.legacy.farlanders.client.render.entity.TitanRenderer;
import com.legacy.farlanders.entity.hostile.TitanEntity;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;

public class TitanModel<T extends TitanEntity> extends HierarchicalModel<T>
{
	private final ModelPart root;

	protected final ModelPart rightMouth;
	protected final ModelPart leftMouth;
	protected final ModelPart chest;
	protected final ModelPart body;
	protected final ModelPart rightArm;
	protected final ModelPart rightArmBottom;
	protected final ModelPart leftArm;
	protected final ModelPart leftArmBottom;
	protected final ModelPart spine;
	protected final ModelPart waist;
	protected final ModelPart rightLeg;
	protected final ModelPart leftLeg;
	protected final ModelPart rightLegBottom;
	protected final ModelPart leftLegBottom;
	protected final ModelPart leftHead;
	protected final ModelPart rightHead;

	private boolean showRightHead, showLeftHead;

	public TitanModel(ModelPart root)
	{
		this(root, true, true);
	}

	public TitanModel(ModelPart root, boolean showRight, boolean showLeft)
	{
		this.showRightHead = showRight;
		this.showLeftHead = showLeft;

		this.root = root;

		this.rightMouth = root.getChild("right_mouth");
		this.leftMouth = root.getChild("left_mouth");
		this.chest = root.getChild("chest");
		this.body = root.getChild("body");
		this.rightArm = root.getChild("right_arm");
		this.rightArmBottom = this.rightArm.getChild("right_arm_bottom");
		this.leftArm = root.getChild("left_arm");
		this.leftArmBottom = this.leftArm.getChild("left_arm_bottom");
		this.spine = root.getChild("spine");
		this.waist = root.getChild("waist");
		this.rightLeg = root.getChild("right_leg");
		this.leftLeg = root.getChild("left_leg");
		this.rightLegBottom = this.rightLeg.getChild("right_leg_bottom");
		this.leftLegBottom = this.leftLeg.getChild("left_leg_bottom");
		this.leftHead = this.leftMouth.getChild("left_head");
		this.rightHead = this.rightMouth.getChild("right_head");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();
		var fightFix = new CubeDeformation(0.001F);

		PartDefinition right_mouth = partdefinition.addOrReplaceChild("right_mouth", CubeListBuilder.create().texOffs(0, 18).addBox(-3.5F, -8.0F, -4.5F, 7.0F, 8.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(-11.0F, -50.0F, -1.0F));

		PartDefinition right_head = right_mouth.addOrReplaceChild("right_head", CubeListBuilder.create().texOffs(0, 0).mirror().addBox(-3.5F, -9.0F, -4.5F, 9.0F, 9.0F, 9.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-1.0F, 0.0F, -1.0F));

		PartDefinition right_bottom_left_horn = right_head.addOrReplaceChild("right_bottom_left_horn", CubeListBuilder.create().texOffs(29, 19).addBox(0.0F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(5.5F, -6.0F, 0.0F));
		right_bottom_left_horn.addOrReplaceChild("right_top_left_horn", CubeListBuilder.create().texOffs(29, 24).addBox(-1.0F, -3.0F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(4.0333F, -1.0F, 0.0F));

		PartDefinition right_bottom_right_horn = right_head.addOrReplaceChild("right_bottom_right_horn", CubeListBuilder.create().texOffs(29, 19).mirror().addBox(-5.0F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-3.4667F, -6.0F, 0.0F));
		right_bottom_right_horn.addOrReplaceChild("right_top_right_horn", CubeListBuilder.create().texOffs(29, 24).mirror().addBox(-1.0F, -3.0F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-4.0F, -1.0F, 0.0F));

		PartDefinition left_mouth = partdefinition.addOrReplaceChild("left_mouth", CubeListBuilder.create().texOffs(0, 18).addBox(-3.5F, -8.0F, -4.5F, 7.0F, 8.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offset(11.0F, -50.0F, -1.0F));

		PartDefinition left_head = left_mouth.addOrReplaceChild("left_head", CubeListBuilder.create().texOffs(0, 0).addBox(-3.5F, -9.0F, -4.5F, 9.0F, 9.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 0.0F, -1.0F));

		PartDefinition left_bottom_left_horn = left_head.addOrReplaceChild("left_bottom_left_horn", CubeListBuilder.create().texOffs(29, 19).addBox(0.0F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(5.5F, -6.0F, 0.0F));
		left_bottom_left_horn.addOrReplaceChild("left_top_left_horn", CubeListBuilder.create().texOffs(29, 24).addBox(-1.0F, -3.0F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(4.0F, -1.0F, 0.0F));

		PartDefinition left_bottom_right_horn = left_head.addOrReplaceChild("left_bottom_right_horn", CubeListBuilder.create().texOffs(29, 19).mirror().addBox(-5.0F, -1.0F, -1.0F, 5.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-3.5333F, -6.0F, 0.0F));
		left_bottom_right_horn.addOrReplaceChild("left_top_right_horn", CubeListBuilder.create().texOffs(29, 24).mirror().addBox(-1.0F, -3.0F, -1.0F, 2.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-3.9667F, -1.0F, 0.0F));

		partdefinition.addOrReplaceChild("right_neck", CubeListBuilder.create().texOffs(112, 19).mirror().addBox(1.0F, 0.0F, 0.0F, 4.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(-14.0F, -50.0F, -2.2F, 0.2603F, 0.0F, 0.0F));
		partdefinition.addOrReplaceChild("left_neck", CubeListBuilder.create().texOffs(112, 19).addBox(1.0F, 0.0F, 0.0F, 4.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(8.0F, -50.0F, -2.2F, 0.2603F, 0.0F, 0.0F));

		partdefinition.addOrReplaceChild("chest", CubeListBuilder.create().texOffs(40, 0).addBox(1.0F, -2.0F, 0.0F, 36.0F, 10.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-19.0F, -43.0F, -2.0F, 0.2603F, 0.0F, 0.0F));
		partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(42, 19).addBox(-10.0F, -16.0F, -2.5F, 20.0F, 25.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -28.0F, 3.5F));

		PartDefinition right_arm = partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(0, 35).mirror().addBox(-5.0F, -2.0F, -2.5F, 5.0F, 24.0F, 5.0F, fightFix).mirror(false), PartPose.offset(-18.0F, -43.0F, 2.5F));
		right_arm.addOrReplaceChild("right_arm_bottom", CubeListBuilder.create().texOffs(21, 35).mirror().addBox(-2.5F, 0.0F, -5.5F, 5.0F, 26.0F, 5.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-2.5F, 22.0F, 3.0F));

		PartDefinition left_arm = partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(0, 35).addBox(0.0F, -2.0F, -2.5F, 5.0F, 24.0F, 5.0F, fightFix), PartPose.offset(18.0F, -43.0F, 2.5F));
		left_arm.addOrReplaceChild("left_arm_bottom", CubeListBuilder.create().texOffs(21, 35).addBox(-1.5F, 0.0F, -5.0F, 5.0F, 26.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(1.5F, 22.0F, 2.5F));

		partdefinition.addOrReplaceChild("spine", CubeListBuilder.create().texOffs(108, 29).addBox(-2.5F, -4.0F, -2.5F, 5.0F, 4.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -15.0F, 3.5F));

		partdefinition.addOrReplaceChild("waist", CubeListBuilder.create().texOffs(98, 39).addBox(1.0F, 0.0F, 0.0F, 10.0F, 3.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(-6.0F, -15.0F, 1.0F));

		PartDefinition right_leg = partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(0, 67).mirror().addBox(-5.0F, -1.5F, -2.5F, 5.0F, 20.0F, 5.0F, fightFix).mirror(false), PartPose.offset(-5.0F, -13.5F, 3.5F));
		right_leg.addOrReplaceChild("right_leg_bottom", CubeListBuilder.create().texOffs(21, 67).mirror().addBox(-2.5F, 0.0F, 0.0F, 5.0F, 19.0F, 5.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-2.5F, 18.5F, -2.5F));

		PartDefinition left_leg = partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(0, 67).addBox(0.0F, -1.5F, -2.5F, 5.0F, 20.0F, 5.0F, fightFix), PartPose.offset(5.0F, -13.5F, 3.5F));
		left_leg.addOrReplaceChild("left_leg_bottom", CubeListBuilder.create().texOffs(21, 67).addBox(-2.5F, 0.0F, 0.0F, 5.0F, 19.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offset(2.5F, 18.5F, -2.5F));

		return LayerDefinition.create(meshdefinition, 128, 96);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.rightLeg.setRotation(0, 0, 0);
		this.leftLeg.setRotation(0, 0, 0);
		this.rightLegBottom.setRotation(0, 0, 0);
		this.leftLegBottom.setRotation(0, 0, 0);

		this.rightHead.visible = this.showRightHead;
		this.leftHead.visible = this.showLeftHead;

		rightLeg.xRot += /*-0.1487144F + */Mth.cos(limbSwing * 0.5662F) * limbSwingAmount;
		// rightLegBottom.xRot = 0.1115358F + Mth.cos(limbSwing * 0.5662F) *
		// limbSwingAmount;
		leftLeg.xRot += /*-0.1487144F + */Mth.cos(limbSwing * 0.5662F + 3.141593F) * limbSwingAmount;
		// leftLegBottom.xRot = 0.1115358F + Mth.cos(limbSwing * 0.5662F + 3.141593F) *
		// limbSwingAmount;

		/*float var7 = -46.5F;
		
		this.rightHead.y = var7 - 3.0F;
		this.leftHead.y = var7 - 3.0F;
		
		this.rightMouth.y = var7 - 2.0F;
		this.leftMouth.y = var7 - 2.0F;
		
		if (entity.isVehicle())
		{
			float var9 = 1.0F;
			this.leftHead.y -= var9 * 5.0F;
			this.rightHead.y -= var9 * 5.0F;
			this.rightArm.xRot = 0.1115358F - 1.5F;
			this.rightArmBottom.xRot = -0.2974289F - 1.5F;
			this.leftArm.xRot = 0.1115358F - 1.5F;
			this.leftArmBottom.xRot = -0.2974289F - 1.5F;
		
			this.leftHead.yRot = 0.5f;
			this.leftHead.xRot = -0.05f;
		
			this.rightHead.yRot = -0.5f;
			this.rightHead.xRot = -0.05f;
		
			this.leftMouth.yRot = 0.5f;
			this.leftMouth.xRot = -0.05f;
		
			this.rightMouth.yRot = -0.5f;
			this.rightMouth.xRot = -0.05f;
		}
		
		this.leftHead.yRot = (10f + netHeadYaw) / 57.29578F;
		this.leftHead.xRot = headPitch / 57.29578F;
		this.rightHead.yRot = (-10.f + netHeadYaw) / 57.29578F;
		this.rightHead.xRot = headPitch / 57.29578F;
		this.leftMouth.yRot = (10f + netHeadYaw) / 57.29578F;
		this.leftMouth.xRot = headPitch / 57.29578F;
		this.rightMouth.yRot = (-10f + netHeadYaw) / 57.29578F;
		this.rightMouth.xRot = headPitch / 57.29578F;*/

		this.rightHead.y = this.leftHead.y = 0;
		this.rightMouth.setRotation(0, 0, 0);
		this.leftMouth.setRotation(0, 0, 0);
		this.leftHead.setRotation(0, 0, 0);
		this.rightHead.setRotation(0, 0, 0);

		float yaw = netHeadYaw / 57.29578F;
		float pitch = headPitch / 57.29578F;
		this.rightMouth.yRot += yaw;
		this.leftMouth.yRot += yaw;

		this.rightMouth.xRot += pitch + (Mth.cos(ageInTicks * 0.1F) * 0.05F);
		this.leftMouth.xRot += pitch + (Mth.cos(ageInTicks * 0.1F) * 0.05F);

		float inwardHeadAngle = 0.15F;
		this.leftMouth.yRot += inwardHeadAngle;
		this.rightMouth.yRot += -inwardHeadAngle;

		if (entity.isAggressive() || entity.isVehicle() || entity.inRage())
		{
			float mouthSpeed = entity.isVehicle() ? 0.8F : 0.4F;
			float mouthRange = entity.isVehicle() ? 0.8F : 0.2F;

			this.rightHead.y += (-1.0F * 5.0F) + (Mth.cos(ageInTicks * mouthSpeed) * mouthRange);
			this.leftHead.y += (-1.0F * 5.0F) + (Mth.sin(ageInTicks * mouthSpeed) * mouthRange);
		}

		float rotAngle = TitanRenderer.getRotAngle(entity, ageInTicks - entity.tickCount) * Mth.DEG_TO_RAD;
		this.rightLeg.xRot -= rotAngle;
		this.leftLeg.xRot -= rotAngle;

		this.leftHead.xRot -= rotAngle;
		this.rightHead.xRot -= rotAngle;

		float legAngle = -0.2F;
		this.rightLeg.xRot += legAngle;
		this.leftLeg.xRot += legAngle;

		this.rightLegBottom.xRot += -legAngle * 1.2F;
		this.leftLegBottom.xRot += -legAngle * 1.2F;

		if (entity.isVehicle())
		{
			/*float var9 = 1.0F;
			this.leftHead.y -= var9 * 5.0F;
			this.rightHead.y -= var9 * 5.0F;*/

			float upwardArms = -1.18F;
			float bendArms = -1.6F;
			float inwardArms = 0.6F;

			this.rightArm.xRot = upwardArms;
			this.rightArmBottom.xRot = bendArms;
			this.leftArm.xRot = upwardArms;
			this.leftArmBottom.xRot = bendArms;
			this.rightArm.yRot = -inwardArms;
			this.leftArm.yRot = inwardArms;

			/*this.leftHead.yRot = 0.5f;
			this.leftHead.xRot = -0.05f;
			
			this.rightHead.yRot = -0.5f;
			this.rightHead.xRot = -0.05f;*/

			inwardHeadAngle += 0.4F;
			float upwardHeadAngle = 0.05F;
			this.leftMouth.yRot = inwardHeadAngle;
			this.leftMouth.xRot = -upwardHeadAngle;

			this.rightMouth.yRot = -inwardHeadAngle;
			this.rightMouth.xRot = -upwardHeadAngle;
		}
	}

	@Override
	public void prepareMobModel(T entityIn, float limbSwing, float limbSwingAmount, float partialTick)
	{
		this.leftArm.setRotation(0, 0, 0);
		this.leftArmBottom.setRotation(0, 0, 0);
		this.rightArm.setRotation(0, 0, 0);
		this.rightArmBottom.setRotation(0, 0, 0);

		int i = entityIn.getAttackTimer();
		float ageInTicks = entityIn.tickCount + partialTick;

		if (i > 0)
		{
			this.rightArm.xRot += 0.1115358F + (-2.0F + 2.2F * Mth.triangleWave((float) i - partialTick, 10.0F));
			this.rightArmBottom.xRot += -0.2974289F/* + (-2.0F + 2.2F * Mth.triangleWave((float) i - par4, 10.0F))*/;
			this.leftArm.xRot += 0.1115358F + (-2.0F + 2.2F * Mth.triangleWave((float) i - partialTick, 10.0F));
			this.leftArmBottom.xRot += -0.2974289F/* + (-2.0F + 2.2F * Mth.triangleWave((float) i - par4, 10.0F))*/;
		}
		else
		{
			float rot = TitanRenderer.getRotAngle(entityIn, partialTick) * Mth.DEG_TO_RAD;
			this.rightArm.xRot -= rot;
			this.leftArm.xRot -= rot;

			float speed = 0.5662F;
			float right = 0, left = 0;
			this.rightArm.xRot += right = (Mth.cos(0.3F + limbSwing * speed + Mth.PI) * 2.0F * limbSwingAmount * 0.8F);
			this.leftArm.xRot += left = (Mth.cos(0.3F + limbSwing * speed) * 2.0F * limbSwingAmount * 0.8F);

			this.rightArmBottom.xRot += right * 0.3F;
			this.leftArmBottom.xRot += left * 0.3F;

		}

		float outZ = 0.1F + (0.1F * limbSwingAmount);

		this.rightArm.zRot += outZ;
		this.leftArm.zRot -= outZ;

		AnimationUtils.bobArms(rightArm, leftArm, ageInTicks);
		float angle = Mth.cos(ageInTicks * 0.05F);
		float angleOff = Mth.sin(ageInTicks * 0.05F);

		this.rightArm.xRot += angleOff * 0.1F;
		this.leftArm.xRot += angleOff * 0.1F;

		this.rightArmBottom.xRot += -0.4F + angle * 0.1F;
		this.leftArmBottom.xRot += -0.4F + angle * 0.1F;
	}

}