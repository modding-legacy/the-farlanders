package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.TexturedGlowingLayer;
import com.legacy.farlanders.client.render.model.FarlanderModel;
import com.legacy.farlanders.entity.hostile.RebelEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;

public class RebelRenderer<T extends RebelEntity> extends MobRenderer<T, FarlanderModel<T>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/rebel.png");
	private static final ResourceLocation EYES = TheFarlandersMod.locate("textures/entity/rebel_eyes.png");

	public RebelRenderer(EntityRendererProvider.Context context)
	{
		super(context, new FarlanderModel<>(context.bakeLayer(FLRenderRefs.FARLANDER)), 0.5F);
		this.addLayer(new ItemInHandLayer<T, FarlanderModel<T>>(this, context.getItemInHandRenderer()));
		this.addLayer(new TexturedGlowingLayer<>(this, EYES));
	}

	@Override
	protected void scale(T entitylivingbaseIn, PoseStack matrixStackIn, float partialTickTime)
	{
		float f1 = 0.9375F;
		matrixStackIn.scale(f1, f1, f1);
	}

	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}