package com.legacy.farlanders.client.render.model;

import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.item.BowItem;

public class EnderGuardianModel<T extends EnderGuardianEntity> extends HierarchicalModel<T> implements ArmedModel
{
	private final ModelPart root;

	protected final ModelPart body;
	protected final ModelPart rightArm;
	protected final ModelPart leftArm;
	protected final ModelPart rightLeg;
	protected final ModelPart leftLeg;
	protected final ModelPart neck;
	protected final ModelPart waist;
	protected final ModelPart quiver, arrow;
	protected final ModelPart mouth, head;

	public EnderGuardianModel(ModelPart root)
	{
		this.root = root;

		this.body = root.getChild("body");
		this.rightArm = root.getChild("right_arm");
		this.leftArm = root.getChild("left_arm");
		this.rightLeg = root.getChild("right_leg");
		this.leftLeg = root.getChild("left_leg");
		this.neck = root.getChild("neck");
		this.waist = root.getChild("waist");
		this.quiver = root.getChild("quiver");
		this.arrow = this.quiver.getChild("arrow");
		this.mouth = root.getChild("mouth");
		this.head = this.mouth.getChild("head");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(32, 16).addBox(-4.0F, 0.0F, -2.0F, 8.0F, 4.0F, 4.0F), PartPose.offset(0.0F, 1.0F, 0.0F));

		partdefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(56, 0).mirror().addBox(-2.25F, -1.0F, -1.0F, 2.0F, 17.0F, 2.0F).mirror(false), PartPose.offset(-3.75F, 2.0F, 0.0F));
		partdefinition.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(56, 0).addBox(0.0F, -1.0F, -1.0F, 2.0F, 17.0F, 2.0F), PartPose.offset(4.0F, 2.0F, 0.0F));

		partdefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(56, 19).mirror().addBox(-2.0F, -1.0F, -1.0F, 2.0F, 17.0F, 2.0F).mirror(false), PartPose.offset(-1.0F, 8.0F, 0.0F));
		partdefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(56, 19).addBox(0.0F, -1.0F, -1.0F, 2.0F, 17.0F, 2.0F), PartPose.offset(1.0F, 8.0F, 0.0F));

		partdefinition.addOrReplaceChild("neck", CubeListBuilder.create().texOffs(24, 5).addBox(0.0F, 0.0F, 0.0F, 2.0F, 1.0F, 2.0F), PartPose.offset(-1.0F, 0.0F, -1.0F));
		partdefinition.addOrReplaceChild("waist", CubeListBuilder.create().texOffs(48, 24).addBox(0.0F, 0.0F, 0.0F, 2.0F, 4.0F, 2.0F), PartPose.offset(-1.0F, 5.0F, -1.0F));

		PartDefinition quiver = partdefinition.addOrReplaceChild("quiver", CubeListBuilder.create().texOffs(40, 0).addBox(0.0F, 0.0F, 0.0F, 3.0F, 10.0F, 3.0F), PartPose.offset(-4.0F, 2.0F, 2.0F));

		PartDefinition arrow = quiver.addOrReplaceChild("arrow", CubeListBuilder.create(), PartPose.offset(1.5F, 0.5F, 1.5F));

		arrow.addOrReplaceChild("arrowEastWest_r1", CubeListBuilder.create().texOffs(31, -3).addBox(0.0F, -4.0F, -1.5F, 0.0F, 5.0F, 3.0F).texOffs(31, 0).addBox(-1.5F, -4.0F, 0.0F, 3.0F, 5.0F, 0.0F), PartPose.offsetAndRotation(0.0F, -0.5F, 0.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition mouth = partdefinition.addOrReplaceChild("mouth", CubeListBuilder.create().texOffs(0, 16).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition head = mouth.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition left_lower_horn = head.addOrReplaceChild("left_lower_horn", CubeListBuilder.create().texOffs(32, 24).addBox(0.0F, -1.0F, -1.0F, 4.0F, 2.0F, 2.0F), PartPose.offset(4.0F, -5.0F, 0.0F));
		left_lower_horn.addOrReplaceChild("left_upper_horn", CubeListBuilder.create().texOffs(32, 28).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 2.0F), PartPose.offset(3.0F, -1.0F, 0.0F));

		PartDefinition right_lower_horn = head.addOrReplaceChild("right_lower_horn", CubeListBuilder.create().texOffs(32, 24).mirror().addBox(-4.0F, -1.0F, -1.0F, 4.0F, 2.0F, 2.0F).mirror(false), PartPose.offset(-4.0F, -5.0F, 0.0F));
		right_lower_horn.addOrReplaceChild("right_upper_horn", CubeListBuilder.create().texOffs(32, 28).mirror().addBox(-1.0F, -2.0F, -1.0F, 2.0F, 2.0F, 2.0F).mirror(false), PartPose.offset(-3.0F, -1.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		// z -0.409F
		this.head.resetPose();
		this.rightArm.resetPose();
		this.leftArm.resetPose();
		this.quiver.setRotation(0, 0, -0.409F);

		this.head.setPos(0, entity.isAggressive() ? -5.5F + (Mth.cos(ageInTicks * 0.35F) * 0.2F) : 0, 0);

		float pitch = headPitch / 57.29578F;
		float yaw = netHeadYaw / 57.29578F;
		mouth.yRot = yaw;
		mouth.xRot = pitch;

		rightLeg.xRot = Mth.cos(limbSwing * 0.6662F) * limbSwingAmount;
		leftLeg.xRot = Mth.cos(limbSwing * 0.6662F + 3.141593F) * limbSwingAmount;
		rightLeg.yRot = 0.0F;
		leftLeg.yRot = 0.0F;
		this.rightArm.xRot += Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * 2.0F * limbSwingAmount * 0.5F;
		this.leftArm.xRot += Mth.cos(limbSwing * 0.6662F) * 2.0F * limbSwingAmount * 0.5F;
		quiver.xRot += limbSwingAmount + ((Mth.sin(limbSwing * 0.6662F) * 0.1F) * limbSwingAmount);

		var arrow = this.arrow;
		arrow.resetPose();

		if (!entity.isAggressive())
			arrow.xRot += -0.5F;

		arrow.zRot += -0.5F * limbSwingAmount;

		arrow.zRot += Mth.cos(limbSwing * 0.5F) * limbSwingAmount * 0.1F;

		arrow.y += Mth.sin(limbSwing * 1F) * limbSwingAmount * 0.2F;
		AnimationUtils.bobArms(rightArm, leftArm, ageInTicks);

		if (entity.isAggressive() && entity.getMainHandItem().getItem() instanceof BowItem)
		{
			this.rightArm.yRot = -0.1F + this.mouth.yRot;
			this.leftArm.yRot = 0.1F + this.mouth.yRot + 0.4F;
			this.rightArm.xRot = (-(float) Math.PI / 2F) + this.mouth.xRot;
			this.leftArm.xRot = (-(float) Math.PI / 2F) + this.mouth.xRot;
		}
	}

	@Override
	public void translateToHand(HumanoidArm pSide, PoseStack pPoseStack)
	{
		boolean left = pSide == HumanoidArm.LEFT;
		ModelPart arm = left ? this.leftArm : this.rightArm;

		arm.translateAndRotate(pPoseStack);
		pPoseStack.translate(0, 0.3F, 0F);
	}
}