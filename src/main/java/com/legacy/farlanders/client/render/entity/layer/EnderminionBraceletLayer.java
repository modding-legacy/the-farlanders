package com.legacy.farlanders.client.render.entity.layer;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.model.EnderminionModel;
import com.legacy.farlanders.entity.tameable.EnderminionEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class EnderminionBraceletLayer<T extends EnderminionEntity, M extends EnderminionModel<T>> extends RenderLayer<T, M>
{
	private static final ResourceLocation BRACELET_TEXTURE = TheFarlandersMod.locate("textures/entity/enderminion/tame.png");

	public EnderminionBraceletLayer(RenderLayerParent<T, M> parent)
	{
		super(parent);
	}

	@Override
	public void render(PoseStack pose, MultiBufferSource buffer, int packedLightIn, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		if (entity.isTame() && !entity.isInvisible())
		{
			float[] colorArray = entity.getBraceletColor().getTextureDiffuseColors();
			renderColoredCutoutModel(this.getParentModel(), BRACELET_TEXTURE, pose, buffer, packedLightIn, entity, colorArray[0], colorArray[1], colorArray[2]);
		}
	}
}