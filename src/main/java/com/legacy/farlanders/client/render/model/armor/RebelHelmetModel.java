package com.legacy.farlanders.client.render.model.armor;

import java.util.List;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.world.entity.LivingEntity;

public class RebelHelmetModel<T extends LivingEntity> extends HumanoidModel<T>
{
	private final ModelPart helmet;

	public RebelHelmetModel(ModelPart root)
	{
		super(root);
		this.helmet = root.getChild("helmet");
	}

	public static LayerDefinition createHelmetLayer()
	{
		MeshDefinition meshdefinition = HumanoidModel.createMesh(CubeDeformation.NONE, 0.0F);
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition helmet = partdefinition.addOrReplaceChild("helmet", CubeListBuilder.create().texOffs(0, 0).mirror().addBox(-4.5F, -8.0F, -4.5F, 9.0F, 8.0F, 9.0F, new CubeDeformation(0.501F)).mirror(false), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition left_lower_horn = helmet.addOrReplaceChild("left_lower_horn", CubeListBuilder.create().texOffs(27, 4).mirror().addBox(-8.5F, -5.5F, -1.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(0.0F, 0.0F, 0.0F));
		left_lower_horn.addOrReplaceChild("left_upper_horn", CubeListBuilder.create().texOffs(27, 0).mirror().addBox(-8.5F, -8.5F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(0.0F, 1.0F, 0.0F));

		PartDefinition right_lower_horn = helmet.addOrReplaceChild("right_lower_horn", CubeListBuilder.create().texOffs(27, 4).addBox(4.5F, -5.5F, -1.5F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.5F));
		right_lower_horn.addOrReplaceChild("right_upper_horn", CubeListBuilder.create().texOffs(27, 0).mirror().addBox(6.5F, -7.5F, -1.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		this.helmet.copyFrom(this.head);
		return List.of(this.helmet);
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return List.of();
	}
}