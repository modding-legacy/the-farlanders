package com.legacy.farlanders.client.render.entity.layer;

import java.util.Locale;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.model.TitanModel;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class TitanEyeLayer<T extends TitanEntity, M extends TitanModel<T>> extends RenderLayer<T, M>
{
	private final boolean isRight;
	private final TitanModel<T> eyeModel;

	public TitanEyeLayer(RenderLayerParent<T, M> renderer, EntityRendererProvider.Context context, boolean isRight)
	{
		super(renderer);

		this.isRight = isRight;
		this.eyeModel = new TitanModel<>(context.bakeLayer(FLRenderRefs.TITAN), this.isRight, !this.isRight);
	}

	@Override
	public void render(PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		VertexConsumer ivertexbuilder = bufferIn.getBuffer(this.getRenderType(entity));

		this.getParentModel().copyPropertiesTo(this.eyeModel);
		this.eyeModel.setupAnim(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
		this.eyeModel.prepareMobModel(entity, limbSwing, limbSwingAmount, partialTicks);

		this.eyeModel.renderToBuffer(matrixStackIn, ivertexbuilder, 15728640, OverlayTexture.NO_OVERLAY, 1.0F, 1.0F, 1.0F, 1.0F);
	}

	public RenderType getRenderType(T entity)
	{
		ResourceLocation glowTexture = TheFarlandersMod.locate("textures/entity/titan/" + this.getEyeTexture(entity.getEyeData(this.isRight ? entity.getRightEyeColor() : entity.getLeftEyeColor())) + "_eyes.png");
		return RenderType.eyes(glowTexture);
	}

	// what a nightmare
	public String getEyeTexture(IColoredEyes.EyeColor color)
	{
		return color.name().toLowerCase(Locale.ENGLISH);
	}
}