package com.legacy.farlanders.client.render.entity.layer;

import java.util.Locale;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.model.TitanModel;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class TitanHeartLayer<T extends TitanEntity, M extends TitanModel<T>> extends RenderLayer<T, M>
{
	private static final ResourceLocation HEART_TEXTURE = TheFarlandersMod.locate("textures/entity/titan/titan_heart.png");
	private final RenderType heartType;

	public TitanHeartLayer(RenderLayerParent<T, M> renderer)
	{
		super(renderer);

		this.heartType = RenderType.eyes(HEART_TEXTURE);
	}

	@Override
	public void render(PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn, T entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch)
	{
		VertexConsumer ivertexbuilder = bufferIn.getBuffer(this.heartType);

		this.getParentModel().setupAnim(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch);
		this.getParentModel().prepareMobModel(entity, limbSwing, limbSwingAmount, partialTicks);

		float strength = 0.5F + Mth.clamp(((float) Math.cos((entity.heartTicks + partialTicks) * 0.25F) * 1F) - 0.5F, -0.5F, 0.5F);

		strength += Mth.lerp(partialTicks, entity.oHeartBrightness, entity.heartBrightness) * 1 * Mth.PI;
		strength = Mth.clamp(strength, 0, 1);

		this.getParentModel().renderToBuffer(matrixStackIn, ivertexbuilder, 15728640, OverlayTexture.NO_OVERLAY, strength, strength, strength, 1.0F);
	}

	public RenderType getRenderType(T entity)
	{
		return this.heartType;
	}

	// what a nightmare
	public String getEyeTexture(IColoredEyes.EyeColor color)
	{
		return color.name().toLowerCase(Locale.ENGLISH);
	}
}