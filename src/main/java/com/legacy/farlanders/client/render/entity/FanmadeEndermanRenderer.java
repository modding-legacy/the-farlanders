package com.legacy.farlanders.client.render.entity;

import java.util.List;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.render.entity.layer.TexturedGlowingLayer;

import net.minecraft.client.renderer.entity.EndermanRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.layers.EnderEyesLayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.monster.EnderMan;

public class FanmadeEndermanRenderer extends EndermanRenderer
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/fanmade_enderman.png");
	private static final ResourceLocation TEXTURE_EYES = TheFarlandersMod.locate("textures/entity/fanmade_eyes.png");

	public FanmadeEndermanRenderer(EntityRendererProvider.Context context)
	{
		super(context);

		for (var layer : List.copyOf(this.layers))
		{
			if (layer instanceof EnderEyesLayer)
				this.layers.remove(layer);
		}

		this.addLayer(new TexturedGlowingLayer<>(this, TEXTURE_EYES));
	}

	@Override
	public ResourceLocation getTextureLocation(EnderMan entity)
	{
		return TEXTURE;
	}
}