package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.TitanEyeLayer;
import com.legacy.farlanders.client.render.entity.layer.TitanHeartLayer;
import com.legacy.farlanders.client.render.model.TitanModel;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.WalkAnimationState;
import net.minecraft.world.phys.Vec3;

public class TitanRenderer<T extends TitanEntity> extends MobRenderer<T, TitanModel<T>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/titan/titan.png");

	public TitanRenderer(EntityRendererProvider.Context context)
	{
		super(context, new TitanModel<>(context.bakeLayer(FLRenderRefs.TITAN)), 1.0F);
		this.addLayer(new TitanEyeLayer<>(this, context, true));
		this.addLayer(new TitanEyeLayer<>(this, context, false));
		this.addLayer(new TitanHeartLayer<>(this));
	}

	@Override
	protected void scale(T entity, PoseStack pose, float partialTicks)
	{
		float rotAngle = getRotAngle(entity, partialTicks);
		/*if (entity.isVehicle())
			pose.translate(0, 0, 1.6F);*/

		pose.translate(0, 0.1F, (rotAngle * Mth.DEG_TO_RAD) * 2.4F);

		pose.mulPose(Axis.XP.rotationDegrees(rotAngle));
	}

	@Override
	protected void setupRotations(T entity, PoseStack pose, float pAgeInTicks, float pRotationYaw, float pPartialTicks)
	{
		super.setupRotations(entity, pose, pAgeInTicks, pRotationYaw, pPartialTicks);

		WalkAnimationState anim = entity.walkAnimation;
		float f1 = anim.position(pPartialTicks) + 6.0F;
		float f2 = (Math.abs(f1 % 13.0F - 6.5F) - 3.25F) / 3.25F;
		pose.mulPose(Axis.ZP.rotationDegrees(f2 * (3.5F * (anim.speed(pPartialTicks)))));
	}

	@Override
	public Vec3 getRenderOffset(T entity, float p_114337_)
	{
		if (entity.inRage())
		{
			double d0 = 0.01D;
			return new Vec3(entity.getRandom().nextGaussian() * d0, 0.0D, entity.getRandom().nextGaussian() * d0);
		}
		else
		{
			return super.getRenderOffset(entity, p_114337_);
		}
	}

	public static <E extends TitanEntity> float getRotAngle(E titan, float partialTicks)
	{
		return 8.0F + Mth.sin((titan.tickCount + partialTicks) * 0.1F) * 3F;
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}