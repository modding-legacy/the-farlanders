package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.MultiEyeGlowingLayer;
import com.legacy.farlanders.client.render.model.FarlanderModel;
import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;

public class LooterRenderer<T extends LooterEntity> extends MobRenderer<T, FarlanderModel<T>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/looter/looter.png");

	public LooterRenderer(EntityRendererProvider.Context context)
	{
		super(context, new FarlanderModel<>(context.bakeLayer(FLRenderRefs.FARLANDER)), 0.5F);
		this.addLayer(new ItemInHandLayer<T, FarlanderModel<T>>(this, context.getItemInHandRenderer()));
		this.addLayer(new MultiEyeGlowingLayer<>(this, "looter/"));
	}

	@Override
	protected void scale(T entity, PoseStack pose, float partialTicks)
	{
		float scale = 0.9375F;
		pose.scale(scale, scale, scale);
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}