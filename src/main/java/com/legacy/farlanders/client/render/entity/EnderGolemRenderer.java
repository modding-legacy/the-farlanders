package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.MultiEyeGlowingLayer;
import com.legacy.farlanders.client.render.model.EnderGolemModel;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Axis;

import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.WalkAnimationState;
import net.minecraft.world.phys.Vec3;

public class EnderGolemRenderer<T extends EnderGolemEntity> extends MobRenderer<T, EnderGolemModel<T>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/golem/ender_golem.png");

	public EnderGolemRenderer(EntityRendererProvider.Context context)
	{
		super(context, new EnderGolemModel<>(context.bakeLayer(FLRenderRefs.ENDER_GOLEM)), 0.5F);
		this.addLayer(new MultiEyeGlowingLayer<>(this, "golem/"));
	}

	@Override
	public void render(T entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn)
	{
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		this.getModel().isAttacking = entityIn.getAngry();
	}

	@Override
	public Vec3 getRenderOffset(T entity, float partialTicks)
	{
		if (entity.getAngry())
			return new Vec3(entity.getRandom().nextGaussian() * 0.02D, 0.0D, entity.getRandom().nextGaussian() * 0.02D);

		return super.getRenderOffset(entity, partialTicks);
	}

	@Override
	protected void setupRotations(T pEntityLiving, PoseStack pose, float pAgeInTicks, float pRotationYaw, float pPartialTicks)
	{
		super.setupRotations(pEntityLiving, pose, pAgeInTicks, pRotationYaw, pPartialTicks);

		WalkAnimationState anim = pEntityLiving.walkAnimation;
		float f1 = anim.position(pPartialTicks) + 6.0F;
		float f2 = (Math.abs(f1 % 13.0F - 6.5F) - 3.25F) / 3.25F;
		pose.mulPose(Axis.ZP.rotationDegrees(f2 * (6.5F * (anim.speed(pPartialTicks)))));
	}

	@Override
	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}