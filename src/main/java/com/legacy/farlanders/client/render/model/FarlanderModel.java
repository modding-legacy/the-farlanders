package com.legacy.farlanders.client.render.model;

import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.entity.hostile.RebelEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.item.ItemStack;

public class FarlanderModel<T extends Mob> extends HierarchicalModel<T> implements ArmedModel
{
	private final ModelPart root;

	protected final ModelPart mouth, head, helmet;
	protected final ModelPart body;
	protected final ModelPart rightArm;
	protected final ModelPart leftArm;
	protected final ModelPart rightLeg;
	protected final ModelPart leftLeg;
	protected final ModelPart neck;

	public FarlanderModel(ModelPart root)
	{
		this.root = root;

		this.mouth = root.getChild("mouth");
		this.head = this.mouth.getChild("head");
		this.helmet = this.head.getChild("helmet");
		this.body = root.getChild("body");
		this.rightArm = root.getChild("right_arm");
		this.leftArm = root.getChild("left_arm");
		this.rightLeg = root.getChild("right_leg");
		this.leftLeg = root.getChild("left_leg");
		this.neck = root.getChild("neck");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition mesh = new MeshDefinition();
		PartDefinition root = mesh.getRoot();
		var scale = new CubeDeformation(0.0005F);

		var mouth = root.addOrReplaceChild("mouth", CubeListBuilder.create().texOffs(0, 16).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F), PartPose.offset(0.0F, 0.0F, 0.0F));
		var head = mouth.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, scale), PartPose.offset(0.0F, 0.0F, 0.0F));

		head.addOrReplaceChild("cloth", CubeListBuilder.create().texOffs(0, 32).addBox(-4.0F, 9.0F, -4.0F, 8.0F, 3.0F, 8.0F, scale), PartPose.offset(0.0F, -9.0F, 0.0F));

		var helmet = head.addOrReplaceChild("helmet", CubeListBuilder.create().texOffs(0, 43).addBox(-4.5F, -8.5F, -4.5F, 9.0F, 9.0F, 9.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		var rightHorn = helmet.addOrReplaceChild("right_horn_bottom", CubeListBuilder.create().texOffs(32, 36).mirror().addBox(-4.0F, 22.0F, 0.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-4.5F, -28.0F, -1.0F));
		rightHorn.addOrReplaceChild("right_horn_top", CubeListBuilder.create().texOffs(32, 32).mirror().addBox(-2.0F, 22.0F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-2.0F, -2.0F, 0.0F));

		var leftHorn = helmet.addOrReplaceChild("left_horn_bottom", CubeListBuilder.create().texOffs(32, 36).addBox(-4.0F, 22.0F, 0.0F, 4.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(8.5F, -28.0F, -1.0F));
		leftHorn.addOrReplaceChild("left_horn_top", CubeListBuilder.create().texOffs(32, 32).addBox(-2.0F, 22.0F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -2.0F, 0.0F));

		root.addOrReplaceChild("body", CubeListBuilder.create().texOffs(32, 16).addBox(-4.0F, 0.0F, -2.0F, 8.0F, 6.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 1.0F, 0.0F));

		root.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(56, 0).mirror().addBox(-2.0F, -2.0F, -1.0F, 2.0F, 19.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-4.0F, 3.0F, 0.0F));
		root.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(56, 0).addBox(0.0F, -2.0F, -1.0F, 2.0F, 19.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(4.0F, 3.0F, 0.0F));

		root.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(56, 21).mirror().addBox(-1.0F, 0.0F, -1.0F, 2.0F, 17.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(-2.0F, 7.0F, 0.0F));
		root.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(56, 21).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 17.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(2.0F, 7.0F, 0.0F));

		root.addOrReplaceChild("neck", CubeListBuilder.create().texOffs(24, 5).addBox(0.0F, 0.0F, 0.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.0F, 0.0F, -1.0F));

		return LayerDefinition.create(mesh, 64, 64);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		if (!(entity instanceof RebelEntity))
			this.helmet.visible = false;

		mouth.yRot = netHeadYaw / 57.29578F;
		mouth.xRot = headPitch / 57.29578F;

		rightArm.setRotation(0, 0, 0);
		leftArm.setRotation(0, 0, 0);

		this.head.setPos(0, entity instanceof FarlanderEntity fl && fl.isTempted() ? -4.5F + (Mth.cos(ageInTicks * 0.35F) * 0.2F) : entity.isAggressive() ? -5.5F + (Mth.cos(ageInTicks * 0.35F) * 0.2F) : 0, 0);

		boolean rightHanded = entity.getMainArm() == HumanoidArm.RIGHT;
		ItemStack leftItem = rightHanded ? entity.getOffhandItem() : entity.getMainHandItem();
		ItemStack rightItem = rightHanded ? entity.getMainHandItem() : entity.getOffhandItem();

		boolean aggro = false;
		if (entity instanceof Enemy && !entity.getMainHandItem().isEmpty())
		{
			if (entity.isAggressive())
			{
				AnimationUtils.swingWeaponDown(rightArm, leftArm, entity, this.attackTime, ageInTicks);

				var nonDominantArm = rightHanded ? this.leftArm : this.rightArm;
				nonDominantArm.setRotation(0, 0, 0);
				aggro = true;
			}
		}
		else
		{
			if (entity.swingingArm == InteractionHand.MAIN_HAND && rightHanded || entity.swingingArm == InteractionHand.OFF_HAND && !rightHanded)
				setupAttackAnimation(entity, ageInTicks, this.rightArm);
			else
				setupAttackAnimation(entity, ageInTicks, this.leftArm);
		}

		/*if (entity.isAggressive())
			AnimationUtils.swingWeaponDown(rightArm, leftArm, entity, this.attackTime, ageInTicks);
		else*/
		{
			if (!(aggro && rightHanded))
			{
				if (!rightItem.isEmpty())
				{
					this.rightArm.xRot += -0.25F + Mth.cos(limbSwing * 0.6662F + Mth.PI) * 2.0F * limbSwingAmount * 0.3F;

					/*if (entity.isAggressive())
						this.rightArm.xRot -= 1.45F;*/

				}
				else
					rightArm.xRot += limbSwingAmount;
			}

			if (!(aggro && !rightHanded))
			{
				if (!leftItem.isEmpty())
				{
					this.leftArm.xRot += -0.25F + Mth.cos(limbSwing * 0.6662F) * 2.0F * limbSwingAmount * 0.3F;
				}
				else
					leftArm.xRot += limbSwingAmount;
			}
		}

		rightLeg.xRot = Mth.cos(limbSwing * 0.6662F) * limbSwingAmount;
		leftLeg.xRot = Mth.cos(limbSwing * 0.6662F + Mth.PI) * limbSwingAmount;

		AnimationUtils.bobArms(rightArm, leftArm, ageInTicks);

		float scale = entity.isBaby() ? 1.7F : 1.0F;
		this.mouth.xScale = scale;
		this.mouth.yScale = scale;
		this.mouth.zScale = scale;

		rightArm.x = -4;
		leftArm.x = 4;
	}

	@Override
	public void translateToHand(HumanoidArm pSide, PoseStack pPoseStack)
	{
		boolean left = pSide == HumanoidArm.LEFT;
		ModelPart arm = left ? this.leftArm : this.rightArm;

		arm.translateAndRotate(pPoseStack);
		pPoseStack.translate(0, 0.4F, 0F);
	}

	protected void setupAttackAnimation(T pLivingEntity, float pAgeInTicks, ModelPart modelpart)
	{
		if (!(this.attackTime <= 0.0F))
		{
			float f = this.attackTime;
			this.body.yRot = Mth.sin(Mth.sqrt(f) * ((float) Math.PI * 2F)) * 0.2F;

			if (pLivingEntity.getMainArm() == HumanoidArm.LEFT)
			{
				this.body.yRot *= -1.0F;
			}

			this.rightArm.z = Mth.sin(this.body.yRot) * 5.0F;
			this.rightArm.x = -Mth.cos(this.body.yRot) * 5.0F;
			this.leftArm.z = -Mth.sin(this.body.yRot) * 5.0F;
			this.leftArm.x = Mth.cos(this.body.yRot) * 5.0F;
			this.rightArm.yRot += this.body.yRot;
			this.leftArm.yRot += this.body.yRot;
			this.leftArm.xRot += this.body.yRot;
			f = 1.0F - this.attackTime;
			f *= f;
			f *= f;
			f = 1.0F - f;
			float f1 = Mth.sin(f * (float) Math.PI);
			float f2 = Mth.sin(this.attackTime * (float) Math.PI) * -(this.mouth.xRot - 0.7F) * 0.75F;
			modelpart.xRot -= f1 * 1.2F + f2;
			modelpart.yRot += this.body.yRot * 2.0F;
			modelpart.zRot += Mth.sin(this.attackTime * (float) Math.PI) * -0.4F;
		}
	}
}