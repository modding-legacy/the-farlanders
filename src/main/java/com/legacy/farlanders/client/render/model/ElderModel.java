package com.legacy.farlanders.client.render.model;

import com.legacy.farlanders.entity.FarlanderEntity;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;

public class ElderModel<T extends LivingEntity> extends HierarchicalModel<T>
{
	private final ModelPart root;

	protected final ModelPart head;
	protected final ModelPart eyebrows;
	protected final ModelPart body;
	protected final ModelPart rightArm;
	protected final ModelPart leftArm;
	protected final ModelPart rightLeg;
	protected final ModelPart leftLeg;
	protected final ModelPart rightArmBend;
	protected final ModelPart backCape;
	protected final ModelPart frontCape;
	protected final ModelPart caneHandle;
	protected final ModelPart caneBottom;
	protected final ModelPart mouth;

	public ElderModel(ModelPart root)
	{
		this.root = root;

		this.head = root.getChild("head");
		this.eyebrows = this.head.getChild("eye_brows");
		this.body = root.getChild("body");
		this.rightArm = root.getChild("right_arm");
		this.rightArmBend = this.rightArm.getChild("right_arm_bend");
		this.caneHandle = this.rightArmBend.getChild("cane_handle");
		this.caneBottom = this.caneHandle.getChild("cane_bottom");
		this.leftArm = root.getChild("left_arm");
		this.rightLeg = root.getChild("right_leg");
		this.leftLeg = root.getChild("left_leg");
		this.backCape = this.body.getChild("back_cape");
		this.frontCape = this.body.getChild("front_cape");
		this.mouth = root.getChild("mouth");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition mesh = new MeshDefinition();
		PartDefinition root = mesh.getRoot();

		PartDefinition head = root.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F, new CubeDeformation(0.005F)), PartPose.offset(0.0F, 0.0F, -2.0F));
		head.addOrReplaceChild("eye_brows", CubeListBuilder.create().texOffs(31, 62).addBox(2.0F, -6.0F, -4.3333F, 3.0F, 1.0F, 1.0F).texOffs(31, 62).mirror().addBox(-5.0F, -6.0F, -4.2667F, 3.0F, 1.0F, 1.0F).mirror(false), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition body = root.addOrReplaceChild("body", CubeListBuilder.create().texOffs(32, 16).addBox(-4.0F, 0.0F, -2.0F, 8.0F, 6.0F, 4.0F), PartPose.offset(0.0F, 1.0F, 0.0F));

		body.addOrReplaceChild("front_cape", CubeListBuilder.create().texOffs(0, 48).addBox(-1.0F, 0.0F, 2.1333F, 8.0F, 16.0F, 0.0F), PartPose.offset(-3.0F, 0.0F, -4.1583F));
		body.addOrReplaceChild("back_cape", CubeListBuilder.create().texOffs(0, 32).addBox(-1.0F, 0.0F, -0.0417F, 8.0F, 16.0F, 0.0F), PartPose.offset(-3.0F, 0.0F, 2.0667F));

		PartDefinition right_arm = root.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(16, 32).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 8.0F, 2.0F, new CubeDeformation(0.001F)), PartPose.offset(-5.0F, 3.0F, 0.0F));

		PartDefinition right_arm_bend = right_arm.addOrReplaceChild("right_arm_bend", CubeListBuilder.create().texOffs(16, 42).addBox(-1.0F, 0.0F, -2.0F, 2.0F, 8.0F, 2.0F), PartPose.offset(0.0F, 6.0F, 1.0F));

		PartDefinition cane_handle = right_arm_bend.addOrReplaceChild("cane_handle", CubeListBuilder.create().texOffs(56, 32).addBox(-1.5F, 0.0F, -0.5F, 3.0F, 1.0F, 1.0F), PartPose.offset(0.0F, 8.0F, 0.0F));
		cane_handle.addOrReplaceChild("cane_bottom", CubeListBuilder.create().texOffs(60, 35).addBox(-0.5F, -0.5F, -0.5F, 1.0F, 14.0F, 1.0F), PartPose.offset(0.0F, 0.5F, 0.0F));

		root.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(25, 32).addBox(-1.0F, -2.0F, -1.0F, 2.0F, 19.0F, 2.0F), PartPose.offset(5.0F, 3.0F, 0.0F));
		root.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(56, 0).mirror().addBox(-1.0F, 0.0F, -1.0F, 2.0F, 17.0F, 2.0F).mirror(false), PartPose.offset(-2.0F, 7.0F, 0.0F));
		root.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(56, 0).addBox(-1.0F, 0.0F, -1.0F, 2.0F, 17.0F, 2.0F), PartPose.offset(2.0F, 7.0F, 0.0F));
		root.addOrReplaceChild("neck", CubeListBuilder.create().texOffs(24, 5).addBox(0.0F, 0.0F, 0.0F, 2.0F, 1.0F, 2.0F), PartPose.offset(-1.0F, 0.0F, -1.0F));

		PartDefinition headwear = root.addOrReplaceChild("mouth", CubeListBuilder.create().texOffs(0, 16).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F), PartPose.offset(0.0F, 0.0F, -2.0F));
		headwear.addOrReplaceChild("beard", CubeListBuilder.create().texOffs(40, 51).addBox(-4.5F, -1.0F, -4.5F, 9.0F, 10.0F, 3.0F), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(mesh, 64, 64);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		// limbSwing = ageInTicks * 0.7F;
		// limbSwingAmount = 1.0F;
		mouth.yRot = netHeadYaw / 57.29578F;
		mouth.xRot = headPitch / 57.29578F;

		head.copyFrom(mouth);
		leftArm.setRotation(limbSwingAmount, 0, -0.1F);
		rightArm.setRotation(0, 0, 0);
		rightArmBend.setRotation(0, 0, 0);
		caneHandle.setRotation(0, 0, 0);
		body.setRotation(0, 0, 0);

		this.head.y += entity instanceof FarlanderEntity fl && fl.isTempted() ? -3.5F + (Mth.cos(ageInTicks * 0.35F) * 0.1F) : 0;

		AnimationUtils.bobModelPart(this.leftArm, ageInTicks * 0.5F, -0.5F);

		float rightArmAngle = -0.5236F, bendAngle = -0.9163F;
		this.rightArm.xRot += rightArmAngle;
		this.rightArmBend.xRot += bendAngle;
		this.caneHandle.xRot += 1.4399F;

		float rightArmIdleRange = 1.0F;
		AnimationUtils.bobModelPart(this.rightArmBend, ageInTicks * 0.5F, rightArmIdleRange);
		this.rightArmBend.zRot -= 0.025F;
		AnimationUtils.bobModelPart(this.caneHandle, ageInTicks * 0.5F, -rightArmIdleRange);
		this.caneHandle.zRot += 0.025F;

		float limbSpeed = 0.6662F;

		rightArm.xRot += ((Mth.cos(limbSwing * limbSpeed + Mth.PI) * limbSwingAmount) + ((rightArmAngle) * limbSwingAmount)) * 0.5F;
		float a = Math.min(0, (-0.1F * limbSwingAmount) + ((Mth.sin(0.5F - limbSwing * limbSpeed + Mth.PI) * limbSwingAmount) * 0.7F));
		rightArmBend.xRot += -this.rightArm.xRot + rightArmAngle + a;
		caneHandle.xRot -= (Mth.cos(0.5F - limbSwing * limbSpeed) * limbSwingAmount) * 0.4F;

		leftLeg.xRot = Mth.sin(limbSwing * limbSpeed) * limbSwingAmount;
		rightLeg.xRot = Mth.sin(limbSwing * limbSpeed + Mth.PI) * limbSwingAmount;
		rightLeg.yRot = 0.0F;
		leftLeg.yRot = 0.0F;
		backCape.xRot = limbSwingAmount * 0.4F;

		if (!(this.attackTime <= 0.0F))
		{
			float f = this.attackTime;
			this.body.yRot = Mth.sin(Mth.sqrt(f) * ((float) Math.PI * 2F)) * 0.2F;

			this.rightArm.z = Mth.sin(this.body.yRot) * 5.0F;
			this.rightArm.x = -Mth.cos(this.body.yRot) * 5.0F;
			this.leftArm.z = -Mth.sin(this.body.yRot) * 5.0F;
			this.leftArm.x = Mth.cos(this.body.yRot) * 5.0F;
			this.rightArm.yRot += this.body.yRot;
			this.leftArm.yRot += this.body.yRot;
			this.leftArm.xRot += this.body.yRot;
			f = 1.0F - this.attackTime;
			f *= f;
			f *= f;
			f = 1.0F - f;
			float f1 = Mth.sin(f * (float) Math.PI);
			float f2 = Mth.sin(this.attackTime * (float) Math.PI) * -(this.mouth.xRot - 0.7F) * 0.75F;
			rightArm.xRot -= f1 * 1.2F + f2;
			/*rightArm.yRot += this.body.yRot * 2.0F;*/
			rightArm.zRot += Mth.sin(this.attackTime * (float) Math.PI) * -0.4F;

			this.caneHandle.xRot -= Mth.sin(f * Mth.PI) * 1.2F + f2;
		}
	}
}