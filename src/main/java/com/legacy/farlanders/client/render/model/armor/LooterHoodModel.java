package com.legacy.farlanders.client.render.model.armor;

import java.util.List;

import com.legacy.farlanders.TheFarlandersMod;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.LivingEntity;

public class LooterHoodModel<T extends LivingEntity> extends HumanoidModel<T>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/models/armor/looter_layer_1.png");

	private final ModelPart helmet;

	public LooterHoodModel(ModelPart root)
	{
		super(root);
		this.helmet = root.getChild("helmet");
	}

	public static LayerDefinition createHelmetLayer()
	{
		MeshDefinition meshdefinition = HumanoidModel.createMesh(CubeDeformation.NONE, 0.0F);
		PartDefinition partdefinition = meshdefinition.getRoot();

		partdefinition.addOrReplaceChild("helmet", CubeListBuilder.create().texOffs(0, 0).mirror().addBox(-4.0F, -8.0F, -4.0F, 8.0F, 11.0F, 8.0F, new CubeDeformation(0.501F)).mirror(false), PartPose.offset(0.0F, -1.0F, 1.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	protected Iterable<ModelPart> headParts()
	{
		this.helmet.copyFrom(this.head);
		return List.of(this.helmet);
	}

	@Override
	protected Iterable<ModelPart> bodyParts()
	{
		return List.of();
	}

	@Override
	public void renderToBuffer(PoseStack matrixStackIn, VertexConsumer bufferIn, int packedLightIn, int packedOverlayIn, float red, float green, float blue, float alpha)
	{
		VertexConsumer ivertexbuilder = Minecraft.getInstance().renderBuffers().bufferSource().getBuffer(RenderType.entityTranslucent(TEXTURE));
		super.renderToBuffer(matrixStackIn, ivertexbuilder, packedLightIn, packedOverlayIn, red, green, blue, alpha);
	}
}