package com.legacy.farlanders.client.render.model;

import com.legacy.farlanders.entity.hostile.EnderGolemEntity;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;

public class EnderGolemModel<T extends EnderGolemEntity> extends HierarchicalModel<T>
{
	private final ModelPart root;

	protected final ModelPart mouth, head;
	protected final ModelPart rightArm;
	protected final ModelPart leftArm;
	protected final ModelPart rightLeg;
	protected final ModelPart leftLeg;
	protected final ModelPart waist;
	protected final ModelPart neck;

	public boolean isAttacking;

	public EnderGolemModel(ModelPart root)
	{
		this.root = root;

		this.mouth = root.getChild("mouth");
		this.head = this.mouth.getChild("head");
		this.rightArm = root.getChild("right_arm");
		this.leftArm = root.getChild("left_arm");
		this.rightLeg = root.getChild("right_leg");
		this.leftLeg = root.getChild("left_leg");
		this.waist = root.getChild("waist");
		this.neck = root.getChild("neck");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition root = meshdefinition.getRoot();

		PartDefinition mouth = root.addOrReplaceChild("mouth", CubeListBuilder.create().texOffs(0, 18).addBox(-3.5F, -8.0F, -3.5F, 7.0F, 8.0F, 7.0F, new CubeDeformation(0.001F)), PartPose.offset(0.0F, -28.0F, 0.5F));

		PartDefinition head = mouth.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.5F, -8.0F, -4.0F, 9.0F, 9.0F, 9.0F), PartPose.offset(0.0F, -1.0F, -0.5F));

		PartDefinition left_lower_horn = head.addOrReplaceChild("left_lower_horn", CubeListBuilder.create().texOffs(54, 39).addBox(0.0F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F), PartPose.offset(4.5F, -5.0F, 1.0F));
		left_lower_horn.addOrReplaceChild("left_upper_horn", CubeListBuilder.create().texOffs(56, 44).addBox(0.0F, -4.0F, -1.0F, 2.0F, 5.0F, 2.0F), PartPose.offset(3.0F, 0.0F, 0.0F));

		PartDefinition right_lower_horn = head.addOrReplaceChild("right_lower_horn", CubeListBuilder.create().texOffs(54, 39).mirror().addBox(-3.0F, -1.0F, -1.0F, 3.0F, 2.0F, 2.0F).mirror(false), PartPose.offset(-4.5F, -5.0F, 1.0F));

		right_lower_horn.addOrReplaceChild("right_upper_horn", CubeListBuilder.create().texOffs(56, 44).mirror().addBox(-2.0F, -4.0F, -1.0F, 2.0F, 5.0F, 2.0F).mirror(false), PartPose.offset(-3.0F, 0.0F, 0.0F));

		root.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(52, 0).addBox(-1.5F, -2.0F, -1.0F, 3.0F, 35.0F, 3.0F), PartPose.offset(-7.0F, -23.0F, 0.0F));
		root.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(52, 0).mirror().addBox(-1.5F, -2.0F, -1.0F, 3.0F, 35.0F, 3.0F).mirror(false), PartPose.offset(7.0F, -23.0F, 0.0F));

		root.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(39, 0).mirror().addBox(-3.0F, -1.5F, -1.5F, 3.0F, 34.0F, 3.0F).mirror(false), PartPose.offset(-1.5F, -8.5F, 0.5F));
		root.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(39, 0).addBox(0.0F, -1.5F, -1.5F, 3.0F, 34.0F, 3.0F), PartPose.offset(1.5F, -8.5F, 0.5F));

		PartDefinition waist = root.addOrReplaceChild("waist", CubeListBuilder.create().texOffs(0, 54).addBox(-1.5F, -3.0F, -1.5F, 3.0F, 6.0F, 3.0F), PartPose.offset(0.0F, -10.0F, 0.5F));

		PartDefinition lower_body = waist.addOrReplaceChild("lower_body", CubeListBuilder.create().texOffs(0, 44).addBox(-4.5F, -7.0F, -1.5F, 9.0F, 7.0F, 3.0F), PartPose.offset(0.0F, -3.0F, 0.0F));
		lower_body.addOrReplaceChild("upper_body", CubeListBuilder.create().texOffs(0, 33).addBox(-5.5F, -6.0F, -2.5F, 11.0F, 6.0F, 5.0F), PartPose.offset(0.0F, -7.0F, 0.0F));

		root.addOrReplaceChild("neck", CubeListBuilder.create().texOffs(27, 4).addBox(-1.5F, -2.0F, -1.5F, 3.0F, 2.0F, 3.0F), PartPose.offset(0.0F, -26.0F, 0.5F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public ModelPart root()
	{
		return this.root;
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		float speed = this.getAnimSpeed(entity);
		this.rightLeg.xRot = Mth.cos(limbSwing * speed) * 1.1F * limbSwingAmount;
		this.leftLeg.xRot = Mth.cos(limbSwing * speed + (float) Math.PI) * 1.1F * limbSwingAmount;

		rightLeg.yRot = 0.0F;
		leftLeg.yRot = 0.0F;
		this.head.y = -1.0F;

		float yaw = netHeadYaw / 57.29578F;
		float pitch = headPitch / 57.29578F;
		this.mouth.yRot = yaw;
		this.mouth.xRot = pitch;

		/*this.head.y = var7 - 3.0F;
		this.mouth.y = var7 - 2.0F;*/

		if (entity.isAggressive())
			this.head.y += -1.0F * 5.0F;
	}

	@Override
	public void prepareMobModel(T entity, float limbSwing, float limbSwingAmount, float partialTick)
	{
		float speed = this.getAnimSpeed(entity);
		int i = entity.getAttackTimer();

		/*if (entity.getAttackTimer() <= 0)
		{
			this.rightArm.setRotation(0, 0, 0);
			this.leftArm.setRotation(0, 0, 0);
		
			leftArm.xRot = (Mth.cos(0.2F + limbSwing * speed) * limbSwingAmount) * 1.5F;
			rightArm.xRot = (Mth.cos(0.2F + limbSwing * speed + Mth.PI) * limbSwingAmount) * 1.5F;
		
			AnimationUtils.bobArms(rightArm, leftArm, ageInTicks);
		}*/

		this.rightArm.setRotation(0, 0, 0);
		this.leftArm.setRotation(0, 0, 0);

		if (i > 0)
		{
			this.rightArm.xRot += -2.0F + 2.0F * Mth.triangleWave((float) i - partialTick, 10.0F);
			this.leftArm.xRot += -2.0F + 2.0F * Mth.triangleWave((float) i - partialTick, 10.0F);
		}
		else
		{
			this.rightArm.xRot = Mth.cos(0.3F + limbSwing * speed + Mth.PI) * 2.0F * limbSwingAmount * 0.8F;
			this.leftArm.xRot = Mth.cos(0.3F + limbSwing * speed) * 2.0F * limbSwingAmount * 0.8F;

			AnimationUtils.bobArms(rightArm, leftArm, entity.tickCount + partialTick);

			float outZ = (0.1F * limbSwingAmount);

			this.rightArm.zRot += outZ;
			this.leftArm.zRot -= outZ;
		}

		/*if (entityIn instanceof EnderColossusShadowEntity)
		{
			int i = ((EnderColossusShadowEntity) entityIn).getAttackTimer();
			if (i > 0)
			{
				this.RightArm.xRot = -2.0F + 2.0F * this.triangleWave((float) i - partialTick, 10.0F);
				this.LeftArm.xRot = -2.0F + 2.0F * this.triangleWave((float) i - partialTick, 10.0F);
			}
			else
			{
				this.RightArm.xRot = (-0.2F + 1.5F * this.triangleWave(limbSwing, 13.0F)) * limbSwingAmount;
				this.LeftArm.xRot = (-0.2F - 1.5F * this.triangleWave(limbSwing, 13.0F)) * limbSwingAmount;
			}
		}*/

	}

	public float getAnimSpeed(T entity)
	{
		return /*entity instanceof EnderColossusShadowEntity ? 0.2262F :*/ 0.5662F;
	}
}