package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.MultiEyeGlowingLayer;
import com.legacy.farlanders.client.render.model.FarlanderModel;
import com.legacy.farlanders.entity.FarlanderEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;

public class FarlanderRenderer<T extends FarlanderEntity> extends MobRenderer<T, FarlanderModel<T>>
{
	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/farlander/farlander.png");

	public FarlanderRenderer(EntityRendererProvider.Context context)
	{
		super(context, new FarlanderModel<>(context.bakeLayer(FLRenderRefs.FARLANDER)), 0.5F);
		this.addLayer(new ItemInHandLayer<T, FarlanderModel<T>>(this, context.getItemInHandRenderer()));
		this.addLayer(new MultiEyeGlowingLayer<>(this, "farlander/"));
	}

	@Override
	protected void scale(T entity, PoseStack pose, float f)
	{
		float scale = entity.isBaby() ? 0.4375F : 0.9375F;
		pose.scale(scale, scale, scale);
	}

	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}