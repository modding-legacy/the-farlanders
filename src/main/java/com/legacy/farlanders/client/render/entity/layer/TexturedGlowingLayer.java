package com.legacy.farlanders.client.render.entity.layer;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.EyesLayer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class TexturedGlowingLayer<T extends Entity, M extends EntityModel<T>> extends EyesLayer<T, M>
{
	private final RenderType renderType;

	public TexturedGlowingLayer(RenderLayerParent<T, M> rendererIn, ResourceLocation texture)
	{
		super(rendererIn);
		this.renderType = RenderType.eyes(texture);
	}

	@Override
	public RenderType renderType()
	{
		return this.renderType;
	}
}