package com.legacy.farlanders.client.render.entity;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.entity.layer.MultiEyeGlowingLayer;
import com.legacy.farlanders.client.render.model.EnderGuardianModel;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.layers.ItemInHandLayer;
import net.minecraft.resources.ResourceLocation;

public class EnderGuardianRenderer<T extends EnderGuardianEntity> extends MobRenderer<T, EnderGuardianModel<T>>
{

	private static final ResourceLocation TEXTURE = TheFarlandersMod.locate("textures/entity/guardian/ender_guardian.png");

	public EnderGuardianRenderer(EntityRendererProvider.Context context)
	{
		super(context, new EnderGuardianModel<>(context.bakeLayer(FLRenderRefs.ENDER_GUARDIAN)), 0.5F);
		this.addLayer(new ItemInHandLayer<T, EnderGuardianModel<T>>(this, context.getItemInHandRenderer()));
		this.addLayer(new MultiEyeGlowingLayer<>(this, "guardian/"));
	}

	@Override
	protected void scale(T entityliving, PoseStack matrixStackIn, float f)
	{
		float f1 = 0.9375F;
		matrixStackIn.scale(f1, f1, f1);
	}

	public ResourceLocation getTextureLocation(T entity)
	{
		return TEXTURE;
	}
}