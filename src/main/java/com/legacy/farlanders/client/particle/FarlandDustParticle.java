package com.legacy.farlanders.client.particle;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.TextureSheetParticle;
import net.minecraft.world.item.DyeColor;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class FarlandDustParticle extends TextureSheetParticle
{
	private final double xStart;
	private final double yStart;
	private final double zStart;

	public FarlandDustParticle(ClientLevel pLevel, double pX, double pY, double pZ, double pXSpeed, double pYSpeed, double pZSpeed, FarlandDustData data)
	{
		super(pLevel, pX, pY, pZ);
		this.xd = pXSpeed;
		this.yd = pYSpeed;
		this.zd = pZSpeed;
		this.x = pX;
		this.y = pY;
		this.z = pZ;
		this.xStart = this.x;
		this.yStart = this.y;
		this.zStart = this.z;
		this.quadSize = 0.1F * (this.random.nextFloat() * 0.2F + 0.5F);
		this.setColor(data.getRed(), data.getGreen(), data.getBlue());

		this.lifetime = (int) (Math.random() * 10.0D) + 40;
	}

	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.PARTICLE_SHEET_OPAQUE;
	}

	public void move(double pX, double pY, double pZ)
	{
		this.setBoundingBox(this.getBoundingBox().move(pX, pY, pZ));
		this.setLocationFromBoundingbox();
	}

	public float getQuadSize(float pScaleFactor)
	{
		float f = ((float) this.age + pScaleFactor) / (float) this.lifetime;
		f = 1.0F - f;
		f *= f;
		f = 1.0F - f;
		return this.quadSize * f;
	}

	public int getLightColor(float pPartialTick)
	{
		int i = super.getLightColor(pPartialTick);
		float f = (float) this.age / (float) this.lifetime;
		f *= f;
		f *= f;
		int j = i & 255;
		int k = i >> 16 & 255;
		k += (int) (f * 15.0F * 16.0F);
		if (k > 240)
		{
			k = 240;
		}

		return j | k << 16;
	}

	public void tick()
	{
		this.xo = this.x;
		this.yo = this.y;
		this.zo = this.z;
		if (this.age++ >= this.lifetime)
		{
			this.remove();
		}
		else
		{
			float f = (float) this.age / (float) this.lifetime;
			float f1 = -f + f * f * 2.0F;
			float f2 = 1.0F - f1;
			this.x = this.xStart + this.xd * (double) f2;
			this.y = this.yStart + this.yd * (double) f2 + (double) (1.0F - f);
			this.z = this.zStart + this.zd * (double) f2;
			this.setPos(this.x, this.y, this.z);
		}
	}

	public static FarlandDustData dyeToRGB(DyeColor color)
	{
		float[] rgb = color.getTextureDiffuseColors();
		float s = (color == DyeColor.BLACK ? 0.9F : 1.15F) + (float) (Math.random() * 0.10);
		return new FarlandDustData(Math.min(rgb[0] * s, 1.0F), Math.min(rgb[1] * s, 1.0F), Math.min(rgb[2] * s, 1.0F));
	}

	public static FarlandDustData rgb(int r, int g, int b)
	{
		return new FarlandDustData((float) r / 255F, (float) g / 255F, (float) b / 255F);
	}

	@OnlyIn(Dist.CLIENT)
	public static class Provider implements ParticleProvider<FarlandDustData>
	{
		private final SpriteSet sprite;

		public Provider(SpriteSet pSprite)
		{
			this.sprite = pSprite;
		}

		public Particle createParticle(FarlandDustData pType, ClientLevel pLevel, double pX, double pY, double pZ, double pXSpeed, double pYSpeed, double pZSpeed)
		{
			FarlandDustParticle portalparticle = new FarlandDustParticle(pLevel, pX, pY, pZ, pXSpeed, pYSpeed, pZSpeed, pType);
			portalparticle.pickSprite(this.sprite);
			return portalparticle;
		}
	}
}
