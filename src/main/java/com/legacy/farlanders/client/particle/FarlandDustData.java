package com.legacy.farlanders.client.particle;

import java.util.Locale;

import com.legacy.farlanders.registry.FLParticles;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.network.FriendlyByteBuf;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class FarlandDustData implements ParticleOptions
{
	@SuppressWarnings("deprecation")
	public static final ParticleOptions.Deserializer<FarlandDustData> DESERIALIZER = new ParticleOptions.Deserializer<FarlandDustData>()
	{
		public FarlandDustData fromCommand(ParticleType<FarlandDustData> particleTypeIn, StringReader reader) throws CommandSyntaxException
		{
			reader.expect(' ');
			float r = (float) reader.readDouble();
			reader.expect(' ');
			float g = (float) reader.readDouble();
			reader.expect(' ');
			float b = (float) reader.readDouble();
			return new FarlandDustData(r, g, b);
		}

		public FarlandDustData fromNetwork(ParticleType<FarlandDustData> particleTypeIn, FriendlyByteBuf buffer)
		{
			return new FarlandDustData(buffer.readFloat(), buffer.readFloat(), buffer.readFloat());
		}
	};

	public static final Codec<FarlandDustData> CODEC = RecordCodecBuilder.create((instance) ->
	{
		return instance.group(Codec.FLOAT.fieldOf("r").forGetter((starFlareData) ->
		{
			return starFlareData.getRed();
		}), Codec.FLOAT.fieldOf("g").forGetter((starFlareData) ->
		{
			return starFlareData.getGreen();
		}), Codec.FLOAT.fieldOf("b").forGetter((starFlareData) ->
		{
			return starFlareData.getBlue();
		})).apply(instance, FarlandDustData::new);
	});

	private final float red;
	private final float green;
	private final float blue;

	public FarlandDustData(float r, float g, float b)
	{
		this.red = r;
		this.green = g;
		this.blue = b;
	}

	@Override
	public void writeToNetwork(FriendlyByteBuf buffer)
	{
		buffer.writeFloat(this.red);
		buffer.writeFloat(this.green);
		buffer.writeFloat(this.blue);
	}

	@Override
	public String writeToString()
	{
		return String.format(Locale.ROOT, "%s %.2f %.2f %.2f", BuiltInRegistries.PARTICLE_TYPE.getKey(this.getType()), this.red, this.green, this.blue);
	}

	@Override
	public ParticleType<FarlandDustData> getType()
	{
		return FLParticles.FARLAND_DUST;
	}

	@OnlyIn(Dist.CLIENT)
	public float getRed()
	{
		return this.red;
	}

	@OnlyIn(Dist.CLIENT)
	public float getGreen()
	{
		return this.green;
	}

	@OnlyIn(Dist.CLIENT)
	public float getBlue()
	{
		return this.blue;
	}
}
