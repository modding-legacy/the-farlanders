package com.legacy.farlanders.client;

import com.legacy.farlanders.TheFarlandersMod;

import net.minecraft.client.model.geom.ModelLayerLocation;

// ModelLayers
public class FLRenderRefs
{
	public static final ModelLayerLocation FARLANDER = layer("farlander");
	public static final ModelLayerLocation ELDER_FARLANDER = layer("elder_farlander");
	public static final ModelLayerLocation ENDER_GUARDIAN = layer("ender_guardian");
	public static final ModelLayerLocation WANDERER = layer("wanderer");
	public static final ModelLayerLocation ENDERMINION = layer("enderminion");
	public static final ModelLayerLocation MYSTIC_ENDERMAN = layer("mystic_enderman");
	public static final ModelLayerLocation ENDER_GOLEM = layer("ender_golem");
	public static final ModelLayerLocation TITAN = layer("titan");

	public static final ModelLayerLocation NIGHTFALL_HELMET = layer("nightfall_helmet");
	public static final ModelLayerLocation REBEL_HELMET = layer("rebel_helmet");
	public static final ModelLayerLocation LOOTER_HOOD = layer("looter_hood");

	private static ModelLayerLocation layer(String name)
	{
		return layer(name, "main");
	}

	private static ModelLayerLocation layer(String name, String layer)
	{
		return new ModelLayerLocation(TheFarlandersMod.locate(name), layer);
	}
}
