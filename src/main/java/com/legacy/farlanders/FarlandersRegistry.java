package com.legacy.farlanders;

import com.legacy.farlanders.registry.FLBlocks;
import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLFeatures;
import com.legacy.farlanders.registry.FLItems;
import com.legacy.farlanders.registry.FLJigsawTypes;
import com.legacy.farlanders.registry.FLSchedules;
import com.legacy.farlanders.registry.FLSounds;
import com.legacy.farlanders.registry.FLTriggers;
import com.legacy.structure_gel.api.registry.StructureGelRegistries;

import net.minecraft.core.registries.Registries;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

public class FarlandersRegistry
{
	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(Registries.ENTITY_TYPE))
			FLEntityTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.ITEM))
			FLItems.init(event);
		else if (event.getRegistryKey().equals(Registries.BLOCK))
			FLBlocks.init(event);
		else if (event.getRegistryKey().equals(Registries.SOUND_EVENT))
			FLSounds.init();
		else if (event.getRegistryKey().equals(Registries.SCHEDULE))
			FLSchedules.init(event);
		else if (event.getRegistryKey().equals(Registries.FEATURE))
			FLFeatures.init(event);
		else if (event.getRegistryKey().equals(Registries.PARTICLE_TYPE))
			com.legacy.farlanders.registry.FLParticles.init(event);
		else if (event.getRegistryKey().equals(Registries.TRIGGER_TYPE))
			FLTriggers.init();
		else if (event.getRegistryKey().equals(StructureGelRegistries.Keys.JIGSAW_TYPE))
			FLJigsawTypes.init(event);
	}
}