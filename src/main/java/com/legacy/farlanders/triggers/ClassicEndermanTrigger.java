package com.legacy.farlanders.triggers;

import java.util.Optional;

import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLTriggers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ContextAwarePredicate;
import net.minecraft.advancements.critereon.EntityFlagsPredicate;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.EntityTypePredicate;
import net.minecraft.advancements.critereon.ItemPredicate;
import net.minecraft.advancements.critereon.SimpleCriterionTrigger;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.ExtraCodecs;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.block.Block;

public class ClassicEndermanTrigger extends SimpleCriterionTrigger<ClassicEndermanTrigger.Instance>
{
	public void trigger(ServerPlayer player, Entity entity, Block heldBlock)
	{
		this.trigger(player, instance -> instance.test(player, entity, heldBlock));
	}

	@Override
	public Codec<ClassicEndermanTrigger.Instance> codec()
	{
		return ClassicEndermanTrigger.Instance.CODEC;
	}

	public static record Instance(Optional<ContextAwarePredicate> player, Optional<EntityPredicate> entity, Optional<ItemPredicate> item) implements SimpleCriterionTrigger.SimpleInstance
	{

		// @formatter:off
		public static final Codec<ClassicEndermanTrigger.Instance> CODEC = 
				RecordCodecBuilder.create(instance -> instance.group(
						ExtraCodecs.strictOptionalField(EntityPredicate.ADVANCEMENT_CODEC, "player").forGetter(ClassicEndermanTrigger.Instance::player), 
						ExtraCodecs.strictOptionalField(EntityPredicate.CODEC, "entity").forGetter(ClassicEndermanTrigger.Instance::entity),
                        ExtraCodecs.strictOptionalField(ItemPredicate.CODEC, "item").forGetter(ClassicEndermanTrigger.Instance::item))
						.apply(instance, ClassicEndermanTrigger.Instance::new));
		// @formatter:on

		public static Criterion<ClassicEndermanTrigger.Instance> withHeldBlockOnFire(Block block)
		{
			return withEntityHoldingBlock(EntityFlagsPredicate.Builder.flags().setOnFire(true), block);
		}

		public static Criterion<ClassicEndermanTrigger.Instance> withEntityHoldingBlock(EntityFlagsPredicate.Builder flags, Block block)
		{
			return FLTriggers.CLASSIC_ENDERMAN_KILL.createCriterion(new ClassicEndermanTrigger.Instance(Optional.empty(), Optional.of(EntityPredicate.Builder.entity().entityType(EntityTypePredicate.of(FLEntityTypes.CLASSIC_ENDERMAN)).flags(flags).build()), Optional.of(ItemPredicate.Builder.item().of(block).build())));
		}

		public static Criterion<ClassicEndermanTrigger.Instance> withHeldBlock(Block block)
		{
			return FLTriggers.CLASSIC_ENDERMAN_KILL.createCriterion(new ClassicEndermanTrigger.Instance(Optional.empty(), Optional.of(EntityPredicate.Builder.entity().entityType(EntityTypePredicate.of(FLEntityTypes.CLASSIC_ENDERMAN)).build()), Optional.of(ItemPredicate.Builder.item().of(block).build())));
		}

		public static Criterion<ClassicEndermanTrigger.Instance> any()
		{
			return FLTriggers.CLASSIC_ENDERMAN_KILL.createCriterion(new ClassicEndermanTrigger.Instance(Optional.empty(), Optional.empty(), Optional.empty()));
		}

		public boolean test(ServerPlayer player, Entity entity, Block heldBlock)
		{
			if (!(!this.entity.isPresent() || this.entity.get().matches(player, entity)))
				return false;

			return this.item.isPresent() && this.item.get().matches(heldBlock.asItem().getDefaultInstance());
		}
	}

}
