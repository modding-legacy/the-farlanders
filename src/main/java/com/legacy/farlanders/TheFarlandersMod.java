package com.legacy.farlanders;

import java.util.logging.Logger;

import com.legacy.farlanders.client.FarlandersClientEvents;
import com.legacy.farlanders.data.FarlandersDataGen;
import com.legacy.farlanders.event.FarlandersEvents;
import com.legacy.farlanders.registry.FLBiomeModifiers;
import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLFeatures;
import com.legacy.farlanders.registry.FLProcessors;
import com.legacy.farlanders.registry.FLStructures;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModLoadingContext;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.NeoForge;

@Mod(TheFarlandersMod.MODID)
public class TheFarlandersMod
{
	public static final String NAME = "The Farlanders";
	public static final String MODID = "farlanders";
	public static final Logger LOGGER = Logger.getLogger(MODID);

	public static ResourceLocation locate(String name)
	{
		return new ResourceLocation(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public TheFarlandersMod(IEventBus modBus)
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, FarlandersConfig.COMMON_SPEC);
		/*ModLoadingContext.get().registerConfig(ModConfig.Type.SERVER, FarlandersConfig.SERVER_SPEC);*/

		RegistrarHandler.registerHandlers(MODID, modBus, FLFeatures.Configured.HANDLER, FLFeatures.Placed.HANDLER, FLProcessors.HANDLER, FLStructures.HANDLER, FLBiomeModifiers.HANDLER, FLBiomeModifiers.Serializers.SERIALIZER_HANDLER);

		modBus.addListener(FLEntityTypes::onAttributesRegistered);
		modBus.addListener(EventPriority.LOWEST, FLEntityTypes::registerPlacements);
		modBus.register(FarlandersRegistry.class);
		modBus.addListener(TheFarlandersMod::commonInit);

		if (FMLEnvironment.dist == Dist.CLIENT)
		{
			ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, FarlandersConfig.CLIENT_SPEC);

			modBus.addListener(TheFarlandersMod::clientInit);
			modBus.addListener(com.legacy.farlanders.registry.FLParticles.Register::registerParticleFactories);
		}

		modBus.register(FarlandersDataGen.class);
	}

	public static void clientInit(FMLClientSetupEvent event)
	{
		// MinecraftForge.EVENT_BUS.register(new FarlandersMusicHandler());
		NeoForge.EVENT_BUS.register(FarlandersClientEvents.class);
	}

	public static void commonInit(FMLCommonSetupEvent event)
	{
		NeoForge.EVENT_BUS.register(FarlandersEvents.class);
	}
}
