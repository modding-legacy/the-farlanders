package com.legacy.farlanders.item;

import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import com.legacy.farlanders.registry.FLItems;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.util.Lazy;

public class NightfallSwordItem extends SwordItem
{
	private static final Lazy<AttributeModifier> NIGHTFALL_SWORD_DAMAGE_MOD = Lazy.of(() -> new AttributeModifier(UUID.fromString("881e4a48-d16a-4081-97d1-92c0686c34a8"), "Nightfall Damage Boost", 3.0D, AttributeModifier.Operation.ADDITION));

	public NightfallSwordItem(Tier pTier, int pAttackDamageModifier, float pAttackSpeedModifier, Properties pProperties)
	{
		super(pTier, pAttackDamageModifier, pAttackSpeedModifier, pProperties);
	}

	@Override
	public void inventoryTick(ItemStack pStack, Level level, Entity pEntity, int pSlotId, boolean pIsSelected)
	{
		if (!level.isClientSide && pEntity instanceof LivingEntity living && living.getMainHandItem().equals(pStack))
		{
			var damageAttr = living.getAttribute(Attributes.ATTACK_DAMAGE);

			if (damageAttr != null)
			{
				if (!level.dimensionType().hasCeiling() && !level.isDay() || level.dimension().equals(Level.END))
				{
					if (!damageAttr.hasModifier(getNightfallBoost()))
						damageAttr.addTransientModifier(getNightfallBoost());
				}
				else if (damageAttr.hasModifier(getNightfallBoost()))
					damageAttr.removeModifier(getNightfallBoost().getId());
			}
		}
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> lore, TooltipFlag flagIn)
	{
		lore.add(Component.empty());
		lore.add(Component.translatable("gui.item.farlanders.night_use").withStyle(ChatFormatting.GRAY));
		lore.add(Component.literal("+" + getNightfallBoost().getAmount() + " ").append(Component.translatable(Attributes.ATTACK_DAMAGE.getDescriptionId())).withStyle(ChatFormatting.DARK_GREEN));
	}

	public static AttributeModifier getNightfallBoost()
	{
		return NIGHTFALL_SWORD_DAMAGE_MOD.get();
	}

	@Override
	public boolean isValidRepairItem(ItemStack pToRepair, ItemStack pRepair)
	{
		return pRepair.is(FLItems.ender_horn) || pRepair.is(Items.DIAMOND);
	}
}
