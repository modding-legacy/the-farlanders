package com.legacy.farlanders.item.wand;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class MysticWandBaseItem extends Item
{
	private ChatFormatting color = ChatFormatting.WHITE;

	public MysticWandBaseItem(Properties properties)
	{
		super(properties);
	}

	@Override
	public String getDescriptionId(ItemStack stack)
	{
		return "item.farlanders.mystic_wand";
	}

	@Override
	public Component getName(ItemStack pStack)
	{
		return super.getName(pStack).copy().withStyle(this.color);
	}

	protected ChatFormatting getTextColor()
	{
		return color;
	}

	protected void setTextColor(ChatFormatting color)
	{
		this.color = color;
	}
}
