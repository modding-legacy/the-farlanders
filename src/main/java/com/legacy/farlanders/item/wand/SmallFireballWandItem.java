package com.legacy.farlanders.item.wand;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.SmallFireball;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

public class SmallFireballWandItem extends MysticWandBaseItem
{
	public SmallFireballWandItem(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(ChatFormatting.GOLD);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		player.getCooldowns().addCooldown(this, 20);

		if (!level.isClientSide)
		{
			Vec3 look = player.getLookAngle();
			SmallFireball fireball2 = new SmallFireball(level, player, 1, 1, 1);
			fireball2.setPos(player.getX() + look.x, player.getY() + 1.5F + look.y, player.getZ() + look.z);
			fireball2.xPower = look.x * 0.1;
			fireball2.yPower = look.y * 0.1;
			fireball2.zPower = look.z * 0.1;
			level.addFreshEntity(fireball2);

			itemstack.hurtAndBreak(1, player, (p) -> p.broadcastBreakEvent(p.getUsedItemHand()));
		}

		level.playSound(null, player, FLSounds.ITEM_MYSTIC_WAND_CAST_FIREBALL, SoundSource.PLAYERS, 1.5F, 1.3F + ((level.random.nextFloat() - level.random.nextFloat()) * 0.1F));
		player.awardStat(Stats.ITEM_USED.get(this));
		return new InteractionResultHolder<>(InteractionResult.SUCCESS, itemstack);
	}

	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn)
	{
		tooltip.add(Component.translatable("gui.item.wand.small_fireball").withStyle(ChatFormatting.GRAY));
	}
}