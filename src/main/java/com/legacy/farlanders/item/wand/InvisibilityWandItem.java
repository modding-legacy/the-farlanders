package com.legacy.farlanders.item.wand;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.ChatFormatting;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class InvisibilityWandItem extends MysticWandBaseItem
{
	public InvisibilityWandItem(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(ChatFormatting.AQUA);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);

		if (!level.isClientSide)
		{
			if (!player.hasEffect(MobEffects.INVISIBILITY))
			{
				if (level instanceof ServerLevel sl)
					sl.sendParticles(ParticleTypes.POOF, player.getX(), player.getY() + (player.getBbHeight() / 2), player.getZ(), 30, 0.25D, 0.5D, 0.25D, 0.05D);

				player.getCooldowns().addCooldown(this, 30 * 20);

				level.playSound(null, player, FLSounds.ITEM_MYSTIC_WAND_CAST_INVIS, SoundSource.PLAYERS, 1.0F, 1.0F);
				player.addEffect(new MobEffectInstance(MobEffects.INVISIBILITY, 30 * 20, 0));
				player.awardStat(Stats.ITEM_USED.get(this));
				itemstack.hurtAndBreak(1, player, (p) -> p.broadcastBreakEvent(p.getUsedItemHand()));
			}
			else
				player.displayClientMessage(Component.translatable("gui.item.wand.invisibility.failure"), true);
		}

		return new InteractionResultHolder<>(InteractionResult.SUCCESS, itemstack);
	}

	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn)
	{
		tooltip.add(Component.translatable("gui.item.wand.invisibility").withStyle(ChatFormatting.GRAY));
	}
}