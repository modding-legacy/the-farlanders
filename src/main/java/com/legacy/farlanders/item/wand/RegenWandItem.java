package com.legacy.farlanders.item.wand;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.ChatFormatting;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class RegenWandItem extends MysticWandBaseItem
{
	public RegenWandItem(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(ChatFormatting.RED);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level level, Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);

		if (!level.isClientSide)
		{
			if (!player.hasEffect(MobEffects.REGENERATION))
			{
				if (level instanceof ServerLevel sl)
					sl.sendParticles(ParticleTypes.EFFECT, player.getX(), player.getY() + (player.getBbHeight() / 2), player.getZ(), 15, 0.25D, 0.5D, 0.25D, 0.5D);

				player.getCooldowns().addCooldown(this, 20 * 20);

				level.playSound(null, player, FLSounds.ITEM_MYSTIC_WAND_CAST_REGEN, SoundSource.PLAYERS, 1.5F, 1.0F);
				player.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 20 * 20, 0));
				player.awardStat(Stats.ITEM_USED.get(this));
				itemstack.hurtAndBreak(1, player, (p) -> p.broadcastBreakEvent(p.getUsedItemHand()));
			}
			else
			{
				Component chat = Component.translatable("gui.item.wand.regeneration.failure");
				player.displayClientMessage(chat, true);
				return new InteractionResultHolder<>(InteractionResult.FAIL, itemstack);
			}
		}

		return new InteractionResultHolder<>(InteractionResult.SUCCESS, itemstack);
	}

	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level level, List<Component> tooltip, TooltipFlag flagIn)
	{
		tooltip.add(Component.translatable("gui.item.wand.regeneration").withStyle(ChatFormatting.GRAY));
	}
}