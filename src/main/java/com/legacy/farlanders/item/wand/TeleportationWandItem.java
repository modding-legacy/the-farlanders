package com.legacy.farlanders.item.wand;

import java.util.List;
import java.util.Optional;

import javax.annotation.Nullable;

import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.core.GlobalPos;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RespawnAnchorBlock;
import net.minecraft.world.phys.Vec3;

public class TeleportationWandItem extends MysticWandBaseItem
{
	public TeleportationWandItem(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(ChatFormatting.LIGHT_PURPLE);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level levelIn, Player playerIn, InteractionHand handIn)
	{
		ItemStack itemstack = playerIn.getItemInHand(handIn);

		if (levelIn instanceof ServerLevel sl && playerIn instanceof ServerPlayer sp && sp.getRespawnPosition() != null)
		{
			Optional<Vec3> vec = Player.findRespawnPositionAndUseSpawnBlock(sl, sp.getRespawnPosition(), sp.yHeadRot, true, false);

			if (sp.getRespawnPosition() != null && vec.isPresent() && (sl.getBlockState(sp.getRespawnPosition()).getBlock() instanceof RespawnAnchorBlock || sl.getBlockState(sp.getRespawnPosition()).is(BlockTags.BEDS)))
			{
				levelIn.playSound(sp, sp.blockPosition(), SoundEvents.CHORUS_FRUIT_TELEPORT, SoundSource.PLAYERS, 1.0F, 1.0F);

				var p = vec.get();
				sp.teleportTo(p.x(), p.y(), p.z());
				levelIn.playSound(null, playerIn.blockPosition(), SoundEvents.CHORUS_FRUIT_TELEPORT, SoundSource.PLAYERS, 1.0F, 1.0F);

				sp.getCooldowns().addCooldown(this, sp.isCreative() ? 60 : (60 * 3) * 20);
				itemstack.hurtAndBreak(1, playerIn, (player) -> player.broadcastBreakEvent(playerIn.getUsedItemHand()));
			}
			else if (sl.dimensionType().natural())
			{
				BlockPos a = GlobalPos.of(sl.dimension(), sl.getSharedSpawnPos()).pos();
				sl.playSound(sp, sp.blockPosition(), SoundEvents.CHORUS_FRUIT_TELEPORT, SoundSource.PLAYERS, 1.0F, 1.0F);
				sp.teleportTo(a.getX() + 0.5F, a.getY(), a.getZ() + 0.5F);
				levelIn.playSound(null, playerIn.blockPosition(), SoundEvents.CHORUS_FRUIT_TELEPORT, SoundSource.PLAYERS, 1.0F, 1.0F);

				sp.getCooldowns().addCooldown(this, sp.isCreative() ? 60 : (60 * 3) * 20);
				itemstack.hurtAndBreak(1, playerIn, (player) -> player.broadcastBreakEvent(playerIn.getUsedItemHand()));
			}
			else
			{
				Component chat = Component.translatable("gui.item.wand.teleportation.failure");
				sp.displayClientMessage(chat, true);

				return new InteractionResultHolder<>(InteractionResult.SUCCESS, itemstack);
			}
		}

		playerIn.awardStat(Stats.ITEM_USED.get(this));

		return new InteractionResultHolder<>(InteractionResult.SUCCESS, itemstack);
	}

	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn)
	{
		tooltip.add(Component.translatable("gui.item.wand.teleportation").withStyle(ChatFormatting.GRAY));
	}
}