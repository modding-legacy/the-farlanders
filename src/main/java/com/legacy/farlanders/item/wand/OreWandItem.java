package com.legacy.farlanders.item.wand;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.registry.FLBlocks;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.item.enchantment.DigDurabilityEnchantment;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.item.enchantment.MendingEnchantment;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;

public class OreWandItem extends MysticWandBaseItem
{
	public OreWandItem(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(ChatFormatting.GREEN);
	}

	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Player player = context.getPlayer();
		Level level = context.getLevel();
		BlockPos blockpos = context.getClickedPos();
		int randomNr = 0;

		if (!player.mayUseItemAt(blockpos, null, context.getItemInHand()))
		{
			return InteractionResult.FAIL;
		}

		Block clickedBlock = level.getBlockState(blockpos).getBlock();

		boolean slate = clickedBlock == Blocks.DEEPSLATE;
		if (slate || clickedBlock == Blocks.STONE)
		{
			Block block = null;
			if (level.isClientSide())
			{
				return InteractionResult.SUCCESS;
			}
			else
			{
				boolean hasUnbreaking = this.getAllEnchantments(context.getItemInHand()).containsKey(Enchantments.UNBREAKING);
				randomNr = level.getRandom().nextInt(30);

				if (randomNr == 0 && !hasUnbreaking)
					block = slate ? Blocks.DEEPSLATE_DIAMOND_ORE : Blocks.DIAMOND_ORE;
				else if (randomNr >= 1 && randomNr <= 2 && !hasUnbreaking)
					block = slate ? Blocks.DEEPSLATE_GOLD_ORE : Blocks.GOLD_ORE;
				else if (randomNr >= 3 && randomNr <= 4)
					block = slate ? Blocks.DEEPSLATE_LAPIS_ORE : Blocks.LAPIS_ORE;
				else if (randomNr >= 5 && randomNr <= 7)
					block = slate ? Blocks.DEEPSLATE_REDSTONE_ORE : Blocks.REDSTONE_ORE;
				else if (randomNr >= 8 && randomNr <= 11)
					block = slate ? Blocks.DEEPSLATE_IRON_ORE : Blocks.IRON_ORE;
				else if (randomNr == 12 && !hasUnbreaking)
					block = slate ? Blocks.DEEPSLATE_EMERALD_ORE : Blocks.EMERALD_ORE;
				else if (randomNr == 13 && !hasUnbreaking)
					block = slate ? FLBlocks.deepslate_endumium_ore : FLBlocks.endumium_ore;
				else
					block = slate ? Blocks.DEEPSLATE_COAL_ORE : Blocks.COAL_ORE;

				if (level instanceof ServerLevel sl)
					sl.sendParticles(ParticleTypes.ENCHANTED_HIT, blockpos.getX() + 0.5F, blockpos.getY() + 0.5F, blockpos.getZ() + 0.5F, 35, 0.25D, 0.25D, 0.25D, 0.5D);

				level.setBlock(blockpos, block.defaultBlockState(), 2);
				player.getCooldowns().addCooldown(this, 60);
				player.awardStat(Stats.ITEM_USED.get(this));

				context.getItemInHand().hurtAndBreak(1, player, (p) -> p.broadcastBreakEvent(p.getUsedItemHand()));
				level.playSound(null, player, FLSounds.ITEM_MYSTIC_WAND_CAST_ORE, SoundSource.PLAYERS, 1.0F, 1.0F);

				return InteractionResult.SUCCESS;
			}
		}
		else if (clickedBlock == Blocks.NETHERRACK)
		{
			Block block = null;

			if (level.isClientSide())
			{
				return InteractionResult.SUCCESS;
			}
			else
			{
				block = Blocks.NETHER_QUARTZ_ORE;
				level.setBlock(blockpos, block.defaultBlockState(), 2);
				player.getCooldowns().addCooldown(this, 60);
				player.awardStat(Stats.ITEM_USED.get(this));

				context.getItemInHand().hurtAndBreak(1, player, (p) -> p.broadcastBreakEvent(player.getUsedItemHand()));
				level.playSound(null, player, FLSounds.ITEM_MYSTIC_WAND_CAST_ORE, SoundSource.PLAYERS, 1.0F, 1.0F);

				return InteractionResult.SUCCESS;
			}
		}
		else
		{
			player.displayClientMessage(Component.translatable("gui.item.wand.ore.failure"), true);
			return InteractionResult.FAIL;
		}
	}

	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn)
	{
		tooltip.add(Component.translatable("gui.item.wand.ore").withStyle(ChatFormatting.GRAY));
	}

	@Override
	public boolean canApplyAtEnchantingTable(ItemStack stack, Enchantment enchantment)
	{
		if (enchantment instanceof MendingEnchantment || enchantment instanceof DigDurabilityEnchantment)
			return false;

		return super.canApplyAtEnchantingTable(stack, enchantment);
	}
}