package com.legacy.farlanders.item.wand;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.LargeFireball;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ItemUtils;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.UseAnim;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

public class LargeFireballWandItem extends MysticWandBaseItem
{
	public LargeFireballWandItem(Item.Properties properties)
	{
		super(properties);
		this.setTextColor(ChatFormatting.DARK_RED);
	}

	@Override
	public int getUseDuration(ItemStack pStack)
	{
		return 72000;
	}

	@Override
	public UseAnim getUseAnimation(ItemStack pStack)
	{
		return UseAnim.BOW;
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level world, Player player, InteractionHand hand)
	{
		return ItemUtils.startUsingInstantly(world, player, hand);
	}

	@Override
	public void releaseUsing(ItemStack itemstack, Level level, LivingEntity entity, int timeLeft)
	{
		int i = this.getUseDuration(itemstack) - timeLeft;

		if (i >= 20)
		{
			if (entity instanceof Player player)
			{
				player.awardStat(Stats.ITEM_USED.get(this));
				player.getCooldowns().addCooldown(this, 40);
			}

			if (!level.isClientSide)
			{
				Vec3 look = entity.getLookAngle();
				LargeFireball fireball2 = new LargeFireball(level, entity, 1, 1, 1, 0);
				fireball2.setPos(entity.getX() + look.x, entity.getY() + 1.5F + look.y, entity.getZ() + look.z);
				fireball2.xPower = look.x * 0.1;
				fireball2.yPower = look.y * 0.1;
				fireball2.zPower = look.z * 0.1;
				level.addFreshEntity(fireball2);

				itemstack.hurtAndBreak(1, entity, (p) -> p.broadcastBreakEvent(p.getUsedItemHand()));
				entity.swing(entity.getUsedItemHand(), true);
			}

			level.playSound(null, entity, FLSounds.ITEM_MYSTIC_WAND_CAST_FIREBALL, SoundSource.PLAYERS, 1.5F, 1.0F + ((level.random.nextFloat() - level.random.nextFloat()) * 0.1F));
		}
	}

	@Override
	public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> tooltip, TooltipFlag flagIn)
	{
		tooltip.add(Component.translatable("gui.item.wand.large_fireball").withStyle(ChatFormatting.GRAY));
	}
}