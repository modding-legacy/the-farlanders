package com.legacy.farlanders.item;

import java.util.List;
import java.util.stream.Collectors;

import com.legacy.farlanders.TheFarlandersMod;
import com.legacy.farlanders.registry.FLBlocks;
import com.legacy.farlanders.registry.FLItems;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.item.CreativeModeTab.TabVisibility;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.level.ItemLike;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.common.util.MutableHashedLinkedMap;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;

@Mod.EventBusSubscriber(modid = TheFarlandersMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class FarlandersCreativeTabs
{
	public static final Lazy<List<Item>> SPAWN_EGGS = Lazy.of(() -> BuiltInRegistries.ITEM.stream().filter(item -> BuiltInRegistries.ITEM.getKey(item).getNamespace().equals(TheFarlandersMod.MODID) && item instanceof SpawnEggItem).sorted((o, n) -> BuiltInRegistries.ITEM.getKey(o).compareTo(BuiltInRegistries.ITEM.getKey(n))).collect(Collectors.toList()));

	@SubscribeEvent
	public static void modifyExisting(BuildCreativeModeTabContentsEvent event)
	{
		if (event.getTabKey() == CreativeModeTabs.SPAWN_EGGS)
		{
			for (Item item : SPAWN_EGGS.get())
				event.accept(item);
		}

		if (event.getTabKey() == CreativeModeTabs.COMBAT)
		{
			insertAfter(event.getEntries(), List.of(FLItems.nightfall_sword), Items.NETHERITE_SWORD);
			insertAfter(event.getEntries(), List.of(FLItems.nightfall_helmet, FLItems.nightfall_chestplate, FLItems.nightfall_leggings, FLItems.nightfall_boots), Items.NETHERITE_BOOTS);
			insertAfter(event.getEntries(), List.of(FLItems.rebel_farlander_helmet, FLItems.looter_hood), Items.TURTLE_HELMET);
			insertAfter(event.getEntries(), List.of(FLItems.mystic_wand_fire_small, FLItems.mystic_wand_fire_large, FLItems.mystic_wand_ore, FLItems.mystic_wand_teleport, FLItems.mystic_wand_regen, FLItems.mystic_wand_invisible), Items.CROSSBOW);
		}

		if (event.getTabKey() == CreativeModeTabs.INGREDIENTS)
		{
			insertAfter(event.getEntries(), List.of(FLItems.endumium_crystal), Items.EMERALD);
			insertAfter(event.getEntries(), List.of(FLItems.ender_horn, FLItems.titan_hide), Items.ENDER_EYE);
		}

		if (event.getTabKey() == CreativeModeTabs.NATURAL_BLOCKS)
			insertAfter(event.getEntries(), List.of(FLBlocks.endumium_ore, FLBlocks.deepslate_endumium_ore), Items.DEEPSLATE_EMERALD_ORE);

		if (event.getTabKey() == CreativeModeTabs.BUILDING_BLOCKS)
			insertAfter(event.getEntries(), List.of(FLBlocks.endumium_block), Items.EMERALD_BLOCK);
	}

	protected static void insertAfter(MutableHashedLinkedMap<ItemStack, TabVisibility> entries, List<ItemLike> items, ItemLike target)
	{
		ItemStack currentStack = null;

		for (var e : entries)
		{
			if (e.getKey().getItem() == target)
			{
				currentStack = e.getKey();
				break;
			}
		}

		for (var item : items)
			entries.putAfter(currentStack, currentStack = item.asItem().getDefaultInstance(), TabVisibility.PARENT_AND_SEARCH_TABS);
	}
}
