package com.legacy.farlanders.item.armor;

import com.legacy.farlanders.client.FLRenderRefs;
import com.legacy.farlanders.client.render.model.armor.LooterHoodModel;
import com.legacy.farlanders.client.render.model.armor.NightfallHelmetModel;
import com.legacy.farlanders.client.render.model.armor.RebelHelmetModel;
import com.legacy.farlanders.registry.FLItems;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.client.extensions.common.IClientItemExtensions;
import net.neoforged.neoforge.common.util.NonNullLazy;

public class FarlandersArmorItem extends ArmorItem
{
	public FarlandersArmorItem(ArmorMaterial materialIn, ArmorItem.Type slot, Properties builder)
	{
		super(materialIn, slot, builder);
	}

	@OnlyIn(Dist.CLIENT)
	protected static final class Rendering implements IClientItemExtensions
	{
		protected static final Rendering INSTANCE = new FarlandersArmorItem.Rendering();

		private final NonNullLazy<NightfallHelmetModel<LivingEntity>> nightfallHelmet = NonNullLazy.of(() -> new NightfallHelmetModel<LivingEntity>(getModel().bakeLayer(FLRenderRefs.NIGHTFALL_HELMET)));
		private final NonNullLazy<RebelHelmetModel<LivingEntity>> rebelHelmet = NonNullLazy.of(() -> new RebelHelmetModel<LivingEntity>(getModel().bakeLayer(FLRenderRefs.REBEL_HELMET)));
		private final NonNullLazy<LooterHoodModel<LivingEntity>> looterHood = NonNullLazy.of(() -> new LooterHoodModel<LivingEntity>(getModel().bakeLayer(FLRenderRefs.LOOTER_HOOD)));

		private Rendering()
		{
		}

		@Override
		public net.minecraft.client.model.HumanoidModel<?> getHumanoidArmorModel(LivingEntity wearer, ItemStack stack, EquipmentSlot slot, net.minecraft.client.model.HumanoidModel<?> defaultModel)
		{
			Item item = stack.getItem();

			return item == FLItems.looter_hood ? this.looterHood.get() : item == FLItems.rebel_farlander_helmet ? this.rebelHelmet.get() : this.nightfallHelmet.get();
		}

		@OnlyIn(Dist.CLIENT)
		private static net.minecraft.client.model.geom.EntityModelSet getModel()
		{
			return net.minecraft.client.Minecraft.getInstance().getEntityModels();
		}
	}
}
