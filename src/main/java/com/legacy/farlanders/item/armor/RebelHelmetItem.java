package com.legacy.farlanders.item.armor;

import java.util.function.Consumer;

import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.client.extensions.common.IClientItemExtensions;

public class RebelHelmetItem extends FarlandersArmorItem
{
	public RebelHelmetItem(ArmorMaterial materialIn, ArmorItem.Type slot, Properties builder)
	{
		super(materialIn, slot, builder);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void initializeClient(Consumer<IClientItemExtensions> consumer)
	{
		consumer.accept(Rendering.INSTANCE);
	}
}
