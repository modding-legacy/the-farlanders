package com.legacy.farlanders.item.armor;

import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.util.Lazy;

public class NightfallArmorItem extends FarlandersArmorItem
{
	private final Lazy<MobEffect> effect;

	public NightfallArmorItem(ArmorMaterial materialIn, ArmorItem.Type slot, Lazy<MobEffect> effect, Properties builder)
	{
		super(materialIn, slot, builder);
		this.effect = effect;
	}

	@Override
	public void onArmorTick(ItemStack stack, Level level, Player player)
	{
		super.onArmorTick(stack, level, player);
	}

	// FIXME: Change to onArmorTick
	@Override
	public void inventoryTick(ItemStack pStack, Level pLevel, Entity pEntity, int pSlotId, boolean pIsSelected)
	{
		if (!pLevel.isClientSide && (!pLevel.dimensionType().hasCeiling() && !pLevel.isDay() || pLevel.dimension().equals(Level.END)) && pEntity instanceof LivingEntity living && living.tickCount % 20 == 0 && pStack.is(this) && living.getItemBySlot(this.getType().getSlot()).equals(pStack))
		{
			int time = this.getEffect() == MobEffects.NIGHT_VISION ? 12 * 20 : 4 * 20;

			if (this.getEffect() != null && (living.getEffect(this.getEffect()) == null || living.getEffect(this.getEffect()).getAmplifier() <= 0))
				living.addEffect(new MobEffectInstance(this.getEffect(), time, 0, true, true, false));
		}
	}

	public MobEffect getEffect()
	{
		return this.effect.get();
	}

	@Override
	@OnlyIn(Dist.CLIENT)
	public void appendHoverText(ItemStack stack, @Nullable Level worldIn, List<Component> lore, TooltipFlag flagIn)
	{
		MobEffect effect = this.getEffect();

		if (effect != null)
		{
			lore.add(Component.empty());
			lore.add(Component.translatable("gui.item.farlanders.night_worn").withStyle(ChatFormatting.GRAY));
			lore.add(Component.literal(" ").append(Component.translatable(effect.getDescriptionId()).append(Component.literal(" I"))).withStyle(ChatFormatting.DARK_GREEN));
		}
	}
}
