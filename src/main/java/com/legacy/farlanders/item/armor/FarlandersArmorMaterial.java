package com.legacy.farlanders.item.armor;

import java.util.EnumMap;
import java.util.function.Supplier;

import com.legacy.farlanders.TheFarlandersMod;

import net.minecraft.Util;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.crafting.Ingredient;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.util.Lazy;

public enum FarlandersArmorMaterial implements ArmorMaterial
{
	// @formatter:off
	NIGHTFALL("nightfall", 15, armorValues(2, 5, 6, 2), 9, SoundEvents.ARMOR_EQUIP_IRON, 0.0F, 0.0F, () -> Ingredient.EMPTY), 
	REBEL("rebel", 7, armorValues(2, 5, 6, 2), 9, SoundEvents.ARMOR_EQUIP_TURTLE, 0.0F, 0.0F, () -> Ingredient.EMPTY), 
	LOOTER("looter", 7, armorValues(2, 5, 6, 2), 9, SoundEvents.ARMOR_EQUIP_LEATHER, 0.0F, 0.0F, () -> Ingredient.EMPTY);
	// @formatter:on

	private static final EnumMap<ArmorItem.Type, Integer> HEALTH_FUNCTION_FOR_TYPE = armorValues(13, 15, 16, 11);
	private final String name;
	private final int maxDamageFactor;
	private final EnumMap<ArmorItem.Type, Integer> damageReductionAmountArray;
	private final int enchantability;
	private final SoundEvent soundEvent;
	private final float toughness;
	private final float knockbackResist;
	private final Lazy<Ingredient> repairMaterial;

	private FarlandersArmorMaterial(String nameIn, int maxDamageFactorIn, EnumMap<ArmorItem.Type, Integer> damageReductionAmountsIn, int enchantabilityIn, SoundEvent equipSoundIn, float p_i48533_8_, float knockbackResistIn, Supplier<Ingredient> repairMaterialSupplier)
	{
		this.name = TheFarlandersMod.find(nameIn);
		this.maxDamageFactor = maxDamageFactorIn;
		this.damageReductionAmountArray = damageReductionAmountsIn;
		this.enchantability = enchantabilityIn;
		this.soundEvent = equipSoundIn;
		this.toughness = p_i48533_8_;
		this.knockbackResist = knockbackResistIn;
		this.repairMaterial = Lazy.of(repairMaterialSupplier);
	}

	@Override
	public int getDurabilityForType(ArmorItem.Type type)
	{
		return HEALTH_FUNCTION_FOR_TYPE.get(type) * this.maxDamageFactor;
	}

	@Override
	public int getDefenseForType(ArmorItem.Type type)
	{
		return this.damageReductionAmountArray.get(type);
	}

	public int getEnchantmentValue()
	{
		return this.enchantability;
	}

	public SoundEvent getEquipSound()
	{
		return this.soundEvent;
	}

	public Ingredient getRepairIngredient()
	{
		return this.repairMaterial.get();
	}

	@OnlyIn(Dist.CLIENT)
	public String getName()
	{
		return this.name;
	}

	public float getToughness()
	{
		return this.toughness;
	}

	@Override
	public float getKnockbackResistance()
	{
		return this.knockbackResist;
	}

	public static EnumMap<ArmorItem.Type, Integer> armorValues(int boots, int legs, int chest, int helmet)
	{
		return Util.make(new EnumMap<>(ArmorItem.Type.class), (material) ->
		{
			material.put(ArmorItem.Type.BOOTS, boots);
			material.put(ArmorItem.Type.LEGGINGS, legs);
			material.put(ArmorItem.Type.CHESTPLATE, chest);
			material.put(ArmorItem.Type.HELMET, helmet);
		});
	}
}