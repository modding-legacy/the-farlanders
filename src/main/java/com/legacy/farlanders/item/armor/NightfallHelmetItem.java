package com.legacy.farlanders.item.armor;

import java.util.function.Consumer;

import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.client.extensions.common.IClientItemExtensions;
import net.neoforged.neoforge.common.util.Lazy;

public class NightfallHelmetItem extends NightfallArmorItem
{
	public NightfallHelmetItem(ArmorMaterial materialIn, ArmorItem.Type slot, Lazy<MobEffect> effect, Properties builder)
	{
		super(materialIn, slot, effect, builder);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void initializeClient(Consumer<IClientItemExtensions> consumer)
	{
		consumer.accept(Rendering.INSTANCE);
	}
}
