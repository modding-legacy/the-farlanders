package com.legacy.farlanders;

import org.apache.commons.lang3.tuple.Pair;

import net.neoforged.neoforge.common.ModConfigSpec;

public class FarlandersConfig
{
	public static final Common COMMON;
	public static final ModConfigSpec COMMON_SPEC;
	static
	{
		Pair<Common, ModConfigSpec> specPair = new ModConfigSpec.Builder().configure(Common::new);
		COMMON_SPEC = specPair.getRight();
		COMMON = specPair.getLeft();
	}

	public static final Client CLIENT;
	public static final ModConfigSpec CLIENT_SPEC;
	static
	{
		Pair<Client, ModConfigSpec> specPair = new ModConfigSpec.Builder().configure(Client::new);
		CLIENT_SPEC = specPair.getRight();
		CLIENT = specPair.getLeft();
	}

	public static final Server SERVER;
	public static final ModConfigSpec SERVER_SPEC;
	static
	{
		Pair<Server, ModConfigSpec> specPair = new ModConfigSpec.Builder().configure(Server::new);
		SERVER_SPEC = specPair.getRight();
		SERVER = specPair.getLeft();
	}

	public static class Common
	{
		public final ModConfigSpec.ConfigValue<Boolean> disableLooterStealing;

		public final ModConfigSpec.ConfigValue<Integer> enderminionMinApples;
		public final ModConfigSpec.ConfigValue<Integer> enderminionTameChance;

		public Common(ModConfigSpec.Builder builder)
		{
			builder.comment("The Farlanders Common Configuration").push("common");

			disableLooterStealing = builder.translation("disableLooterStealing").comment("Disables the Looter's ability to take the player's weapon (This makes them spawn with a random weapon, good and bad)").define("disableLooterStealing", false);

			enderminionMinApples = builder.translation("enderminionMinApple").comment("Minimum apples required to tame an Enderminion").define("enderminionMinApple", 5);
			enderminionTameChance = builder.translation("enderminionTameChance").comment("Chance for an apple to successfully tame an Enderminion (1 in X)").define("enderminionTameChance", 7);

			builder.pop();
		}
	}

	public static class Client
	{
		public final ModConfigSpec.ConfigValue<Boolean> firstPersonNightfallParticles;

		public Client(ModConfigSpec.Builder builder)
		{
			builder.comment("The Farlanders Client Configuration").push("client");

			firstPersonNightfallParticles = builder.translation("firstPersonNightfallParticles").comment("If enabled, the particles shown by Nightfall armor will display in first person.").define("firstPersonNightfallParticles", true);

			builder.pop();
		}
	}

	// currently unused
	public static class Server
	{
		/*public final ForgeConfigSpec.ConfigValue<Double> nightfallSwordDamageBoost;*/

		public Server(ModConfigSpec.Builder builder)
		{
			builder.comment("The Farlanders World Configuration").push("world");

			/*nightfallSwordDamageBoost = builder.translation("nightfallSwordDamageBoost").comment("Damage boost the Nightfall Sword gains during nighttime").worldRestart().define("nightfallSwordDamageBoost", 3.0D);*/

			builder.pop();
		}
	}
}
