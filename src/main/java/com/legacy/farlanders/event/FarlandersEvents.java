package com.legacy.farlanders.event;

import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.entity.hostile.EnderGuardianEntity;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.entity.tameable.EnderminionEntity;
import com.legacy.farlanders.entity.util.FarlanderTrades;
import com.legacy.farlanders.item.NightfallSwordItem;
import com.legacy.farlanders.registry.FLItems;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.projectile.Arrow;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.bus.api.Event.Result;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.entity.EntityMobGriefingEvent;
import net.neoforged.neoforge.event.entity.EntityMountEvent;
import net.neoforged.neoforge.event.entity.living.EnderManAngerEvent;
import net.neoforged.neoforge.event.entity.living.LivingAttackEvent;
import net.neoforged.neoforge.event.entity.living.LivingChangeTargetEvent;
import net.neoforged.neoforge.event.entity.living.LivingEquipmentChangeEvent;
import net.neoforged.neoforge.event.village.WandererTradesEvent;

public class FarlandersEvents
{
	@SubscribeEvent
	public static void onEquipmentChange(LivingEquipmentChangeEvent event)
	{
		if (event.getEntity().level().isClientSide())
			return;

		var damageAttr = event.getEntity().getAttribute(Attributes.ATTACK_DAMAGE);

		if (damageAttr != null)
		{
			// reset nightfall sword attribute when not holding
			if (event.getFrom().is(FLItems.nightfall_sword) && !event.getTo().is(FLItems.nightfall_sword) && damageAttr.hasModifier(NightfallSwordItem.getNightfallBoost()))
				damageAttr.removeModifier(NightfallSwordItem.getNightfallBoost().getId());
		}

	}

	@SubscribeEvent
	public static void onMobGrief(EntityMobGriefingEvent event)
	{
		// don't want the mystic's fireballs setting fire
		if (event.getEntity() instanceof EnderminionEntity)
			event.setResult(Result.DENY);
	}

	@SubscribeEvent
	public static void onEndermanAnger(EnderManAngerEvent event)
	{
		if (wearingFullNightfall(event.getPlayer()))
			event.setCanceled(true);
	}

	@SubscribeEvent
	public static void onMount(EntityMountEvent event)
	{
		if (!event.getEntity().level().isClientSide() && event.isDismounting() && event.getEntityBeingMounted()instanceof TitanEntity t && t.isHoldingPlayer())
			event.setCanceled(true);
	}

	@SubscribeEvent
	public static void onDamage(LivingAttackEvent event)
	{
		Entity source = null;

		if (event.getEntity().getType().is(FLTags.Entities.FARLANDER_ALLIES) && (source = event.getSource().getEntity()) != null && source.isAlliedTo(event.getEntity()))
		{
			if (event.getSource().getDirectEntity()instanceof Arrow a)
				a.discard();

			event.setCanceled(true);
		}
	}

	@SubscribeEvent
	public static void onTargetChange(LivingChangeTargetEvent event)
	{
		if (event.getEntity() instanceof Creeper && event.getNewTarget() instanceof EnderGuardianEntity)
			event.setCanceled(true);
	}

	@SubscribeEvent
	public static void onTradesAssigned(WandererTradesEvent event)
	{
		FarlanderTrades.WanderingTrades.addWanderingTraderTrades(event);
	}

	public static boolean targettableByHostile(LivingEntity target)
	{
		return notWearingPumpkin(target) && !wearingFullNightfall(target);
	}

	public static boolean notWearingPumpkin(LivingEntity target)
	{
		return target.getItemBySlot(EquipmentSlot.HEAD).getItem() != Blocks.CARVED_PUMPKIN.asItem();
	}

	public static boolean wearingFullNightfall(LivingEntity target)
	{
		return target.getItemBySlot(EquipmentSlot.HEAD).is(FLItems.nightfall_helmet) && target.getItemBySlot(EquipmentSlot.CHEST).is(FLItems.nightfall_chestplate) && target.getItemBySlot(EquipmentSlot.LEGS).is(FLItems.nightfall_leggings) && target.getItemBySlot(EquipmentSlot.FEET).is(FLItems.nightfall_boots);
	}
}
