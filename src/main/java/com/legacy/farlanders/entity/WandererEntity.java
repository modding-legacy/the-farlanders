package com.legacy.farlanders.entity;

import java.util.EnumSet;
import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.legacy.farlanders.entity.hostile.TitanEntity;
import com.legacy.farlanders.entity.util.FarlanderTrades;
import com.legacy.farlanders.registry.FLBlocks;
import com.legacy.farlanders.registry.FLItems;
import com.legacy.farlanders.registry.FLSounds;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.InteractGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.LookAtTradingPlayerGoal;
import net.minecraft.world.entity.ai.goal.MoveTowardsRestrictionGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.TemptGoal;
import net.minecraft.world.entity.ai.goal.TradeWithPlayerGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.npc.AbstractVillager;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.item.trading.MerchantOffers;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.Vec3;

public class WandererEntity extends AbstractVillager
{
	public Int2ObjectMap<VillagerTrades.ItemListing[]> wandererTrades = new Int2ObjectOpenHashMap<>();
	private BlockPos targetWanderingPos;

	public WandererEntity(EntityType<? extends WandererEntity> typeIn, Level worldIn)
	{
		super(typeIn, worldIn);
		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(0, new DisguiseSelfGoal());

		this.goalSelector.addGoal(1, new TradeWithPlayerGoal(this));
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, TitanEntity.class, 20.0F, 0.5D, 0.5D));
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, LooterEntity.class, 8.0F, 0.5D, 0.5D));
		this.goalSelector.addGoal(1, new AvoidEntityGoal<>(this, Zombie.class, 8.0F, 0.5D, 0.5D));

		this.goalSelector.addGoal(2, new PanicGoal(this, 0.5D));
		this.goalSelector.addGoal(1, new LookAtTradingPlayerGoal(this));
		this.goalSelector.addGoal(2, new WandererEntity.MoveToGoal(this, 2.0D, 0.35D));
		this.goalSelector.addGoal(4, new MoveTowardsRestrictionGoal(this, 1.0D));
		this.goalSelector.addGoal(8, new WaterAvoidingRandomStrollGoal(this, 0.35D));
		this.goalSelector.addGoal(9, new InteractGoal(this, Player.class, 3.0F, 1.0F));
		this.goalSelector.addGoal(10, new LookAtPlayerGoal(this, Mob.class, 8.0F));

		this.goalSelector.addGoal(2, new TemptGoal(this, 0.6D, Ingredient.of(FLBlocks.endumium_block.asItem()), false));
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return Mob.createMobAttributes().add(Attributes.MOVEMENT_SPEED, 0.6D);
	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel worldIn, AgeableMob ageableIn)
	{
		return null;
	}

	@Override
	public boolean showProgressBar()
	{
		return false;
	}

	@Override
	public InteractionResult mobInteract(Player p_230254_1_, InteractionHand p_230254_2_)
	{
		ItemStack itemstack = p_230254_1_.getItemInHand(p_230254_2_);
		if (itemstack.getItem() != FLItems.wanderer_spawn_egg && this.isAlive() && !this.isTrading() && !this.isBaby())
		{
			if (p_230254_2_ == InteractionHand.MAIN_HAND)
			{
				p_230254_1_.awardStat(Stats.TALKED_TO_VILLAGER);
			}

			if (this.getOffers().isEmpty())
			{
				return InteractionResult.sidedSuccess(this.level().isClientSide());
			}
			else
			{
				if (!this.level().isClientSide())
				{
					this.setTradingPlayer(p_230254_1_);
					this.openTradingScreen(p_230254_1_, this.getDisplayName(), 1);
				}

				return InteractionResult.sidedSuccess(this.level().isClientSide());
			}
		}
		else
		{
			return super.mobInteract(p_230254_1_, p_230254_2_);
		}
	}

	@Override
	protected void updateTrades()
	{
		VillagerTrades.ItemListing[] wandererTradeList = FarlanderTrades.WANDERER_TRADES.get(1);

		if (wandererTradeList != null)
		{
			MerchantOffers merchantoffers = this.getOffers();
			this.addOffersFromItemListings(merchantoffers, wandererTradeList, 5);
		}
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		if (this.targetWanderingPos != null)
			compound.put("WanderTarget", NbtUtils.writeBlockPos(this.targetWanderingPos));
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);

		if (compound.contains("WanderTarget"))
			this.targetWanderingPos = NbtUtils.readBlockPos(compound.getCompound("WanderTarget"));

		this.setAge(Math.max(0, this.getAge()));
	}

	@Override
	public boolean removeWhenFarAway(double distanceToClosestPlayer)
	{
		return this.tickCount > 600 * 20 && super.removeWhenFarAway(distanceToClosestPlayer);
	}

	@Override
	protected void rewardTradeXp(MerchantOffer offer)
	{
		if (offer.shouldRewardExp())
		{
			int i = 3 + this.random.nextInt(4);
			this.level().addFreshEntity(new ExperienceOrb(this.level(), this.getX(), this.getY() + 0.5D, this.getZ(), i));
		}
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FLSounds.ENTITY_WANDERER_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FLSounds.ENTITY_WANDERER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_WANDERER_DEATH;
	}

	@Override
	public SoundEvent getNotifyTradeSound()
	{
		this.playSound(FLSounds.ENTITY_WANDERER_IDLE, this.getSoundVolume(), 1.3F);
		return null;
	}

	@Override
	protected SoundEvent getTradeUpdatedSound(boolean positive)
	{
		return FLSounds.ENTITY_WANDERER_IDLE;
	}

	@Override
	public void aiStep()
	{
		super.aiStep();

		if (this.isInWaterOrBubble())
			this.hurt(this.damageSources().drown(), 1);
	}

	public void setWanderPos(BlockPos pos)
	{
		this.targetWanderingPos = pos;
	}

	@Nullable
	private BlockPos wanderPos()
	{
		return this.targetWanderingPos;
	}

	protected class MoveToGoal extends Goal
	{
		final WandererEntity entity;
		final double maxDistance;
		final double speed;

		protected MoveToGoal(WandererEntity entityIn, double maxDistanceIn, double speedIn)
		{
			this.entity = entityIn;
			this.maxDistance = maxDistanceIn;
			this.speed = speedIn;
			this.setFlags(EnumSet.of(Goal.Flag.MOVE));
		}

		@Override
		public void stop()
		{
			this.entity.setWanderPos((BlockPos) null);
			WandererEntity.this.navigation.stop();
		}

		@Override
		public boolean canUse()
		{
			BlockPos blockpos = this.entity.wanderPos();
			return blockpos != null && this.canMoveToPos(blockpos, this.maxDistance);
		}

		@Override
		public void tick()
		{
			BlockPos blockpos = this.entity.wanderPos();
			if (blockpos != null && WandererEntity.this.navigation.isDone())
			{
				if (this.canMoveToPos(blockpos, 10.0D))
				{
					Vec3 vec3d = (new Vec3((double) blockpos.getX() - this.entity.getX(), (double) blockpos.getY() - this.entity.getY(), (double) blockpos.getZ() - this.entity.getZ())).normalize();
					Vec3 vec3d1 = vec3d.scale(10.0D).add(this.entity.getX(), this.entity.getY(), this.entity.getZ());
					WandererEntity.this.navigation.moveTo(vec3d1.x, vec3d1.y, vec3d1.z, this.speed);
				}
				else
				{
					WandererEntity.this.navigation.moveTo((double) blockpos.getX(), (double) blockpos.getY(), (double) blockpos.getZ(), this.speed);
				}
			}
		}

		private boolean canMoveToPos(BlockPos posIn, double distanceIn)
		{
			return !posIn.closerToCenterThan(this.entity.position(), distanceIn);
		}
	}

	public class DisguiseSelfGoal extends Goal
	{
		private final WandererEntity mob = WandererEntity.this;
		private final float maxDist = 20.0F;

		public DisguiseSelfGoal()
		{
		}

		@Override
		public boolean canUse()
		{
			return !getTargets().isEmpty();
		}

		@Override
		public boolean canContinueToUse()
		{
			return super.canContinueToUse();
		}

		@Override
		public void start()
		{
			this.mob.setItemSlot(EquipmentSlot.HEAD, Items.CARVED_PUMPKIN.getDefaultInstance());
			this.mob.playSound(SoundEvents.ARMOR_EQUIP_LEATHER);
		}

		@Override
		public void tick()
		{
		}

		@Override
		public void stop()
		{
			this.mob.setItemSlot(EquipmentSlot.HEAD, ItemStack.EMPTY);
			this.mob.playSound(SoundEvents.ARMOR_EQUIP_GENERIC);
		}

		private List<LivingEntity> getTargets()
		{
			return this.mob.level().getEntitiesOfClass(LivingEntity.class, this.mob.getBoundingBox().inflate((double) this.maxDist, 15.0D, (double) this.maxDist), (e) -> e.getType().is(FLTags.Entities.FARLANDER_VILLAGE_GUARDS) || e instanceof TitanEntity);
		}
	}
}