package com.legacy.farlanders.entity.util;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableMap;
import com.legacy.farlanders.registry.FLItems;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import it.unimi.dsi.fastutil.ints.Int2ObjectOpenHashMap;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.event.village.WandererTradesEvent;

public class FarlanderTrades
{
	public static final Lazy<ItemLike> EC = Lazy.of(() -> FLItems.endumium_crystal);
	public static final Lazy<ItemLike> E = Lazy.of(() -> Items.EMERALD);

	// emerald for items = give items get emeralds
	// @formatter:off
	public static final Int2ObjectMap<VillagerTrades.ItemListing[]> FARLANDER_TRADES = toIntMap(ImmutableMap.of(1, new VillagerTrades.ItemListing[]{
			//real
			trade().firstItem(Items.BONE, 16).result(EC, 1).build(),
			trade().firstItem(EC, 2).result(Items.CLAY_BALL, 24).build(),
			trade().firstItem(EC, 6).result(Items.PUMPKIN_SEEDS, 4).build(),
			trade().firstItem(EC, 6).result(Items.POTATO, 4).build(),
			trade().firstItem(EC, 2).result(Items.FIRE_CHARGE, 4).build(),
			trade().firstItem(EC, 6).result(Items.CARROT, 4).build(),
			trade().firstItem(EC, 2).result(Items.GUNPOWDER, 7).build(),
			trade().firstItem(EC, 1).result(Items.FLINT, 8).build(),
			trade().firstItem(EC, 3).result(Items.ARROW, 16).build(),
			// @formatter:on		
	}, 2, new VillagerTrades.ItemListing[] {
			// @formatter:off
			trade().firstItem(EC, 6).result(Items.SPIDER_EYE, 4).givenXP(5).build(),
			trade().firstItem(Items.GOLD_INGOT, 6).result(EC, 1).givenXP(8).build(),
			trade().firstItem(EC, 3).result(Items.LAPIS_LAZULI, 8).givenXP(7).build(),
			trade().firstItem(Items.APPLE, 20).result(EC, 2).givenXP(10).build(),
			trade().firstItem(EC, 6).result(Items.REDSTONE, 4).givenXP(7).build(),
			trade().firstItem(EC, 6).result(Items.SLIME_BALL, 4).givenXP(6).build()
			// @formatter:on				
	}, 3, new VillagerTrades.ItemListing[] {
			// @formatter:off
			trade().firstItem(EC, 4).result(Items.MILK_BUCKET, 1).givenXP(10).build(),
			trade().firstItem(EC, 2).result(Items.CLOCK, 1).givenXP(12).build(),
			trade().firstItem(EC, 3).result(Items.SHEARS, 1).givenXP(10).build(),
			trade().firstItem(Items.IRON_INGOT, 6).result(EC, 4).givenXP(13).build(),
			trade().firstItem(EC, 2).result(Items.FERMENTED_SPIDER_EYE, 3).givenXP(8).build(),
			trade().firstItem(EC, 2).result(Items.GLISTERING_MELON_SLICE, 4).givenXP(7).build(),
			trade().firstItem(EC, 5).result(Items.PRISMARINE_SHARD, 12).givenXP(9).build(),
			trade().firstItem(EC, 1).result(Items.PUMPKIN, 3).givenXP(7).build()
					// @formatter:on
	}, 4, new VillagerTrades.ItemListing[] {

			// @formatter:off
			trade().firstItem(EC, 8).result(Items.ENDER_PEARL, 1).givenXP(15).build(),
			trade().firstItem(EC, 6).result(Items.MAGMA_CREAM, 4).givenXP(10).build(),
			trade().firstItem(Items.BLAZE_ROD, 8).result(EC, 1).givenXP(13).build(),
			trade().firstItem(Items.GLOWSTONE_DUST, 13).result(EC, 1).givenXP(14).build(),
			// @formatter:on
	}, 5, new VillagerTrades.ItemListing[] {

			// @formatter:off
			trade().firstItem(EC, 6).result(Items.NETHER_WART, 4).givenXP(15).build(),
			trade().firstItem(EC, 10).result(Items.GHAST_TEAR, 1).givenXP(18).build(),
			// @formatter:on
	}));

	// @formatter:off
	   public static final Int2ObjectMap<VillagerTrades.ItemListing[]> WANDERER_TRADES = toIntMap(ImmutableMap.of(1, new VillagerTrades.ItemListing[]{
		   trade().firstItem(E, 3).result(EC, 3).build(),
		   trade().firstItem(Items.GOLD_INGOT, 5).result(EC, 1).build(),
		   trade().firstItem(EC, 1).result(Items.FIRE_CHARGE, 3).build(),
		   trade().firstItem(EC, 2).result(Items.ARROW, 16).build(),
		   trade().firstItem(EC, 3).result(Items.PRISMARINE_SHARD, 14).build(),
		   trade().firstItem(EC, 2).result(Items.BAMBOO, 4).build(),
		   trade().firstItem(EC, 2).result(Blocks.CACTUS, 3).build(),
		   trade().firstItem(EC, 2).result(Blocks.ICE, 8).build(),
		   trade().firstItem(EC, 1).result(Blocks.END_STONE, 8).build(),
		   trade().firstItem(EC, 5).result(Blocks.AZALEA, 1).build(),
		   trade().firstItem(EC, 4).result(Blocks.COCOA, 5).build(),
		   trade().firstItem(EC, 1).result(Blocks.GLOW_LICHEN, 5).build(),
		   trade().firstItem(EC, 6).result(Blocks.MYCELIUM, 2).build(),
		   trade().firstItem(EC, 8).result(Items.POINTED_DRIPSTONE, 5).build(),
		   trade().firstItem(EC, 2).result(Blocks.CRYING_OBSIDIAN, 6).build(),
		   trade().firstItem(EC, 2).result(Blocks.ROOTED_DIRT, 12).build(),
		   trade().firstItem(EC, 1).result(Items.CHARCOAL, 8).build(),
		   trade().firstItem(EC, 3).result(Items.SLIME_BALL, 5).build()
			   }));

	   public static final Int2ObjectMap<VillagerTrades.ItemListing[]> ELDER_TRADES = toIntMap(ImmutableMap.of(
			1, new VillagerTrades.ItemListing[]{
		   trade().firstItem(EC, 3).result(Items.IRON_INGOT, 2).build(),
		   trade().firstItem(Items.AMETHYST_SHARD, 12).result(EC, 1).build(),
		   trade().firstItem(Items.EMERALD, 2).result(EC, 1).build(),
		   trade().firstItem(EC, 1).result(Items.LAPIS_LAZULI, 8).build(),
		   trade().firstItem(EC, 5).result(Items.GLOWSTONE_DUST, 8).build(),
				},
			2, new VillagerTrades.ItemListing[]{
			trade().firstItem(EC, 3).result(Items.EXPERIENCE_BOTTLE, 1).build(),
			trade().firstItem(EC, 8).result(Blocks.BREWING_STAND, 1).givenXP(5).build(),
			trade().firstItem(EC, 5).result(Blocks.RAIL, 25).givenXP(5).build(),
			trade().firstItem(EC, 5).result(Blocks.POWERED_RAIL, 15).givenXP(10).build(),
			trade().firstItem(EC, 5).result(Blocks.DETECTOR_RAIL, 20).givenXP(7).build(),
				},
			3, new VillagerTrades.ItemListing[]{
		   trade().firstItem(EC, 8).result(Blocks.JUKEBOX, 1).givenXP(10).build(),
		   trade().firstItem(EC, 3).result(Blocks.TNT, 8).givenXP(8).build(),
		   trade().firstItem(EC, 8).result(Items.SADDLE, 1).givenXP(9).build(),
				},
			4, new VillagerTrades.ItemListing[]{
		   trade().firstItem(EC, 3).result(Blocks.OBSIDIAN, 5).givenXP(12).build(),
		   trade().firstItem(EC, 12).result(Blocks.SPONGE, 1).givenXP(14).build(),
				},
			5, new VillagerTrades.ItemListing[]{
		   trade().firstItem(EC, 40).result(Items.ENDER_CHEST, 1).givenXP(16).build(),
		   trade().firstItem(EC, 32).secondItem(Items.ENDER_PEARL, 2).result(Items.ENDER_EYE, 1).givenXP(15).build()
				}));
	// @formatter:on

	private static Trade.Builder trade()
	{
		return new Trade.Builder();
	}

	private static Int2ObjectMap<VillagerTrades.ItemListing[]> toIntMap(ImmutableMap<Integer, VillagerTrades.ItemListing[]> p_221238_0_)
	{
		return new Int2ObjectOpenHashMap<>(p_221238_0_);
	}

	static class ItemsForEmeraldsTrade implements VillagerTrades.ItemListing
	{

		private final ItemStack itemSold;

		private final int emeraldCount;

		private final int soldItemCount;

		private final int maxUses;

		private final int givenXP;

		private final float priceMultiplier;

		public ItemsForEmeraldsTrade(Block itemSoldIn, int emeraldCountIn, int soldItemCountIn, int maxUsesIn, int givenXPIn)
		{
			this(new ItemStack(itemSoldIn), emeraldCountIn, soldItemCountIn, maxUsesIn, givenXPIn);
		}

		public ItemsForEmeraldsTrade(Item itemSoldIn, int emeraldCountIn, int soldItemCountIn, int givenXPIn)
		{
			this(new ItemStack(itemSoldIn), emeraldCountIn, soldItemCountIn, 12, givenXPIn);
		}

		public ItemsForEmeraldsTrade(Item itemSoldIn, int emeraldCountIn, int soldItemCountIn, int maxUsesIn, int givenXPIn)
		{
			this(new ItemStack(itemSoldIn), emeraldCountIn, soldItemCountIn, maxUsesIn, givenXPIn);
		}

		public ItemsForEmeraldsTrade(ItemStack itemSoldIn, int emeraldCountIn, int soldItemCountIn, int maxUsesIn, int givenXPIn)
		{
			this(itemSoldIn, emeraldCountIn, soldItemCountIn, maxUsesIn, givenXPIn, 0.05F);
		}

		public ItemsForEmeraldsTrade(ItemStack itemSoldIn, int emeraldCountIn, int soldItemCountIn, int maxUsesIn, int givenXPIn, float priceMultiplierIn)
		{
			this.itemSold = itemSoldIn;
			this.emeraldCount = emeraldCountIn;
			this.soldItemCount = soldItemCountIn;
			this.maxUses = maxUsesIn;
			this.givenXP = givenXPIn;
			this.priceMultiplier = priceMultiplierIn;
		}

		@Override
		public MerchantOffer getOffer(Entity p_221182_1_, RandomSource p_221182_2_)
		{
			return new MerchantOffer(new ItemStack(FLItems.endumium_crystal, this.emeraldCount), new ItemStack(this.itemSold.getItem(), this.soldItemCount), this.maxUses, this.givenXP, this.priceMultiplier);
		}
	}

	static class EmeraldFromItemsTrade implements VillagerTrades.ItemListing
	{

		private final Item item;

		private final int cost;

		private final int maxUses;

		private final int villagerXp;

		private final float priceMultiplier;

		public EmeraldFromItemsTrade(ItemLike itemIn, int itemCountIn, int maxUsesIn, int givenXP)
		{
			this.item = itemIn.asItem();
			this.cost = itemCountIn;
			this.maxUses = maxUsesIn;
			this.villagerXp = givenXP;
			this.priceMultiplier = 0.05F;
		}

		@Override
		public MerchantOffer getOffer(Entity p_221182_1_, RandomSource p_221182_2_)
		{
			ItemStack itemstack = new ItemStack(this.item, this.cost);
			return new MerchantOffer(itemstack, new ItemStack(FLItems.endumium_crystal), this.maxUses, this.villagerXp, this.priceMultiplier);
		}
	}

	static class ItemsForEmeraldsAndItemsTrade implements VillagerTrades.ItemListing
	{

		private final ItemStack fromItem;

		private final int fromCount;

		private final int emeraldCost;

		private final ItemStack toItem;

		private final int toCount;

		private final int maxUses;

		private final int villagerXp;

		private final float priceMultiplier;

		public ItemsForEmeraldsAndItemsTrade(ItemLike p_i50533_1_, int p_i50533_2_, Item p_i50533_3_, int p_i50533_4_, int p_i50533_5_, int p_i50533_6_)
		{
			this(p_i50533_1_, p_i50533_2_, 1, p_i50533_3_, p_i50533_4_, p_i50533_5_, p_i50533_6_);
		}

		public ItemsForEmeraldsAndItemsTrade(ItemLike p_i50534_1_, int p_i50534_2_, int p_i50534_3_, Item p_i50534_4_, int p_i50534_5_, int p_i50534_6_, int p_i50534_7_)
		{
			this.fromItem = new ItemStack(p_i50534_1_);
			this.fromCount = p_i50534_2_;
			this.emeraldCost = p_i50534_3_;
			this.toItem = new ItemStack(p_i50534_4_);
			this.toCount = p_i50534_5_;
			this.maxUses = p_i50534_6_;
			this.villagerXp = p_i50534_7_;
			this.priceMultiplier = 0.05F;
		}

		@Nullable
		@Override
		public MerchantOffer getOffer(Entity p_221182_1_, RandomSource p_221182_2_)
		{
			return new MerchantOffer(new ItemStack(FLItems.endumium_crystal, this.emeraldCost), new ItemStack(this.fromItem.getItem(), this.fromCount), new ItemStack(this.toItem.getItem(), this.toCount), this.maxUses, this.villagerXp, this.priceMultiplier);
		}
	}

	public static class Trade implements VillagerTrades.ItemListing
	{
		private final ItemStack itemGiven1;
		private final int itemGiven1Count;

		private final ItemStack itemGiven2;
		private final int itemGiven2Count;

		private final ItemStack itemSold;
		private final int soldItemCount;

		private final int maxUses;
		private final int givenXP;
		private final float priceMultiplier;

		public Trade(Item itemGiven1, int itemGiven1Count, Item itemGiven2, int itemGiven2Count, Item soldItem, int soldItemCount, int maxUsesIn, int givenXPIn)
		{
			this(new ItemStack(itemGiven1), itemGiven1Count, itemGiven2 != null ? new ItemStack(itemGiven2) : ItemStack.EMPTY, itemGiven2Count, new ItemStack(soldItem), soldItemCount, maxUsesIn, givenXPIn);
		}

		public Trade(ItemStack itemGiven1In, int itemGiven1CountIn, ItemStack itemGiven2In, int itemGiven2CountIn, ItemStack itemSoldIn, int itemSoldCountIn, int maxUsesIn, int givenXPIn)
		{
			this.itemGiven1 = itemGiven1In;
			this.itemGiven1Count = itemGiven1CountIn;

			this.itemGiven2 = itemGiven2In;
			this.itemGiven2Count = itemGiven2CountIn;

			this.itemSold = itemSoldIn;
			this.soldItemCount = itemSoldCountIn;

			this.maxUses = maxUsesIn; // 12 default
			this.givenXP = givenXPIn;
			this.priceMultiplier = 0.05F;
		}

		@Override
		public MerchantOffer getOffer(@Nullable Entity trader, @Nullable RandomSource rand)
		{
			MerchantOffer offer = new MerchantOffer(new ItemStack(itemGiven1.getItem(), this.itemGiven1Count), itemGiven2 != null ? new ItemStack(itemGiven2.getItem(), this.itemGiven2Count) : ItemStack.EMPTY, new ItemStack(this.itemSold.getItem(), this.soldItemCount), this.maxUses, this.givenXP, this.priceMultiplier);

			return offer;
		}

		public static class Builder
		{
			private Item itemGiven1 = Items.WHEAT;
			private int itemGiven1Count = 1;

			private Item itemGiven2 = null;
			private int itemGiven2Count = 1;

			private Item itemSold = Items.BREAD;
			private int itemSoldCount = 1;

			private int maxUses = 12;
			private int givenXP = 3;

			public Builder()
			{
			}

			public Builder firstItem(Lazy<ItemLike> item, int count)
			{
				return this.firstItem(item.get(), count);
			}

			public Builder firstItem(ItemLike item, int count)
			{
				this.itemGiven1 = item.asItem();
				this.itemGiven1Count = count;
				return this;
			}

			public Builder secondItem(ItemLike item, int count)
			{
				this.itemGiven2 = item.asItem();
				this.itemGiven2Count = count;
				return this;
			}

			public Builder result(ItemLike item, int count)
			{
				this.itemSold = item.asItem();
				this.itemSoldCount = count;
				return this;
			}

			public Builder result(Lazy<ItemLike> item, int count)
			{
				return this.result(item.get(), count);
			}

			public Builder maxUses(int maxUses)
			{
				this.maxUses = maxUses;
				return this;
			}

			public Builder givenXP(int givenXp)
			{
				this.givenXP = givenXp;
				return this;
			}

			public Trade build()
			{
				return new Trade(itemGiven1, itemGiven1Count, itemGiven2, itemGiven2Count, itemSold, itemSoldCount, maxUses, givenXP);
			}
		}
	}

	public static class WanderingTrades
	{
		public static void addWanderingTraderTrades(WandererTradesEvent event)
		{
			event.getGenericTrades().add(new Trade.Builder().firstItem(FLItems.endumium_crystal, 3).result(Items.EMERALD, 3).build());
		}
	}
}
