package com.legacy.farlanders.entity.util;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.Vec3;

public interface ITeleportingEntity
{
	default boolean teleportRandomly(LivingEntity entity)
	{
		if (!entity.level().isClientSide() && entity.isAlive())
		{
			double d0 = entity.getX() + (entity.getRandom().nextDouble() - 0.5D) * 16.0D;
			double d1 = entity.getY() + (double) (entity.getRandom().nextInt(64) - 32);
			double d2 = entity.getZ() + (entity.getRandom().nextDouble() - 0.5D) * 16.0D;
			return this.teleportTo(entity, d0, d1, d2);
		}
		else
		{
			return false;
		}
	}

	@SuppressWarnings("deprecation")
	default boolean teleportTo(LivingEntity entity, double pX, double pY, double pZ)
	{
		BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos(pX, pY, pZ);

		while (blockpos$mutableblockpos.getY() > entity.level().getMinBuildHeight() && !entity.level().getBlockState(blockpos$mutableblockpos).blocksMotion())
		{
			blockpos$mutableblockpos.move(Direction.DOWN);
		}

		BlockState blockstate = entity.level().getBlockState(blockpos$mutableblockpos);
		boolean flag = blockstate.blocksMotion();
		boolean flag1 = blockstate.getFluidState().is(FluidTags.WATER);
		if (flag && !flag1)
		{
			net.neoforged.neoforge.event.entity.EntityTeleportEvent.EnderEntity event = net.neoforged.neoforge.event.EventHooks.onEnderTeleport(entity, pX, pY, pZ);
			if (event.isCanceled())
				return false;
			Vec3 vec3 = entity.position();
			boolean flag2 = entity.randomTeleport(event.getTargetX(), event.getTargetY(), event.getTargetZ(), true);
			if (flag2)
			{
				entity.level().gameEvent(GameEvent.TELEPORT, vec3, GameEvent.Context.of(entity));
				if (!entity.isSilent())
				{
					entity.level().playSound((Player) null, entity.xo, entity.yo, entity.zo, SoundEvents.ENDERMAN_TELEPORT, entity.getSoundSource(), 1.0F, 1.0F);
					entity.playSound(SoundEvents.ENDERMAN_TELEPORT, 1.0F, 1.0F);
				}
			}

			return flag2;
		}
		else
		{
			return false;
		}
	}
}
