package com.legacy.farlanders.entity.util;

import java.util.List;

import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.util.Lazy;

public interface IColoredEyes
{
	private LivingEntity getMe()
	{
		return (LivingEntity) this;
	}

	String EYE_COLOR_KEY = "EyeColor";
	List<EyeColor> DEFAULT_EYE_COLORS = List.of(EyeColor.values());

	void setEyeColor(int colorID);

	int getEyeColor();

	default EyeColor getEyeData(int color)
	{
		return color >= 0 && color < this.getEyeColors().size() ? this.getEyeColors().get(color) : EyeColor.PURPLE;
	}

	default EyeColor getEyeData()
	{
		return this.getEyeData(this.getEyeColor());
	}

	default void spawnEyeParticles()
	{
		this.getEyeData().spawnParticle(this.getMe());
	}

	default List<EyeColor> getEyeColors()
	{
		return DEFAULT_EYE_COLORS;
	}

	default void randomizeEyeColor()
	{
		this.setEyeColor(this.getRandomEyeColor());
	}

	default int getRandomEyeColor()
	{
		return this.getMe().getRandom().nextInt(this.getEyeColors().size());
	}

	public enum EyeColor
	{
		PURPLE(() -> net.minecraft.core.particles.ParticleTypes.PORTAL), GREEN(DyeColor.LIME), RED(DyeColor.RED), WHITE(DyeColor.WHITE), BROWN(DyeColor.BROWN), BLUE(DyeColor.BLUE);

		public final Lazy<ParticleOptions> particle;
		public final int particleAmount;
		public final float particleChance;

		private EyeColor(Lazy<ParticleOptions> particle, int particleAmount, float particleChance)
		{
			this.particle = particle;
			this.particleAmount = particleAmount;
			this.particleChance = particleChance;
		}

		private EyeColor(Lazy<ParticleOptions> particle)
		{
			this(particle, 2, 1.0F);
		}

		private EyeColor(DyeColor particle)
		{
			this(Lazy.of(() -> com.legacy.farlanders.client.particle.FarlandDustParticle.dyeToRGB(particle)), 2, 1.0F);
		}

		public void spawnParticle(LivingEntity entity)
		{
			if (entity.level().isClientSide())
			{
				RandomSource rand = entity.getRandom();

				if (rand.nextFloat() < this.particleChance)
				{
					for (int k = 0; k < this.particleAmount; k++)
					{
						Vec3 motion = new Vec3((rand.nextDouble() - 0.5D) * 2D, -rand.nextDouble(), (rand.nextDouble() - 0.5D) * 2D);
						entity.level().addParticle(this.particle.get(), entity.getX() + (rand.nextDouble() - 0.5D) * (double) entity.getBbWidth(), (entity.getY() + rand.nextDouble() * (double) entity.getBbHeight()) - 0.25D, entity.getZ() + (rand.nextDouble() - 0.5D) * (double) entity.getBbWidth(), motion.x(), motion.y(), motion.z());
					}
				}
			}
		}
	}
}
