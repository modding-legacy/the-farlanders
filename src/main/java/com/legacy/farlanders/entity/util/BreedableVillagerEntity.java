package com.legacy.farlanders.entity.util;

import java.util.UUID;

import javax.annotation.Nullable;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.npc.AbstractVillager;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.entity.living.BabyEntitySpawnEvent;

public abstract class BreedableVillagerEntity extends AbstractVillager
{
	private int inLove;
	private UUID playerInLove;

	public BreedableVillagerEntity(EntityType<? extends AbstractVillager> type, Level worldIn)
	{
		super(type, worldIn);
	}

	@Override
	protected void customServerAiStep()
	{
		if (this.getAge() != 0)
			this.inLove = 0;

		super.customServerAiStep();
	}

	@Override
	public void aiStep()
	{
		super.aiStep();

		if (this.getAge() != 0)
			this.inLove = 0;

		if (this.inLove > 0)
		{
			--this.inLove;

			if (this.inLove % 10 == 0)
			{
				double d0 = this.random.nextGaussian() * 0.02D;
				double d1 = this.random.nextGaussian() * 0.02D;
				double d2 = this.random.nextGaussian() * 0.02D;
				this.level().addParticle(ParticleTypes.HEART, this.getRandomX(1.0D), this.getRandomY() + 0.5D, this.getRandomZ(1.0D), d0, d1, d2);
			}
		}

	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("InLove", this.inLove);

		if (this.playerInLove != null)
			compound.putUUID("LoveCause", this.playerInLove);
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
		this.inLove = compound.getInt("InLove");
		this.playerInLove = compound.hasUUID("LoveCause") ? compound.getUUID("LoveCause") : null;
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
			return false;

		this.inLove = 0;
		return super.hurt(source, amount);
	}

	public boolean isBreedingItem(ItemStack stack)
	{
		return stack.getItem() == Items.APPLE;
	}

	@Override
	public InteractionResult mobInteract(Player p_230254_1_, InteractionHand p_230254_2_)
	{
		ItemStack itemstack = p_230254_1_.getItemInHand(p_230254_2_);
		if (this.isBreedingItem(itemstack))
		{
			int i = this.getAge();
			if (!this.level().isClientSide() && i == 0 && this.canFallInLove())
			{
				this.consumeItemFromStack(p_230254_1_, itemstack);
				this.setInLove(p_230254_1_);
				return InteractionResult.SUCCESS;
			}

			if (this.isBaby())
			{
				this.consumeItemFromStack(p_230254_1_, itemstack);
				this.ageUp((int) ((float) (-i / 20) * 0.1F), true);
				return InteractionResult.sidedSuccess(this.level().isClientSide());
			}

			if (this.level().isClientSide())
				return InteractionResult.CONSUME;
		}

		return super.mobInteract(p_230254_1_, p_230254_2_);
	}

	protected void consumeItemFromStack(Player player, ItemStack stack)
	{
		if (!player.getAbilities().instabuild)
			stack.shrink(1);
	}

	public boolean canFallInLove()
	{
		return this.inLove <= 0;
	}

	public void setInLove(@Nullable Player player)
	{
		this.inLove = 600;

		if (player != null)
			this.playerInLove = player.getUUID();

		this.level().broadcastEntityEvent(this, (byte) 18);
	}

	public void setInLove(int ticks)
	{
		this.inLove = ticks;
	}

	public int inLove()
	{
		return this.inLove;
	}

	@Nullable
	public ServerPlayer getLoveCause()
	{
		if (this.playerInLove == null)
		{
			return null;
		}
		else
		{
			Player playerentity = this.level().getPlayerByUUID(this.playerInLove);
			return playerentity instanceof ServerPlayer ? (ServerPlayer) playerentity : null;
		}
	}

	public boolean isInLove()
	{
		return this.inLove > 0;
	}

	public void resetInLove()
	{
		this.inLove = 0;
	}

	public boolean canMateWith(BreedableVillagerEntity otherVillager)
	{
		if (otherVillager == this)
			return false;
		else if (otherVillager.getClass() != this.getClass())
			return false;
		else
			return this.isInLove() && otherVillager.isInLove();
	}

	public void spawnChildFromBreeding(ServerLevel world, BreedableVillagerEntity villager)
	{
		AgeableMob ageableentity = this.getBreedOffspring(world, villager);
		BabyEntitySpawnEvent event = new BabyEntitySpawnEvent(this, villager, ageableentity);
		boolean cancelled = NeoForge.EVENT_BUS.post(event).isCanceled();
		ageableentity = event.getChild();

		if (cancelled)
		{
			this.setAge(6000);
			villager.setAge(6000);
			this.resetInLove();
			villager.resetInLove();
			return;
		}

		if (ageableentity != null)
		{
			ServerPlayer serverplayerentity = this.getLoveCause();

			if (serverplayerentity == null && villager.getLoveCause() != null)
				serverplayerentity = villager.getLoveCause();

			this.setAge(6000);
			villager.setAge(6000);
			this.resetInLove();
			villager.resetInLove();
			ageableentity.setBaby(true);
			ageableentity.moveTo(this.getX(), this.getY(), this.getZ(), 0.0F, 0.0F);
			world.addFreshEntityWithPassengers(ageableentity);
			world.broadcastEntityEvent(this, (byte) 18);

			if (world.getGameRules().getBoolean(GameRules.RULE_DOMOBLOOT))
				world.addFreshEntity(new ExperienceOrb(world, this.getX(), this.getY(), this.getZ(), this.getRandom().nextInt(7) + 1));
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void handleEntityEvent(byte id)
	{
		if (id == 18)
		{
			for (int i = 0; i < 7; ++i)
			{
				double d0 = this.random.nextGaussian() * 0.02D;
				double d1 = this.random.nextGaussian() * 0.02D;
				double d2 = this.random.nextGaussian() * 0.02D;
				this.level().addParticle(ParticleTypes.HEART, this.getRandomX(1.0D), this.getRandomY() + 0.5D, this.getRandomZ(1.0D), d0, d1, d2);
			}
		}
		else
		{
			super.handleEntityEvent(id);
		}

	}
}