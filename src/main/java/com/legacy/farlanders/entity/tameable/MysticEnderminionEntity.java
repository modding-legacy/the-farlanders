package com.legacy.farlanders.entity.tameable;

import javax.annotation.Nullable;

import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.registry.FLItems;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.behavior.BehaviorUtils;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RangedAttackGoal;
import net.minecraft.world.entity.monster.RangedAttackMob;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.SmallFireball;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;

public class MysticEnderminionEntity extends EnderminionEntity implements RangedAttackMob
{
	private int smallWandCounter, largeWandCounter;

	public MysticEnderminionEntity(EntityType<? extends MysticEnderminionEntity> type, Level level)
	{
		super(type, level);
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();
	}

	@Override
	public void addAttackGoals()
	{
		this.goalSelector.addGoal(4, new MeleeAttackGoal(this, 1.0D, true)
		{
			@Override
			public boolean canUse()
			{
				return shouldMeleeTarget() && super.canUse();
			}

			@Override
			public boolean canContinueToUse()
			{
				return shouldMeleeTarget() && super.canContinueToUse();
			}
		});

		this.goalSelector.addGoal(5, new RangedAttackGoal(this, 0.62D, 30, 10.0F)
		{
			@Override
			public boolean canUse()
			{
				return isHoldingWand() && !shouldMeleeTarget() && super.canUse();
			}

			@Override
			public boolean canContinueToUse()
			{
				return isHoldingWand() && !shouldMeleeTarget() && super.canContinueToUse();
			}
		});
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		this.setItemSlot(EquipmentSlot.OFFHAND, FLItems.mystic_wand_fire_small.getDefaultInstance());
		this.setDropChance(EquipmentSlot.OFFHAND, 0.0F);

		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		compound.putInt("SmallWandTradeCounter", this.smallWandCounter);
		compound.putInt("LargeWandTradeCounter", this.largeWandCounter);
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);

		this.smallWandCounter = compound.getInt("SmallWandTradeCounter");
		this.largeWandCounter = compound.getInt("LargeWandTradeCounter");
	}

	@Override
	public InteractionResult mobInteract(Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		String space = " ";

		if (this.isTame() && this.isOwnedBy(player))
		{
			if (itemstack.getItem() == Items.PAPER)
			{
				if (this.level().isClientSide())
					return InteractionResult.SUCCESS;

				Component chat = Component.translatable("Health: " + (int) this.getHealth() + "/" + this.getMaxHealth());
				/*player.displayClientMessage(chat, false);*/

				String wand = Component.translatable("item.farlanders.mystic_wand").getString();
				String small = Component.translatable("gui.item.wand.small_fireball").getString();
				String large = Component.translatable("gui.item.wand.large_fireball").getString();

				if ((5 - this.smallWandCounter) == 1)
				{
					chat = Component.literal(wand + " - " + small + ": " + 1 + space + itemDesc(Items.GOLD_INGOT) + "/" + itemDesc(Items.IRON_INGOT));
					player.displayClientMessage(chat, false);
				}
				else
				{
					chat = Component.literal(wand + " - " + small + ": " + (5 - this.smallWandCounter) + space + itemDesc(Items.GOLD_INGOT) + "/" + itemDesc(Items.IRON_INGOT));
					player.displayClientMessage(chat, false);
				}
				if ((5 - this.largeWandCounter) == 1)
				{
					chat = Component.literal(wand + " - " + large + ": " + 1 + space + itemDesc(Items.DIAMOND) + "/" + itemDesc(Items.EMERALD) + "/" + itemDesc(FLItems.endumium_crystal));
					player.displayClientMessage(chat, false);
				}
				else
				{
					chat = Component.literal(wand + " - " + large + ": " + (5 - this.smallWandCounter) + space + itemDesc(Items.DIAMOND) + "/" + itemDesc(Items.EMERALD) + "/" + itemDesc(FLItems.endumium_crystal));
					player.displayClientMessage(chat, false);
				}

				chat = Component.empty();

				player.displayClientMessage(chat, false);

				return InteractionResult.SUCCESS;
			}
			else if ((itemstack.getItem() == Items.GOLD_INGOT || itemstack.getItem() == Items.IRON_INGOT) && !this.level().isClientSide)
			{
				if (!player.isCreative())
				{
					itemstack.shrink(1);
				}

				this.playSound(SoundEvents.EXPERIENCE_ORB_PICKUP, 1.0F, 0.7F + (0.1F * this.smallWandCounter));

				this.getLookControl().setLookAt(player);

				if (smallWandCounter < 4)
				{
					smallWandCounter++;
				}
				else
				{
					smallWandCounter = 0;
					this.playSound(SoundEvents.ARMOR_EQUIP_GENERIC, 1.0F, 1.0F);
					BehaviorUtils.throwItem(this, FLItems.mystic_wand_fire_small.getDefaultInstance(), player.position().add(0.0D, 1.0D, 0.0D));
					this.swing(InteractionHand.OFF_HAND);
				}

				return InteractionResult.SUCCESS;
			}
			else if ((itemstack.getItem() == Items.DIAMOND || itemstack.getItem() == Items.EMERALD || itemstack.getItem() == FLItems.endumium_crystal) && !this.level().isClientSide)
			{
				if (!player.isCreative())
					itemstack.shrink(1);

				this.playSound(SoundEvents.EXPERIENCE_ORB_PICKUP, 1.0F, 0.9F + (0.1F * this.largeWandCounter));
				this.getLookControl().setLookAt(player);

				if (largeWandCounter < 4)
				{
					largeWandCounter++;
				}
				else
				{
					largeWandCounter = 0;
					this.playSound(SoundEvents.ARMOR_EQUIP_GENERIC, 1.0F, 1.0F);
					BehaviorUtils.throwItem(this, FLItems.mystic_wand_fire_large.getDefaultInstance(), player.position().add(0.0D, 1.0D, 0.0D));
					this.swing(InteractionHand.OFF_HAND);
				}

				return InteractionResult.SUCCESS;
			}
		}

		return super.mobInteract(player, hand);
	}

	private String itemDesc(Item item)
	{
		return item.getDescription().getString();
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FLSounds.ENTITY_MYSTIC_ENDERMINION_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FLSounds.ENTITY_MYSTIC_ENDERMINION_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_MYSTIC_ENDERMINION_DEATH;
	}

	@Override
	public float getVoicePitch()
	{
		return super.getVoicePitch() + 0.35F;
	}

	public boolean shouldMeleeTarget()
	{
		if (!this.isHoldingWand())
			return true;

		var target = this.getTarget();
		return this.getMainHandItem().is(FLTags.Items.ENDERMINION_HOLDABLE) && target != null && this.distanceTo(target) <= 5;
	}

	public boolean isHoldingWand()
	{
		return this.getMainHandItem().is(FLTags.Items.MYSTIC_WANDS) || this.getOffhandItem().is(FLTags.Items.MYSTIC_WANDS);
	}

	@Override
	public void performRangedAttack(LivingEntity target, float distanceFactor)
	{
		double d = target.getX() - getX();
		double d1 = (target.getBoundingBox().minY + (double) (target.getBbHeight() / 2.0F)) - (getY() + (double) (this.getBbHeight() / 2.0F));
		double d2 = target.getZ() - getZ();
		SmallFireball ball = new SmallFireball(this.level(), this, d, d1 - 0.7f, d2);
		ball.setPos(ball.getX(), getY() + (double) (this.getBbHeight() / 2.0F) + 0.5D, ball.getZ());
		ball.setOwner(this);
		this.level().addFreshEntity(ball);
		this.playSound(FLSounds.ITEM_MYSTIC_WAND_CAST_FIREBALL, 1.0F, 1.3F + ((this.random.nextFloat() - this.random.nextFloat()) * 0.1F));

		this.swing(InteractionHand.OFF_HAND);
	}
}
