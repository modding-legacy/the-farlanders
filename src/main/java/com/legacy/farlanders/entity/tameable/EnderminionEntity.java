package com.legacy.farlanders.entity.tameable;

import com.legacy.farlanders.FarlandersConfig;
import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.Tag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.OwnableEntity;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.TamableAnimal;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.FollowOwnerGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.SitWhenOrderedToGoal;
import net.minecraft.world.entity.ai.goal.TemptGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.goal.target.OwnerHurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.OwnerHurtTargetGoal;
import net.minecraft.world.entity.animal.horse.AbstractHorse;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.Endermite;
import net.minecraft.world.entity.monster.Ghast;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.entity.projectile.Fireball;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.DyeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.event.EventHooks;

public class EnderminionEntity extends TamableAnimal implements IColoredEyes
{
	protected static final float BASE_HEALTH = 10.0F, TAME_HEALTH = 20.0F;
	private static final EntityDataAccessor<Integer> EYE_COLOR = SynchedEntityData.defineId(EnderminionEntity.class, EntityDataSerializers.INT);
	private static final EntityDataAccessor<Integer> BRACELET_COLOR = SynchedEntityData.defineId(EnderminionEntity.class, EntityDataSerializers.INT);

	protected int chatCooldown, tameItemsUsed;

	public EnderminionEntity(EntityType<? extends EnderminionEntity> type, Level worldIn)
	{
		super(type, worldIn);
		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(1, new FloatGoal(this));
		this.goalSelector.addGoal(2, new SitWhenOrderedToGoal(this));
		this.goalSelector.addGoal(3, new TemptGoal(this, 1.1D, Ingredient.of(FLTags.Items.ENDERMINION_TAME_FOOD), false)
		{
			@Override
			public boolean canUse()
			{
				return super.canUse() && !isTame();
			}

			@Override
			public boolean canContinueToUse()
			{
				return super.canContinueToUse() && !isTame();
			}
		});
		this.goalSelector.addGoal(6, new FollowOwnerGoal(this, 1.0D, 10.0F, 2.0F, true));
		this.goalSelector.addGoal(8, new WaterAvoidingRandomStrollGoal(this, 1.0D));
		this.goalSelector.addGoal(10, new LookAtPlayerGoal(this, Player.class, 8.0F));
		this.goalSelector.addGoal(10, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(1, new OwnerHurtByTargetGoal(this));
		this.targetSelector.addGoal(2, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(3, new OwnerHurtTargetGoal(this));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, Endermite.class, false));
		
		this.addAttackGoals();
	}

	public void addAttackGoals()
	{
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 1.0D, true));
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return Mob.createMobAttributes().add(Attributes.MOVEMENT_SPEED, 0.35F).add(Attributes.MAX_HEALTH, BASE_HEALTH).add(Attributes.ATTACK_DAMAGE, 2.0D);
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor pLevel, DifficultyInstance pDifficulty, MobSpawnType pReason, SpawnGroupData pSpawnData, CompoundTag pDataTag)
	{
		this.setEyeColor(this.random.nextInt(this.getEyeColors().size()));
		return super.finalizeSpawn(pLevel, pDifficulty, pReason, pSpawnData, pDataTag);
	}

	/*@Override
	public boolean removeWhenFarAway(double distanceToClosestPlayer)
	{
		// 10 minute despawn time
		return !this.isTame() && !this.isLeashed() && this.tickCount > 600 * 20;
	}*/

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(EYE_COLOR, 0);
		this.entityData.define(BRACELET_COLOR, DyeColor.PURPLE.getId());
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		compound.putInt(EYE_COLOR_KEY, this.getEyeColor());
		compound.putInt("TameItemsUsed", this.tameItemsUsed);
		compound.putInt("ChatCooldown", this.chatCooldown);
		compound.putByte("BraceletColor", (byte) this.getBraceletColor().getId());
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);

		this.setEyeColor(compound.getInt(EYE_COLOR_KEY));
		this.tameItemsUsed = compound.getInt("TameItemsUsed");
		this.chatCooldown = compound.getInt("ChatCooldown");

		if (compound.contains("BraceletColor", Tag.TAG_ANY_NUMERIC))
			this.setBraceletColor(DyeColor.byId(compound.getInt("BraceletColor")));
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FLSounds.ENTITY_ENDERMINION_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return FLSounds.ENTITY_ENDERMINION_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_ENDERMINION_DEATH;
	}

	@Override
	protected void playStepSound(BlockPos pos, BlockState state)
	{
	}

	@Override
	protected float getSoundVolume()
	{
		return 1.0F;
	}

	@Override
	public void aiStep()
	{
		this.updateSwingTime();
		super.aiStep();
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isInWaterOrBubble())
			this.hurt(this.damageSources().drown(), 1);

		if (this.level().isClientSide() && this.isTame() && (!this.isInSittingPose() || this.getRandom().nextFloat() < 0.1F))
			this.spawnEyeParticles();

		if (this.chatCooldown > 0)
			--this.chatCooldown;

		if (!this.level().isClientSide() && this.isAlive() && this.random.nextInt(900) == 0 && this.deathTime == 0)
			this.heal(1.0F);
	}

	@Override
	public void die(DamageSource cause)
	{
		super.die(cause);

		if (this.getMainHandItem().is(FLTags.Items.LOOTER_STEALABLE))
			this.spawnAtLocation(this.getMainHandItem());
	}

	@Override
	protected float getStandingEyeHeight(Pose pose, EntityDimensions size)
	{
		return 2.2F;
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		// preventing fireball friendly fire
		if (source.getDirectEntity()instanceof Fireball fb && fb.getOwner()instanceof OwnableEntity fbOwner && fbOwner.getOwner() == this.getOwner())
			return false;

		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else
		{
			Entity entity = source.getEntity();

			if (this.getOwner() != null)
				this.setOrderedToSit(false);

			if (entity != null && !(entity instanceof Player) && !(entity instanceof AbstractArrow))
				amount = (amount + 1.0F) / 2.0F;

			return super.hurt(source, amount);
		}
	}

	@Override
	public boolean doHurtTarget(Entity entity)
	{
		boolean hurt = super.doHurtTarget(entity);

		if (this.getMainHandItem().is(FLTags.Items.ENDERMINION_HOLDABLE))
			this.getMainHandItem().hurtAndBreak(2, this, (e) -> e.broadcastBreakEvent(InteractionHand.MAIN_HAND));

		return hurt;
	}

	@Override
	public int getAmbientSoundInterval()
	{
		return this.isTame() ? 240 + (this.random.nextInt(10) * 20) : super.getAmbientSoundInterval();
	}

	@Override
	public void setTame(boolean tamed)
	{
		super.setTame(tamed);

		if (tamed)
		{
			this.getAttribute(Attributes.MAX_HEALTH).setBaseValue(TAME_HEALTH);
			this.setHealth(this.getMaxHealth());
		}
		else
			this.getAttribute(Attributes.MAX_HEALTH).setBaseValue(BASE_HEALTH);

		this.setHealth(this.getMaxHealth());
	}

	@Override
	public InteractionResult mobInteract(Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);
		Item item = itemstack.getItem();

		if (this.isTame() && this.isOwnedBy(player))
		{
			if (!itemstack.isEmpty())
			{
				boolean special = false;

				if (item.isEdible() && itemstack.is(FLTags.Items.ENDERMINION_TAME_FOOD) && this.getHealth() < this.getMaxHealth())
				{
					this.playSound(SoundEvents.GENERIC_EAT, 1.0F, 0.7F);
					this.level().broadcastEntityEvent(this, (byte) 9);

					this.heal(itemstack.getFoodProperties(this).getNutrition());

					if (!player.isCreative())
						itemstack.shrink(1);

					return InteractionResult.SUCCESS;
				}
				else if (((special = (itemstack.getItem() == Items.ENCHANTED_GOLDEN_APPLE)) || itemstack.getItem() == Items.GOLDEN_APPLE) && !this.hasEffect(MobEffects.DAMAGE_BOOST))
				{
					int time = 30 * 20;

					if (special)
						time *= 2;

					this.addEffect(new MobEffectInstance(MobEffects.REGENERATION, time));
					this.addEffect(new MobEffectInstance(MobEffects.DAMAGE_BOOST, time));
					this.addEffect(new MobEffectInstance(MobEffects.FIRE_RESISTANCE, time));
					this.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SPEED, time));
					this.addEffect(new MobEffectInstance(MobEffects.JUMP, time));
					this.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, time));

					this.playSound(SoundEvents.ZOMBIE_VILLAGER_CURE, 0.5F, 1.5F);
					this.playSound(SoundEvents.GENERIC_EAT, 1.0F, 0.7F);
					this.level().broadcastEntityEvent(this, (byte) 10);

					if (!player.isCreative())
						itemstack.shrink(1);

					return InteractionResult.SUCCESS;
				}
				else if (this.getMainHandItem().isEmpty() && itemstack.is(FLTags.Items.ENDERMINION_HOLDABLE))
				{
					this.setItemSlot(EquipmentSlot.MAINHAND, itemstack.copyWithCount(1));
					this.setGuaranteedDrop(EquipmentSlot.MAINHAND);

					if (!player.isCreative())
						itemstack.shrink(1);

					this.playSound(SoundEvents.ITEM_PICKUP);
					this.playSound(SoundEvents.ARMOR_EQUIP_GENERIC);

					return InteractionResult.SUCCESS;
				}
				else if (item instanceof DyeItem dye)
				{
					DyeColor color = dye.getDyeColor();

					if (color != this.getBraceletColor())
					{
						this.setBraceletColor(color);

						if (!player.isCreative())
							itemstack.shrink(1);

						this.playSound(SoundEvents.DYE_USE);
						return InteractionResult.SUCCESS;
					}
				}
			}
			else if (player.isCrouching() && this.getMainHandItem().is(FLTags.Items.ENDERMINION_HOLDABLE))
			{
				ItemStack copy = this.getMainHandItem().copy();
				player.setItemSlot(EquipmentSlot.MAINHAND, copy);
				this.setItemSlot(EquipmentSlot.MAINHAND, ItemStack.EMPTY);

				this.playSound(SoundEvents.ITEM_PICKUP);
				this.playSound(SoundEvents.ARMOR_EQUIP_GENERIC);

				return InteractionResult.SUCCESS;
			}

			InteractionResult superResult = super.mobInteract(player, hand);

			if (!superResult.consumesAction() && this.isOwnedBy(player))
			{
				if (this.level().isClientSide())
					return InteractionResult.SUCCESS;

				int messageNum;
				if (!this.isOrderedToSit())
				{
					if (this.chatCooldown <= 0)
					{
						this.setOrderedToSit(!this.isOrderedToSit());
						messageNum = random.nextInt(3);

						this.setTarget(null);

						player.displayClientMessage(messageNum == 0 ? Component.translatable("gui.enderminion.sit.one") : messageNum == 1 ? Component.translatable("gui.enderminion.sit.two") : Component.translatable("gui.enderminion.sit.three"), true);
						this.chatCooldown = 5;

						return InteractionResult.SUCCESS;
					}
				}
				else
				{
					if (this.chatCooldown <= 0)
					{
						this.setOrderedToSit(!this.isOrderedToSit());
						messageNum = random.nextInt(3);

						if (messageNum == 0)
							player.displayClientMessage(Component.translatable("gui.enderminion.stand.one"), true);
						else if (messageNum == 1)
							player.displayClientMessage(Component.translatable("gui.enderminion.stand.two"), true);
						else
							player.displayClientMessage(Component.translatable("gui.enderminion.stand.three"), true);

						this.chatCooldown = 5;

						return InteractionResult.SUCCESS;
					}
				}
			}

			return superResult;
		}
		else if (!this.isTame() && itemstack.is(FLTags.Items.ENDERMINION_TAME_FOOD))
		{
			if (!this.level().isClientSide())
			{
				if (!player.isCreative())
					itemstack.shrink(1);

				this.setLastHurtByPlayer(null);
				this.setLastHurtByMob(null);
				this.setTarget(null);

				this.playSound(SoundEvents.GENERIC_EAT, 1.0F, 0.7F);

				if (this.tameItemsUsed < FarlandersConfig.COMMON.enderminionMinApples.get())
				{
					++this.tameItemsUsed;
					this.level().broadcastEntityEvent(this, (byte) 6);
					return InteractionResult.SUCCESS;
				}
				else if (this.random.nextInt(FarlandersConfig.COMMON.enderminionTameChance.get()) == 0 && !EventHooks.onAnimalTame(this, player))
				{
					this.tame(player);
					this.navigation.stop();
					this.setTarget(null);
					this.setOrderedToSit(true);
					this.setHealth(40.0F);
					this.level().broadcastEntityEvent(this, (byte) 7);

					this.tameItemsUsed = 0;
				}
				else
				{
					this.level().broadcastEntityEvent(this, (byte) 6);
				}
			}

			return InteractionResult.SUCCESS;
		}

		return super.mobInteract(player, hand);
	}

	@Override
	public boolean isFood(ItemStack stack)
	{
		return false;
	}

	@Override
	public int getMaxSpawnClusterSize()
	{
		return 1;
	}

	public void setEyeColor(int colorID)
	{
		this.entityData.set(EYE_COLOR, colorID);
	}

	public int getEyeColor()
	{
		return this.entityData.get(EYE_COLOR);
	}

	public DyeColor getBraceletColor()
	{
		return DyeColor.byId(this.entityData.get(BRACELET_COLOR));
	}

	public void setBraceletColor(DyeColor color)
	{
		this.entityData.set(BRACELET_COLOR, color.getId());
	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel worldIn, AgeableMob ageableIn)
	{
		return null;
	}

	@Override
	public boolean wantsToAttack(LivingEntity target, LivingEntity owner)
	{
		if (!(target instanceof Creeper) && !(target instanceof Ghast))
		{
			if (target instanceof OwnableEntity tamed && tamed.getOwner() == owner)
				return false;
			else if (target instanceof Player tp && owner instanceof Player op && !op.canHarmPlayer(tp))
				return false;
			else if (target instanceof AbstractHorse horse && horse.isTamed())
				return false;
		}

		return super.wantsToAttack(target, owner);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void handleEntityEvent(byte id)
	{
		if (id == 9)
		{
			for (int i = 0; i < 10; ++i)
			{
				double dx = this.random.nextGaussian() * 0.02D;
				double dy = this.random.nextGaussian() * 0.02D;
				double dz = this.random.nextGaussian() * 0.02D;
				this.level().addParticle(ParticleTypes.HAPPY_VILLAGER, this.getX() + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), this.getY() + (double) (this.random.nextFloat() * this.getBbHeight()), this.getZ() + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), dx, dy, dz);
			}

			for (int i = 0; i < 15; ++i)
				this.level().addParticle(ParticleTypes.AMBIENT_ENTITY_EFFECT, this.getX() + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), this.getY() + (double) (this.random.nextFloat() * this.getBbHeight()), this.getZ() + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), 248F / 255F, 36F / 255F, 35F / 255F);
		}
		else if (id == 10)
		{
			for (int i = 0; i < 15; ++i)
			{
				this.level().addParticle(ParticleTypes.AMBIENT_ENTITY_EFFECT, this.getX() + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), this.getY() + (double) (this.random.nextFloat() * this.getBbHeight()), this.getZ() + (double) (this.random.nextFloat() * this.getBbWidth() * 2.0F) - (double) this.getBbWidth(), 255F / 255F, 203F / 255F, 65F / 255F);
				this.level().addParticle(ParticleTypes.SMOKE, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), (this.random.nextDouble() - 0.5D) * 0.2F, 0.2F, (this.random.nextDouble() - 0.5D) * 0.2F);
			}
		}
		else
			super.handleEntityEvent(id);
	}
}