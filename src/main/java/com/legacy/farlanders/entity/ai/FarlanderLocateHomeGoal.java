package com.legacy.farlanders.entity.ai;

import java.util.stream.Collectors;

import com.legacy.farlanders.entity.FarlanderEntity;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.block.state.properties.BedPart;
import net.minecraft.world.phys.Vec3;

public class FarlanderLocateHomeGoal extends Goal
{
	private final FarlanderEntity mob;

	public FarlanderLocateHomeGoal(FarlanderEntity creature)
	{
		this.mob = creature;
	}

	@Override
	public boolean canUse()
	{
		return mob.getHomePos() == null && mob.tickCount % 100 <= 2;
	}

	/*@Override
	public boolean canContinueToUse()
	{
		return mob.getHomePos();
	}*/

	@Override
	public void start()
	{
		Level level = this.mob.level();
		BlockPos start = this.mob.blockPosition();
		int scale = 15;

		var beds = BlockPos.betweenClosedStream(start.offset(-scale, -scale, -scale), start.offset(scale, scale, scale)).map(BlockPos::immutable).filter(p -> level.getBlockState(p).getBlock()instanceof BedBlock bed && level.getBlockState(p).getValue(BedBlock.PART) == BedPart.FOOT).collect(Collectors.toList());

		BlockPos possibleHome = null;

		for (BlockPos bedPos : beds)
		{
			if (possibleHome != null)
			{
				if (this.mob.distanceToSqr(Vec3.atBottomCenterOf(bedPos)) < this.mob.distanceToSqr(Vec3.atBottomCenterOf(possibleHome)))
					possibleHome = bedPos;
			}
			else
				possibleHome = bedPos;
		}

		if (possibleHome != null)
		{
			mob.homePos = possibleHome;
			this.mob.homeFoundNaturally = true;
			mob.level().broadcastEntityEvent(mob, (byte) 14);
		}
	}

	@Override
	public void tick()
	{
		/*if (mob.tickCount % 10 == 0 || mob.tickCount < 5)
		{
			
		}*/
	}
}
