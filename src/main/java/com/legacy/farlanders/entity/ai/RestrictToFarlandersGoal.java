package com.legacy.farlanders.entity.ai;

import java.util.List;

import com.legacy.farlanders.entity.FarlanderEntity;
import com.legacy.farlanders.entity.hostile.EnderGolemEntity;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.Goal;

public class RestrictToFarlandersGoal extends Goal
{
	private List<BlockPos> nearbyFarlanders;
	private final PathfinderMob restrictingMob;
	private int checkDelay;

	public RestrictToFarlandersGoal(PathfinderMob mob)
	{
		this.restrictingMob = mob;
	}

	@Override
	public boolean canUse()
	{
		if (this.restrictingMob instanceof EnderGolemEntity golem && golem.spirePos != null)
			return false;

		if (--this.checkDelay > 0)
			return false;

		this.checkDelay = 10 * (20 / 2);

		List<FarlanderEntity> list = this.restrictingMob.level().getEntitiesOfClass(FarlanderEntity.class, this.restrictingMob.getBoundingBox().inflate(30.0D, 10.0D, 30.0D));

		if (list.isEmpty())
			return false;

		return (this.nearbyFarlanders = list.stream().map(e -> e.blockPosition()).toList()) != null && !this.nearbyFarlanders.isEmpty();
	}

	@Override
	public boolean canContinueToUse()
	{
		return false;
	}

	@Override
	public void start()
	{
		if (this.nearbyFarlanders == null)
			return;

		int count = nearbyFarlanders.size();

		if (count > 0)
		{
			int totalX = 0;
			int totalZ = 0;

			for (BlockPos pos : nearbyFarlanders)
			{
				totalX += pos.getX();
				totalZ += pos.getZ();
			}

			BlockPos avg = new BlockPos(totalX / count, this.restrictingMob.getBlockY(), totalZ / count);
			this.restrictingMob.restrictTo(avg, 20);

			/*if (this.golem.level instanceof ServerLevel lv)
				lv.sendParticles(new BlockParticleOption(ParticleTypes.BLOCK_MARKER, Blocks.BARRIER.defaultBlockState()), avg.getX(), avg.getY() + 2, avg.getZ(), 1, 0, 0, 0, 0);*/
		}
	}

	@Override
	public void stop()
	{
		this.nearbyFarlanders = null;
		this.checkDelay = 20 * (20 / 2);
	}

	@Override
	public void tick()
	{
	}
}