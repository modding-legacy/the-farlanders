package com.legacy.farlanders.entity.ai;

import java.util.EnumSet;
import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.entity.util.BreedableVillagerEntity;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.ai.goal.BreedGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.targeting.TargetingConditions;
import net.minecraft.world.level.Level;

/**
 * Modified version of {@link BreedGoal} to support non-animals
 */
public class FarlanderBreedGoal extends Goal
{
	private static final TargetingConditions PARTNER_TARGETING = TargetingConditions.forNonCombat().range(8.0D).ignoreLineOfSight();
	protected final BreedableVillagerEntity animal;
	private final Class<? extends BreedableVillagerEntity> partnerClass;
	protected final Level level;
	@Nullable
	protected BreedableVillagerEntity partner;
	private int loveTime;
	private final double speedModifier;

	public FarlanderBreedGoal(BreedableVillagerEntity pBreedableVillagerEntity, double pSpeedModifier)
	{
		this(pBreedableVillagerEntity, pSpeedModifier, pBreedableVillagerEntity.getClass());
	}

	public FarlanderBreedGoal(BreedableVillagerEntity pBreedableVillagerEntity, double pSpeedModifier, Class<? extends BreedableVillagerEntity> pPartnerClass)
	{
		this.animal = pBreedableVillagerEntity;
		this.level = pBreedableVillagerEntity.level();
		this.partnerClass = pPartnerClass;
		this.speedModifier = pSpeedModifier;
		this.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
	}

	@Override
	public boolean canUse()
	{
		if (!this.animal.isInLove())
		{
			return false;
		}
		else
		{
			this.partner = this.getFreePartner();
			return this.partner != null;
		}
	}

	@Override
	public boolean canContinueToUse()
	{
		return this.partner.isAlive() && this.partner.isInLove() && this.loveTime < 60;
	}

	@Override
	public void stop()
	{
		this.partner = null;
		this.loveTime = 0;
	}

	@Override
	public void tick()
	{
		this.animal.getLookControl().setLookAt(this.partner, 10.0F, (float) this.animal.getMaxHeadXRot());
		this.animal.getNavigation().moveTo(this.partner, this.speedModifier);
		++this.loveTime;
		
		if (this.loveTime >= this.adjustedTickDelay(60) && this.animal.distanceToSqr(this.partner) < 9.0D)
			this.breed();
	}

	@Nullable
	private BreedableVillagerEntity getFreePartner()
	{
		List<? extends BreedableVillagerEntity> list = this.level.getNearbyEntities(this.partnerClass, PARTNER_TARGETING, this.animal, this.animal.getBoundingBox().inflate(8.0D));
		double d0 = Double.MAX_VALUE;
		BreedableVillagerEntity fl = null;

		for (BreedableVillagerEntity possibleMate : list)
		{
			if (this.animal.canMateWith(possibleMate) && this.animal.distanceToSqr(possibleMate) < d0)
			{
				fl = possibleMate;
				d0 = this.animal.distanceToSqr(possibleMate);
			}
		}

		return fl;
	}

	protected void breed()
	{
		this.animal.spawnChildFromBreeding((ServerLevel) this.level, this.partner);
	}
}