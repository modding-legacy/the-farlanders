package com.legacy.farlanders.entity.ai;

import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.MoveTowardsRestrictionGoal;

public class MoveTowardsRestrictionIfPresentGoal extends MoveTowardsRestrictionGoal
{
	private final PathfinderMob checkingMob;

	public MoveTowardsRestrictionIfPresentGoal(PathfinderMob pMob, double pSpeedModifier)
	{
		super(pMob, pSpeedModifier);

		this.checkingMob = pMob;
	}

	@Override
	public boolean canUse()
	{
		return super.canUse() && this.checkingMob.hasRestriction();
	}
}
