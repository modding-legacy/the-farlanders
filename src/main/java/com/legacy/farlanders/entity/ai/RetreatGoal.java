package com.legacy.farlanders.entity.ai;

import java.util.EnumSet;

import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.monster.Monster;

public class RetreatGoal extends Goal
{
	private final Monster attacker;

	public RetreatGoal(Monster target)
	{
		super();
		this.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));

		this.attacker = target;
	}

	@Override
	public boolean canUse()
	{
		return this.attacker.isAlive() && this.attacker.getTarget() != null && (this.attacker.getTarget().getY() > this.attacker.getEyeY() || this.attacker.getTarget().isInWaterOrBubble() || !this.attacker.level().getFluidState(this.attacker.getTarget().blockPosition().below()).isEmpty());
	}

	@Override
	public boolean canContinueToUse()
	{
		return this.canUse();
	}

	@Override
	public void start()
	{
		this.attacker.getNavigation().stop();
	}

	@Override
	public void tick()
	{
		var target = this.attacker.getTarget();

		if (target == null)
			return;

		this.attacker.getLookControl().setLookAt(target);

		if (this.attacker.distanceTo(target) < 8.0F)
		{
			this.attacker.getNavigation().stop();
			this.attacker.setYRot(this.attacker.getYHeadRot());
			this.attacker.setYBodyRot(this.attacker.getYRot());
			this.attacker.getMoveControl().strafe(-1.2F, 0.0F);
		}
		else if (this.attacker.distanceTo(target) > 12 && this.attacker.distanceTo(target) < 18)
			this.attacker.getNavigation().moveTo(target, 0.4F);
	}

	@Override
	public void stop()
	{
	}

	@Override
	public boolean requiresUpdateEveryTick()
	{
		return true;
	}
}