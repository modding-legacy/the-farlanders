package com.legacy.farlanders.entity;

import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.legacy.farlanders.entity.hostile.RebelEntity;
import com.legacy.farlanders.entity.util.FarlanderTrades;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.npc.VillagerTrades.ItemListing;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;

public class ElderFarlanderEntity extends FarlanderEntity
{
	public ElderFarlanderEntity(EntityType<? extends ElderFarlanderEntity> type, Level world)
	{
		super(type, world);
		this.setEyeColor(10);
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return FarlanderEntity.registerAttributeMap().add(Attributes.MOVEMENT_SPEED, 0.4D).add(Attributes.ATTACK_KNOCKBACK, 2.5D);
	}

	@Override
	public void addAvoidGoals(float farSpeed, float closeSpeed)
	{
		this.goalSelector.addGoal(1, new AvoidAndAlertGuardsAttackGoal<>(this, LooterEntity.class, 8.0F, farSpeed, closeSpeed));
		this.goalSelector.addGoal(1, new AvoidAndAlertGuardsAttackGoal<>(this, RebelEntity.class, 8.0F, farSpeed, closeSpeed));
		this.goalSelector.addGoal(1, new AvoidAndAlertGuardsAttackGoal<>(this, Zombie.class, 8.0F, farSpeed, closeSpeed));
	}

	@Override
	protected Int2ObjectMap<ItemListing[]> getTradeList()
	{
		return FarlanderTrades.ELDER_TRADES;
	}

	@Override
	public float getVoicePitch()
	{
		return 0.7F;
	}

	@Override
	public boolean isBreedingItem(ItemStack stack)
	{
		return false;
	}

	// TODO
	@Override
	protected AABB getAttackBoundingBox()
	{
		return super.getAttackBoundingBox().inflate(2.2F);
	}

	@Override
	public void spawnEyeParticles()
	{
	}

	class AvoidAndAlertGuardsAttackGoal<T extends LivingEntity> extends AvoidAndAlertGuardsGoal<T>
	{
		public AvoidAndAlertGuardsAttackGoal(PathfinderMob pMob, Class<T> pEntityClassToAvoid, float pMaxDistance, double pWalkSpeedModifier, double pSprintSpeedModifier)
		{
			super(pMob, pEntityClassToAvoid, pMaxDistance, pWalkSpeedModifier, pSprintSpeedModifier);
		}

		@Override
		public void tick()
		{
			/*if (this.lookBackTime > 0)
				--this.lookBackTime;
			
			if (this.lookBackTime > 0 && this.toAvoid != null)
			{
				this.mob.lookAt(this.toAvoid, 100, 100);
				return;
			}*/

			super.tick();

			if (this.toAvoid != null && !this.toAvoid.isRemoved() && this.toAvoid.hurtTime <= 0 && this.mob.isWithinMeleeAttackRange(this.toAvoid))
			{
				this.mob.swing(InteractionHand.MAIN_HAND);
				this.mob.doHurtTarget(this.toAvoid);
			}

		}

		@Override
		public boolean canContinueToUse()
		{
			return super.canContinueToUse()/* || this.mob != null && !this.mob.isRemoved() && lookBackTime > 0*/;
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return this.toAvoid != null && !this.toAvoid.isRemoved();
		}
	}
}