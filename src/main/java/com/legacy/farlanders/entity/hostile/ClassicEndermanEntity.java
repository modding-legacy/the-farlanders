package com.legacy.farlanders.entity.hostile;

import java.util.function.Predicate;

import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.registry.FLTriggers;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.goal.target.ResetUniversalAngerTargetGoal;
import net.minecraft.world.entity.monster.EnderMan;
import net.minecraft.world.entity.monster.Endermite;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;

public class ClassicEndermanEntity extends EnderMan
{
	public ClassicEndermanEntity(EntityType<? extends ClassicEndermanEntity> type, Level level)
	{
		super(type, level);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(1, new EnderMan.EndermanFreezeWhenLookedAt(this));
		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.0D, false));
		this.goalSelector.addGoal(7, new WaterAvoidingRandomStrollGoal(this, 1.0D, 0.0F));
		this.goalSelector.addGoal(8, new LookAtPlayerGoal(this, Player.class, 8.0F));
		this.goalSelector.addGoal(8, new RandomLookAroundGoal(this));
		this.goalSelector.addGoal(10, new EnderMan.EndermanLeaveBlockGoal(this));
		this.goalSelector.addGoal(11, new ClassicTakeBlockGoal(this));
		this.targetSelector.addGoal(1, new ClassicLookForPlayerGoal(this, this::isAngryAt));
		this.targetSelector.addGoal(2, new HurtByTargetGoal(this));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, Endermite.class, true, false));
		this.targetSelector.addGoal(4, new ResetUniversalAngerTargetGoal<>(this, false));
	}

	@Override
	public void aiStep()
	{
		if (this.level().isClientSide())
			this.level().addParticle(ParticleTypes.LARGE_SMOKE, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), 0, 0, 0); // largesmoke

		super.aiStep();
	}

	@Override
	public void die(DamageSource pDamageSource)
	{
		if (pDamageSource.getEntity()instanceof ServerPlayer sp && this.getCarriedBlock() != null)
			FLTriggers.CLASSIC_ENDERMAN_KILL.trigger(sp, this, this.getCarriedBlock().getBlock());

		super.die(pDamageSource);
	}

	private static class ClassicTakeBlockGoal extends EndermanTakeBlockGoal
	{
		private final EnderMan man;

		public ClassicTakeBlockGoal(EnderMan pEnderman)
		{
			super(pEnderman);
			this.man = pEnderman;
		}

		@Override
		public void tick()
		{
			RandomSource randomsource = this.man.getRandom();
			Level level = this.man.level();
			int i = Mth.floor(this.man.getX() - 2.0D + randomsource.nextDouble() * 4.0D);
			int j = Mth.floor(this.man.getY() + randomsource.nextDouble() * 3.0D);
			int k = Mth.floor(this.man.getZ() - 2.0D + randomsource.nextDouble() * 4.0D);
			BlockPos blockpos = new BlockPos(i, j, k);
			BlockState blockstate = level.getBlockState(blockpos);
			Vec3 vec3 = new Vec3((double) this.man.getBlockX() + 0.5D, (double) j + 0.5D, (double) this.man.getBlockZ() + 0.5D);
			Vec3 vec31 = new Vec3((double) i + 0.5D, (double) j + 0.5D, (double) k + 0.5D);
			BlockHitResult blockhitresult = level.clip(new ClipContext(vec3, vec31, ClipContext.Block.OUTLINE, ClipContext.Fluid.NONE, this.man));
			boolean flag = blockhitresult.getBlockPos().equals(blockpos);

			if (blockstate.is(FLTags.Blocks.CLASSIC_ENDERMAN_HOLDABLE) && flag)
			{
				level.removeBlock(blockpos, false);
				level.gameEvent(GameEvent.BLOCK_DESTROY, blockpos, GameEvent.Context.of(this.man, blockstate));
				this.man.setCarriedBlock(blockstate.getBlock().defaultBlockState());
			}

		}
	}

	private static class ClassicLookForPlayerGoal extends EndermanLookForPlayerGoal
	{
		public ClassicLookForPlayerGoal(EnderMan pEnderman, Predicate<LivingEntity> pSelectionPredicate)
		{
			super(pEnderman, pSelectionPredicate);
		}

		@Override
		public void start()
		{
			super.start();
			this.pendingTarget.addEffect(new MobEffectInstance(MobEffects.CONFUSION, 8 * 41));
		}
	}
}