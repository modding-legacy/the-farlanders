package com.legacy.farlanders.entity.hostile.boss.summon;

public class MiniDragonEntity// extends Mob implements Enemy
{
	/*private final ServerBossEvent bossInfo = (ServerBossEvent) (new ServerBossEvent(this.getDisplayName(), BossEvent.BossBarColor.GREEN, BossEvent.BossBarOverlay.PROGRESS)).setDarkenScreen(false);
	
	public double targetX;
	public double targetY;
	public double targetZ;
	
	public double[][] ringBuffer = new double[64][3];
	public int ringBufferIndex = -1;
	
	public MiniDragonPartEntity[] dragonPartArray;
	public MiniDragonPartEntity dragonPartHead;
	public MiniDragonPartEntity dragonPartBody;
	public MiniDragonPartEntity dragonPartTail1;
	public MiniDragonPartEntity dragonPartTail2;
	public MiniDragonPartEntity dragonPartTail3;
	public MiniDragonPartEntity dragonPartWing1;
	public MiniDragonPartEntity dragonPartWing2;
	
	public float prevAnimTime;
	public float animTime;
	public boolean forceNewTarget;
	public boolean slowed;
	private Entity target;
	public int deathTicks;
	private boolean shotFireball;
	private float randomYawVelocity; // TODO
	public double posX = this.getX();
	public double posY = this.getY();
	public double posZ = this.getZ();
	
	public MiniDragonEntity(EntityType<? extends MiniDragonEntity> type, Level world)
	{
		super(type, world);
		this.shotFireball = false;
		this.dragonPartArray = new MiniDragonPartEntity[] { this.dragonPartHead = new MiniDragonPartEntity(this, "head", 6.0F, 6.0F), this.dragonPartBody = new MiniDragonPartEntity(this, "body", 8.0F, 8.0F), this.dragonPartTail1 = new MiniDragonPartEntity(this, "tail", 4.0F, 4.0F), this.dragonPartTail2 = new MiniDragonPartEntity(this, "tail", 4.0F, 4.0F), this.dragonPartTail3 = new MiniDragonPartEntity(this, "tail", 4.0F, 4.0F), this.dragonPartWing1 = new MiniDragonPartEntity(this, "wing", 4.0F, 4.0F), this.dragonPartWing2 = new MiniDragonPartEntity(this, "wing", 4.0F, 4.0F) };
		this.setHealth(this.getMaxHealth());
		this.noPhysics = false;
		this.targetY = 100.0D;
		this.noCulling = true;
	}
	
	protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(50.0D);
	}
	
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
	
		if (this.hasCustomName())
		{
			this.bossInfo.setName(this.getDisplayName());
		}
	}
	
	public void setCustomName(@Nullable Component name)
	{
		super.setCustomName(name);
		this.bossInfo.setName(this.getDisplayName());
	}
	
	public void startSeenByPlayer(ServerPlayer player)
	{
		super.startSeenByPlayer(player);
		this.bossInfo.addPlayer(player);
	}
	
	public void stopSeenByPlayer(ServerPlayer player)
	{
		super.stopSeenByPlayer(player);
		this.bossInfo.removePlayer(player);
	}
	
	public double[] getMovementOffsets(int p_70974_1_, float p_70974_2_)
	{
		if (this.getHealth() <= 0.0F)
		{
			p_70974_2_ = 0.0F;
		}
	
		p_70974_2_ = 1.0F - p_70974_2_;
		int j = this.ringBufferIndex - p_70974_1_ * 1 & 63;
		int k = this.ringBufferIndex - p_70974_1_ * 1 - 1 & 63;
		double[] adouble = new double[3];
		double d0 = this.ringBuffer[j][0];
		double d1 = Mth.wrapDegrees(this.ringBuffer[k][0] - d0);
		adouble[0] = d0 + d1 * (double) p_70974_2_;
		d0 = this.ringBuffer[j][1];
		d1 = this.ringBuffer[k][1] - d0;
		adouble[1] = d0 + d1 * (double) p_70974_2_;
		adouble[2] = this.ringBuffer[j][2] + (this.ringBuffer[k][2] - this.ringBuffer[j][2]) * (double) p_70974_2_;
		return adouble;
	}
	
	@Override
	public void aiStep()
	{
		float f;
		float f1;
	
		this.remove();
	
		if (!this.isNoAi())
		{
			this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
	
			if (this.level.isClientSide)
			{
				f = Mth.cos(this.animTime * (float) Math.PI * 2.0F);
				f1 = Mth.cos(this.prevAnimTime * (float) Math.PI * 2.0F);
	
				if (f1 <= -0.3F && f >= -0.3F)
				{
					this.level.playLocalSound(this.posX, this.posY, this.posZ, SoundEvents.ENDER_DRAGON_FLAP, SoundSource.HOSTILE, 3.0F, 1.0F + this.random.nextFloat() * 0.3F, false);
				}
			}
	
			this.prevAnimTime = this.animTime;
	
			if (this.getHealth() <= 0.0F)
			{
				f = (this.random.nextFloat() - 0.5F) * 8.0F;
				f1 = (this.random.nextFloat() - 0.5F) * 4.0F;
				for (int i = 0; i < 3; i++)
				{
					this.level.addParticle(ParticleTypes.LAVA, this.posX + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.posY + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.posZ + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), 0, 0, 0);
				}
			}
			else
			{
				for (int i = 0; i < 10; i++)
					level.addParticle(ParticleTypes.SMOKE, posX + (random.nextDouble() - 0.5D) * (double) this.getBbWidth(), (posY + random.nextDouble() * (double) this.getBbHeight()) - 0.25D, posZ + (random.nextDouble() - 0.5D) * (double) this.getBbWidth(), 0, 0, 0);
				level.addParticle(ParticleTypes.FLAME, posX + (random.nextDouble() - 0.5D) * (double) this.getBbWidth(), (posY + random.nextDouble() * (double) this.getBbHeight()) - 0.25D, posZ + (random.nextDouble() - 0.5D) * (double) this.getBbWidth(), 0, 0, 0);
				Vec3 vec3d4 = this.getDeltaMovement();
				float f12 = 0.2F / (Mth.sqrt(getHorizontalDistanceSqr(vec3d4)) * 10.0F + 1.0F);
				f12 = f12 * (float) Math.pow(2.0D, vec3d4.y);
	
				if (this.slowed)
				{
					this.animTime += f12 * 0.5F;
				}
				else
				{
					this.animTime += f12;
				}
	
				this.yRot = Mth.wrapDegrees(this.yRot);
				if (this.isNoAi())
				{
					this.animTime = 0.5F;
				}
				else
				{
					if (this.ringBufferIndex < 0)
					{
						for (int i = 0; i < this.ringBuffer.length; ++i)
						{
							this.ringBuffer[i][0] = (double) this.yRot;
							this.ringBuffer[i][1] = this.posY;
						}
					}
	
					if (++this.ringBufferIndex == this.ringBuffer.length)
					{
						this.ringBufferIndex = 0;
					}
	
					this.ringBuffer[this.ringBufferIndex][0] = (double) this.yRot;
					this.ringBuffer[this.ringBufferIndex][1] = this.posY;
					if (this.level.isClientSide)
					{
						if (this.lerpSteps > 0)
						{
							double d7 = this.posX + (this.lerpX - this.posX) / (double) this.lerpSteps;
							double d0 = this.posY + (this.lerpY - this.posY) / (double) this.lerpSteps;
							double d1 = this.posZ + (this.lerpZ - this.posZ) / (double) this.lerpSteps;
							double d2 = Mth.wrapDegrees(this.lerpYRot - (double) this.yRot);
							this.yRot = (float) ((double) this.yRot + d2 / (double) this.lerpSteps);
							this.xRot = (float) ((double) this.xRot + (this.lerpXRot - (double) this.xRot) / (double) this.lerpSteps);
							--this.lerpSteps;
							this.setPos(d7, d0, d1);
							this.setRot(this.yRot, this.xRot);
						}
	
					}
					else
					{
						Vec3 vec3d = new Vec3(this.targetX, this.targetY, this.targetZ);
	
						// if (this.target != null)
						{
							double d8 = vec3d.x - this.posX;
							double d9 = vec3d.y - this.posY;
							double d10 = vec3d.z - this.posZ;
							double d3 = d8 * d8 + d9 * d9 + d10 * d10;
							float f5 = 10;
							double d4 = (double) Mth.sqrt(d8 * d8 + d10 * d10);
							if (d4 > 0.0D)
							{
								d9 = Mth.clamp(d9 / d4, (double) (-f5), (double) f5);
							}
	
							this.setDeltaMovement(this.getDeltaMovement().add(0.0D, d9 * 0.01D, 0.0D));
							this.yRot = Mth.wrapDegrees(this.yRot);
							double d5 = Mth.clamp(Mth.wrapDegrees(180.0D - Mth.atan2(d8, d10) * (double) (180F / (float) Math.PI) - (double) this.yRot), -50.0D, 50.0D);
							Vec3 vec3d1 = vec3d.subtract(this.posX, this.posY, this.posZ).normalize();
							Vec3 vec3d2 = (new Vec3((double) Mth.sin(this.yRot * ((float) Math.PI / 180F)), this.getDeltaMovement().y, (double) (-Mth.cos(this.yRot * ((float) Math.PI / 180F))))).normalize();
							float f8 = Math.max(((float) vec3d2.dot(vec3d1) + 0.5F) / 1.5F, 0.0F);
							this.randomYawVelocity *= 0.8F;
							this.randomYawVelocity = (float) ((double) this.randomYawVelocity + d5);
							this.yRot += this.randomYawVelocity * 0.1F;
							float f9 = (float) (2.0D / (d3 + 1.0D));
							this.moveRelative(0.06F * (f8 * f9 + (1.0F - f9)), new Vec3(0.0D, 0.0D, -1.0D));
							if (this.slowed)
							{
								this.move(MoverType.SELF, this.getDeltaMovement().scale((double) 0.8F));
							}
							else
							{
								this.move(MoverType.SELF, this.getDeltaMovement());
							}
	
							Vec3 vec3d3 = this.getDeltaMovement().normalize();
							double d6 = 0.8D + 0.15D * (vec3d3.dot(vec3d2) + 1.0D) / 2.0D;
							this.setDeltaMovement(this.getDeltaMovement().multiply(d6, (double) 0.91F, d6));
						}
	
						if (this.target != null)
						{
							this.targetX = this.target.getX();
							this.targetZ = this.target.getZ();
							double d3 = this.targetX - this.posX;
							double d5 = this.targetZ - this.posZ;
							double d7 = Math.sqrt(d3 * d3 + d5 * d5);
							double d8 = 0.4000000059604645D + d7 / 80.0D - 1.0D;
	
							if (d8 > 10.0D)
							{
								d8 = 10.0D;
							}
	
							this.targetY = this.target.getBoundingBox().minY + d8;
						}
						else
						{
							this.targetX += this.random.nextGaussian() * 2.0D;
							this.targetZ += this.random.nextGaussian() * 2.0D;
						}
	
						double d10 = this.targetX - this.posX;
						double d0 = this.targetY - this.posY;
						double d1 = this.targetZ - this.posZ;
						double d2 = d10 * d10 + d0 * d0 + d1 * d1;
	
						if (this.forceNewTarget || d2 < 100.0D || d2 > 22500.0D || this.horizontalCollision || this.verticalCollision)
						{
							this.setNewTarget();
						}
					}
	
					this.yBodyRot = this.yRot;
					Vec3[] avec3d = new Vec3[this.dragonPartArray.length];
	
					for (int j = 0; j < this.dragonPartArray.length; ++j)
					{
						avec3d[j] = new Vec3(this.dragonPartArray[j].getX(), this.dragonPartArray[j].getY(), this.dragonPartArray[j].getZ());
					}
	
					float f15 = (float) (this.getMovementOffsets(5, 1.0F)[1] - this.getMovementOffsets(10, 1.0F)[1]) * 10.0F * ((float) Math.PI / 180F);
					float f16 = Mth.cos(f15);
					float f21 = Mth.sin(f15);
					float f17 = this.yRot * ((float) Math.PI / 180F);
					float f3 = Mth.sin(f17);
					float f18 = Mth.cos(f17);
					this.dragonPartBody.tick();
					this.dragonPartBody.moveTo(this.posX + (double) (f3 * 0.5F), this.posY, this.posZ - (double) (f18 * 0.5F), 0.0F, 0.0F);
					this.dragonPartWing1.tick();
					this.dragonPartWing1.moveTo(this.posX + (double) (f18 * 4.5F), this.posY + 2.0D, this.posZ + (double) (f3 * 4.5F), 0.0F, 0.0F);
					this.dragonPartWing2.tick();
					this.dragonPartWing2.moveTo(this.posX - (double) (f18 * 4.5F), this.posY + 2.0D, this.posZ - (double) (f3 * 4.5F), 0.0F, 0.0F);
					if (!this.level.isClientSide && this.hurtTime == 0)
					{
						this.collideWithEntities(this.level.getEntities(this, this.dragonPartWing1.getBoundingBox().inflate(4.0D, 2.0D, 4.0D).move(0.0D, -2.0D, 0.0D), EntitySelector.NO_CREATIVE_OR_SPECTATOR));
						this.collideWithEntities(this.level.getEntities(this, this.dragonPartWing2.getBoundingBox().inflate(4.0D, 2.0D, 4.0D).move(0.0D, -2.0D, 0.0D), EntitySelector.NO_CREATIVE_OR_SPECTATOR));
						this.attackEntitiesInList(this.level.getEntities(this, this.dragonPartHead.getBoundingBox().inflate(1.0D), EntitySelector.NO_CREATIVE_OR_SPECTATOR));
					}
	
					double[] adouble = this.getMovementOffsets(5, 1.0F);
					float f19 = Mth.sin(this.yRot * ((float) Math.PI / 180F) - this.randomYawVelocity * 0.01F);
					float f4 = Mth.cos(this.yRot * ((float) Math.PI / 180F) - this.randomYawVelocity * 0.01F);
					this.dragonPartHead.tick();
					float f20 = 0;
					this.dragonPartHead.moveTo(this.posX + (double) (f19 * 6.5F * f16), this.posY + (double) f20 + (double) (f21 * 6.5F), this.posZ - (double) (f4 * 6.5F * f16), 0.0F, 0.0F);
	
					for (int k = 0; k < 3; ++k)
					{
						MiniDragonPartEntity enderdragonpartentity = null;
						if (k == 0)
						{
							enderdragonpartentity = this.dragonPartTail1;
						}
	
						if (k == 1)
						{
							enderdragonpartentity = this.dragonPartTail2;
						}
	
						if (k == 2)
						{
							enderdragonpartentity = this.dragonPartTail3;
						}
	
						double[] adouble1 = this.getMovementOffsets(12 + k * 2, 1.0F);
						float f6 = Mth.sin(f21);
						float f22 = Mth.cos(f21);
						float f23 = (float) (k + 1) * 2.0F;
						enderdragonpartentity.tick();
						enderdragonpartentity.moveTo(this.posX - (double) ((f3 * 1.5F + f6 * f23) * f16), this.posY + (adouble1[1] - adouble[1]) - (double) ((f23 + 1.5F) * f21) + 1.5D, this.posZ + (double) ((f18 * 1.5F + f22 * f23) * f16), 0.0F, 0.0F);
					}
	
					if (!this.level.isClientSide)
					{
						this.slowed = this.destroyBlocksInAABB(this.dragonPartHead.getBoundingBox()) | this.destroyBlocksInAABB(this.dragonPartBody.getBoundingBox());
					}
	
					for (int l = 0; l < this.dragonPartArray.length; ++l)
					{
						this.dragonPartArray[l].xo = avec3d[l].x;
						this.dragonPartArray[l].yo = avec3d[l].y;
						this.dragonPartArray[l].zo = avec3d[l].z;
					}
	
				}
			}
	
			if (this.target != null)
			{
				if (this.target instanceof Player)
				{
					if (this.target.distanceToSqr(this) < 256.0D && shotFireball == false)
					{
						double d0 = target.getX() - this.posX;
						double d1 = target.getBoundingBox().minY + (double) (target.getBbHeight() / 2.0F) - (this.posY + (double) (this.getBbHeight() / 2.0F));
						double d2 = target.getZ() - this.posZ;
						double d5 = this.target.getX() - this.posX;
						double d6 = this.target.getBoundingBox().minY + (double) (this.target.getBbHeight() / 2.0F) - (this.posY + (double) (this.getBbHeight() / 2.0F));
						double d7 = this.target.getZ() - this.posZ;
	
						if (random.nextInt(15) == 0)
						{
							for (int i = 0; i < 3; i++)
							{
								SmallFireball entitysmallfireball = new SmallFireball(this.level, this, d0 + this.random.nextGaussian() * 0.6D, d1, d2 + this.random.nextGaussian() * 0.6D);
								entitysmallfireball.setPos(entitysmallfireball.getX(), this.posY + (double) (this.getBbHeight() / 2.0F) - 0.5D, entitysmallfireball.getZ());
								;
								this.level.addFreshEntity(entitysmallfireball);
								shotFireball = true;
								level.levelEvent((Player) null, 1017, new BlockPos(this.position()), 0);
							}
						}
	
						else if (random.nextInt(10) == 0)
						{
							level.levelEvent((Player) null, 1017, new BlockPos(this.position()), 0);
							LargeFireball entitylargefireball = new LargeFireball(this.level, this, d5, d6, d7);
							entitylargefireball.setPos(entitylargefireball.getX(), this.posY + (double) (this.getBbHeight() / 2.0F) - 0.5D, entitylargefireball.getZ());
							;
	
							this.level.addFreshEntity(entitylargefireball);
							shotFireball = true;
						}
	
						else if (random.nextInt(5) == 0)
						{
							SmallFireball entitysmallfireball = new SmallFireball(this.level, this, d0 + this.random.nextGaussian() * 0.6D, d1, d2 + this.random.nextGaussian() * 0.6D);
							entitysmallfireball.setPos(entitysmallfireball.getX(), this.posY + (double) (this.getBbHeight() / 2.0F) - 0.5D, entitysmallfireball.getZ());
							;
	
							this.level.addFreshEntity(entitysmallfireball);
							shotFireball = true;
							level.levelEvent((Player) null, 1017, new BlockPos(this.position()), 0);
	
						}
					}
					else if (this.target.distanceToSqr(this) > 256.0D)
						shotFireball = false;
				}
			}
		}
	}
	
	private void collideWithEntities(List<?> p_70970_1_)
	{
		double d0 = (this.getBoundingBox().minX + this.getBoundingBox().maxX) / 2.0D;
		double d1 = (this.getBoundingBox().minZ + this.getBoundingBox().maxZ) / 2.0D;
		Iterator<?> iterator = p_70970_1_.iterator();
	
		while (iterator.hasNext())
		{
			Entity entity = (Entity) iterator.next();
	
			if (entity instanceof LivingEntity && !(entity instanceof MiniDragonEntity) && !(entity instanceof EnderColossusEntity) && !(entity instanceof EnderColossusShadowEntity))
			{
				double d2 = entity.getX() - d0;
				double d3 = entity.getZ() - d1;
				double d4 = d2 * d2 + d3 * d3;
				entity.push(d2 / d4 * 1.1D, 0.20000000298023224D, d3 / d4 * 0.7D);
			}
		}
	}
	
	private void attackEntitiesInList(List<?> p_70971_1_)
	{
		for (int i = 0; i < p_70971_1_.size(); ++i)
		{
			Entity entity = (Entity) p_70971_1_.get(i);
	
			if (entity instanceof LivingEntity && !(entity instanceof MiniDragonEntity) && !(entity instanceof EnderColossusEntity) && !(entity instanceof EnderColossusShadowEntity))
			{
				entity.hurt(DamageSource.mobAttack(this), 1.0F);
			}
		}
	}
	
	private void setNewTarget()
	{
		this.forceNewTarget = false;
	
		if (this.random.nextInt(2) == 0 && !this.level.players().isEmpty())
		{
			this.target = (Entity) this.level.players().get(this.random.nextInt(this.level.players().size()));
		}
		else
		{
			boolean flag = false;
	
			do
			{
				this.targetX = 0.0D;
				this.targetY = (double) (70.0F + this.random.nextFloat() * 50.0F);
				this.targetZ = 0.0D;
				this.targetX += (double) (this.random.nextFloat() * 120.0F - 60.0F);
				this.targetZ += (double) (this.random.nextFloat() * 120.0F - 60.0F);
				double d0 = this.posX - this.targetX;
				double d1 = this.posY - this.targetY;
				double d2 = this.posZ - this.targetZ;
				flag = d0 * d0 + d1 * d1 + d2 * d2 > 100.0D;
			}
			while (!flag);
	
			this.target = null;
		}
	}
	
	public boolean hurt(DamageSource source, float amount)
	{
		return super.hurt(source, amount);
	}
	
	public boolean hurt(MiniDragonPartEntity p_70965_1_, DamageSource p_70965_2_, float p_70965_3_)
	{
		if (p_70965_1_ != this.dragonPartHead)
		{
			p_70965_3_ = p_70965_3_ / 4.0F + 1.0F;
		}
	
		float f1 = this.yRot * (float) Math.PI / 180.0F;
		float f2 = Mth.sin(f1);
		float f3 = Mth.cos(f1);
		this.targetX = this.posX + (double) (f2 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
		this.targetY = this.posY + (double) (this.random.nextFloat() * 3.0F) + 1.0D;
		this.targetZ = this.posZ - (double) (f3 * 5.0F) + (double) ((this.random.nextFloat() - 0.5F) * 2.0F);
		this.target = null;
	
		if (p_70965_2_.getEntity() instanceof Player || p_70965_2_.isExplosion())
		{
			this.hurt(p_70965_2_, p_70965_3_);
		}
	
		return true;
	}
	
	// commented 1
	public boolean hurt(EnderDragonPartEntity p_213403_1_, DamageSource p_213403_2_, float p_213403_3_) {
	      p_213403_3_ = this.phaseManager.getCurrentPhase().onHurt(p_213403_2_, p_213403_3_);
	      if (p_213403_1_ != this.dragonPartHead) {
	         p_213403_3_ = p_213403_3_ / 4.0F + Math.min(p_213403_3_, 1.0F);
	      }
	
	      if (p_213403_3_ < 0.01F) {
	         return false;
	      } else {
	         if (p_213403_2_.getTrueSource() instanceof PlayerEntity || p_213403_2_.isExplosion()) {
	            float f = this.getHealth();
	            this.attackDragonFrom(p_213403_2_, p_213403_3_);
	            if (this.getHealth() <= 0.0F && !this.phaseManager.getCurrentPhase().getIsStationary()) {
	               this.setHealth(1.0F);
	               this.phaseManager.setPhase(PhaseType.DYING);
	            }
	
	            if (this.phaseManager.getCurrentPhase().getIsStationary()) {
	               this.sittingDamageReceived = (int)((float)this.sittingDamageReceived + (f - this.getHealth()));
	               if ((float)this.sittingDamageReceived > 0.25F * this.getMaxHealth()) {
	                  this.sittingDamageReceived = 0;
	                  this.phaseManager.setPhase(PhaseType.TAKEOFF);
	               }
	            }
	         }
	
	         return true;
	      }
	   }
	// commented 1
	
	protected boolean attackDragonFrom(DamageSource source, float amount)
	{
		return super.hurt(source, amount);
	}
	
	private boolean destroyBlocksInAABB(AABB p_70972_1_)
	{
		return false;
	}
	
	protected void tickDeath()
	{
		++this.deathTicks;
		if (this.deathTicks >= 100 && this.deathTicks <= 150)
		{
			float f = (this.random.nextFloat() - 0.5F) * 8.0F;
			float f1 = (this.random.nextFloat() - 0.5F) * 4.0F;
			float f2 = (this.random.nextFloat() - 0.5F) * 8.0F;
			this.level.addParticle(ParticleTypes.EXPLOSION_EMITTER, this.posX + (double) f, this.posY + 2.0D + (double) f1, this.posZ + (double) f2, 0.0D, 0.0D, 0.0D);
		}
	
		// int i = 500;
	
		if (!this.level.isClientSide)
		{
			if (this.deathTicks == 1)
			{
				this.playSound(SoundEvents.ENDER_DRAGON_DEATH, 5.0F, 1.2F);
			}
		}
	
		this.move(MoverType.SELF, new Vec3(0.0D, (double) 0.1F, 0.0D));
		this.yRot += 20.0F;
		this.yBodyRot = this.yRot;
		if (this.deathTicks == 150 && !this.level.isClientSide)
		{
			this.remove();
		}
	}
	
	protected boolean canDespawn()
	{
		return false;
	}
	
	public Entity[] getPartsOLD()
	{
		return this.dragonPartArray;
	}
	
	public boolean isPickable()
	{
		return true;
	}
	
	public Level func_82194_d()
	{
		return this.level;
	}
	
	protected SoundEvent getAmbientSound()
	{
		return SoundEvents.ENDER_DRAGON_AMBIENT;
	}
	
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return SoundEvents.ENDER_DRAGON_HURT;
	}
	
	protected float getSoundVolume()
	{
		return 3.0F;
	}
	
	protected float getVoicePitch()
	{
		return 1.7f;
	}
	
	public boolean canChangeDimensions()
	{
		return false;
	}
	
	public void kill()
	{
		this.remove();
	}*/
}