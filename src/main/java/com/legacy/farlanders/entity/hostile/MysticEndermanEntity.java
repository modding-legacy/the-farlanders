package com.legacy.farlanders.entity.hostile;

import java.util.List;

import javax.annotation.Nullable;

import com.legacy.farlanders.data.FLLootProv;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.registry.FLItems;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.goal.target.ResetUniversalAngerTargetGoal;
import net.minecraft.world.entity.monster.EnderMan;
import net.minecraft.world.entity.monster.Endermite;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootParams;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.neoforged.neoforge.common.NeoForgeMod;

public class MysticEndermanEntity extends EnderMan implements IColoredEyes
{
	private static final List<EyeColor> MYSTIC_EYE_COLORS = List.of(EyeColor.PURPLE, EyeColor.GREEN);
	public static final EntityDataAccessor<Integer> EYE_COLOR = SynchedEntityData.<Integer>defineId(MysticEndermanEntity.class, EntityDataSerializers.INT);

	private int power, powerCooldown;

	public MysticEndermanEntity(EntityType<? extends MysticEndermanEntity> p_i50210_1_, Level p_i50210_2_)
	{
		super(p_i50210_1_, p_i50210_2_);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(1, new EnderMan.EndermanFreezeWhenLookedAt(this));
		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.0D, false));
		this.goalSelector.addGoal(7, new WaterAvoidingRandomStrollGoal(this, 1.0D, 0.0F));
		this.goalSelector.addGoal(8, new LookAtPlayerGoal(this, Player.class, 8.0F));
		this.goalSelector.addGoal(8, new RandomLookAroundGoal(this));
		this.goalSelector.addGoal(10, new EnderMan.EndermanLeaveBlockGoal(this));
		this.targetSelector.addGoal(1, new EnderMan.EndermanLookForPlayerGoal(this, this::isAngryAt));
		this.targetSelector.addGoal(2, new HurtByTargetGoal(this));
		this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, Endermite.class, true, false));
		this.targetSelector.addGoal(4, new ResetUniversalAngerTargetGoal<>(this, false));
	}

	public static AttributeSupplier.Builder createMysticAttributes()
	{
		return EnderMan.createAttributes().add(NeoForgeMod.STEP_HEIGHT.value(), 1.25F);
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		LootTable lootTable = level.getServer().getLootData().getLootTable(FLLootProv.ENDERMAN_WANDS);
		List<ItemStack> stacks = lootTable.getRandomItems(new LootParams.Builder(level.getLevel()).create(LootContextParamSets.EMPTY));
		this.setItemSlot(EquipmentSlot.MAINHAND, stacks.isEmpty() ? new ItemStack(FLItems.mystic_wand_teleport) : Util.getRandom(stacks, this.random));
		this.setGuaranteedDrop(EquipmentSlot.MAINHAND);

		if (!this.level().isClientSide())
			this.power = random.nextInt(6);

		return super.finalizeSpawn(level, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	protected void dropCustomDeathLoot(DamageSource pSource, int pLooting, boolean pRecentlyHit)
	{
		super.dropCustomDeathLoot(pSource, pLooting, pRecentlyHit);
	}

	@Override
	public void aiStep()
	{
		if (this.level().isClientSide())
		{
			if (this.getEyeColor() == 1)
			{
				this.level().addParticle(ParticleTypes.LARGE_SMOKE, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), 0, 0, 0);
			}
			else
			{
				for (int i = 0; i < 2; ++i)
					this.level().addParticle(ParticleTypes.PORTAL, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), (this.random.nextDouble() - 0.5D) * 2.0D, -this.random.nextDouble(), (this.random.nextDouble() - 0.5D) * 2.0D);
			}
		}
		else if (powerCooldown > 0)
			--powerCooldown;

		super.aiStep();
	}

	@Override
	public List<EyeColor> getEyeColors()
	{
		return MYSTIC_EYE_COLORS;
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(EYE_COLOR, this.random.nextInt(2));
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		compound.putInt(EYE_COLOR_KEY, this.getEyeColor());
		compound.putInt("MagicPower", this.power);
		compound.putInt("MagicPowerCooldown", this.powerCooldown);
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);

		this.setEyeColor(compound.getInt(EYE_COLOR_KEY));
		this.power = compound.getInt("MagicPower");
		this.powerCooldown = compound.getInt("MagicPowerCooldown");
	}

	@Override
	public void setEyeColor(int colorID)
	{
		this.entityData.set(EYE_COLOR, colorID);
	}

	@Override
	public int getEyeColor()
	{
		return this.entityData.get(EYE_COLOR);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return this.isAngry() ? SoundEvents.ENDERMAN_SCREAM : FLSounds.ENTITY_MYSTIC_ENDERMAN_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FLSounds.ENTITY_MYSTIC_ENDERMAN_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_MYSTIC_ENDERMAN_DEATH;
	}

	@Override
	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
			return false;

		if (source.getDirectEntity()instanceof Player player && !source.isCreativePlayer())
		{
			if (!this.level().isClientSide() && powerCooldown <= 0 && this.hurtTime <= 0)
			{
				if (power == 0)
				{
					player.addEffect(new MobEffectInstance(MobEffects.BLINDNESS, 3 * 20));

					this.playSound(FLSounds.ENTITY_MYSTIC_ENDERMAN_BLINDNESS, 1.0F, 1.0F);
				}
				else if (power == 1)
				{
					player.addEffect(new MobEffectInstance(MobEffects.CONFUSION, 8 * 20));

					this.playSound(FLSounds.ENTITY_MYSTIC_ENDERMAN_CONFUSION, 1.0F, 1.0F);
				}
				else if (power == 2)
				{
					player.addEffect(new MobEffectInstance(MobEffects.WEAKNESS, 9 * 20));
					player.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 9 * 20));

					this.playSound(SoundEvents.DROWNED_HURT, 1.0F, 1.0F);
				}
				/*else if (power == 3 && isTimerSet == false)
				{
					int power3x = (int) player.getX() + (random.nextInt(5));
					int power3y = (int) player.getY() - 1;
					int power3z = (int) player.getZ() + (random.nextInt(5));
					int randomNr = random.nextInt(18);
					BlockState block;
					if (randomNr == 0)
						block = Blocks.DIAMOND_ORE.defaultBlockState();
					else if (randomNr >= 1 && randomNr <= 2)
						block = Blocks.GOLD_ORE.defaultBlockState();
					else if (randomNr >= 3 && randomNr <= 4)
						block = Blocks.LAPIS_ORE.defaultBlockState();
					else if (randomNr >= 5 && randomNr <= 7)
						block = Blocks.REDSTONE_ORE.defaultBlockState();
					else if (randomNr >= 8 && randomNr <= 11)
						block = Blocks.IRON_ORE.defaultBlockState();
					else if (randomNr == 12)
						block = Blocks.EMERALD_ORE.defaultBlockState();
					else if (randomNr == 13)
						block = FLBlocks.endumium_ore.defaultBlockState();
					else
						block = Blocks.COAL_ORE.defaultBlockState();
					this.level().setBlockAndUpdate(new BlockPos(power3x, power3y, power3z), block);
					isTimerSet = true;
					//System.currentTimeMillis();
				
					this.playSound(SoundEvents.AMBIENT_CAVE.get(), 1.0F, 1.0F);
				}*/
				else if (power == 3)
				{
					BlockPos base = player.blockPosition().offset(-1, 5, -1);

					for (int x = 0; x < 3; ++x)
					{
						for (int z = 0; z < 3; ++z)
						{
							for (int y = 0; y < 2; ++y)
							{
								BlockPos offset = base.offset(x, y, z);
								if (this.level().getBlockState(offset).isAir())
								{
									this.level().setBlock(offset, this.getRandom().nextBoolean() ? Blocks.SAND.defaultBlockState() : Blocks.GRAVEL.defaultBlockState(), Block.UPDATE_CLIENTS);
								}
							}
						}
					}

					this.playSound(SoundEvents.ZOMBIE_VILLAGER_CURE, 1.0F, 1.0F);
					this.teleport();
				}
				else if (power == 4)
				{
					this.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 5 * 20));
					this.addEffect(new MobEffectInstance(MobEffects.HEALTH_BOOST, 2));

					this.playSound(SoundEvents.ZOMBIE_INFECT, 1.0F, 1.0F);
				}

				power = random.nextInt(6);
				this.powerCooldown = 60;
			}

			return super.hurt(source, amount);
		}
		else if (!(source.is(DamageTypeTags.IS_PROJECTILE)))
		{
			boolean flag = super.hurt(source, amount);
			if (source.is(DamageTypeTags.BYPASSES_ARMOR) && this.random.nextInt(10) != 0)
			{
				this.teleport();
			}
			return flag;
		}

		else
		{
			for (int i = 0; i < 64; ++i)
			{
				if (this.teleport())
				{
					return true;
				}
			}
			return false;
		}
	}
}