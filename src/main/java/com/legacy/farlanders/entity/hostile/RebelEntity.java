package com.legacy.farlanders.entity.hostile;

import javax.annotation.Nullable;

import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.event.FarlandersEvents;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.OpenDoorGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.monster.Endermite;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.pathfinder.BlockPathTypes;

public class RebelEntity extends Monster
{

	public RebelEntity(EntityType<? extends RebelEntity> type, Level world)
	{
		super(type, world);

		if (this.getNavigation()instanceof GroundPathNavigation p)
		{
			p.setCanOpenDoors(true);
			p.setCanFloat(true);
		}

		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.6D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, Player.class, 10, false, false, FarlandersEvents::targettableByHostile));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Mob.class, 5, false, false, (entity) -> FarlandersEvents.targettableByHostile(entity) && entity.getType().is(FLTags.Entities.HUNTED_BY_REBELS)));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, Endermite.class, false));
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		this.setItemSlot(EquipmentSlot.MAINHAND, new ItemStack(Items.WOODEN_SWORD));
		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 1.60F;
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return Monster.createMonsterAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.MAX_HEALTH, 30.0D).add(Attributes.ARMOR, 3.0D);
	}

	@Override
	public boolean doHurtTarget(Entity entityIn)
	{
		if (entityIn instanceof LivingEntity living && !this.level().isClientSide())
			living.addEffect(new MobEffectInstance(MobEffects.POISON, 3 * 20));

		return super.doHurtTarget(entityIn);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isInWaterOrBubble())
			this.hurt(this.damageSources().drown(), 1);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FLSounds.ENTITY_REBEL_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FLSounds.ENTITY_REBEL_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_REBEL_DEATH;
	}
}