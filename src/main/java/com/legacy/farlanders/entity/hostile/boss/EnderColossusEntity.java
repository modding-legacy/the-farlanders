package com.legacy.farlanders.entity.hostile.boss;

public class EnderColossusEntity // extends Monster implements RangedAttackMob
{
	/*protected static final EntityDataAccessor<Boolean> NIGHTFALL_SPAWNED = SynchedEntityData.defineId(EnderColossusEntity.class, EntityDataSerializers.BOOLEAN);
	public static final EntityDataAccessor<Boolean> ANGRY = SynchedEntityData.<Boolean>defineId(EnderColossusEntity.class, EntityDataSerializers.BOOLEAN);
	public static final EntityDataAccessor<Integer> SHADOW_COUNTER = SynchedEntityData.<Integer>defineId(EnderColossusEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Integer> DRAGON_COUNTER = SynchedEntityData.<Integer>defineId(EnderColossusEntity.class, EntityDataSerializers.INT);
	
	private final ServerBossEvent bossInfo = (ServerBossEvent) (new ServerBossEvent(this.getDisplayName(), BossEvent.BossBarColor.WHITE, BossEvent.BossBarOverlay.NOTCHED_6)).setDarkenScreen(false).setPlayBossMusic(true).setCreateWorldFog(true);
	
	public int deathTicks;
	private int attackTimer, screamTimer;
	public boolean angry;
	
	public EnderColossusEntity(EntityType<? extends EnderColossusEntity> type, Level world)
	{
		super(type, world);
		this.noCulling = true;
	
		/if (this.getHealthPercentage() > 75)
		{
			this.setShadowCounter(2);
			this.setDragonCounter(1);
		}
		else if (this.getHealthPercentage() > 50)
		{
			this.setShadowCounter(1);
			this.setDragonCounter(1);
		}
		else if (this.getHealthPercentage() > 25)
		{
			this.setShadowCounter(1);
			this.setDragonCounter(0);
		}
		else
		{
			this.setShadowCounter(0);
			this.setDragonCounter(0);
		}/
	}
	
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(3, new RangedAttackGoal(this, 0.6D, 30, 40.0F)
		{
			public boolean canContinueToUse()
			{
				Mob entity = ObfuscationReflectionHelper.getPrivateValue(RangedAttackGoal.class, this, "mob");
	
				if (entity.getTarget() != null)
				{
					return entity.distanceTo(entity.getTarget()) > 15.0F && super.canContinueToUse();
				}
				else
				{
					return super.canUse();
				}
			}
	
			public boolean canUse()
			{
				Mob entity = ObfuscationReflectionHelper.getPrivateValue(RangedAttackGoal.class, this, "mob");
	
				if (entity.getTarget() != null)
				{
					return entity.distanceTo(entity.getTarget()) > 15.0F && super.canUse();
				}
				else
				{
					return super.canUse();
				}
			}
		});
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.6D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, Player.class, 40.0F));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, Player.class, false));
	}
	
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}
	
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 19.00F;
	}
	
	protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(40.0D);
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(300.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.5D);
		this.getAttribute(SharedMonsterAttributes.KNOCKBACK_RESISTANCE).setBaseValue(2.0D);
	}
	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(NIGHTFALL_SPAWNED, false);
		this.entityData.define(ANGRY, false);
	
		this.entityData.define(SHADOW_COUNTER, 2);
		this.entityData.define(DRAGON_COUNTER, 1);
	}
	
	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);
	
		if (this.hasCustomName())
		{
			this.bossInfo.setName(this.getDisplayName());
		}
	
		this.setNightfallSpawned(compound.getBoolean("NightfallSpawned"));
		this.setAngry(compound.getBoolean("IsAngry"));
	
		this.setShadowCounter(compound.getInt("ShadowCounter"));
		this.setDragonCounter(compound.getInt("DragonCounter"));
	}
	
	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putBoolean("NightfallSpawned", this.getNightfallSpawned());
		compound.putBoolean("IsAngry", this.getAngry());
	
		compound.putInt("ShadowCounter", this.getShadowCounter());
		compound.putInt("DragonCounter", this.getDragonCounter());
	}
	
	public void setCustomName(@Nullable Component name)
	{
		super.setCustomName(name);
		this.bossInfo.setName(this.getDisplayName());
	}
	
	public void startSeenByPlayer(ServerPlayer player)
	{
		super.startSeenByPlayer(player);
		this.bossInfo.addPlayer(player);
	}
	
	public void stopSeenByPlayer(ServerPlayer player)
	{
		super.stopSeenByPlayer(player);
		this.bossInfo.removePlayer(player);
	}
	
	public void setNightfallSpawned(boolean natural)
	{
		this.entityData.set(NIGHTFALL_SPAWNED, natural);
	}
	
	public boolean getNightfallSpawned()
	{
		return this.entityData.get(NIGHTFALL_SPAWNED);
	}
	
	private double getHealthPercentage()
	{
		return (this.getHealth() * 100) / this.getMaxHealth();
	}
	
	public boolean doHurtTarget(Entity entityIn)
	{
		this.attackTimer = 10;
		this.level.broadcastEntityEvent(this, (byte) 4);
		boolean flag = entityIn.hurt(DamageSource.mobAttack(this), 10.0F);
		if (flag)
		{
			double d0 = (this.getBoundingBox().minX + this.getBoundingBox().maxX) / 2.0D;
			double d1 = (this.getBoundingBox().minZ + this.getBoundingBox().maxZ) / 2.0D;
			double d2 = entityIn.getX() - d0;
			double d3 = entityIn.getZ() - d1;
			double d4 = d2 * d2 + d3 * d3;
			entityIn.setDeltaMovement(entityIn.getDeltaMovement().add(d2 / d4 * 1.1D, 0.50000000298023224D, d3 / d4 * 10.1D));
			this.doEnchantDamageEffects(this, entityIn);
		}
		this.playSound(SoundEvents.IRON_GOLEM_ATTACK, 2.0F, 0.5F);
	
		return flag;
	}
	
	@OnlyIn(Dist.CLIENT)
	public void handleEntityEvent(byte id)
	{
		if (id == 4)
		{
			this.attackTimer = 10;
			this.playSound(SoundEvents.IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		}
		if (id == 5)
		{
			this.screamTimer = 100;
			this.playSound(FarlandersSounds.ENTITY_MYSTIC_ENDERMINION_DEATH, 5.0F, 0.1F);
		}
		else
		{
			super.handleEntityEvent(id);
		}
	}
	
	@OnlyIn(Dist.CLIENT)
	public int getAttackTimer()
	{
		return this.attackTimer;
	}
	
	@OnlyIn(Dist.CLIENT)
	public int getScreamTimer()
	{
		return this.screamTimer;
	}
	
	public void setTarget(@Nullable LivingEntity entitylivingbaseIn)
	{
		super.setTarget(entitylivingbaseIn);
	
		if (entitylivingbaseIn != null)
		{
			this.setAngry(true);
		}
		else
		{
			this.setAngry(false);
		}
	}
	
	@Override
	public void tick()
	{
		super.tick();
		this.bossInfo.setPercent(this.getHealth() / this.getMaxHealth());
	
		if (this.attackTimer > 0)
		{
			--this.attackTimer;
		}
	
		if (this.screamTimer > 0)
		{
			--this.screamTimer;
		}
	
		if (this.getHealthPercentage() < 25)
		{
			if (random.nextInt(400) == 0 && !this.level.isClientSide && this.onGround && this.getScreamTimer() == 0 && this.getTarget() != null)
			{
				this.screamTimer = 100;
				this.playSound(FarlandersSounds.ENTITY_MYSTIC_ENDERMINION_DEATH, 5.0F, 0.1F);
				this.level.broadcastEntityEvent(this, (byte) 5);
	
				List<Player> list = this.level.<Player>getEntitiesOfClass(Player.class, this.getBoundingBox().inflate(50.0D, 100.0D, 50.0D));
				if (!list.isEmpty())
				{
					for (int k = 0; k < list.size(); k++)
					{
						Player entity = list.get(k);
	
						if (!entity.isCreative())
						{
							entity.addEffect(new MobEffectInstance(MobEffects.BLINDNESS, 100, 1));
							entity.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 50, 2));
						}
					}
				}
			}
			return;
		}
	
		if (this.getHealthPercentage() < 75 && this.getHealthPercentage() > 25)
		{
			if (random.nextInt(400) == 0 && !this.level.isClientSide && this.onGround && this.getTarget() != null && this.getScreamTimer() == 0)
			{
				this.playSound(FarlandersSounds.ENTITY_ENDER_GOLEM_DEATH, 5.0F, 0.5F);
				this.setDeltaMovement(this.getDeltaMovement().add(0, 1.70000000149011612D, 0));
			}
		}
	
		if (this.getHealthPercentage() < 25 && this.getShadowCounter() == 1)
		{
			this.setShadowCounter(this.getShadowCounter() - 1);
			for (int i = 0; i < 3; i++)
			{
				BlockPos offsetPos = this.level.getHeightmapPos(Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, this.blockPosition().offset(random.nextInt(16) - 8, 0, random.nextInt(16) - 8));
				EnderColossusShadowEntity ent = new EnderColossusShadowEntity(FarlandersEntityTypes.ENDER_COLOSSUS_SHADOW, level);
				ent.moveTo(offsetPos.getX(), offsetPos.getY(), offsetPos.getZ(), 0F, 0F);
				ent.spawnAnim();
				this.playSound(SoundEvents.ZOMBIE_VILLAGER_CONVERTED, 3.0F, 0.8F);
	
				if (!level.isClientSide)
					level.addFreshEntity(ent);
			}
		}
		else if (this.getHealthPercentage() < 50 && this.getDragonCounter() == 1)
		{
			this.setDragonCounter(this.getDragonCounter() - 1);
			MiniDragonEntity ent = new MiniDragonEntity(FarlandersEntityTypes.MINI_ENDER_DRAGON, level);
			ent.moveTo(this.getX() + random.nextInt(32) - 16, this.getY() + this.getEyeHeight() + random.nextInt(30), this.getZ() + random.nextInt(32) - 16, 0F, 0F);
			this.playSound(SoundEvents.ZOMBIE_VILLAGER_CONVERTED, 3.0F, 0.8F);
	
			if (!level.isClientSide)
				level.addFreshEntity(ent);
		}
		else if (this.getHealthPercentage() < 75 && this.getShadowCounter() == 2)
		{
			this.setShadowCounter(this.getShadowCounter() - 1);
			for (int i = 0; i < 2; i++)
			{
				EnderColossusShadowEntity ent = new EnderColossusShadowEntity(FarlandersEntityTypes.ENDER_COLOSSUS_SHADOW, level);
				BlockPos offsetPos = this.level.getHeightmapPos(Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, this.blockPosition().offset(random.nextInt(16) - 8, 0, random.nextInt(16) - 8));
				ent.moveTo(offsetPos.getX(), offsetPos.getY(), offsetPos.getZ(), 0F, 0F);
				ent.spawnAnim();
				this.playSound(SoundEvents.ZOMBIE_VILLAGER_CONVERTED, 3.0F, 0.8F);
	
				if (!level.isClientSide)
					level.addFreshEntity(ent);
			}
		}
	
		if (this.getTarget() != null && this.getHealth() > 0)
		{
			if (this.distanceTo(this.getTarget()) >= 20.0F)
			{
				this.getMoveControl().setWantedPosition(this.getTarget().getX(), this.getTarget().getY(), this.getTarget().getZ(), 1.0F);
			}
	
			if (this.distanceTo(this.getTarget()) >= 30.0F)
			{
				this.setPos(this.getTarget().getX(), this.getTarget().getY() + 25.0F, this.getTarget().getZ());
				this.playSound(FarlandersSounds.ENTITY_ENDER_GOLEM_DEATH, 5.0F, 0.5F);
				this.playSound(SoundEvents.ENDERMAN_TELEPORT, 5.0F, 0.5F);
				this.playSound(SoundEvents.ENDERMAN_TELEPORT, 5.0F, 0.7F);
			}
		}
	
	}
	
	@Override
	protected void tickDeath()
	{
		this.setNoGravity(true);
		++this.deathTicks;
	
		if (this.deathTicks >= 180 && this.deathTicks <= 200)
		{
			float f = (this.random.nextFloat() - 0.5F) * 8.0F;
			float f1 = (this.random.nextFloat() - 0.5F) * 20.0F;
			float f2 = (this.random.nextFloat() - 0.5F) * 8.0F;
			this.level.addParticle(ParticleTypes.EXPLOSION_EMITTER, this.getX() + (double) f, this.getY() + 13.0F + (double) f1, this.getZ() + (double) f2, 0.0D, 0.0D, 0.0D);
		}
	
		boolean flag = this.level.getGameRules().getBoolean(GameRules.RULE_DOMOBLOOT);
		int i = this.getNightfallSpawned() ? 12000 : 500;
	
		if (!this.level.isClientSide)
		{
			if (this.deathTicks > 150 && this.deathTicks % 5 == 0 && flag)
			{
				this.dropExperience(Mth.floor((float) i * 0.08F));
			}
	
			if (this.deathTicks == 1)
			{
				this.setDeltaMovement(Vec3.ZERO);
				this.level.globalLevelEvent(1028, new BlockPos(this.position()), 0);
			}
		}
	
		this.move(MoverType.SELF, new Vec3(0.0D, (double) 0.2F, 0.0D));
		this.yBodyRot += 0.0F;
		this.yHeadRot += 0.0F;
		this.xRot += 0.0F;
	
		if (this.deathTicks == 197 && !this.level.isClientSide)
		{
			if (flag)
			{
				this.dropExperience(Mth.floor((float) i * 0.2F));
			}
	
			this.remove();
		}
	
	}
	
	private void dropExperience(int amount)
	{
		while (amount > 0)
		{
			int i = ExperienceOrb.getExperienceValue(amount);
			amount -= i;
			this.level.addFreshEntity(new ExperienceOrb(this.level, this.getX(), this.getY(), this.getZ(), i));
		}
	
	}
	
	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_TITAN_IDLE;
	}
	
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		this.playSound(FarlandersSounds.ENTITY_TITAN_HURT, this.getSoundVolume(), 0.1F);
		return null;
	}
	
	protected SoundEvent getDeathSound()
	{
		this.playSound(FarlandersSounds.ENTITY_TITAN_DEATH_ECHO, 5.0F, 0.1F);
		return null;
	}
	
	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
		this.playSound(SoundEvents.IRON_GOLEM_STEP, 2.5F, 0.5F);
		this.playSound(SoundEvents.SNOW_BREAK, 0.5F, 0.1F);
	}
	
	protected float getVoicePitch()
	{
		return 0.6F;
	}
	
	@Override
	protected float getSoundVolume()
	{
		return 2.0F;
	}
	
	public boolean canChangeDimensions()
	{
		return false;
	}
	
	public boolean removeWhenFarAway(double distanceToClosestPlayer)
	{
		return false;
	}
	
	public void fall(float distance, float damageMultiplier)
	{
		if (distance >= 10)
		{
			this.level.explode(this, this.getX(), this.getY(), this.getZ(), 3, BlockInteraction.NONE);
	
			List<LivingEntity> list = this.level.<LivingEntity>getEntitiesOfClass(LivingEntity.class, this.getBoundingBox().inflate(10.0D, 5.0D, 10.0D));
			if (!list.isEmpty())
			{
				for (int k = 0; k < list.size(); k++)
				{
					LivingEntity entity = list.get(k);
	
					if (!entity.hasImpulse)
						entity.setDeltaMovement(entity.getDeltaMovement().add(0.0F, 1.0F, 0.0F));
				}
			}
		}
	}
	
	@Override
	public void performRangedAttack(LivingEntity target, float distanceFactor)
	{
		this.level.broadcastEntityEvent(this, (byte) 4);
		Vec3 look = this.getViewVector(1.0F);
		double xPos = this.blockPosition().getX() + look.x * 4.0D;
		double yPos = this.blockPosition().getY() + this.getEyeHeight() - 5.0D;
		double zPos = this.blockPosition().getZ() + look.z * 4.0D;
	
		ThrownBlockEntity thrownBlock = new ThrownBlockEntity(this.level, xPos, yPos, zPos, this);
	
		thrownBlock.time = 200;
		double d0 = target.getX() - this.getX();
		double d1 = target.getBoundingBox().minY + (double) (target.getBbHeight() / 3.0F - thrownBlock.getY() + 15.0F);
		double d2 = target.getZ() - this.getZ();
	
		this.level.playSound((Player) null, this.getX(), this.getY(), this.getZ(), SoundEvents.IRON_GOLEM_ATTACK, this.getSoundSource(), 4.0F, 0.5F + (this.random.nextFloat() - this.random.nextFloat()) * 0.2F);
	
		float inaccuracy = 1.0F;
	
		float velocity = this.distanceTo(target) / 26;
	
		Vec3 vec3d = (new Vec3(d0, d1, d2)).normalize().add(this.random.nextGaussian() * (double) 0.0075F * (double) inaccuracy, this.random.nextGaussian() * (double) 0.0075F * (double) inaccuracy, this.random.nextGaussian() * (double) 0.0075F * (double) inaccuracy).scale((double) velocity);
		thrownBlock.setDeltaMovement(vec3d);
		float f1 = Mth.sqrt(getHorizontalDistanceSqr(vec3d));
		thrownBlock.yRot = (float) (Mth.atan2(vec3d.x, d2) * (double) (180F / (float) Math.PI));
		thrownBlock.xRot = (float) (Mth.atan2(vec3d.y, (double) f1) * (double) (180F / (float) Math.PI));
		thrownBlock.yRotO = this.yRot;
		thrownBlock.xRotO = this.xRot;
	
		this.level.addFreshEntity(thrownBlock);
	}
	
	public void setAngry(boolean ang)
	{
		this.entityData.set(ANGRY, ang);
	}
	
	public boolean getAngry()
	{
		return this.entityData.get(ANGRY);
	}
	
	public void setShadowCounter(int count)
	{
		this.entityData.set(SHADOW_COUNTER, count);
	}
	
	public int getShadowCounter()
	{
		return this.entityData.get(SHADOW_COUNTER);
	}
	
	public void setDragonCounter(int count)
	{
		this.entityData.set(DRAGON_COUNTER, count);
	}
	
	public int getDragonCounter()
	{
		return this.entityData.get(DRAGON_COUNTER);
	}
	
	public void kill()
	{
		this.remove();
	}*/
}