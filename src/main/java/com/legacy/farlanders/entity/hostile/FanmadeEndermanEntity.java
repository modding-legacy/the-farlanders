package com.legacy.farlanders.entity.hostile;

import java.util.ArrayList;
import java.util.List;

import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntitySelector;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.goal.target.ResetUniversalAngerTargetGoal;
import net.minecraft.world.entity.ai.util.LandRandomPos;
import net.minecraft.world.entity.monster.EnderMan;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.CommonHooks;

public class FanmadeEndermanEntity extends EnderMan
{
	public FanmadeEndermanEntity(EntityType<? extends FanmadeEndermanEntity> type, Level level)
	{
		super(type, level);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		/*this.goalSelector.addGoal(1, new EnderMan.EndermanFreezeWhenLookedAt(this));*/

		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.0D, false)

		{
			@Override
			public void start()
			{
				super.start();
			}

			@Override
			public boolean canContinueToUse()
			{
				/*if (this.mob.getTarget() != null && this.mob.distanceTo(this.mob.getTarget()) > 10)
					return false;*/

				return super.canContinueToUse();
			}

			@Override
			public boolean canUse()
			{
				if (this.mob.getTarget() != null && this.mob.distanceTo(this.mob.getTarget()) > 10)
					return false;

				return super.canUse();
			}
		});

		/*this.goalSelector.addGoal(7, new WaterAvoidingRandomStrollGoal(this, 1.0D, 0.0F)
		{
			@Override
			public boolean canUse()
			{
				return this.mob.getTarget() != null ? false : super.canUse();
			}
		});*/
		this.goalSelector.addGoal(6, new LookAndMoveCloserGoal(this));
		this.goalSelector.addGoal(8, new RandomLookAroundGoal(this));

		this.goalSelector.addGoal(10, new EnderMan.EndermanLeaveBlockGoal(this));
		this.goalSelector.addGoal(11, new EnderMan.EndermanTakeBlockGoal(this));
		/*this.targetSelector.addGoal(1, new EnderMan.EndermanLookForPlayerGoal(this, this::isAngryAt));*/

		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, Player.class, 10, true, false, e -> e.distanceTo(this) < 8));
		this.targetSelector.addGoal(3, new HurtByTargetGoal(this));
		this.targetSelector.addGoal(5, new ResetUniversalAngerTargetGoal<>(this, false));
	}

	@Override
	public void aiStep()
	{
		if (this.level().isClientSide())
			this.level().addParticle(ParticleTypes.AMBIENT_ENTITY_EFFECT, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), 152F / 255F, 207F / 255F, 249F / 255F);

		super.aiStep();
	}

	public void playStareSound()
	{
		if (this.tickCount >= this.lastStareSound + 200)
		{
			this.lastStareSound = this.tickCount;
			if (!this.isSilent())
				this.level().playSound(null, this, FLSounds.ENTITY_FANMADE_ENDERMAN_IDLE, this.getSoundSource(), 1.5F, 1.0F);
		}

	}

	@Override
	public boolean isLookingAtMe(Player player)
	{
		ItemStack itemstack = player.getInventory().armor.get(3);

		if (CommonHooks.shouldSuppressEnderManAnger(this, player, itemstack))
			return false;
		else if (player.distanceTo(this) < 5.0F)
			return false; // super.isLookingAtMe(player);

		Vec3 vec3d = player.getViewVector(1.0F).normalize();
		Vec3 vec3d1 = new Vec3(this.getX() - player.getX(), this.getBoundingBox().minY + this.getEyeHeight() - (player.getY() + player.getEyeHeight()), this.getZ() - player.getZ());
		double d0 = vec3d1.length();
		vec3d1 = vec3d1.normalize();
		double d1 = vec3d.dot(vec3d1);

		if ((d1 < 1.0D / d0))
			return false;

		return true;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return /*this.isAngry() && this.getTarget() != null && (this.getTarget()instanceof Player p && this.isLookingAtMe(p) || !(this.getTarget() instanceof Player)) ? FLSounds.ENTITY_FANMADE_ENDERMAN_IDLE : */null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return SoundEvents.ENDERMAN_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_FANMADE_ENDERMAN_DEATH;
	}

	public void forceCreepy(boolean creepy)
	{
		this.entityData.set(DATA_CREEPY, creepy);
	}

	static class LookAndMoveCloserGoal extends LookAtPlayerGoal
	{
		protected int teleportCooldown;

		public LookAndMoveCloserGoal(Mob pMob)
		{
			super(pMob, Player.class, 40.0F, Float.MAX_VALUE);
		}

		@Override
		public void tick()
		{
			super.tick();

			if (this.mob instanceof FanmadeEndermanEntity mob && this.lookAt != null && EntitySelector.NO_CREATIVE_OR_SPECTATOR.test(this.lookAt) && mob.distanceTo(this.lookAt) > 10)
			{
				if (--this.teleportCooldown <= 0)
				{
					Vec3 closerPos = null;

					List<Vec3> possiblePosList = new ArrayList<>();

					for (int tries = 0; tries < 10; ++tries)
					{
						Vec3 p = LandRandomPos.getPosTowards(mob, 10, 15, this.lookAt.position());

						if (p != null)
							possiblePosList.add(p);
					}

					for (Vec3 possiblePos : possiblePosList)
					{
						if (closerPos != null)
						{
							if (this.mob.distanceToSqr(possiblePos) < this.mob.distanceToSqr(closerPos))
								closerPos = possiblePos;
						}
						else
							closerPos = possiblePos;
					}

					if (this.lookAt instanceof Player p && mob.isLookingAtMe(p))
						closerPos = null;

					if (closerPos != null && this.lookAt.distanceToSqr(closerPos) >= 10)
					{
						/*mob.level.playSound(null, this.lookAt.blockPosition(), SoundEvents.EXPERIENCE_ORB_PICKUP, SoundSource.HOSTILE);*/

						mob.playSound(SoundEvents.ENDERMAN_TELEPORT, 1.0F, 1.0F);
						mob.randomTeleport(closerPos.x(), closerPos.y(), closerPos.z(), true);
						mob.playSound(SoundEvents.ENDERMAN_TELEPORT, 1.0F, 1.0F);
						mob.playStareSound();

						this.teleportCooldown = 5 * 20;
					}
					else
						this.teleportCooldown = 1 * 20;
				}

				if (this.lookAt instanceof Player p && mob.getTarget() == null)
				{
					mob.forceCreepy(!mob.isLookingAtMe(p));
				}
			}
		}

		@Override
		public boolean requiresUpdateEveryTick()
		{
			return this.lookAt != null || this.teleportCooldown > 0;
		}
	}
}