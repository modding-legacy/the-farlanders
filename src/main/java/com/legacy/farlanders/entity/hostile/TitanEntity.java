package com.legacy.farlanders.entity.hostile;

import java.util.Optional;

import javax.annotation.Nullable;

import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.ai.RetreatGoal;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.entity.util.ITeleportingEntity;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.BlockParticleOption;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.util.Mth;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.monster.Endermite;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.NeoForgeMod;

public class TitanEntity extends Monster implements IColoredEyes, ITeleportingEntity
{
	public static final EntityDataAccessor<Integer> RIGHT_EYE_COLOR = SynchedEntityData.<Integer>defineId(TitanEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Integer> LEFT_EYE_COLOR = SynchedEntityData.<Integer>defineId(TitanEntity.class, EntityDataSerializers.INT);

	public static final EntityDataAccessor<Boolean> HOLDING_PLAYER = SynchedEntityData.<Boolean>defineId(TitanEntity.class, EntityDataSerializers.BOOLEAN);
	public static final EntityDataAccessor<Boolean> RAGE = SynchedEntityData.<Boolean>defineId(TitanEntity.class, EntityDataSerializers.BOOLEAN);

	public static final EntityDataAccessor<Boolean> HEART_HURT = SynchedEntityData.<Boolean>defineId(TitanEntity.class, EntityDataSerializers.BOOLEAN);

	private int attackTimer, holdingTimer, rageTimer;

	private int holdingChance;
	private LivingEntity heldEntity;
	private int holdType;

	public float heartBrightness, oHeartBrightness;
	public int heartTicks;

	public TitanEntity(EntityType<? extends TitanEntity> type, Level world)
	{
		super(type, world);

		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);

		holdingChance = random.nextInt(15);
		holdType = -1;
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		// this.goalSelector.addGoal(5, new WaterAvoidingRandomWalkingGoal(this, 0.6D));
		this.goalSelector.addGoal(1, new RetreatGoal(this));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, Player.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, LooterEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, RebelEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, WandererEntity.class, false));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, Endermite.class, false));
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		this.randomizeEyeColor();
		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	public void randomizeEyeColor()
	{
		this.setRightEyeColor(this.getRandomEyeColor());
		this.setLeftEyeColor(this.getRandomEyeColor());
	}

	@Override
	public void spawnEyeParticles()
	{
		this.getEyeData(this.getRightEyeColor()).spawnParticle(this);
		this.getEyeData(this.getLeftEyeColor()).spawnParticle(this);
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();

		this.entityData.define(RIGHT_EYE_COLOR, 0);
		this.entityData.define(LEFT_EYE_COLOR, 0);
		this.entityData.define(HOLDING_PLAYER, false);
		this.entityData.define(RAGE, false);
		this.entityData.define(HEART_HURT, false);
	}

	@Override
	protected void checkFallDamage(double y, boolean onGroundIn, BlockState state, BlockPos pos)
	{
	}

	public static final String RIGHT_EYE_COLOR_KEY = "RightEyeColor";
	public static final String LEFT_EYE_COLOR_KEY = "LeftEyeColor";

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt(RIGHT_EYE_COLOR_KEY, this.getRightEyeColor());
		compound.putInt(LEFT_EYE_COLOR_KEY, this.getLeftEyeColor());

		compound.putBoolean("HoldingPlayer", this.isHoldingPlayer());
		compound.putBoolean("InRage", this.inRage());

		compound.putInt("HoldingTime", this.holdingTimer);
		compound.putInt("RageTime", this.rageTimer);
		compound.putInt("HoldType", this.holdType);
		compound.putInt("HoldingChance", this.holdingChance);
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.setRightEyeColor(compound.getInt(RIGHT_EYE_COLOR_KEY));
		this.setLeftEyeColor(compound.getInt(LEFT_EYE_COLOR_KEY));

		this.setHoldingPlayer(compound.getBoolean("HoldingPlayer"));
		this.setInRage(compound.getBoolean("InRage"));

		this.holdingTimer = compound.getInt("HoldingTime");
		this.rageTimer = compound.getInt("RageTime");
		this.holdType = compound.getInt("HoldType");
		this.holdingChance = compound.getInt("HoldingChance");
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 4.8F;
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return Monster.createMonsterAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.MAX_HEALTH, 250.0D).add(Attributes.KNOCKBACK_RESISTANCE, 1.0F).add(Attributes.ATTACK_DAMAGE, 10.0D).add(NeoForgeMod.STEP_HEIGHT.value(), 1.25F);
	}

	@Override
	public void tick()
	{
		super.tick();

		/*if (this.isNoAi())
		{
			this.setYHeadRot(this.getYHeadRot() + 4);
			this.setYBodyRot(this.getYHeadRot());
		}*/
	}

	@Override
	public void aiStep()
	{
		super.aiStep();

		if (this.heldEntity != null)
			this.getNavigation().stop();

		if (this.isInWaterOrBubble())
			this.teleportRandomly(this);

		if (this.attackTimer > 0)
			--this.attackTimer;

		if (!this.level().isClientSide())
		{
			if (this.holdingTimer > 0)
				--this.holdingTimer;

			boolean inRageHealth = this.getHealth() <= this.getRageHealth();

			if (this.rageTimer > 0)
				--this.rageTimer;

			// remount dismounted entity
			if (this.isHoldingPlayer() && this.getVehicle() == null && this.heldEntity != null)
				this.heldEntity.startRiding(this, true);

			// start rage
			if (inRageHealth && !this.inRage() && this.getTarget() != null)
			{
				this.addEffect(new MobEffectInstance(MobEffects.DAMAGE_BOOST, 8 * 29));
				this.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, 8 * 29));
				this.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SPEED, 8 * 29));
				this.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 8 * 29));
				this.addEffect(new MobEffectInstance(MobEffects.HEAL, 5));

				this.level().broadcastEntityEvent(this, (byte) 6);

				if (!this.isSilent())
					this.level().playSound((Player) null, this.getX(), this.getY() + this.getEyeHeight(), this.getZ(), FLSounds.ENTITY_TITAN_ENRAGE, this.getSoundSource(), 3, 1.0F);

				this.rageTimer = 15 * 20;
			}

			this.setInRage(this.rageTimer > 0);

			if (holdingChance == 1 && this.isHoldingPlayer() && heldEntity != null)
			{
				if (!heldEntity.isAlive() || this.holdingTimer <= 0 || this.getTarget() == null)
				{
					this.holdingChance = 2;
					this.setHoldingPlayer(false);
					this.heldEntity.stopRiding();

					var view = this.getViewVector(1.0F);
					this.heldEntity.hurtMarked = true;
					this.heldEntity.setDeltaMovement(view.x() * 1, 0.5D, view.z() * 1);

					this.heldEntity = null;
					this.holdType = -1;

					this.holdingTimer = 0;
				}
			}
			else
				holdingChance = random.nextInt(15);

			if (this.isHoldingPlayer() && this.heldEntity == null)
			{
				this.setHoldingPlayer(false);
				this.ejectPassengers();
			}
		}
		else
		{
			this.oHeartBrightness = this.heartBrightness;

			if (this.entityData.get(HEART_HURT))
			{
				this.heartBrightness += (1.0F - this.heartBrightness) * 0.4F;
			}
			else
			{
				++heartTicks;
				this.heartBrightness += (0.0F - this.heartBrightness) * 0.8F;
			}

			if (this.random.nextFloat() < 0.2F)
				this.spawnHeartParticles(ParticleTypes.PORTAL, 1.0F);

			if (this.inRage())
			{
				/*this.spawnEyeParticles();*/

				for (int i = 0; i < 5; ++i)
					this.level().addParticle(ParticleTypes.SMOKE, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), 0, 0, 0);
			}
		}

		if (this.getDeltaMovement().horizontalDistanceSqr() > (double) 2.5000003E-7F && this.random.nextInt(5) == 0)
		{
			int i = Mth.floor(this.getX());
			int j = Mth.floor(this.getY() - (double) 0.2F);
			int k = Mth.floor(this.getZ());
			BlockPos pos = new BlockPos(i, j, k);
			BlockState blockstate = this.level().getBlockState(pos);

			if (!this.level().isEmptyBlock(pos))
				this.level().addParticle(new BlockParticleOption(ParticleTypes.BLOCK, blockstate), this.getX() + ((double) this.random.nextFloat() - 0.5D) * (double) this.getBbWidth(), this.getBoundingBox().minY + 0.1D, this.getZ() + ((double) this.random.nextFloat() - 0.5D) * (double) this.getBbWidth(), 4.0D * ((double) this.random.nextFloat() - 0.5D), 0.5D, ((double) this.random.nextFloat() - 0.5D) * 4.0D);
		}

		if (this.entityData.get(HEART_HURT) && this.hurtTime <= 0)
			this.entityData.set(HEART_HURT, false);
	}

	private void spawnHeartParticles(ParticleOptions particle, float scale)
	{
		double max = 0.5D;
		double range = max / 2;

		float dist = 0.35F;
		float x = Mth.sin(this.yBodyRot * Mth.DEG_TO_RAD) * dist;
		float z = Mth.cos(this.yBodyRot * Mth.DEG_TO_RAD) * dist;

		Vec3 motion = new Vec3((this.random.nextDouble() - range) * (range * 2), -this.random.nextDouble(), (this.random.nextDouble() - range) * (range * 2)).add(-x * 2, 0, z * 2).scale(scale);
		this.level().addParticle(particle, this.getX() - x + ((this.random.nextDouble() * max) - range), this.getY() + 3.6F + ((this.random.nextDouble() * max) - range), this.getZ() + z + ((this.random.nextDouble() * max) - range), motion.x(), motion.y(), motion.z());
	}

	@Override
	public boolean doHurtTarget(Entity entityIn)
	{
		this.attackTimer = 10;
		this.level().broadcastEntityEvent(this, (byte) 4);

		boolean hurt;

		if (holdingChance == 1 && !this.isHoldingPlayer() && entityIn instanceof LivingEntity living)
		{
			hurt = entityIn.hurt(this.damageSources().mobAttack(this), 0);

			if (!this.level().isClientSide())
			{
				this.setHoldingPlayer(true);
				holdType = random.nextInt(3);
				this.rageTimer = 0;
				living.startRiding(this, true);

				if (holdType == 0)
				{
					this.playSound(FLSounds.ENTITY_TITAN_HOLDING_LONG, this.getSoundVolume(), 0.6F);

					living.addEffect(new MobEffectInstance(MobEffects.BLINDNESS, 8 * 15));
					living.addEffect(new MobEffectInstance(MobEffects.CONFUSION, 8 * 22));
					living.addEffect(new MobEffectInstance(MobEffects.WEAKNESS, 8 * 15));
					living.addEffect(new MobEffectInstance(MobEffects.WITHER, 8 * 15, 2));

					this.holdingTimer = 5 * 20;
				}
				else if (holdType == 1)
				{
					this.playSound(FLSounds.ENTITY_TITAN_HOLDING_MED, this.getSoundVolume(), 0.6F);

					living.addEffect(new MobEffectInstance(MobEffects.CONFUSION, 8 * 18));
					living.addEffect(new MobEffectInstance(MobEffects.WITHER, 8 * 10, 2));

					this.holdingTimer = 72;
				}
				else
				{
					this.playSound(FLSounds.ENTITY_TITAN_HOLDING_SHORT, this.getSoundVolume(), 0.6F);
					living.addEffect(new MobEffectInstance(MobEffects.WITHER, 8 * 7, 1));

					this.holdingTimer = 44;
				}

				heldEntity = living;
			}
		}
		else if (this.isHoldingPlayer())
			hurt = entityIn.hurt(this.damageSources().mobAttack(this), 1);
		else
			hurt = super.doHurtTarget(entityIn);

		if (hurt)
			entityIn.setDeltaMovement(entityIn.getDeltaMovement().add(0.0D, 0.4F, 0.0D));

		return hurt;
	}

	@Override
	protected float getSoundVolume()
	{
		return 2.0F;
	}

	@Override
	public boolean shouldRiderSit()
	{
		return false;
	}

	@Override
	public boolean canRiderInteract()
	{
		return true;
	}

	@Override
	public boolean showVehicleHealth()
	{
		return false;
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else if (!(source.is(DamageTypeTags.IS_PROJECTILE)))
		{
			boolean hitHeart = false;
			if (source.getEntity()instanceof LivingEntity living && !this.getPassengers().contains(living))
			{
				float dist = -0.38F;
				float x = -Mth.sin(this.yBodyRot * Mth.DEG_TO_RAD) * dist;
				float z = Mth.cos(this.yBodyRot * Mth.DEG_TO_RAD) * dist;

				float range = 10;
				Vec3 lookVec = living.getLookAngle();
				Vec3 playerPos = living.getEyePosition();

				float size = 0.5F;
				AABB shieldBox = new AABB(this.position().subtract(size, size * 1.5F, size), this.position().add(size, size * 1.5F, size)).move(x, 3.3F, z);
				Optional<Vec3> shieldClickPos = shieldBox.clip(playerPos, playerPos.add(lookVec.multiply(range, range, range)));

				float heartSize = 0.3F;
				AABB heartBox = new AABB(this.position().subtract(heartSize, heartSize, heartSize), this.position().add(heartSize, heartSize, heartSize)).move(0, 3.6F, 0);
				Optional<Vec3> heartClickPos = heartBox.clip(playerPos, playerPos.add(lookVec.multiply(range, range, range)));

				if (heartClickPos.isPresent() && (!shieldClickPos.isPresent() || shieldClickPos.isPresent() && heartClickPos.get().distanceTo(playerPos) < shieldClickPos.get().distanceTo(playerPos)))
				{
					hitHeart = true;
				}
			}

			boolean flag = super.hurt(source, amount * (hitHeart ? 1.2F : 1));

			if (flag && hitHeart && !this.level().isClientSide)
			{
				this.entityData.set(HEART_HURT, true);

				this.playSound(FLSounds.ENTITY_TITAN_HURT_HEART, this.getSoundVolume(), super.getVoicePitch());
				this.level().broadcastEntityEvent(this, (byte) 5);
			}

			if (!this.level().isClientSide() && source.is(DamageTypeTags.BYPASSES_ARMOR) && this.random.nextInt(10) != 0)
				this.teleportRandomly(this);

			return flag;
		}
		else
		{
			for (int i = 0; i < 64; ++i)
			{
				if (this.teleportRandomly(this))
					return true;
			}

			return false;
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void handleEntityEvent(byte id)
	{
		if (id == 4)
		{
			this.attackTimer = 10;
			this.playSound(SoundEvents.IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		}
		else if (id == 5)
		{
			this.heartTicks = 0;

			for (int i = 0; i < 10; ++i)
				spawnHeartParticles(ParticleTypes.DRAGON_BREATH, i < 5 ? 0.2F : 0.3F);
		}
		else if (id == 6)
		{
			for (int i = 0; i < 40; ++i)
			{
				this.level().addParticle(ParticleTypes.LARGE_SMOKE, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), (this.random.nextDouble() - 0.5D) * 0.2F, 0.2F, (this.random.nextDouble() - 0.5D) * 0.2F);
				this.level().addParticle(ParticleTypes.REVERSE_PORTAL, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), (this.random.nextDouble() - 0.5D) * 0.2F, 0.2F, (this.random.nextDouble() - 0.5D) * 0.2F);
			}
		}
		else
			super.handleEntityEvent(id);
	}

	@Override
	public void positionRider(Entity passenger, Entity.MoveFunction func)
	{
		if (this.hasPassenger(passenger))
		{
			var vec = this.holdPositon();
			func.accept(passenger, vec.x(), vec.y(), vec.z());
		}
	}

	@Override
	public Vec3 getDismountLocationForPassenger(LivingEntity living)
	{
		return this.holdPositon();
	}

	private Vec3 holdPositon()
	{
		float mov = Mth.sin((this.tickCount) * 0.1F) * 0.15F;
		float dist = 1.6F + mov;
		float x = Mth.sin(this.yBodyRot * Mth.DEG_TO_RAD) * dist;
		float z = Mth.cos(this.yBodyRot * Mth.DEG_TO_RAD) * dist;

		return new Vec3(this.getX() - x, this.getY() + 3.3F - mov, this.getZ() + z);
	}

	@Override
	public void knockback(double strength, double x, double z)
	{
	}

	@Override
	public void push(double x, double y, double z)
	{
	}

	@OnlyIn(Dist.CLIENT)
	public int getAttackTimer()
	{
		return this.attackTimer;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FLSounds.ENTITY_TITAN_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		float range = 0.2F;
		this.playSound(FLSounds.ENTITY_TITAN_HURT, 1.0F, 1.0F + ((this.random.nextFloat() * range) - (range / 2)));
		return null;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_TITAN_DEATH;
	}

	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
		this.playSound(SoundEvents.IRON_GOLEM_STEP, 1.0F, 1.0F);
	}

	@Override
	public float getVoicePitch()
	{
		return 0.8F;
	}

	@Override
	public void setEyeColor(int colorID)
	{
	}

	@Override
	public int getEyeColor()
	{
		return 0;
	}

	public void setLeftEyeColor(int colorID)
	{
		this.entityData.set(LEFT_EYE_COLOR, colorID);
	}

	public int getLeftEyeColor()
	{
		return this.entityData.get(LEFT_EYE_COLOR);
	}

	public void setRightEyeColor(int colorID)
	{
		this.entityData.set(RIGHT_EYE_COLOR, colorID);
	}

	public int getRightEyeColor()
	{
		return this.entityData.get(RIGHT_EYE_COLOR);
	}

	public void setInRage(boolean rage)
	{
		this.entityData.set(RAGE, rage);
	}

	public boolean inRage()
	{
		return this.entityData.get(RAGE);
	}

	public void setHoldingPlayer(boolean hold)
	{
		this.entityData.set(HOLDING_PLAYER, hold);
	}

	public boolean isHoldingPlayer()
	{
		return this.entityData.get(HOLDING_PLAYER);
	}

	public float getRageHealth()
	{
		return this.getMaxHealth() * 0.25F;
	}
}