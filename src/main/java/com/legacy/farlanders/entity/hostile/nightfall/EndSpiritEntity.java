package com.legacy.farlanders.entity.hostile.nightfall;

import java.util.List;

import net.minecraft.core.BlockPos;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.BlockPathTypes;

public class EndSpiritEntity extends PathfinderMob
{
	public static final EntityDataAccessor<Integer> INVIS_VALUE = SynchedEntityData.<Integer>defineId(EndSpiritEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Boolean> CAN_VANISH = SynchedEntityData.<Boolean>defineId(EndSpiritEntity.class, EntityDataSerializers.BOOLEAN);

	public int deathTicks;
	public int invisTick = 300;

	public EndSpiritEntity(EntityType<? extends EndSpiritEntity> type, Level world)
	{
		super(type, world);
		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.8D));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 10.0F, 9999.0F));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
	}

	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 1.87F;
	}

	/*protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(1.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3D);
	}*/

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(INVIS_VALUE, 200);
		this.entityData.define(CAN_VANISH, this.random.nextBoolean());
	}

	@Override
	public void tick()
	{
		super.tick();
		this.discard();
		if (!this.onGround() && this.getDeltaMovement().y < 0.0D)
		{
			this.setDeltaMovement(this.getDeltaMovement().multiply(1.0D, 0.7D, 1.0D));
		}

		List<Player> list = this.level().<Player>getEntitiesOfClass(Player.class, this.getBoundingBox().inflate(5.0D, 3.0D, 5.0D));
		if (!list.isEmpty() && this.entityData.get(CAN_VANISH) && this.tickCount > 50)
		{
			this.setHealth(0);
		}
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		this.setHealth(0);
		return false;
	}

	@Override
	protected void tickDeath()
	{
		++this.deathTicks;

		for (int k = 0; k < 4; k++)
		{
			this.setInvisValue(this.getInvisValue() - 1);
		}

		if (this.deathTicks > 55)
		{
			this.discard();
		}
	}

	public void setInvisValue(int value)
	{
		this.entityData.set(INVIS_VALUE, value);
	}

	public int getInvisValue()
	{
		return this.entityData.get(INVIS_VALUE);
	}

	public void fall(float distance, float damageMultiplier)
	{
	}

	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
	}

	@Override
	public boolean isPickable()
	{
		return super.isPickable();
	}

	public void push(Entity entityIn)
	{
	}

	public boolean isPushable()
	{
		return false;
	}

	protected SoundEvent getAmbientSound()
	{
		return null;
	}

	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return null;
	}

	protected SoundEvent getDeathSound()
	{
		return null;
	}
}