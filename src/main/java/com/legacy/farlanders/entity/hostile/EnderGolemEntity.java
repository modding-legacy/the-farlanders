package com.legacy.farlanders.entity.hostile;

import javax.annotation.Nullable;

import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.ai.MoveTowardsRestrictionIfPresentGoal;
import com.legacy.farlanders.entity.ai.RestrictToFarlandersGoal;
import com.legacy.farlanders.entity.ai.RetreatGoal;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.entity.util.ITeleportingEntity;
import com.legacy.farlanders.event.FarlandersEvents;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.BlockParticleOption;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.util.Mth;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.common.NeoForgeMod;

public class EnderGolemEntity extends Monster implements IColoredEyes, ITeleportingEntity
{
	public static final EntityDataAccessor<Integer> EYE_COLOR = SynchedEntityData.<Integer>defineId(EnderGolemEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Boolean> ANGRY = SynchedEntityData.<Boolean>defineId(EnderGolemEntity.class, EntityDataSerializers.BOOLEAN);

	private int attackTimer;
	public BlockPos spirePos;

	public EnderGolemEntity(EntityType<? extends EnderGolemEntity> type, Level world)
	{
		super(type, world);

		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(1, new RetreatGoal(this));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.45D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(1, (new HurtByTargetGoal(this).setAlertOthers()));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, Player.class, 10, false, false, FarlandersEvents::targettableByHostile));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Mob.class, 5, false, false, (entity) -> FarlandersEvents.targettableByHostile(entity) && (entity instanceof Enemy && !(entity instanceof Creeper) && !(entity.getType().is(FLTags.Entities.FARLANDER_ALLIES)) || entity instanceof WandererEntity)));

		this.goalSelector.addGoal(1, new RestrictToFarlandersGoal(this));
		this.goalSelector.addGoal(2, new MoveTowardsRestrictionIfPresentGoal(this, 0.55D));
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();

		this.entityData.define(EYE_COLOR, 0);
		this.entityData.define(ANGRY, false);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt(EYE_COLOR_KEY, this.getEyeColor());
		compound.putBoolean("IsAngry", this.getAngry());

		if (this.spirePos != null)
			compound.put("SpirePos", NbtUtils.writeBlockPos(this.spirePos));
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.setEyeColor(compound.getInt(EYE_COLOR_KEY));
		this.setAngry(compound.getBoolean("IsAngry"));

		if (compound.contains("SpirePos"))
			this.spirePos = NbtUtils.readBlockPos(compound.getCompound("SpirePos"));
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor pLevel, DifficultyInstance pDifficulty, MobSpawnType pReason, SpawnGroupData pSpawnData, CompoundTag pDataTag)
	{
		this.randomizeEyeColor();
		return super.finalizeSpawn(pLevel, pDifficulty, pReason, pSpawnData, pDataTag);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 3.55F;
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return Monster.createMonsterAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.MAX_HEALTH, 100.0D).add(Attributes.KNOCKBACK_RESISTANCE, 0.5D).add(Attributes.ATTACK_DAMAGE, 8.0D).add(NeoForgeMod.STEP_HEIGHT.value(), 1.0F);
	}

	@Override
	public void setTarget(@Nullable LivingEntity target)
	{
		this.setAngry(target != null);

		super.setTarget(target);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isInWaterOrBubble())
			this.hurt(this.damageSources().drown(), 1);

		if (this.spirePos != null)
			this.restrictTo(this.spirePos.atY((int) this.getY()), 25);

		if (this.isAggressive())
			this.spawnEyeParticles();
		
		if (!this.level().isClientSide() && this.isAlive() && this.getTarget() == null && this.random.nextInt(700) == 0 && this.deathTime == 0)
			this.heal(1.0F);
	}

	@Override
	public void aiStep()
	{
		super.aiStep();

		if (this.attackTimer > 0)
			--this.attackTimer;

		if (this.getDeltaMovement().horizontalDistanceSqr() > (double) 2.5000003E-7F && this.random.nextInt(5) == 0)
		{
			int i = Mth.floor(this.getX());
			int j = Mth.floor(this.getY() - (double) 0.2F);
			int k = Mth.floor(this.getZ());
			BlockState blockstate = this.level().getBlockState(new BlockPos(i, j, k));

			if (!this.level().isEmptyBlock(new BlockPos(i, j, k)))
				this.level().addParticle(new BlockParticleOption(ParticleTypes.BLOCK, blockstate), this.getX() + ((double) this.random.nextFloat() - 0.5D) * (double) this.getBbWidth(), this.getBoundingBox().minY + 0.1D, this.getZ() + ((double) this.random.nextFloat() - 0.5D) * (double) this.getBbWidth(), 4.0D * ((double) this.random.nextFloat() - 0.5D), 0.5D, ((double) this.random.nextFloat() - 0.5D) * 4.0D);
		}

		if (this.getTarget() != null && !this.getTarget().isAlive())
			this.setTarget(null);
	}

	@Override
	public boolean doHurtTarget(Entity entityIn)
	{
		this.attackTimer = 10;
		this.level().broadcastEntityEvent(this, (byte) 4);
		boolean flag = super.doHurtTarget(entityIn);

		if (flag)
		{
			entityIn.setDeltaMovement(entityIn.getDeltaMovement().add(0.0D, (double) 0.4F, 0.0D));
			this.doEnchantDamageEffects(this, entityIn);
		}

		this.playSound(SoundEvents.IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		return flag;
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else if (!(source.is(DamageTypeTags.IS_PROJECTILE)))
		{
			boolean flag = super.hurt(source, amount);
			if (!this.level().isClientSide() && source.is(DamageTypeTags.BYPASSES_ARMOR) && this.random.nextInt(10) != 0)
			{
				this.teleportRandomly(this);
			}

			return flag;
		}
		else
		{
			for (int i = 0; i < 64; ++i)
			{
				if (this.teleportRandomly(this))
				{
					return true;
				}
			}

			return false;
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void handleEntityEvent(byte id)
	{
		if (id == 4)
		{
			this.attackTimer = 10;
			this.playSound(SoundEvents.IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		}
		else
		{
			super.handleEntityEvent(id);
		}
	}

	@OnlyIn(Dist.CLIENT)
	public int getAttackTimer()
	{
		return this.attackTimer;
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FLSounds.ENTITY_ENDER_GOLEM_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		float range = 0.1F;
		this.playSound(FLSounds.ENTITY_ENDER_GOLEM_HURT, this.getSoundVolume(), 0.8F + ((this.random.nextFloat() * range) - (range / 2)));
		return null;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_ENDER_GOLEM_DEATH;
	}

	@Override
	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
		this.playSound(SoundEvents.COW_STEP, 0.15F, 1.0F);
	}

	@Override
	public float getVoicePitch()
	{
		return 1.2F;
	}

	@Override
	public void setEyeColor(int colorID)
	{
		this.entityData.set(EYE_COLOR, colorID);
	}

	@Override
	public int getEyeColor()
	{
		return this.entityData.get(EYE_COLOR);
	}

	public void setAngry(boolean ang)
	{
		this.entityData.set(ANGRY, ang);
	}

	public boolean getAngry()
	{
		return this.entityData.get(ANGRY);
	}

	@Override
	public boolean isAlliedTo(Entity entity)
	{
		if (entity.getType().is(FLTags.Entities.FARLANDER_ALLIES) && entity.getTeam() == this.getTeam())
			return true;

		return super.isAlliedTo(entity);
	}

	@Override
	protected void checkFallDamage(double y, boolean onGroundIn, BlockState state, BlockPos pos)
	{
	}

	@Override
	protected boolean shouldDespawnInPeaceful()
	{
		return false;
	}

	@Override
	public boolean removeWhenFarAway(double pDistanceToClosestPlayer)
	{
		return false;
	}
}