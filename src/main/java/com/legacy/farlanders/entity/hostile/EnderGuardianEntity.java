package com.legacy.farlanders.entity.hostile;

import javax.annotation.Nullable;

import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.ai.MoveTowardsRestrictionIfPresentGoal;
import com.legacy.farlanders.entity.ai.RestrictToFarlandersGoal;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.entity.util.ITeleportingEntity;
import com.legacy.farlanders.event.FarlandersEvents;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.util.Mth;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.OpenDoorGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.RangedAttackGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.monster.RangedAttackMob;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.AbstractArrow;
import net.minecraft.world.entity.projectile.ProjectileUtil;
import net.minecraft.world.item.BowItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.Vec3;

public class EnderGuardianEntity extends Monster implements IColoredEyes, RangedAttackMob, ITeleportingEntity
{
	public static final EntityDataAccessor<Integer> EYE_COLOR = SynchedEntityData.<Integer>defineId(EnderGuardianEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Boolean> GUARDIAN_ATTACKING = SynchedEntityData.<Boolean>defineId(EnderGuardianEntity.class, EntityDataSerializers.BOOLEAN);

	Vec3 oldTargetVec, targetVec;

	public EnderGuardianEntity(EntityType<? extends EnderGuardianEntity> type, Level world)
	{
		super(type, world);
		((GroundPathNavigation) this.getNavigation()).setCanOpenDoors(true);
		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
	}

	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(2, new GuardianBowAtackGoal<EnderGuardianEntity>(this, 0.65D, 20, 15.0F));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.6D));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));

		this.targetSelector.addGoal(1, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, Player.class, 10, false, false, FarlandersEvents::targettableByHostile));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Mob.class, 5, false, false, (entity) -> FarlandersEvents.targettableByHostile(entity) && (entity instanceof Enemy && !(entity instanceof Creeper) && !(entity.getType().is(FLTags.Entities.FARLANDER_ALLIES)) || entity instanceof WandererEntity)));

		this.goalSelector.addGoal(1, new RestrictToFarlandersGoal(this));
		this.goalSelector.addGoal(2, new MoveTowardsRestrictionIfPresentGoal(this, 0.55D));
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		this.setItemSlot(EquipmentSlot.MAINHAND, Items.BOW.getDefaultInstance());
		this.randomizeEyeColor();

		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();

		this.entityData.define(EYE_COLOR, 0);
		this.entityData.define(GUARDIAN_ATTACKING, false);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt(EYE_COLOR_KEY, this.getEyeColor());
		compound.putBoolean("GuardianAttacking", this.getAttacking());
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.setEyeColor(compound.getInt(EYE_COLOR_KEY));
		this.setAttacking(compound.getBoolean("GuardianAttacking"));
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 1.60F;
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return Monster.createMonsterAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.MAX_HEALTH, 40.0D);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isInWaterOrBubble())
			this.hurt(this.damageSources().drown(), 1);

		this.spawnEyeParticles();

		if (!this.level().isClientSide() && this.isAlive() && this.getTarget() == null && this.random.nextInt(700) == 0 && this.deathTime == 0)
			this.heal(1.0F);
	}

	@Override
	public void aiStep()
	{
		if (this.getTarget() != null && !this.getTarget().isAlive())
			this.setTarget(null);

		super.aiStep();
	}

	@Override
	public void setTarget(@Nullable LivingEntity target)
	{
		this.setAttacking(target != null);
		super.setTarget(target);
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else if (!(source.is(DamageTypeTags.IS_PROJECTILE)))
		{
			boolean flag = super.hurt(source, amount);
			if (!this.level().isClientSide() && source.is(DamageTypeTags.BYPASSES_ARMOR) && this.random.nextInt(10) != 0)
			{
				this.teleportRandomly(this);
			}

			return flag;
		}
		else
		{
			for (int i = 0; i < 64; ++i)
			{
				if (this.teleportRandomly(this))
					return true;
			}

			return false;
		}
	}

	public void setEyeColor(int colorID)
	{
		this.entityData.set(EYE_COLOR, colorID);
	}

	public int getEyeColor()
	{
		return this.entityData.get(EYE_COLOR);
	}

	public void setAttacking(boolean att)
	{
		this.entityData.set(GUARDIAN_ATTACKING, att);
	}

	public boolean getAttacking()
	{
		return this.entityData.get(GUARDIAN_ATTACKING);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FLSounds.ENTITY_ENDER_GUARDIAN_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		float range = 0.1F;
		this.playSound(FLSounds.ENTITY_ENDER_GUARDIAN_HURT, this.getSoundVolume(), 1.2F + ((this.random.nextFloat() * range) - (range / 2)));
		return null;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_ENDER_GUARDIAN_DEATH;
	}

	@Override
	public float getVoicePitch()
	{
		return 1.7F;
	}

	@Override
	public boolean isAlliedTo(Entity entity)
	{
		if (entity.getType().is(FLTags.Entities.FARLANDER_ALLIES) && entity.getTeam() == this.getTeam())
			return true;

		return super.isAlliedTo(entity);
	}

	@Override
	public void performRangedAttack(LivingEntity target, float distanceFactor)
	{

		ItemStack itemstack = this.getProjectile(this.getItemInHand(ProjectileUtil.getWeaponHoldingHand(this, item -> item instanceof BowItem)));
		AbstractArrow abstractarrowentity = ProjectileUtil.getMobArrow(this, itemstack, distanceFactor);

		if (this.getMainHandItem().getItem()instanceof BowItem bow)
			abstractarrowentity = bow.customArrow(abstractarrowentity, itemstack);

		Vec3 predictedVec = this.targetVec != null ? this.targetVec.add(this.targetVec.subtract(this.oldTargetVec).scale(7)).add(0, target.getY(), 0) : target.position();

		double d0 = predictedVec.x() - this.getX();
		double d1 = target.getBoundingBox().minY + (double) (target.getBbHeight() / 3.0F) - abstractarrowentity.getY();
		double d2 = predictedVec.z() - this.getZ();
		double d3 = Mth.sqrt((float) (d0 * d0 + d2 * d2));
		abstractarrowentity.shoot(d0, d1 + d3 * (double) 0.2F, d2, 1.6F, (float) (14 - this.level().getDifficulty().getId() * 4));
		this.playSound(FLSounds.ENTITY_ENDER_GUARDIAN_SHOOT, 1.0F, 1.0F / (this.getRandom().nextFloat() * 0.4F + 0.8F));
		this.level().addFreshEntity(abstractarrowentity);
	}

	@Override
	protected boolean shouldDespawnInPeaceful()
	{
		return false;
	}

	@Override
	public boolean removeWhenFarAway(double pDistanceToClosestPlayer)
	{
		return false;
	}

	class GuardianBowAtackGoal<T extends EnderGuardianEntity> extends RangedAttackGoal
	{
		private final T mob;
		private final int maxCooldown;

		public GuardianBowAtackGoal(T mob, double speed, int tickCooldown, float maxAttackDistance)
		{
			super(mob, speed, tickCooldown, maxAttackDistance);

			this.mob = mob;
			this.maxCooldown = tickCooldown;
		}

		@Override
		public void start()
		{
			if (this.mob.getTarget() != null)
			{
				this.mob.oldTargetVec = this.mob.targetVec = this.mob.position();
				this.mob.getLookControl().setLookAt(this.mob.getTarget());
			}

			super.start();
			// this.mob.playSound(SoundEvents.ARMOR_EQUIP_GENERIC, 1.2F, 1.0F /
			// (this.rand.nextFloat() * 0.4F + 0.8F));
			this.mob.setAggressive(true);
			this.mob.startUsingItem(InteractionHand.MAIN_HAND);
		}

		@Override
		public boolean canUse()
		{
			return super.canUse() && this.mob.getMainHandItem().getItem() instanceof BowItem;
		}

		@Override
		public boolean canContinueToUse()
		{
			return super.canContinueToUse() && this.mob.getMainHandItem().getItem() instanceof BowItem;
		}

		@Override
		public void tick()
		{
			super.tick();

			int i = this.mob.getTicksUsingItem();
			if (this.attackTime >= this.maxCooldown)
				this.mob.stopUsingItem();
			else if (i == 0)
				this.mob.startUsingItem(InteractionHand.MAIN_HAND);

			if (this.mob.getTarget() != null)
			{
				if (this.mob.distanceTo(this.mob.getTarget()) <= 6)
				{
					this.mob.setYBodyRot(this.mob.getYHeadRot());
					this.mob.setYRot(this.mob.getYHeadRot());
					this.mob.getMoveControl().strafe(-0.7F, 0.0F);
				}

				this.mob.oldTargetVec = this.mob.targetVec;
				this.mob.targetVec = this.mob.getTarget().position().multiply(1, 0, 1);

				this.mob.getLookControl().setLookAt(this.mob.targetVec.add(0, mob.getTarget().getEyeY(), 0));
			}

		}

		@Override
		public void stop()
		{
			this.mob.oldTargetVec = this.mob.targetVec = null;
			this.mob.stopUsingItem();
			this.mob.setAggressive(false);
			super.stop();
		}
	}
}