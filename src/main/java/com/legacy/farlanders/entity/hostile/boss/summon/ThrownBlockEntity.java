package com.legacy.farlanders.entity.hostile.boss.summon;

public class ThrownBlockEntity// extends FallingBlockEntity
{
	/*private LivingEntity thrower;
	
	public ThrownBlockEntity(EntityType<? extends ThrownBlockEntity> type, Level world)
	{
		super(type, world);
	}
	
	public ThrownBlockEntity(Level worldIn, double x, double y, double z, LivingEntity thrower)
	{
		this(FarlandersEntityTypes.THROWN_BLOCK, worldIn);
		this.blocksBuilding = true;
		this.setPos(x, y + (double) ((1.0F - this.getBbHeight()) / 2.0F), z);
		this.setDeltaMovement(Vec3.ZERO);
		this.xo = x;
		this.yo = y;
		this.zo = z;
		this.setStartPos(new BlockPos(this.position()));
	}
	
	public ThrownBlockEntity(FMLPlayMessages.SpawnEntity spawnEntity, Level world)
	{
		this(FarlandersEntityTypes.THROWN_BLOCK, world);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void tick()
	{
		BlockState fallTile = Blocks.SNOW_BLOCK.defaultBlockState();
	
		if (fallTile.isAir())
		{
			this.remove();
		}
		else
		{
			this.xo = this.getX();
			this.yo = this.getY();
			this.zo = this.getZ();
			Block block = fallTile.getBlock();
	
			if (this.time++ == 0)
			{
				BlockPos blockpos = new BlockPos(this.position());
				if (this.level.getBlockState(blockpos).getBlock() == block)
				{
					this.level.removeBlock(blockpos, false);
				}
				else if (!this.level.isClientSide)
				{
					this.remove();
					return;
				}
			}
	
			if (!this.isNoGravity())
			{
				this.setDeltaMovement(this.getDeltaMovement().add(0.0D, -0.04D, 0.0D));
			}
	
			this.move(MoverType.SELF, this.getDeltaMovement());
			this.setDeltaMovement(this.getDeltaMovement().scale(0.98D));
		}
	
		List<Entity> list = Lists.newArrayList(this.level.getEntities(this, this.getBoundingBox()));
		for (Entity entity : list)
		{
			this.damage(entity, 0.0F);
		}
	
		if (this.tickCount > 500 || this.horizontalCollision || this.verticalCollision)
		{
			if (this.level instanceof ServerLevel)
			{
				for (int k = 0; k < 10; k++)
				{
					this.playSound(SoundEvents.SNOW_BREAK, 0.5F, 1.0F);
					this.playSound(SoundEvents.SNOW_BREAK, 0.5F, 0.5F);
					((ServerLevel) this.level).sendParticles(ParticleTypes.CLOUD, this.getX(), this.getY() + (double) this.getBbHeight() / 1.5D, this.getZ(), 10, (double) (this.getBbWidth() / 4.0F), (double) (this.getBbHeight() / 4.0F), (double) (this.getBbWidth() / 4.0F), 0.05D);
					((ServerLevel) this.level).sendParticles(new BlockParticleOption(ParticleTypes.BLOCK, Blocks.SNOW_BLOCK.defaultBlockState()), this.getX(), this.getY() + (double) this.getBbHeight() / 1.5D, this.getZ(), 10, (double) (this.getBbWidth() / 4.0F), (double) (this.getBbHeight() / 4.0F), (double) (this.getBbWidth() / 4.0F), 0.05D);
				}
			}
	
			this.remove();
		}
	}
	
	private void damage(Entity hitEntityIn, float amp)
	{
		DamageSource damagesource = DamageSource.FALLING_BLOCK;
		LivingEntity thrower = this.thrower;
	
		if (hitEntityIn instanceof LivingEntity && hitEntityIn.isAlive() && !hitEntityIn.isInvulnerable() && hitEntityIn != thrower && ((LivingEntity) hitEntityIn).hurtTime == 0)
		{
			if (thrower == null)
			{
				hitEntityIn.hurt(damagesource.setProjectile(), 7.0F + amp);
			}
			else
			{
				if (thrower.isAlliedTo(hitEntityIn))
				{
					return;
				}
	
				hitEntityIn.hurt(new EntityDamageSource("colossus_crush", thrower).setScalesWithDifficulty().setProjectile(), 7.0F + amp);
	
			}
		}
	}
	
	public void fall(float distance, float damageMultiplier)
	{
		int i = Mth.ceil(distance - 1.0F);
		if (i > 0)
		{
			List<Entity> list = Lists.newArrayList(this.level.getEntities(this, this.getBoundingBox()));
			DamageSource damagesource = DamageSource.FALLING_BLOCK;
	
			for (Entity entity : list)
			{
				entity.hurt(damagesource, (float) Math.min(Mth.floor((float) i * 5), 5));
			}
	
		}
	
	}
	
	@Override
	public Packet<?> getAddEntityPacket()
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}*/
}
