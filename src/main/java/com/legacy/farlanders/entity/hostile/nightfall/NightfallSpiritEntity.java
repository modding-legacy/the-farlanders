package com.legacy.farlanders.entity.hostile.nightfall;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.Level;

public class NightfallSpiritEntity extends EndSpiritEntity
{

	public NightfallSpiritEntity(EntityType<? extends NightfallSpiritEntity> type, Level world)
	{
		super(type, world);
	}
}
