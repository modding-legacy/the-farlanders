package com.legacy.farlanders.entity.hostile.boss.summon;

public class EnderColossusShadowEntity //extends Monster
{
	/*public static final EntityDataAccessor<Integer> INVIS_VALUE = SynchedEntityData.<Integer>defineId(EndSpiritEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Boolean> ANGRY = SynchedEntityData.<Boolean>defineId(EnderColossusShadowEntity.class, EntityDataSerializers.BOOLEAN);
	
	private int attackTimer;
	public int deathTicks, invisTick = 300;
	
	public EnderColossusShadowEntity(EntityType<? extends EnderColossusShadowEntity> type, Level world)
	{
		super(type, world);
	
		this.maxUpStep = 1.0F;
		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
	}
	
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.5D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, Player.class, false));
	}
	
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}
	
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(ANGRY, false);
		this.entityData.define(INVIS_VALUE, 200);
	}
	
	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putBoolean("IsAngry", this.getAngry());
	}
	
	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.setAngry(compound.getBoolean("IsAngry"));
	}
	
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 3.55F;
	}
	
	protected void registerAttributes()
	{
		super.registerAttributes();
		this.getAttribute(SharedMonsterAttributes.FOLLOW_RANGE).setBaseValue(50.0D);
		this.getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(40.0D);
		this.getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.5D);
	}
	
	public void setTarget(@Nullable LivingEntity entitylivingbaseIn)
	{
		super.setTarget(entitylivingbaseIn);
	
		if (entitylivingbaseIn != null)
		{
			this.setAngry(true);
		}
		else
		{
			this.setAngry(false);
		}
	}
	
	@Override
	public void tick()
	{
		super.tick();
	
		this.remove();
	}
	
	@SuppressWarnings("deprecation")
	public void aiStep()
	{
		super.aiStep();
	
		if (this.attackTimer > 0)
		{
			--this.attackTimer;
		}
	
		if (this.getHealth() <= 0.0F)
		{
			for (int i = 0; i < 3; i++)
			{
				this.level.addParticle(ParticleTypes.CLOUD, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), 0, 0, 0);
				this.level.addParticle(ParticleTypes.LARGE_SMOKE, this.getX() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), this.getY() + this.random.nextDouble() * (double) this.getBbHeight() - 0.25D, this.getZ() + (this.random.nextDouble() - 0.5D) * (double) this.getBbWidth(), 0, 0, 0);
			}
		}
	
		if (getHorizontalDistanceSqr(this.getDeltaMovement()) > (double) 2.5000003E-7F && this.random.nextInt(5) == 0)
		{
			int i = Mth.floor(this.getX());
			int j = Mth.floor(this.getY() - (double) 0.2F);
			int k = Mth.floor(this.getZ());
			BlockState blockstate = this.level.getBlockState(new BlockPos(i, j, k));
	
			if (!blockstate.isAir())
			{
				this.level.addParticle(new BlockParticleOption(ParticleTypes.BLOCK, blockstate), this.getX() + ((double) this.random.nextFloat() - 0.5D) * (double) this.getBbWidth(), this.getBoundingBox().minY + 0.1D, this.getZ() + ((double) this.random.nextFloat() - 0.5D) * (double) this.getBbWidth(), 4.0D * ((double) this.random.nextFloat() - 0.5D), 0.5D, ((double) this.random.nextFloat() - 0.5D) * 4.0D);
			}
		}
	}
	
	public boolean doHurtTarget(Entity entityIn)
	{
		this.attackTimer = 10;
		this.level.broadcastEntityEvent(this, (byte) 4);
		boolean flag = entityIn.hurt(DamageSource.mobAttack(this), 7.0F);
		if (flag)
		{
			entityIn.setDeltaMovement(entityIn.getDeltaMovement().add(0.0D, (double) 0.3F, 0.0D));
			this.doEnchantDamageEffects(this, entityIn);
		}
		this.playSound(SoundEvents.IRON_GOLEM_ATTACK, 1.0F, 1.0F);
		return flag;
	}
	
	@OnlyIn(Dist.CLIENT)
	public void handleEntityEvent(byte id)
	{
		if (id == 4)
		{
			this.attackTimer = 10;
			this.playSound(SoundEvents.IRON_GOLEM_ATTACK, 1.0F, 0.7F);
		}
		else
		{
			super.handleEntityEvent(id);
		}
	}
	
	@Override
	protected void tickDeath()
	{
		++this.deathTicks;
	
		for (int k = 0; k < 4; k++)
		{
			this.setInvisValue(this.getInvisValue() - 1);
		}
	
		if (this.deathTicks > 55)
		{
			this.remove();
		}
	}
	
	@OnlyIn(Dist.CLIENT)
	public int getAttackTimer()
	{
		return this.attackTimer;
	}
	
	protected SoundEvent getAmbientSound()
	{
		return FarlandersSounds.ENTITY_ENDER_GOLEM_IDLE;
	}
	
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		this.playSound(FarlandersSounds.ENTITY_ENDER_GOLEM_HURT, this.getSoundVolume(), 0.7F);
		return null;
	}
	
	protected SoundEvent getDeathSound()
	{
		return FarlandersSounds.ENTITY_TITAN_DEATH_ECHO;
	}
	
	protected void playStepSound(BlockPos pos, BlockState blockIn)
	{
		this.playSound(SoundEvents.IRON_GOLEM_STEP, 2.5F, 0.5F);
		this.playSound(SoundEvents.SNOW_BREAK, 0.5F, 0.1F);
	}
	
	protected float getVoicePitch()
	{
		return 0.9F;
	}
	
	@Override
	protected float getSoundVolume()
	{
		return 2.0F;
	}
	
	public boolean isPickable()
	{
		return this.getHealth() > 0;
	}
	
	public void setAngry(boolean ang)
	{
		this.entityData.set(ANGRY, ang);
	}
	
	public boolean getAngry()
	{
		return this.entityData.get(ANGRY);
	}
	
	public void setInvisValue(int value)
	{
		this.entityData.set(INVIS_VALUE, value);
	}
	
	public int getInvisValue()
	{
		return this.entityData.get(INVIS_VALUE);
	}
	
	public void fall(float distance, float damageMultiplier)
	{
	}
	
	public void kill()
	{
		this.remove();
	}*/
}