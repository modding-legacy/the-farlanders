package com.legacy.farlanders.entity.hostile;

import javax.annotation.Nullable;

import com.legacy.farlanders.FarlandersConfig;
import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.entity.WandererEntity;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.event.FarlandersEvents;
import com.legacy.farlanders.registry.FLItems;
import com.legacy.farlanders.registry.FLSounds;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.Difficulty;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.OpenDoorGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.monster.Endermite;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.pathfinder.BlockPathTypes;

public class LooterEntity extends Monster implements IColoredEyes
{
	public static final EntityDataAccessor<Integer> EYE_COLOR = SynchedEntityData.<Integer>defineId(LooterEntity.class, EntityDataSerializers.INT);

	public LooterEntity(EntityType<? extends LooterEntity> type, Level world)
	{
		super(type, world);
		((GroundPathNavigation) this.getNavigation()).setCanOpenDoors(true);
		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return 1.60F;
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return Monster.createMonsterAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.MAX_HEALTH, 30.0D).add(Attributes.ATTACK_DAMAGE, 3.0D);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(5, new WaterAvoidingRandomStrollGoal(this, 0.6D));
		this.goalSelector.addGoal(5, new MeleeAttackGoal(this, 0.65D, true));
		this.goalSelector.addGoal(6, new LookAtPlayerGoal(this, LivingEntity.class, 8.0F));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));
		this.goalSelector.addGoal(7, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(3, (new HurtByTargetGoal(this)));
		this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, Player.class, 10, false, false, FarlandersEvents::targettableByHostile));
		this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Mob.class, 5, false, false, (entity) -> FarlandersEvents.targettableByHostile(entity) && (entity.getType().is(FLTags.Entities.HUNTED_BY_REBELS) || entity instanceof WandererEntity)));
		this.targetSelector.addGoal(5, new NearestAttackableTargetGoal<>(this, Endermite.class, false));
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();

		this.entityData.define(EYE_COLOR, 0);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt(EYE_COLOR_KEY, this.getEyeColor());
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.setEyeColor(compound.getInt(EYE_COLOR_KEY));
	}

	@SuppressWarnings("deprecation")
	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		if (FarlandersConfig.COMMON.disableLooterStealing.get())
		{
			int rand = worldIn.getRandom().nextInt(7);
			Item randomWeapon = rand == 0 ? Items.IRON_AXE : rand == 1 ? Items.STONE_SWORD : rand == 2 ? Items.STONE_AXE : rand == 3 ? Items.DIAMOND_SWORD : rand == 4 ? Items.GOLDEN_SWORD : rand == 5 ? Items.GOLDEN_AXE : Items.IRON_SWORD;
			this.setItemSlot(EquipmentSlot.MAINHAND, randomWeapon.getDefaultInstance());
			this.setDropChance(EquipmentSlot.MAINHAND, 0);
		}

		this.randomizeEyeColor();

		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	@Override
	protected void dropCustomDeathLoot(DamageSource pSource, int pLooting, boolean pRecentlyHit)
	{
	}

	@Override
	public void die(DamageSource cause)
	{
		super.die(cause);

		this.dropStolenItem();
	}

	public boolean stoleItem()
	{
		return !FarlandersConfig.COMMON.disableLooterStealing.get() && !this.getMainHandItem().isEmpty();
	}

	public void dropStolenItem()
	{
		if (this.stoleItem())
		{
			this.spawnAtLocation(this.getMainHandItem(), 1);
			this.setItemSlot(EquipmentSlot.MAINHAND, ItemStack.EMPTY);
			this.swing(InteractionHand.MAIN_HAND);
		}
	}

	@Override
	public boolean requiresCustomPersistence()
	{
		if (this.stoleItem())
			return true;

		return super.requiresCustomPersistence();
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.isInWaterOrBubble())
			this.hurt(this.damageSources().drown(), 1);

		if (this.getMainHandItem() != ItemStack.EMPTY)
			this.spawnEyeParticles();

		if (this.level().getDifficulty().equals(Difficulty.PEACEFUL) && this.stoleItem())
			this.dropStolenItem();
	}

	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		boolean flag = super.hurt(source, amount);

		if (!this.level().isClientSide() && !FarlandersConfig.COMMON.disableLooterStealing.get() && this.isAlive() && this.getMainHandItem().isEmpty() && source.getDirectEntity()instanceof Player player && !source.isCreativePlayer() && player.getItemBySlot(EquipmentSlot.HEAD).getItem() != FLItems.looter_hood)
		{
			ItemStack stack = player.getInventory().getSelected();

			if (!stack.isEmpty() && stack.is(FLTags.Items.LOOTER_STEALABLE))
			{
				ItemStack copy = player.getMainHandItem().copy();
				this.setItemSlot(EquipmentSlot.MAINHAND, copy);
				player.setItemSlot(EquipmentSlot.MAINHAND, ItemStack.EMPTY);

				this.playSound(SoundEvents.ITEM_PICKUP, 1.0F, 0.8F);
				this.playSound(SoundEvents.ARMOR_EQUIP_GENERIC, 1.2F, 0.7F);

				this.level().playSound(null, player.blockPosition(), SoundEvents.ITEM_BREAK, player.getSoundSource(), 0.6F, 1.0F);
			}

		}
		return flag;
	}

	public void setEyeColor(int colorID)
	{
		this.entityData.set(EYE_COLOR, colorID);
	}

	public int getEyeColor()
	{
		return this.entityData.get(EYE_COLOR);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FLSounds.ENTITY_LOOTER_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return FLSounds.ENTITY_LOOTER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_LOOTER_DEATH;
	}

	@Override
	protected boolean shouldDespawnInPeaceful()
	{
		return super.shouldDespawnInPeaceful() && !this.stoleItem();
	}
}