package com.legacy.farlanders.entity;

import java.util.EnumSet;

import javax.annotation.Nullable;

import com.legacy.farlanders.data.FLTags;
import com.legacy.farlanders.entity.ai.FarlanderBreedGoal;
import com.legacy.farlanders.entity.ai.FarlanderLocateHomeGoal;
import com.legacy.farlanders.entity.hostile.LooterEntity;
import com.legacy.farlanders.entity.hostile.RebelEntity;
import com.legacy.farlanders.entity.util.BreedableVillagerEntity;
import com.legacy.farlanders.entity.util.FarlanderTrades;
import com.legacy.farlanders.entity.util.IColoredEyes;
import com.legacy.farlanders.registry.FLBlocks;
import com.legacy.farlanders.registry.FLEntityTypes;
import com.legacy.farlanders.registry.FLItems;
import com.legacy.farlanders.registry.FLSounds;

import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.stats.Stats;
import net.minecraft.util.Mth;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.InteractGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.LookAtTradingPlayerGoal;
import net.minecraft.world.entity.ai.goal.MoveTowardsRestrictionGoal;
import net.minecraft.world.entity.ai.goal.OpenDoorGoal;
import net.minecraft.world.entity.ai.goal.PanicGoal;
import net.minecraft.world.entity.ai.goal.RestrictSunGoal;
import net.minecraft.world.entity.ai.goal.TemptGoal;
import net.minecraft.world.entity.ai.goal.TradeWithPlayerGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.navigation.GroundPathNavigation;
import net.minecraft.world.entity.ai.targeting.TargetingConditions;
import net.minecraft.world.entity.ai.util.DefaultRandomPos;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.entity.npc.VillagerData;
import net.minecraft.world.entity.npc.VillagerTrades;
import net.minecraft.world.entity.npc.VillagerTrades.ItemListing;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.trading.MerchantOffer;
import net.minecraft.world.item.trading.MerchantOffers;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.Vec3;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class FarlanderEntity extends BreedableVillagerEntity implements IColoredEyes
{
	public static final EntityDataAccessor<Integer> LEVEL = SynchedEntityData.<Integer>defineId(FarlanderEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Integer> EYE_COLOR = SynchedEntityData.<Integer>defineId(FarlanderEntity.class, EntityDataSerializers.INT);
	public static final EntityDataAccessor<Boolean> TEMPTED = SynchedEntityData.<Boolean>defineId(FarlanderEntity.class, EntityDataSerializers.BOOLEAN);

	private int xp, timeUntilReset, numberOfRestocksToday;
	private long lastRestockGameTime, lastRestockCheckDayTime;
	private boolean customer;

	public BlockPos homePos, villageCenter;
	public int restrictDistance = 15;
	public boolean homeFoundNaturally = false;

	public FarlanderEntity(EntityType<? extends FarlanderEntity> type, Level world)
	{
		super(type, world);

		if (this.getNavigation()instanceof GroundPathNavigation p)
		{
			p.setCanOpenDoors(true);
			p.setCanFloat(true);
		}
		
		this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
	}

	@Override
	protected void registerGoals()
	{
		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(0, new FarlanderLocateHomeGoal(this));
		this.goalSelector.addGoal(6, new OpenDoorGoal(this, true));

		this.goalSelector.addGoal(1, new TradeWithPlayerGoal(this));
		this.goalSelector.addGoal(1, new LookAtTradingPlayerGoal(this));

		this.goalSelector.addGoal(1, new TemptGoal(this, 0.6D, Ingredient.of(FLBlocks.endumium_block.asItem(), Items.APPLE), false)
		{
			@Override
			public void start()
			{
				super.start();
				getEntityData().set(TEMPTED, true);
			}

			@Override
			public void stop()
			{
				super.stop();
				getEntityData().set(TEMPTED, false);
			}
		});
		this.goalSelector.addGoal(2, new AggressiveStayNearRestrictionGoal(0.7D));
		this.goalSelector.addGoal(2, new MoveTowardsRestrictionGoal(this, 0.7D)
		{
			@Override
			public boolean canUse()
			{
				return !homeFoundNaturally && super.canUse();
			}

			@Override
			public boolean canContinueToUse()
			{
				return !homeFoundNaturally && super.canContinueToUse();
			}
		});

		this.goalSelector.addGoal(3, new PanicGoal(this, 0.5D));

		this.goalSelector.addGoal(8, new WaterAvoidingRandomStrollGoal(this, 0.5D));
		this.goalSelector.addGoal(9, new InteractGoal(this, Player.class, 3.0F, 1.0F));
		this.goalSelector.addGoal(10, new LookAtPlayerGoal(this, Mob.class, 8.0F));

		this.addAvoidGoals(0.7F, 0.9F);

		this.goalSelector.addGoal(3, new RestrictSunGoal(this)
		{
			@Override
			public boolean canUse()
			{
				if (homePos != null && getPosDistance(homePos) > 10)
					return false;

				return (super.canUse() || level().isRainingAt(blockPosition()))/* && isWithinRestriction()*/;
			}

			@Override
			public boolean canContinueToUse()
			{
				if (homePos != null && getPosDistance(homePos) > 10)
					return false;

				return super.canContinueToUse()/* && isWithinRestriction()*/;
			}
		});

		if (!(this instanceof ElderFarlanderEntity))
			this.goalSelector.addGoal(2, new FarlanderBreedGoal(this, 0.6D));
	}

	public void addAvoidGoals(float farSpeed, float closeSpeed)
	{
		this.goalSelector.addGoal(1, new AvoidAndAlertGuardsGoal<>(this, LooterEntity.class, 8.0F, farSpeed, closeSpeed));
		this.goalSelector.addGoal(1, new AvoidAndAlertGuardsGoal<>(this, RebelEntity.class, 8.0F, farSpeed, closeSpeed));
		this.goalSelector.addGoal(1, new AvoidAndAlertGuardsGoal<>(this, Zombie.class, 8.0F, farSpeed, closeSpeed));
	}

	@Override
	public boolean isWithinRestriction(BlockPos pos)
	{
		return super.isWithinRestriction(pos);
	}

	@Override
	protected void ageBoundaryReached()
	{
		super.ageBoundaryReached();
	}

	@Override
	protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn)
	{
		return this.isBaby() ? 0.75F : 1.60F;
	}

	@Override
	public boolean removeWhenFarAway(double distanceToClosestPlayer)
	{
		return false;
	}

	public static AttributeSupplier.Builder registerAttributeMap()
	{
		return Mob.createMobAttributes().add(Attributes.MOVEMENT_SPEED, 0.5D).add(Attributes.FOLLOW_RANGE, 48.0D).add(Attributes.ATTACK_DAMAGE, 3.0D);
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();

		this.entityData.define(EYE_COLOR, 0);
		this.entityData.define(LEVEL, 1);
		this.entityData.define(TEMPTED, false);
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor pLevel, DifficultyInstance pDifficulty, MobSpawnType pReason, SpawnGroupData pSpawnData, CompoundTag pDataTag)
	{
		this.randomizeEyeColor();
		return super.finalizeSpawn(pLevel, pDifficulty, pReason, pSpawnData, pDataTag);
	}

	private static final String HOME_POS_KEY = "HomePosition", VILLAGE_ORIGIN_KEY = "VillageOrigin";

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		compound.putInt(EYE_COLOR_KEY, this.getEyeColor());
		compound.putInt("Xp", this.xp);
		compound.putInt("Level", this.getTradeLevel());
		compound.putLong("LastRestock", this.lastRestockGameTime);
		compound.putInt("RestocksToday", this.numberOfRestocksToday);

		this.lastRestockGameTime = compound.getLong("LastRestock");
		this.numberOfRestocksToday = compound.getInt("RestocksToday");

		if (this.homePos != null)
			compound.put(HOME_POS_KEY, NbtUtils.writeBlockPos(this.homePos));

		if (this.villageCenter != null)
			compound.put(VILLAGE_ORIGIN_KEY, NbtUtils.writeBlockPos(this.villageCenter));

		compound.putInt("RestrictDistance", this.restrictDistance);
		compound.putBoolean("HomeFoundNaturally", this.homeFoundNaturally);
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);

		this.setEyeColor(compound.getInt(EYE_COLOR_KEY));
		this.setTradeLevel(compound.getInt("Level"));

		if (compound.contains("Offers", 10))
			this.offers = new MerchantOffers(compound.getCompound("Offers"));

		if (compound.contains("Xp", 3))
			this.xp = compound.getInt("Xp");

		this.lastRestockGameTime = compound.getLong("LastRestock");
		this.numberOfRestocksToday = compound.getInt("RestocksToday");

		if (compound.contains(HOME_POS_KEY))
			this.homePos = NbtUtils.readBlockPos(compound.getCompound(HOME_POS_KEY));

		if (compound.contains(VILLAGE_ORIGIN_KEY))
			this.villageCenter = NbtUtils.readBlockPos(compound.getCompound(VILLAGE_ORIGIN_KEY));

		this.restrictDistance = compound.getInt("RestrictDistance");
		this.homeFoundNaturally = compound.getBoolean("HomeFoundNaturally");
	}

	@Override
	public float getWalkTargetValue(BlockPos pos, LevelReader reader)
	{
		if (this.homePos != null && this.level().isDay() && this.level().getBrightness(LightLayer.SKY, pos) >= this.level().getMaxLightLevel() - 1)
			return -3.0F;

		return super.getWalkTargetValue(pos, reader);
	}

	@Override
	public void tick()
	{
		super.tick();

		if (this.homePos != null && this.getPosDistance(this.homePos) > 80 && !this.isWithinRestriction())
			this.homePos = null;

		if (this.villageCenter != null && this.getPosDistance(this.villageCenter) > 100)
			this.villageCenter = null;

		int restrictDist = !this.homeFoundNaturally && (this.level().isDay() || this.level().isRaining()) ? (this.isDayOrRainingAndInLight() ? 1 : 5) : this.restrictDistance;
		if (this.villageCenter != null && !this.level().isDay() && !this.level().isRaining())
			this.restrictTo(this.villageCenter, restrictDist);
		else if (this.homePos != null)
			this.restrictTo(this.homePos, restrictDist);
		else
			this.clearRestriction();

		/*if (GoalUtils.hasGroundPathNavigation(this))
			((GroundPathNavigation) getNavigation()).setCanOpenDoors(this.villageCenter == null || this.villageCenter != null && !(level.isDay() && isWithinRestriction()) || this.level().getBrightness(LightLayer.SKY, this.blockPosition()) >= this.level().getMaxLightLevel());*/

		if (this.level().getDayTime() % 6000 == 0 && !this.level().isDay() && this.shouldRestock())
		{
			this.playSound(SoundEvents.ARMOR_EQUIP_GENERIC);
			this.restock();
		}

		if (this.isInWaterOrBubble())
			this.hurt(this.damageSources().drown(), 1);

		if (this.getRandom().nextFloat() < 0.5F)
			this.spawnEyeParticles();

		/*if (this.tickCount % 20 == 0)
			this.swing(InteractionHand.MAIN_HAND);*/
	}

	@Override
	protected void customServerAiStep()
	{
		if (!this.isTrading() && this.timeUntilReset > 0)
		{
			--this.timeUntilReset;

			if (this.timeUntilReset <= 0)
			{
				if (this.customer)
				{
					this.levelUp();
					this.customer = false;
				}

				this.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 200, 0));
			}
		}

		if (this.homeFoundNaturally && this.getHomePos() != null)
		{
			if (!(this.level().getBlockState(this.getHomePos()).getBlock() instanceof BedBlock))
			{
				this.homePos = null;
				this.homeFoundNaturally = false;
				this.level().broadcastEntityEvent(this, (byte) 13);
			}

		}

		super.customServerAiStep();
	}

	@Override
	public void aiStep()
	{
		this.updateSwingTime();
		super.aiStep();
	}

	@OnlyIn(Dist.CLIENT)
	public void handleEntityEvent(byte id)
	{
		switch (id)
		{
		case 12:
			this.addParticlesAroundSelf(ParticleTypes.HEART);
			break;
		case 13:
			this.addParticlesAroundSelf(ParticleTypes.ANGRY_VILLAGER);
			break;
		case 14:
			this.addParticlesAroundSelf(ParticleTypes.HAPPY_VILLAGER);
			break;
		case 42:
			this.addParticlesAroundSelf(ParticleTypes.SPLASH);
			break;
		default:
			super.handleEntityEvent(id);
			break;
		}
	}

	public void setEyeColor(int colorID)
	{
		this.entityData.set(EYE_COLOR, colorID);
	}

	public int getEyeColor()
	{
		return this.entityData.get(EYE_COLOR);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return FLSounds.ENTITY_FARLANDER_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return FLSounds.ENTITY_FARLANDER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return FLSounds.ENTITY_FARLANDER_DEATH;
	}

	@Override
	public SoundEvent getNotifyTradeSound()
	{
		this.playSound(FLSounds.ENTITY_FARLANDER_IDLE, this.getSoundVolume(), 1.3F);
		return null;
	}

	@Override
	protected SoundEvent getTradeUpdatedSound(boolean positive)
	{
		/*float base = this.getVoicePitch();
		this.playSound(FLSounds.ENTITY_FARLANDER_IDLE, this.getSoundVolume(), positive ? base + 0.3F : base - 0.3F);*/
		return FLSounds.ENTITY_FARLANDER_IDLE;
	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel level, AgeableMob otherParent)
	{
		FarlanderEntity baby = FLEntityTypes.FARLANDER.create(this.level());
		baby.setAge(Integer.MAX_VALUE);

		baby.villageCenter = this.villageCenter;
		baby.homePos = this.homePos;

		if (baby.homePos != null)
			baby.homeFoundNaturally = false;

		if (level.getRandom().nextFloat() < 0.10F)
			this.setEyeColor(this.random.nextInt(this.getEyeColors().size()));
		else
			baby.setEyeColor(level.getRandom().nextBoolean() && otherParent instanceof FarlanderEntity fl ? fl.getEyeColor() : this.getEyeColor());

		return baby;
	}

	@Override
	public InteractionResult mobInteract(Player player, InteractionHand hand)
	{
		ItemStack itemstack = player.getItemInHand(hand);

		if (itemstack.getItem() == Items.SUGAR && this.isBaby())
		{
			this.setBaby(false);
			this.consumeItemFromStack(player, itemstack);
			return InteractionResult.SUCCESS;
		}
		else if (itemstack.getItem() != FLItems.farlander_spawn_egg && this.isAlive() && !this.isSleeping() && !this.isBaby() && !player.isSecondaryUseActive() && !this.isBreedingItem(itemstack))
		{
			if (hand == InteractionHand.MAIN_HAND)
				player.awardStat(Stats.TALKED_TO_VILLAGER);

			if (this.getOffers().isEmpty())
			{
				return super.mobInteract(player, hand);
			}
			else
			{
				if (!this.level().isClientSide())
				{
					this.setTradingPlayer(player);
					this.openTradingScreen(player, this.getDisplayName(), this.getTradeLevel());
				}
				return InteractionResult.SUCCESS;
			}
		}
		else
		{
			return super.mobInteract(player, hand);
		}
	}

	private void levelUp()
	{
		this.setTradeLevel(this.getTradeLevel() + 1);
		this.updateTrades();
	}

	// VILLAGER START
	public void restock()
	{
		this.updateDemand();

		for (MerchantOffer merchantoffer : this.getOffers())
		{
			merchantoffer.resetUses();
		}

		this.lastRestockGameTime = this.level().getGameTime();
		++this.numberOfRestocksToday;
	}

	private boolean needsToRestock()
	{
		for (MerchantOffer merchantoffer : this.getOffers())
		{
			if (merchantoffer.needsRestock())
			{
				return true;
			}
		}

		return false;
	}

	private boolean allowedToRestock()
	{
		return this.numberOfRestocksToday == 0 || this.numberOfRestocksToday < 2 && this.level().getGameTime() > this.lastRestockGameTime + 2400L;
	}

	public boolean shouldRestock()
	{
		long i = this.lastRestockGameTime + 12000L;
		long j = this.level().getGameTime();
		boolean flag = j > i;
		long k = this.level().getDayTime();
		if (this.lastRestockCheckDayTime > 0L)
		{
			long l = this.lastRestockCheckDayTime / 24000L;
			long i1 = k / 24000L;
			flag |= i1 > l;
		}

		this.lastRestockCheckDayTime = k;
		if (flag)
		{
			this.lastRestockGameTime = j;
			this.resetNumberOfRestocks();
		}

		return this.allowedToRestock() && this.needsToRestock();
	}

	private void catchUpDemand()
	{
		int i = 2 - this.numberOfRestocksToday;
		if (i > 0)
		{
			for (MerchantOffer merchantoffer : this.getOffers())
			{
				merchantoffer.resetUses();
			}
		}

		for (int j = 0; j < i; ++j)
		{
			this.updateDemand();
		}

	}

	private void updateDemand()
	{
		for (MerchantOffer merchantoffer : this.getOffers())
		{
			merchantoffer.updateDemand();
		}

	}

	private void resetNumberOfRestocks()
	{
		this.catchUpDemand();
		this.numberOfRestocksToday = 0;
	}

	@Override
	public boolean canRestock()
	{
		return true;
	}
	// VILLAGER END

	@Override
	protected void rewardTradeXp(MerchantOffer offer)
	{
		int i = 3 + this.random.nextInt(4);
		this.xp += offer.getXp();

		if (this.canLevelUp())
		{
			this.timeUntilReset = 40;
			this.customer = true;
			i += 5;
		}
		if (offer.shouldRewardExp())
		{
			this.level().addFreshEntity(new ExperienceOrb(this.level(), this.getX(), this.getY() + 0.5D, this.getZ(), i));
		}
	}

	protected Int2ObjectMap<ItemListing[]> getTradeList()
	{
		return FarlanderTrades.FARLANDER_TRADES;
	}

	@Override
	protected void updateTrades()
	{
		VillagerTrades.ItemListing[] avillagertrades$itrade = this.getTradeList().get(this.getTradeLevel());

		if (avillagertrades$itrade != null)
		{
			MerchantOffers merchantoffers = this.getOffers();
			this.addOffersFromItemListings(merchantoffers, avillagertrades$itrade, 2);
		}

	}

	private boolean canLevelUp()
	{
		int i = this.getTradeLevel();
		return VillagerData.canLevelUp(i) && this.xp >= VillagerData.getMaxXpPerLevel(i);
	}

	public void setTradeLevel(int level)
	{
		this.entityData.set(LEVEL, level);
	}

	public int getTradeLevel()
	{
		return this.entityData.get(LEVEL);
	}

	public int getVillagerXp()
	{
		return this.xp;
	}

	public void setXp(int amount)
	{
		this.xp = amount;
	}

	public void setOrigins(@Nullable BlockPos home, @Nullable BlockPos village)
	{
		this.homePos = home;
		this.villageCenter = village;
	}

	@Nullable
	public BlockPos getHomePos()
	{
		return this.homePos;
	}

	@Nullable
	public BlockPos getVillageOrigin()
	{
		return this.villageCenter;
	}

	@Override
	public void die(DamageSource cause)
	{
		super.die(cause);
	}

	public double getPosDistance(BlockPos pos)
	{
		return getPosDistance(Vec3.atBottomCenterOf(pos));
	}

	public double getPosDistance(Vec3 vec)
	{
		return getPosDistance(vec.x(), vec.y(), vec.z());
	}

	public double getPosDistance(double x, double y, double z)
	{
		double f = this.getX() - x;
		double f1 = this.getY() - y;
		double f2 = this.getZ() - z;
		return Math.sqrt(f * f + f1 * f1 + f2 * f2);
	}

	@Override
	public boolean isAlliedTo(Entity entityIn)
	{
		return entityIn.getType().is(FLTags.Entities.FARLANDER_ALLIES) || super.isAlliedTo(entityIn.getTeam());
	}

	@Override
	public boolean canBreed()
	{
		return this.getAge() == 0;
	}

	@Override
	public void startSleeping(BlockPos pos)
	{
		super.startSleeping(pos);
	}

	protected boolean isDayOrRainingAndInLight()
	{
		return this.getHomePos() != null && (this.level().isDay() || this.level().isRainingAt(this.blockPosition())) && this.level().getBrightness(LightLayer.SKY, this.blockPosition()) >= this.level().getMaxLightLevel() - 1;
	}

	public boolean isTempted()
	{
		return this.entityData.get(TEMPTED);
	}

	// a more aggressive version of MoveTowardsRestrictionGoal
	class AggressiveStayNearRestrictionGoal extends Goal
	{
		private final FarlanderEntity mob = FarlanderEntity.this;
		private double wantedX, wantedY, wantedZ;
		private final double speedModifier;

		public AggressiveStayNearRestrictionGoal(double pSpeedModifier)
		{
			this.speedModifier = pSpeedModifier;
			this.setFlags(EnumSet.of(Goal.Flag.MOVE));
		}

		@Override
		public boolean canUse()
		{
			if (this.mob.homeFoundNaturally || !this.mob.hasRestriction())
				return false;

			// if in restriction and not in light, stop
			if (this.mob.isWithinRestriction() && !this.mob.isDayOrRainingAndInLight())
				return false;
			Vec3 home = this.mob.homePos != null ? Vec3.atBottomCenterOf(this.mob.getRestrictCenter()) : null;
			Vec3 wantedVec = home != null ? home : DefaultRandomPos.getPosTowards(this.mob, 16, 7, Vec3.atBottomCenterOf(this.mob.getRestrictCenter()), (double) Mth.HALF_PI);

			if (wantedVec == null)
			{
				return false;
			}
			else
			{
				this.wantedX = wantedVec.x;
				this.wantedY = wantedVec.y;
				this.wantedZ = wantedVec.z;
				return true;
			}
		}

		@Override
		public boolean canContinueToUse()
		{
			return !this.mob.getNavigation().isDone();
		}

		@Override
		public void start()
		{
			this.mob.getNavigation().moveTo(this.wantedX, this.wantedY, this.wantedZ, this.speedModifier);
		}

		@Override
		public void tick()
		{
		}

		@Override
		public boolean isInterruptable()
		{
			return super.isInterruptable();
		}
	}

	class AvoidAndAlertGuardsGoal<T extends LivingEntity> extends AvoidEntityGoal<T>
	{
		public AvoidAndAlertGuardsGoal(PathfinderMob pMob, Class<T> pEntityClassToAvoid, float pMaxDistance, double pWalkSpeedModifier, double pSprintSpeedModifier)
		{
			super(pMob, pEntityClassToAvoid, pMaxDistance, pWalkSpeedModifier, pSprintSpeedModifier);
		}

		@Override
		public void tick()
		{
			super.tick();

			if (this.toAvoid != null && this.mob.tickCount % 20 <= 2)
			{
				double range = 20;
				Mob nearestGuard = this.mob.level().getNearestEntity(Mob.class, TargetingConditions.forNonCombat().ignoreLineOfSight().range(range).ignoreInvisibilityTesting().selector(e -> e.getType().is(FLTags.Entities.FARLANDER_VILLAGE_GUARDS)), this.mob, this.mob.getX(), this.mob.getY(), this.mob.getZ(), this.mob.getBoundingBox().inflate(range));

				if (nearestGuard != null)
					nearestGuard.setTarget(this.toAvoid);
			}
		}
	}
}