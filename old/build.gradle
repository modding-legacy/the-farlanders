/*
 * GradleWorks for ForgeGradle 5
 * Written by Jonathing
 * Version 4.1.0
 *
 * What was once just an effort to make life easier is now a fully-fledged buildscript that does almost all of the work
 * for you! The only things you will need to take care of yourself are dependencies, but that shouldn't be too big of a
 * deal. Anything you'll most likely ever need to change can be found in gradle.properties. Enjoy!
 */

import net.minecraftforge.gradle.common.util.ModConfig
import org.apache.tools.ant.taskdefs.condition.Os

// Detects if GradleWorks should use Mixin.
boolean isUsingMixin() {
	return project.hasProperty('mixin_version')
}

// Detects if GradleWorks should use Structure Gel (ML exclusive).
boolean isUsingStructureGel() {
	return project.hasProperty('gel_version')
}

// Whether or not to use FML only (REMINDER: YOU ARE GIVEN NO SUPPORT FOR THIS).
boolean usingFMLOnly() {
	return project.hasProperty('use_fmlonly') && Boolean.parseBoolean(use_fmlonly)
}

// Buildscript setup (DO NOT EDIT!!!)
buildscript {
	repositories {
		// Minecraft Forge Maven
		maven { url 'https://maven.minecraftforge.net' }

		// SpongePowered Maven
		maven { url 'https://repo.spongepowered.org/maven' }

		// ParchmentMC Maven
		maven { url 'https://maven.parchmentmc.org' }

		// Maven Central
		mavenCentral()
	}
	dependencies {
		// Minecraft Forge ForgeGradle
		classpath group: 'net.minecraftforge.gradle', name: 'ForgeGradle', version: '5.1.+', changing: true

		// SpongePowered MixinGradle
		classpath group: 'org.spongepowered', name: 'mixingradle', version: '0.7.+', changing: true

		// ParchmentMC Librarian
		classpath group: 'org.parchmentmc', name: 'librarian', version: '1.+', changing: true
	}
}

println('GradleWorks for ForgeGradle 5')
println('Written by Jonathing')
println('Version 4.1.0')

// ForgeGradle - The heart and soul of the buildscript and the development environment.
apply plugin: 'net.minecraftforge.gradle'
// MixinGradle - Adds Mixin support to the development environment.
if (isUsingMixin()) {
	if (!project.extensions.findByName('eclipse')) {
		println()
	}
	apply plugin: 'org.spongepowered.mixin'
}
// Librarian - Adds Parchment support to the development environment.
apply plugin: 'org.parchmentmc.librarian.forgegradle'

// Eclipse - Adds compatibility for Eclipse workspaces.
apply plugin: 'eclipse'
apply from: 'https://gist.githubusercontent.com/Technici4n/facbcdf18ce1a556b76e6027180c32ce/raw/059ab3d504a590461746fc6e3065159f4932a960/classremapper.gradle'
// Maven Publishing - Used to be able to publish to maven repositories.
apply plugin: 'maven-publish'

apply from: 'https://raw.githubusercontent.com/SizableShrimp/Forge-Class-Remapper/main/classremapper.gradle'
// Gets a file from a parent directory and folder/file names.
// Accounts for windows backslashes because I'm paranoid as hell.
File getFile(File parent, String... paths) {
	StringBuilder builder = new StringBuilder()
	builder.append(parent)
	for (String path : paths) {
		builder.append('/')
		builder.append(path)
	}

	return file(builder.toString().replace('/', Os.isFamily(Os.FAMILY_WINDOWS) ? '\\' : '/'))
}

// Get the mod version from gradle.properties.
version = mod_version
// Get the mod group from gradle.properties.
group = mod_group
// Get the archives base name from the mod id and the Minecraft version.
archivesBaseName = String.format('%s-%s', mod_id, mc_version)

// This variable points to where the latest built file is placed for usage in publish.gradle.
// Use this when you need to point to a completely built JAR file once it is ready for use.
project.ext.reobfFile = getFile(buildDir, 'libs', String.format('%s-%s.jar', archivesBaseName, version))

// This file contains additional instructions for publishing builds of this mod to maven repositories (commented out by default, see the file for more details).
apply from: 'publish.gradle'

// Ensure that the IDE knows that this project is for Java 16 (or at the least, SDK level 16) and nothing else.
java.toolchain.languageVersion = JavaLanguageVersion.of(Integer.parseInt(java_language_version))

// This section prints out the Java, Minecraft, and Mod info just to double check all the values are correct.
// It is printed on every build, so make sure you double check!
println()
println('Java Information')
printf('- Java: %s%n', System.getProperty('java.version'))
printf('- JVM: %s (%s)%n', System.getProperty('java.vm.version'), System.getProperty('java.vendor'))
printf('- Arch: %s%n', System.getProperty('os.arch'))
println()
println('Minecraft Information')
printf('- Minecraft: %s%n', mc_version)
printf('- Minecraft Forge: %s%n', forge_version)
printf('- Mappings: %s %s%n', mappings_version, mappings_channel)
println()
println('Mod Information')
printf('- ID: %s%n', mod_id)
printf('- Name: %s%n', mod_name)
printf('- Version: %s%n', version)
println()

// This section prints out information for Structure Gel API to make sure its values are correct.
// It is commented out by default in case this specific project does not need compatibility with Structure Gel API.
// ML exclusive
if (isUsingStructureGel()) {
	println('Structure Gel API Information')
	println('- ID: structure_gel')
	println('- Name: Structure Gel API')
	printf('- Version: %s%n', gel_version)
	println()
}

// Any repositories for dependencies go here. Feel free to add to them as needed.
repositories {
	// CurseMaven
	maven {
		url 'https://www.cursemaven.com'
		content {
			includeGroup 'curse.maven'
		}
	}

	// Modding Legacy Maven
	maven {
		name 'ModdingLegacyMaven'
		url 'https://maven.moddinglegacy.com/maven'
	}
}

// Minecraft setup
minecraft {
	// Mappings to use for the project. See default MDK for mappings instructions.
	mappings channel: mappings_channel, version: mappings_version

	// The access transformer file for the project.
	// GradleWorks only tells ModDev we're using an AT if the file exists. If you don't need it, don't make it.
	File atFile = getFile(projectDir, 'src', 'main', 'resources', 'META-INF', 'accesstransformer.cfg')
	if (atFile.exists()) {
		accessTransformer = atFile
	}

	// Run configurations
	runs {
		// All run configurations
		all {
			// Process non-mod libraries for 1.17.1.
			lazyToken('minecraft_classpath') {
				configurations.library.copyRecursive().resolve().collect { it.absolutePath }.join(File.pathSeparator)
			}

			// The directory for Minecraft to run in.
			workingDirectory getFile(projectDir, 'run')

			// If we're not using Mixin, then MixinGradle won't do this for us. We have to do this ourselves.
			// This is logic copied straight from MixinGradle. If we are using Mixin, then MixinGradle does this for us.
			// https://github.com/SpongePowered/MixinGradle/blob/8118e612547371494be3afbad681ea92e0f2afdb/src/main/groovy/org/spongepowered/asm/gradle/plugins/MixinExtension.groovy#L614
			if (!isUsingMixin())
			{
				def srgToMcpFile = project.tasks.createSrgToMcp.outputs.files[0].path

				property 'net.minecraftforge.gradle.GradleStart.srg.srg-mcp', srgToMcpFile
				property 'mixin.env.remapRefMap', 'true'
				property 'mixin.env.refMapRemappingFile', srgToMcpFile
			}

			// Logging markers for Forge. See default MDK for instructions.
			property 'forge.logging.markers', 'REGISTRIES'

			// Console logging level (debug by info, change to debug if you need debug logging).
			property 'forge.logging.console.level', 'info'

			// Tells the mod if it is running in an IDE (must be configured in your project, but not required).
			property String.format('%s.iside', mod_id), 'true'

			// Add the mod's main source set to the total mods in the run configuration.
			ModConfig modConfig = new ModConfig(project as Project, mod_id as String)
			modConfig.source(sourceSets.main as SourceSet)
			mods.add(modConfig)
		}

		// Client run configuration
		client {
			// The client run configuration does not need any additional properties.
			// All of them are defined in the "all" closure. If you'd like to add some of your own, feel free.
		}

		// Server run configuration
		server {
			// Additional arguments exclusive to the server (nogui by default, separated by ",").
			args 'nogui'
		}

		// Data run configuration
		data {
			// Additional arguments exclusive to data generation (do NOT change these unless necessary).
			args '--mod', mod_id, '--all', '--output', getFile(projectDir, 'src', 'generated', 'resources'), '--existing', getFile(projectDir, 'src', 'main', 'resources')

			// Tells the mod if it is running datagen (must be configured in your project, but not required).
			property String.format('%s.datagen', mod_id), 'true'

			// Workaround for https://github.com/MinecraftForge/ForgeGradle/issues/690
			// Written by Shadew
			if (Os.isFamily(Os.FAMILY_MAC)) {
				jvmArg('-XstartOnFirstThread')
			}
		}
	}
}

// Additional settings for the resources source set.
sourceSets.main.resources {
	// Include resources generated by data generators.
	srcDir getFile(projectDir, 'src', 'generated', 'resources')
}

if (isUsingMixin()) {
	// MixinGradle settings
	mixin {
		// Add the refmap to the built JAR file.
		add sourceSets.main, String.format('%s.refmap.json', mod_id)

		// Add the mixin config JSON file to run configs and the built JAR's Manifest.
		config String.format('%s.mixins.json', mod_id)
	}
}

// Additional configuration for non-mod dependencies.
configurations {
	library
	implementation.extendsFrom(library)
}

// Dependencies to use in this project.
dependencies {
	// Minecraft Forge
	String toolchain = 'forge'
	if (usingFMLOnly()) {
		toolchain = 'fmlonly'
	}
	minecraft String.format('net.minecraftforge:%s:%s-%s', toolchain, mc_version, forge_version)

	// Mixin processor
	if (isUsingMixin()) {
		annotationProcessor String.format('org.spongepowered:mixin:%s:processor', mixin_version)
	}

	// LazyDFU
	if (project.hasProperty('lazydfu_version')) {
		runtimeOnly fg.deobf(String.format('me.jonathing.minecraft:lazydfu:%s', lazydfu_version))
	}

	// Structure Gel API (ML Exclusive)
	if (isUsingStructureGel()) {
		implementation fg.deobf(String.format('com.legacy:structure-gel:%s', gel_version))
	}
}

// Attributes to include in the built jar file.
jar {
	LinkedHashMap<String, Object> attributes = [
			'Specification-Title'     : mod_id,
			'Specification-Vendor'    : mod_author,
			'Specification-Version'   : '1',
			'Implementation-Title'    : project.name,
			'Implementation-Version'  : archiveVersion.getOrElse(version),
			'Implementation-Vendor'   : mod_author,
			'Implementation-Timestamp': new Date().format('yyyy-MM-dd\'T\'HH:mm:ssZ')
	]

	manifest.attributes(attributes)
}
